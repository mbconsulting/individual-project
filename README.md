# Individual Project for Health Informatics

## Organization
There are three main folders:
- `back-end`
  - Where backend Lambda code is stored
- `front-end`
  - Where frontend UI code is stored
- `scripts`
  - Helper scripts
  
## Running the project
There are several steps involved to get this up and running. You will need:
- An AWS Account
- Software on you local computer:
  - Java 11 JDK
  - Clojure
  - awscli
    - add a login with your credentials
  - node
  - yarn
  - shadow-cljs

### building the Backend
- You need `health-get-data` and `health-save-data` lambdas set up with Java11 runtime.
- make sure `/scripts/build-and-deploy.sh` is runnable by your user.
- run `./scripts/build-and-deploy.sh get-data prod`
- run `./scripts/build-and-deploy.sh save-data prod`

### Set up AWS Gateway
- set up a new API.
  - Add `db` as a resource
  - add `{patient-id}` under `db`
    - for `{patient-id}` add `GET` and `POST` methods
    - Click "Method Execution" under both both `GET` and `POST` and...
      - Click "Use Lambda Proxy integration"
- note the Gateway ID (an alpha-numeric string) from the breadcrumps on the page you'll need this later

### building the Frontend
- Make sure you have the gateway ID from the API Gateway.
- navigate to `front-end/src/app/core.cljs` and update line 10 (just replace the Gateway ID with your own)
- run `yarn build`

### build SCSS
This is already built, but if you make changes you'll need to install sass and run `sass --watch resources/private/sass:public/css` from the `front-end` directory.

### Create a new CERNER app
- make sure you have set all the following:
    - App Type: provider
    - FHIR Spec: r4 - "https://fhir-ehr-code.cerner.com/r4/ec2458f2-1e24-41c8-b71b-0e701af7583d"
    - Authorized: true
    - Standard Scopes:
        - launch
        - profile
        - openid
        - online_access
    - Patient Scopes:
        - patient/MedicationRequest.read
        - patient/Patient.read
- note the client id
      
### AWS Static site
- update `front-end/resources/public/index.html` line 12 and set `prod` to `true`.
- update `front-end/resources/public/launch.html` line 30 with you CERNER app client id
- Create a new bucket in AWS that is publicly available and served as a website
- Save all the files from `front-end/resources/public` in the bucket.
- make all files publicly available.
- update CERNER app with the public URL of your AWS Bucket

### Run the project (for real)
If all the above steps were completed correctly you can now run the CERNER app normally and it will use the application.

#### Note about AWS Lambdas
Remember that AWS lambdas have a warm up period. If one or both of the lambdas haven't been used for a while it will take several seconds for the app to respond when retrieving or saving data.  
