(ns app.core
  (:require
    [bidi.router :refer [start-router! set-location!]]
    [ajax.core :as ajax]
    [rum.core :as rum :refer-macros [defc]]
    [tick.locale-en-us]
    [tick.alpha.api :as t]
    [app.calculate-schedule :as calc]))

(def base-api-url "https://s07d2i4zvg.execute-api.us-east-1.amazonaws.com/prod")
(defn get-pref-url [patient-id]
      (println patient-id)
      (str base-api-url "/db/" patient-id))
(def save-pref-url get-pref-url)

(defn patient-identifier [patient]
      (-> patient :patient :id))

(defn js-parse [j]
      (js->clj j :keywordize-keys true))

(defn clj-parse [c]
      (clj->js c))

(defn prep-body [time-prefs meds]
      (js/JSON.stringify (clj-parse {:times time-prefs :meds meds})))

(defn dissoc-in
  "Dissociates an entry from a nested associative structure returning a new
  nested structure. keys is a sequence of keys. Any empty maps that result
  will not be present in the new structure."
  [m [k & ks :as keys]]
  (if ks
    (if-let [nextmap (get m k)]
      (let [newmap (dissoc-in nextmap ks)]
        (if (seq newmap)
          (assoc m k newmap)
          (dissoc m k)))
      m)
    (dissoc m k)))

(defn display-date [date-str]
      [:span (if date-str
               (t/format (tick.format/formatter "MM-dd-yyyy") (t/date date-str))
               "unknown")])

(defn parse-med [med]
      (println "parsing" med)
      (let [{:keys [medicationCodeableConcept status dosageInstruction]} (-> med :resource)]
           {(keyword (get-in medicationCodeableConcept [:coding 0 :code])) {:name (-> medicationCodeableConcept :text)
                                                                            :code (get-in medicationCodeableConcept [:coding 0])
                                                                            :dose (select-keys (or (first dosageInstruction) {})
                                                                                               [:sequence :timing :asNeededBoolean])
                                                                            :interactions {}
                                                                            :status status}}))

(defonce all-times [{:key      :wake
                     :label    "Wake Time"
                     :default  "07:00"
                     :position 1}
                    {:key      :breakfast
                     :label    "Breakfast Time"
                     :default  "08:00"
                     :position 2}
                    {:key      :lunch
                     :label    "Lunch Time"
                     :default  "12:00"
                     :position 3}
                    {:key      :dinner
                     :label    "Dinner Time"
                     :default  "18:00"
                     :position 4}
                    {:key      :bed
                     :label    "Bed Time"
                     :default  "22:00"
                     :position 5}])

(defonce location (atom {:handler :patient}))
(defonce patient-state (atom {}))
(defonce time-preferences (atom {}))
(defonce med-preferences (atom {}))
(defonce schedule-state (atom {}))

(add-watch patient-state :watcher
           (fn [k a o n]
               (println "patient-state changed" n)))

(add-watch time-preferences :watcher
           (fn [k a o n]
               (println "time-prefs changed" n)))

(add-watch med-preferences :watcher
           (fn [k a o n]
             (println "med-preferences changed" n)))


(defn update-interaction [med-id previous-interaction-key {:keys [interaction hours]}]
  (reset! med-preferences (-> (assoc-in @med-preferences [med-id :interactions interaction] {:period hours :periodUnit :hours})))
                              ;(assoc-in [interaction :interactions med-id] {:period hours :periodUnit :hours})))
  (when (not= previous-interaction-key interaction)
    (println "removing" previous-interaction-key)
    (reset! med-preferences (dissoc-in @med-preferences [med-id :interactions previous-interaction-key]))))

(defn remove-interaction [med-id interaction-key]
  (println "removing " interaction-key " from " med-id)
  (reset! med-preferences (dissoc-in @med-preferences [med-id :interactions (keyword interaction-key)])))


(def navigation [{:name :patient
                  :display "Patient"
                  :link "patient"}
                 {:name :schedule
                  :display "Schedule"
                  :link "schedule"}
                 {:name :about
                  :display "About"
                  :link "about"}])

(def router
  (start-router!
    ["" {"home"  :home
         "about" :about
         "schedule" :schedule}]

    {:on-navigate #(reset! location (or % {:handler :patient}))}))

(defn nav-links [l]
      [:.navigation
       (for [{:keys [link] n :name d :display} navigation]
            [:a {:href (str "/#" link) :class-name (if (= n l) "active" "")} d])])

(defc pref-time-input < rum/reactive
      [label k]
      (let [ref (rum/create-ref)
            w (k (rum/react time-preferences))]
           [:label label [:input {:ref       ref
                                  :value     (or w "")
                                  :on-change #(swap! time-preferences assoc k (.-value (rum/deref ref)))}]]))

(defn patient-view [patient-state med-state]
      [:div
       [:h1 "Patient Preferences"]
       [:div.patient-info
        [:h3.title.first "User Information"]
        [:div.patient-name [:span.bold "Name: "] (-> patient-state :fullName)]
        [:div.dob [:span.bold "DOB: "] (-> patient-state :patient :birthDate display-date)]]
       [:div [:button.save-button {:on-click #(ajax/POST
                                                (save-pref-url (patient-identifier patient-state))
                                                {:body            (prep-body @time-preferences @med-preferences)
                                                 :response-format :json})}
              "Save All Preferences"]]
       [:div.patient-preferences
        [:h3.title "Time Preferences"]
        (for [t all-times]
             [:div {:key (str (:key t) "-div")} (pref-time-input (:label t) (:key t))])
        [:div
         [:h3.title "Medications"]
         [:table
          [:thead [:td "Status"] [:td "Medication"] [:td "interactions"]]
          [:tbody
           (for [[k m] med-state]
             [:tr {:key (str "med-" k)}
              [:td.status (:status m)]
              [:td.med-name (:name m)]
              [:td.interactions
               [:span.interaction-button.plus {:on-click #(update-interaction k nil {:interaction "" :hours 0})} "+"]
               (for [[ik ii] (:interactions m)]
                 (let [ref-div (rum/create-ref)
                       ref-select (rum/create-ref)
                       ref-input (rum/create-ref)]
                   [:div {:ref ref-div}
                    [:span.interaction-button.minus {:on-click #(remove-interaction k (.-value (rum/deref ref-select)))} "-"]
                    [:select {:ref       ref-select
                              :value     (or ik "")
                              :on-change #(update-interaction k ik {:interaction (.-value (rum/deref ref-select)) :hours (.-value (rum/deref ref-input))})}
                     [:option {:value ""} "select"]
                     (map-indexed
                       (fn [idx [km mm]]
                         [:option {:key   (str (:name m) "-" idx "-" (:name mm))
                                   :value km} (:name mm)])
                       med-state)]                          ; todo replace med-state with list of only non-interacting meds
                    [:input {:ref       ref-input
                             :value     (get-in med-state [k :interactions ik :period])
                             :on-change #(update-interaction k ik {:interaction (.-value (rum/deref ref-select)) :hours (.-value (rum/deref ref-input))})}] "hours"]))]])]]]]])


(defn about-view [patient-state]
      [:div "This is the individual project for my Health Informatics class and is not inteded for production use."])

(defn schedule-view [time-prefs med-state]
  (let [event-times (->> time-prefs
                         (map val)
                         (map t/time)
                         (map calc/time->keyword))
        time-preferences-full (->> (map (fn [[k v]]
                                          {:key  k
                                           :time (t/time v)})
                                        time-prefs)
                                   (sort-by :time)
                                   (map-indexed (fn [i v]
                                                  (assoc v :position i
                                                           :default (= i 1)))))
        {calculated-sched :schedule calculated-meta :meta} (calc/calculate {:meds med-state} time-preferences-full)]
    (println :***-meta calculated-meta (:errors calculated-meta))
    [:div
     [:h1 "Daily Schedule"]
     (when (:errors calculated-meta)
       [:div.errors
        (for [[ek ee] (:errors calculated-meta)]
          [:div.error (-> med-state ek :name) " could not be added due to an interaction."])])
     [:div.times
      (for [kt calc/all-hour-keys]
        (let [class-n "time"
              class-n (if (contains? (set event-times) kt)
                        (str class-n " regular-event")
                        class-n)]
          [:div {:key (str "time-" kt) :class-name class-n}
           [:span.time-display (str (calc/keyword->time kt))]
           (for [med-id (kt calculated-sched)]
             [:span.medication-name {:key (str "med-display-" med-id)} (-> med-state med-id :name)])]))]]))


(defc root < rum/reactive []
      (let [l (-> (rum/react location) :handler)
            s (rum/react patient-state)
            m (rum/react med-preferences)
            tp (rum/react time-preferences)]
        (println "location is " l)
        [:div
         (nav-links l)
         [:.main
          (case l
            :patient (patient-view s m)
            :about (about-view s)
            :schedule (schedule-view tp m))]]))

(defn prep-meds [patient-local patient-saved]
      (let [loc-meds (into {}
                           (map parse-med (:medications patient-local)))
            saved-meds (:meds patient-saved)]
        (println :saved-meds? (empty? saved-meds) saved-meds)
        (if (empty? saved-meds)
          (do (ajax/POST
                (save-pref-url (patient-identifier patient-local))
                {:body            (prep-body @time-preferences loc-meds)
                 :response-format :json})
              (reset! med-preferences saved-meds))
          (reset! med-preferences saved-meds))))

(defn prep-patient [p]
      (reset! patient-state p)
      (ajax/GET (get-pref-url (patient-identifier p))
                {:response-format :raw
                 :handler         #(let [prefs (js-parse (js/JSON.parse %))]
                                     (prep-meds p prefs)
                                     (reset! time-preferences (or (:times prefs)
                                                                  (into {} (map (fn [v] [(:key v) (:default v)]) all-times)))))}))
      ;after getting the medications from server, check if there are any new medications, if there are, save them




(defn ^:export start []
      (rum/mount (root) (.getElementById js/document "app"))
      (println "************************************************************************")
      (println "core.cljs v3")
      (println "************************************************************************")
      (-> (js/getPatientDetails)
          (.then #(prep-patient (js-parse %)))))


; get spls via https://dailymed.nlm.nih.gov/dailymed/services/v2/spls.json?rxcui=312962
; get properties via https://rxnav.nlm.nih.gov/REST/rxcui/315971/properties

;todo mb make save button inactive until their are changes