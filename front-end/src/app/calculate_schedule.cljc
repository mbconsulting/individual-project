(ns app.calculate-schedule
  (:require [clojure.set :as s]
            [tick.alpha.api :as t]))

(defn time->keyword [t]
  (if (keyword? t)
    t
    (-> t
        str
        (clojure.string/replace #":" "-")
        keyword)))

(defn keyword->time [t]
  (-> t
      name
      (clojure.string/replace #"-" ":")
      t/time))

(def all-hour-keys (map #(time->keyword (t/+ (t/time "0:00") (t/new-duration % :hours))) (range 24)))
(def all-hour-strings ())

(defn event-windows [events all-hours]
  (let [ts (->> (map :time events)
                sort)
        f-time (first ts)
        l-time (last ts)
        _ (println :************************************* :f-time f-time :l-time l-time)
        all-hours-times (map keyword->time all-hours)
        before-pref (when f-time (filter #(t/< % f-time) all-hours-times))
        after-pref (when l-time (filter #(t/> % l-time) all-hours-times))
        pref-window (when (and f-time l-time) (filter #(t/<= f-time % l-time) all-hours-times))]
    {:pref-window      pref-window
     :before-preferred before-pref
     :after-preferred  after-pref}))

;prefered-window [start - end]
; after-preferred
; before-preferred)

(defn all-inserted-meds [schedule hour-keys]
  (reduce (fn [p k]
            (concat p (k schedule))) [] hour-keys))

(defn interaction-hours
  "Takes a time and number of hours an interaction may occur and returns whole hour increments when an iteracting drug is prohibited."
  [time hours]
  (println :***interaction-hours time hours)
  (let [time (if (keyword? time) (keyword->time time) time)]
    (map #(time->keyword (t/+ time (t/new-duration % :hours)))
         (concat (range (- (- hours 1)) 0) (range hours)))))

(defn times-for-med [med-code schedule]
  (reduce-kv (fn [p k v]
               (if ((set v) med-code)
                 (conj p k)
                 p))
             []
             schedule))

(defn inserted-interactions [schedule interactions hour-keys]
  (->> (all-inserted-meds schedule hour-keys)
       set
       (s/intersection (set (keys interactions)))
       (map #(hash-map % (% interactions)))
       (into {})))

(defn available-times [available-times schedule inserted-interactions]
  (println :3*****-schedule schedule :ii inserted-interactions)
  (if (or (empty? available-times) (empty? inserted-interactions))
    (set available-times)
    (let [[med {num-hours :period}] (first inserted-interactions)
          _ (println :***-med med :hours num-hours :schedule schedule)]
      (->> (times-for-med med schedule)
           (mapcat #(interaction-hours % num-hours))
           set
           (s/difference (set available-times))))))

(defn time-or-nil
  "Will return time if it is not nil, or will use possible-times and preferred-times to get the window and order-fn to get eight the first, last, or nth"
  [time possible-times preferred-times order-fn]
  (or time
      (->> preferred-times
           (map time->keyword)
           set
           (filter #(contains? possible-times %))
           sort
           order-fn)))


; start with all-hour-keys
; get the times-for-meds for each interaction
; remove interaction hours for all inserted interaction and all times

(defn add-to-schedule [{:keys [interactions] {code :code} :code :as med} sched preferred-time event-times]
  (println :adding-to-schedule code)
  (println :times :pref preferred-time :events event-times)
  (let [{:keys [schedule meta] :or {schedule {} meta {}}} sched
        ii (inserted-interactions schedule interactions all-hour-keys)
        possible-times (available-times all-hour-keys schedule ii)
        e-windows (event-windows event-times all-hour-keys)
        ;see if default time is listed as a possible time
        time (when (contains? possible-times (time->keyword preferred-time)) preferred-time)
        ;if not see if another of the event times is available
        time (time-or-nil time possible-times (map :time event-times) first)
        ; if not, select the first within :prefered window
        time (time-or-nil time possible-times (:pref-window e-windows) first); this isn't tested
        ; if not, select the first after bed
        time (time-or-nil time possible-times (:after-preferred e-windows) first); this isn't tested
        ; if not, select the last before wake
        time (time-or-nil time possible-times (:before-preferred e-windows) last); this isn't tested

        t-key (time->keyword time)
        new-schedule (if time
                       (-> (or (t-key schedule) [])
                           (conj (keyword code))
                           (#(assoc schedule t-key %)))
                       schedule)
        new-meta (if time
                   meta
                   (assoc meta :errors (assoc (or (:errors meta) {})
                                         (keyword code) :no-available-times)))]

    (assoc sched :schedule new-schedule
                 :meta new-meta)))
; if this med has interactions, check if any of the interactions have already been added
; if they have, attempt to add them at another time
; if they can't be added at another time add failure message to meta





; I can use the value from interactions to provide possible values for the other meds and we can narrow it down with a list of preferences
; meta may contain information about drugs that haven't been approached yet
; do required first which must be at the time specified
; do prefered second, but if they don't work, ignore the specified time
; meta may include drugs keyed by id {:drugs {:x {:name :x...}}}
(defn- calc-sched
  ([meds event-times]
   (println "--1")
   (calc-sched meds event-times {:schedule {} :meta {}}))
  ([[{:keys [name code dose interactions status required-time preferred-time] :as med} & xs] event-times sched]
   (println "--2")
   ;(println :med med)
   ;(println :xs xs)
   (if (empty? med)
     sched
     (cond (not= status "active") (calc-sched xs event-times sched)

           (or required-time preferred-time) (calc-sched xs event-times (add-to-schedule med sched (or required-time preferred-time) event-times))

           :else (calc-sched xs event-times (add-to-schedule med
                                                             sched
                                                             (->> event-times
                                                                  (filter :default)
                                                                  first
                                                                  :time)
                                                             event-times))))))


; get the required-time, pref-time
; check if it is in the metaata and what times it's allowed for if any
; check if it interacts with another medications already scheduled for those times
; ...

(defn calculate [meds event-times]
  (let [meds (map val (:meds meds))
        _ (println "processing required meds")
        required (filter :req-time meds)
        _ (println "processing preferred meds")
        preferred (filter :pref-time meds)
        _ (println "processing other meds")
        other (s/difference (set meds) (set (concat required preferred)))]
    (as-> (calc-sched required event-times) $
          (calc-sched preferred event-times $)
          (calc-sched other event-times $))))