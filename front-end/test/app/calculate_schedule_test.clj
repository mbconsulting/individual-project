(ns app.calculate-schedule-test
  (:require [clojure.test :refer :all]
            [app.calculate-schedule :refer :all]
            [tick.alpha.api :as t]))


(defonce all-times [{:key      :wake
                     :label    "Wake Time"
                     :time     (t/time "07:00")
                     :position 1}
                    {:key      :breakfast
                     :label    "Breakfast Time"
                     :time     (t/time "08:00")
                     :default  true
                     :position 2}
                    {:key      :lunch
                     :label    "Lunch Time"
                     :time     (t/time "12:00")
                     :position 3}
                    {:key      :dinner
                     :label    "Dinner Time"
                     :time     (t/time "18:00")
                     :position 4}
                    {:key      :bed
                     :label    "Bed Time"
                     :time     (t/time "22:00")
                     :position 5}])

(def m1 {:1 {:name           "Lasix 40mg",
             :code           {:system  "http://www.nlm.nih.gov/research/umls/rxnorm",
                              :code    "1",
                              :display "Lasix 40mg"},
             :dose           {:sequence        1,
                              :timing          {:repeat {:frequency  1,
                                                         :period     1,
                                                         :periodUnit "d"}},
                              :asNeededBoolean false},
             :interactions   {:3 {:period 8 :periodUnit :hours}
                              :6 {:period 24 :periodUnit :hours}}
             :status         "active"
             :preferred-time (t/time "8:00")}})

(def m2 {:2 {:name   "60 ACTUAT Fluticasone propionate 0.25 MG/ACTUAT / salmeterol 0.05 MG/ACTUAT Dry Powder Inhaler",
             :code   {:system  "http://www.nlm.nih.gov/research/umls/rxnorm",
                      :code    "2",
                      :display "60 ACTUAT Fluticasone propionate 0.25 MG/ACTUAT / salmeterol 0.05 MG/ACTUAT Dry Powder Inhaler"},
             :dose   {},
             :status "stopped"}})

(def m3 {:3 {:name   "24 HR metoprolol succinate 100 MG Extended Release Oral Tablet [Toprol]",
             :code   {:system  "http://www.nlm.nih.gov/research/umls/rxnorm",
                      :code    "3",
                      :display "24 HR metoprolol succinate 100 MG Extended Release Oral Tablet [Toprol]"},
             :dose   {:sequence        1,
                      :timing          {:repeat {:frequency  1,
                                                 :period     1,
                                                 :periodUnit "d"}},
                      :asNeededBoolean false},
             :interactions   {:1 {:period 8 :periodUnit :hours}}
             :status "active"}})

(def m4 {:4 {:name          "Amoxicillin 250 MG / Clavulanate 125 MG Oral Tablet",
             :code          {:system  "http://www.nlm.nih.gov/research/umls/rxnorm",
                             :code    "4",
                             :display "Amoxicillin 250 MG / Clavulanate 125 MG Oral Tablet"},
             :dose          {},
             :interactions  {:1 {:period 8 :periodUnit :hours}}
             :status        "active"
             :required-time (t/time "8:00")}})


(def m5 {:5 {:name   "Naproxen sodium 220 MG Oral Tablet",
             :code   {:system  "http://www.nlm.nih.gov/research/umls/rxnorm",
                      :code    "5",
                      :display "Naproxen sodium 220 MG Oral Tablet"},
             :dose   {},
             :status "active"}})

(def m6 {:6 {:name   "24 HR metoprolol succinate 100 MG Extended Release Oral Tablet [Toprol]",
             :code   {:system  "http://www.nlm.nih.gov/research/umls/rxnorm",
                      :code    "6",
                      :display "24 HR metoprolol succinate 100 MG Extended Release Oral Tablet [Toprol]"},
             :dose   {:sequence        1,
                      :timing          {:repeat {:frequency  1,
                                                 :period     1,
                                                 :periodUnit "d"}},
                      :asNeededBoolean false},
             :interactions {:1 {:period 24 :periodUnit :hours}}
             :status "active"}})

(def m7 {:896209 {:name   "60 ACTUAT Fluticasone propionate 0.25 MG/ACTUAT / salmeterol 0.05 MG/ACTUAT Dry Powder Inhaler",
                  :code   {:system  "http://www.nlm.nih.gov/research/umls/rxnorm",
                           :code    "896209",
                           :display "60 ACTUAT Fluticasone propionate 0.25 MG/ACTUAT / salmeterol 0.05 MG/ACTUAT Dry Powder Inhaler"},
                  :dose   {}
                  :status "stopped"}})

(def one-stopped-med {:meds m2})
(def one-active-one-stopped {:meds (merge m1 m2)})
(def one-required-one-stopped {:meds (merge m2 m4)})
(def interacting-meds {:meds (merge m1 m3)})
(def one-required-one-active-one-no-pref {:meds (merge m1 m4 m5)})
(def no-times-for-one {:meds (merge m1 m6)})

(defn sm
  ([] {:schedule {} :meta {}})
  ([s] {:schedule s :meta {}})
  ([s m] {:schedule s :meta m}))

(defn pd [n]
  (println)
  (println)
  (println "********************************* " n " *********************************"))

(deftest calculate-test
  (pd 1)
  (is (= (calculate [] all-times) (sm)))
  (pd 2)
  (is (= (calculate one-stopped-med all-times) (sm)))
  (pd 3)
  (is (= (:schedule (calculate one-active-one-stopped all-times)) {:08-00 [:1]}))
  (pd 4)
  (is (= (:schedule (calculate one-required-one-stopped all-times) {:08-00 [:562251]})))
  (pd 5)
  (is (= (:schedule (calculate one-required-one-active-one-no-pref all-times)) {:08-00 [:5 :1] :18-00 [:4]}))

  (is (= (:schedule (calculate interacting-meds all-times)) {:08-00 [:1] :18-00 [:3]}))
  (is (= (calculate no-times-for-one all-times) {:schedule {:08-00 [:1]}
                                                 :meta     {:errors {:6 :no-available-times}}})))

(deftest add-to-schedule-test
  (is (= (add-to-schedule (:6 m6) (sm) (t/time "8:00") all-times) (sm {:08-00 [:6]})))
  (is (= (add-to-schedule (:6 m6) (sm {:08-00 [:abc]}) (t/time "8:00") all-times)
         (sm {:08-00 [:abc :6]}))))

(deftest all-inserted-meds-test
  (is (= (all-inserted-meds {} all-hour-keys) []))
  (is (= (all-inserted-meds {:08-00 [:562251 :315971] :12-00 [:849574]} all-hour-keys)
         [:562251 :315971 :849574]))
  (is (= (all-inserted-meds {:08-00 [:562251 :315971]} all-hour-keys)
         [:562251 :315971])))

(deftest times-for-med-test
  (is (= (times-for-med :562251 {:08-00 [:562251 :315971] :12-00 [:849574]})
         [:08-00]))
  (is (= (times-for-med :562251 {:08-00 [:562251 :315971] :12-00 [:849574] :16-00 [:562251]})
         [:08-00 :16-00])))

(deftest inserted-interactions-test
  (is (= (inserted-interactions {:08-00 [:562251 :315971] :12-00 [:849574]} {:315971 {:period 8, :periodUnit :hours}} all-hour-keys)
         {:315971 {:period 8, :periodUnit :hours}}))
  (is (= (inserted-interactions {:08-00 [:562251 :315971] :12-00 [:849574]} {:315971 {:period 8, :periodUnit :hours}
                                                                             :849574 {:period 4, :periodUnit :hours}} all-hour-keys)
         {:315971 {:period 8, :periodUnit :hours}
          :849574 {:period 4, :periodUnit :hours}})))

(deftest event-windows-test
  (is (= (event-windows all-times all-hour-keys)
         {:after-preferred  [#time/time "23:00"]
          :before-preferred [ #time/time "00:00"
                             #time/time "01:00"
                             #time/time "02:00"
                             #time/time "03:00"
                             #time/time "04:00"
                             #time/time "05:00"
                             #time/time "06:00"]
          :pref-window      [ #time/time "07:00"
                             #time/time "08:00"
                             #time/time "09:00"
                             #time/time "10:00"
                             #time/time "11:00"
                             #time/time "12:00"
                             #time/time "13:00"
                             #time/time "14:00"
                             #time/time "15:00"
                             #time/time "16:00"
                             #time/time "17:00"
                             #time/time "18:00"
                             #time/time "19:00"
                             #time/time "20:00"
                             #time/time "21:00"
                             #time/time "22:00"]})))


