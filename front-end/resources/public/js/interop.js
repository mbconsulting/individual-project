console.log("interop.js v1")

//helper functions

function getPatientName(pt) {
    if (pt.name) {
        var names = pt.name.map(function (name) {
            return name.given.join(" ") + " " + name.family;
        });
        return names.join(" / ")
    } else {
        return "anonymous";
    }
}

function getPatient(client){
    return client.request(`Patient/${client.patient.id}`).then(
        function (patient) {
            return patient;
        }
    );
}
function getMedications(client){
    return client.request(`/MedicationRequest?patient=` + client.patient.id, {
        resolveReferences: "medicationReference"
    }).then(
        function (meds) {
            return meds.entry;
        }
    );
}

function getBaseDetails(client){
    return Promise.all([getPatient(client), getMedications(client)])
        .then(([patient, medications]) => {
            let patientData = {};
            patientData.patient = patient;
            patientData.fullName = getPatientName(patient);
            patientData.medications = medications;
            return patientData;
    })
}

// function getPatientDetails() {
//     console.log('js getting patient details')
//     if (prod) {
//         FHIR.oauth2.ready().then(function (client) {
//            return getBaseDetails(client);
//         })
//     } else {
//         const client = new FHIR.client({
//             serverUrl: "https://r4.smarthealthit.org",
//             tokenResponse: {
//                 patient: "a6889c6d-6915-4fac-9d2f-fc6c42b3a82e"
//             }
//         });
//         return getBaseDetails(client);
//     }
// }

function getPatientDetails() {
    console.log('js getting patient details');
    if (prod) {
        return new Promise(function(rslv, rejc){
            FHIR.oauth2.ready().then(function (client) {
                rslv(getBaseDetails(client));
            })}
        );
    } else {
        const client = new FHIR.client({
            serverUrl: "https://r4.smarthealthit.org",
            tokenResponse: {
                patient: "a6889c6d-6915-4fac-9d2f-fc6c42b3a82e"
            }
        });
        return new Promise(function(rslv, rejc){
            rslv(getBaseDetails(client));
        });
    }
}
