goog.provide('sablono.core');
/**
 * Add an optional attribute argument to a function that returns a element vector.
 */
sablono.core.wrap_attrs = (function sablono$core$wrap_attrs(func){
return (function() { 
var G__29521__delegate = function (args){
if(cljs.core.map_QMARK_(cljs.core.first(args))){
var vec__29297 = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(func,cljs.core.rest(args));
var seq__29298 = cljs.core.seq(vec__29297);
var first__29299 = cljs.core.first(seq__29298);
var seq__29298__$1 = cljs.core.next(seq__29298);
var tag = first__29299;
var body = seq__29298__$1;
if(cljs.core.map_QMARK_(cljs.core.first(body))){
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [tag,cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([cljs.core.first(body),cljs.core.first(args)], 0))], null),cljs.core.rest(body));
} else {
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [tag,cljs.core.first(args)], null),body);
}
} else {
return cljs.core.apply.cljs$core$IFn$_invoke$arity$2(func,args);
}
};
var G__29521 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__29527__i = 0, G__29527__a = new Array(arguments.length -  0);
while (G__29527__i < G__29527__a.length) {G__29527__a[G__29527__i] = arguments[G__29527__i + 0]; ++G__29527__i;}
  args = new cljs.core.IndexedSeq(G__29527__a,0,null);
} 
return G__29521__delegate.call(this,args);};
G__29521.cljs$lang$maxFixedArity = 0;
G__29521.cljs$lang$applyTo = (function (arglist__29528){
var args = cljs.core.seq(arglist__29528);
return G__29521__delegate(args);
});
G__29521.cljs$core$IFn$_invoke$arity$variadic = G__29521__delegate;
return G__29521;
})()
;
});
sablono.core.update_arglists = (function sablono$core$update_arglists(arglists){
var iter__4529__auto__ = (function sablono$core$update_arglists_$_iter__29311(s__29312){
return (new cljs.core.LazySeq(null,(function (){
var s__29312__$1 = s__29312;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__29312__$1);
if(temp__5735__auto__){
var s__29312__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__29312__$2)){
var c__4527__auto__ = cljs.core.chunk_first(s__29312__$2);
var size__4528__auto__ = cljs.core.count(c__4527__auto__);
var b__29315 = cljs.core.chunk_buffer(size__4528__auto__);
if((function (){var i__29314 = (0);
while(true){
if((i__29314 < size__4528__auto__)){
var args = cljs.core._nth(c__4527__auto__,i__29314);
cljs.core.chunk_append(b__29315,cljs.core.vec(cljs.core.cons(new cljs.core.Symbol(null,"attr-map?","attr-map?",116307443,null),args)));

var G__29530 = (i__29314 + (1));
i__29314 = G__29530;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__29315),sablono$core$update_arglists_$_iter__29311(cljs.core.chunk_rest(s__29312__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__29315),null);
}
} else {
var args = cljs.core.first(s__29312__$2);
return cljs.core.cons(cljs.core.vec(cljs.core.cons(new cljs.core.Symbol(null,"attr-map?","attr-map?",116307443,null),args)),sablono$core$update_arglists_$_iter__29311(cljs.core.rest(s__29312__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4529__auto__(arglists);
});
/**
 * Include a list of external stylesheet files.
 */
sablono.core.include_css = (function sablono$core$include_css(var_args){
var args__4742__auto__ = [];
var len__4736__auto___29537 = arguments.length;
var i__4737__auto___29538 = (0);
while(true){
if((i__4737__auto___29538 < len__4736__auto___29537)){
args__4742__auto__.push((arguments[i__4737__auto___29538]));

var G__29539 = (i__4737__auto___29538 + (1));
i__4737__auto___29538 = G__29539;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((0) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((0)),(0),null)):null);
return sablono.core.include_css.cljs$core$IFn$_invoke$arity$variadic(argseq__4743__auto__);
});

(sablono.core.include_css.cljs$core$IFn$_invoke$arity$variadic = (function (styles){
var iter__4529__auto__ = (function sablono$core$iter__29330(s__29331){
return (new cljs.core.LazySeq(null,(function (){
var s__29331__$1 = s__29331;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__29331__$1);
if(temp__5735__auto__){
var s__29331__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__29331__$2)){
var c__4527__auto__ = cljs.core.chunk_first(s__29331__$2);
var size__4528__auto__ = cljs.core.count(c__4527__auto__);
var b__29333 = cljs.core.chunk_buffer(size__4528__auto__);
if((function (){var i__29332 = (0);
while(true){
if((i__29332 < size__4528__auto__)){
var style = cljs.core._nth(c__4527__auto__,i__29332);
cljs.core.chunk_append(b__29333,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"link","link",-1769163468),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"type","type",1174270348),"text/css",new cljs.core.Keyword(null,"href","href",-793805698),sablono.util.as_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([style], 0)),new cljs.core.Keyword(null,"rel","rel",1378823488),"stylesheet"], null)], null));

var G__29545 = (i__29332 + (1));
i__29332 = G__29545;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__29333),sablono$core$iter__29330(cljs.core.chunk_rest(s__29331__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__29333),null);
}
} else {
var style = cljs.core.first(s__29331__$2);
return cljs.core.cons(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"link","link",-1769163468),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"type","type",1174270348),"text/css",new cljs.core.Keyword(null,"href","href",-793805698),sablono.util.as_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([style], 0)),new cljs.core.Keyword(null,"rel","rel",1378823488),"stylesheet"], null)], null),sablono$core$iter__29330(cljs.core.rest(s__29331__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4529__auto__(styles);
}));

(sablono.core.include_css.cljs$lang$maxFixedArity = (0));

/** @this {Function} */
(sablono.core.include_css.cljs$lang$applyTo = (function (seq29327){
var self__4724__auto__ = this;
return self__4724__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq29327));
}));

/**
 * Include the JavaScript library at `src`.
 */
sablono.core.include_js = (function sablono$core$include_js(src){
return goog.dom.appendChild(goog.dom.getDocument().body,goog.dom.createDom("script",({"src": src})));
});
/**
 * Include Facebook's React JavaScript library.
 */
sablono.core.include_react = (function sablono$core$include_react(){
return sablono.core.include_js("http://fb.me/react-0.12.2.js");
});
/**
 * Wraps some content in a HTML hyperlink with the supplied URL.
 */
sablono.core.link_to29338 = (function sablono$core$link_to29338(var_args){
var args__4742__auto__ = [];
var len__4736__auto___29546 = arguments.length;
var i__4737__auto___29547 = (0);
while(true){
if((i__4737__auto___29547 < len__4736__auto___29546)){
args__4742__auto__.push((arguments[i__4737__auto___29547]));

var G__29550 = (i__4737__auto___29547 + (1));
i__4737__auto___29547 = G__29550;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((1) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((1)),(0),null)):null);
return sablono.core.link_to29338.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4743__auto__);
});

(sablono.core.link_to29338.cljs$core$IFn$_invoke$arity$variadic = (function (url,content){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"href","href",-793805698),sablono.util.as_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([url], 0))], null),content], null);
}));

(sablono.core.link_to29338.cljs$lang$maxFixedArity = (1));

/** @this {Function} */
(sablono.core.link_to29338.cljs$lang$applyTo = (function (seq29339){
var G__29340 = cljs.core.first(seq29339);
var seq29339__$1 = cljs.core.next(seq29339);
var self__4723__auto__ = this;
return self__4723__auto__.cljs$core$IFn$_invoke$arity$variadic(G__29340,seq29339__$1);
}));


sablono.core.link_to = sablono.core.wrap_attrs(sablono.core.link_to29338);
/**
 * Wraps some content in a HTML hyperlink with the supplied e-mail
 *   address. If no content provided use the e-mail address as content.
 */
sablono.core.mail_to29346 = (function sablono$core$mail_to29346(var_args){
var args__4742__auto__ = [];
var len__4736__auto___29553 = arguments.length;
var i__4737__auto___29554 = (0);
while(true){
if((i__4737__auto___29554 < len__4736__auto___29553)){
args__4742__auto__.push((arguments[i__4737__auto___29554]));

var G__29555 = (i__4737__auto___29554 + (1));
i__4737__auto___29554 = G__29555;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((1) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((1)),(0),null)):null);
return sablono.core.mail_to29346.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4743__auto__);
});

(sablono.core.mail_to29346.cljs$core$IFn$_invoke$arity$variadic = (function (e_mail,p__29349){
var vec__29350 = p__29349;
var content = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__29350,(0),null);
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"href","href",-793805698),["mailto:",cljs.core.str.cljs$core$IFn$_invoke$arity$1(e_mail)].join('')], null),(function (){var or__4126__auto__ = content;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return e_mail;
}
})()], null);
}));

(sablono.core.mail_to29346.cljs$lang$maxFixedArity = (1));

/** @this {Function} */
(sablono.core.mail_to29346.cljs$lang$applyTo = (function (seq29347){
var G__29348 = cljs.core.first(seq29347);
var seq29347__$1 = cljs.core.next(seq29347);
var self__4723__auto__ = this;
return self__4723__auto__.cljs$core$IFn$_invoke$arity$variadic(G__29348,seq29347__$1);
}));


sablono.core.mail_to = sablono.core.wrap_attrs(sablono.core.mail_to29346);
/**
 * Wrap a collection in an unordered list.
 */
sablono.core.unordered_list29353 = (function sablono$core$unordered_list29353(coll){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ul","ul",-1349521403),(function (){var iter__4529__auto__ = (function sablono$core$unordered_list29353_$_iter__29354(s__29355){
return (new cljs.core.LazySeq(null,(function (){
var s__29355__$1 = s__29355;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__29355__$1);
if(temp__5735__auto__){
var s__29355__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__29355__$2)){
var c__4527__auto__ = cljs.core.chunk_first(s__29355__$2);
var size__4528__auto__ = cljs.core.count(c__4527__auto__);
var b__29357 = cljs.core.chunk_buffer(size__4528__auto__);
if((function (){var i__29356 = (0);
while(true){
if((i__29356 < size__4528__auto__)){
var x = cljs.core._nth(c__4527__auto__,i__29356);
cljs.core.chunk_append(b__29357,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),x], null));

var G__29561 = (i__29356 + (1));
i__29356 = G__29561;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__29357),sablono$core$unordered_list29353_$_iter__29354(cljs.core.chunk_rest(s__29355__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__29357),null);
}
} else {
var x = cljs.core.first(s__29355__$2);
return cljs.core.cons(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),x], null),sablono$core$unordered_list29353_$_iter__29354(cljs.core.rest(s__29355__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4529__auto__(coll);
})()], null);
});

sablono.core.unordered_list = sablono.core.wrap_attrs(sablono.core.unordered_list29353);
/**
 * Wrap a collection in an ordered list.
 */
sablono.core.ordered_list29358 = (function sablono$core$ordered_list29358(coll){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ol","ol",932524051),(function (){var iter__4529__auto__ = (function sablono$core$ordered_list29358_$_iter__29359(s__29360){
return (new cljs.core.LazySeq(null,(function (){
var s__29360__$1 = s__29360;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__29360__$1);
if(temp__5735__auto__){
var s__29360__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__29360__$2)){
var c__4527__auto__ = cljs.core.chunk_first(s__29360__$2);
var size__4528__auto__ = cljs.core.count(c__4527__auto__);
var b__29362 = cljs.core.chunk_buffer(size__4528__auto__);
if((function (){var i__29361 = (0);
while(true){
if((i__29361 < size__4528__auto__)){
var x = cljs.core._nth(c__4527__auto__,i__29361);
cljs.core.chunk_append(b__29362,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),x], null));

var G__29566 = (i__29361 + (1));
i__29361 = G__29566;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__29362),sablono$core$ordered_list29358_$_iter__29359(cljs.core.chunk_rest(s__29360__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__29362),null);
}
} else {
var x = cljs.core.first(s__29360__$2);
return cljs.core.cons(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),x], null),sablono$core$ordered_list29358_$_iter__29359(cljs.core.rest(s__29360__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4529__auto__(coll);
})()], null);
});

sablono.core.ordered_list = sablono.core.wrap_attrs(sablono.core.ordered_list29358);
/**
 * Create an image element.
 */
sablono.core.image29364 = (function sablono$core$image29364(var_args){
var G__29375 = arguments.length;
switch (G__29375) {
case 1:
return sablono.core.image29364.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.image29364.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(sablono.core.image29364.cljs$core$IFn$_invoke$arity$1 = (function (src){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"img","img",1442687358),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"src","src",-1651076051),sablono.util.as_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([src], 0))], null)], null);
}));

(sablono.core.image29364.cljs$core$IFn$_invoke$arity$2 = (function (src,alt){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"img","img",1442687358),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"src","src",-1651076051),sablono.util.as_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([src], 0)),new cljs.core.Keyword(null,"alt","alt",-3214426),alt], null)], null);
}));

(sablono.core.image29364.cljs$lang$maxFixedArity = 2);


sablono.core.image = sablono.core.wrap_attrs(sablono.core.image29364);
sablono.core._STAR_group_STAR_ = cljs.core.PersistentVector.EMPTY;
/**
 * Create a field name from the supplied argument the current field group.
 */
sablono.core.make_name = (function sablono$core$make_name(name){
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$2((function (p1__29388_SHARP_,p2__29389_SHARP_){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(p1__29388_SHARP_),"[",cljs.core.str.cljs$core$IFn$_invoke$arity$1(p2__29389_SHARP_),"]"].join('');
}),cljs.core.conj.cljs$core$IFn$_invoke$arity$2(sablono.core._STAR_group_STAR_,sablono.util.as_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([name], 0))));
});
/**
 * Create a field id from the supplied argument and current field group.
 */
sablono.core.make_id = (function sablono$core$make_id(name){
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$2((function (p1__29390_SHARP_,p2__29391_SHARP_){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(p1__29390_SHARP_),"-",cljs.core.str.cljs$core$IFn$_invoke$arity$1(p2__29391_SHARP_)].join('');
}),cljs.core.conj.cljs$core$IFn$_invoke$arity$2(sablono.core._STAR_group_STAR_,sablono.util.as_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([name], 0))));
});
/**
 * Creates a new <input> element.
 */
sablono.core.input_field_STAR_ = (function sablono$core$input_field_STAR_(var_args){
var G__29395 = arguments.length;
switch (G__29395) {
case 2:
return sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$2 = (function (type,name){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"type","type",1174270348),type,new cljs.core.Keyword(null,"name","name",1843675177),sablono.core.make_name(name),new cljs.core.Keyword(null,"id","id",-1388402092),sablono.core.make_id(name)], null)], null);
}));

(sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$3 = (function (type,name,value){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"type","type",1174270348),type,new cljs.core.Keyword(null,"name","name",1843675177),sablono.core.make_name(name),new cljs.core.Keyword(null,"id","id",-1388402092),sablono.core.make_id(name),new cljs.core.Keyword(null,"value","value",305978217),(function (){var or__4126__auto__ = value;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return undefined;
}
})()], null)], null);
}));

(sablono.core.input_field_STAR_.cljs$lang$maxFixedArity = 3);

/**
 * Creates a color input field.
 */
sablono.core.color_field29398 = (function sablono$core$color_field29398(var_args){
var G__29401 = arguments.length;
switch (G__29401) {
case 1:
return sablono.core.color_field29398.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.color_field29398.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(sablono.core.color_field29398.cljs$core$IFn$_invoke$arity$1 = (function (name__29285__auto__){
return sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$2(cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"color","color",-1642760596,null)),name__29285__auto__);
}));

(sablono.core.color_field29398.cljs$core$IFn$_invoke$arity$2 = (function (name__29285__auto__,value__29286__auto__){
return sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$3(cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"color","color",-1642760596,null)),name__29285__auto__,value__29286__auto__);
}));

(sablono.core.color_field29398.cljs$lang$maxFixedArity = 2);


sablono.core.color_field = sablono.core.wrap_attrs(sablono.core.color_field29398);

/**
 * Creates a date input field.
 */
sablono.core.date_field29405 = (function sablono$core$date_field29405(var_args){
var G__29407 = arguments.length;
switch (G__29407) {
case 1:
return sablono.core.date_field29405.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.date_field29405.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(sablono.core.date_field29405.cljs$core$IFn$_invoke$arity$1 = (function (name__29285__auto__){
return sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$2(cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"date","date",177097065,null)),name__29285__auto__);
}));

(sablono.core.date_field29405.cljs$core$IFn$_invoke$arity$2 = (function (name__29285__auto__,value__29286__auto__){
return sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$3(cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"date","date",177097065,null)),name__29285__auto__,value__29286__auto__);
}));

(sablono.core.date_field29405.cljs$lang$maxFixedArity = 2);


sablono.core.date_field = sablono.core.wrap_attrs(sablono.core.date_field29405);

/**
 * Creates a datetime input field.
 */
sablono.core.datetime_field29412 = (function sablono$core$datetime_field29412(var_args){
var G__29414 = arguments.length;
switch (G__29414) {
case 1:
return sablono.core.datetime_field29412.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.datetime_field29412.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(sablono.core.datetime_field29412.cljs$core$IFn$_invoke$arity$1 = (function (name__29285__auto__){
return sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$2(cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"datetime","datetime",2135207229,null)),name__29285__auto__);
}));

(sablono.core.datetime_field29412.cljs$core$IFn$_invoke$arity$2 = (function (name__29285__auto__,value__29286__auto__){
return sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$3(cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"datetime","datetime",2135207229,null)),name__29285__auto__,value__29286__auto__);
}));

(sablono.core.datetime_field29412.cljs$lang$maxFixedArity = 2);


sablono.core.datetime_field = sablono.core.wrap_attrs(sablono.core.datetime_field29412);

/**
 * Creates a datetime-local input field.
 */
sablono.core.datetime_local_field29417 = (function sablono$core$datetime_local_field29417(var_args){
var G__29419 = arguments.length;
switch (G__29419) {
case 1:
return sablono.core.datetime_local_field29417.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.datetime_local_field29417.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(sablono.core.datetime_local_field29417.cljs$core$IFn$_invoke$arity$1 = (function (name__29285__auto__){
return sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$2(cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"datetime-local","datetime-local",-507312697,null)),name__29285__auto__);
}));

(sablono.core.datetime_local_field29417.cljs$core$IFn$_invoke$arity$2 = (function (name__29285__auto__,value__29286__auto__){
return sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$3(cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"datetime-local","datetime-local",-507312697,null)),name__29285__auto__,value__29286__auto__);
}));

(sablono.core.datetime_local_field29417.cljs$lang$maxFixedArity = 2);


sablono.core.datetime_local_field = sablono.core.wrap_attrs(sablono.core.datetime_local_field29417);

/**
 * Creates a email input field.
 */
sablono.core.email_field29421 = (function sablono$core$email_field29421(var_args){
var G__29423 = arguments.length;
switch (G__29423) {
case 1:
return sablono.core.email_field29421.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.email_field29421.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(sablono.core.email_field29421.cljs$core$IFn$_invoke$arity$1 = (function (name__29285__auto__){
return sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$2(cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"email","email",-1238619063,null)),name__29285__auto__);
}));

(sablono.core.email_field29421.cljs$core$IFn$_invoke$arity$2 = (function (name__29285__auto__,value__29286__auto__){
return sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$3(cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"email","email",-1238619063,null)),name__29285__auto__,value__29286__auto__);
}));

(sablono.core.email_field29421.cljs$lang$maxFixedArity = 2);


sablono.core.email_field = sablono.core.wrap_attrs(sablono.core.email_field29421);

/**
 * Creates a file input field.
 */
sablono.core.file_field29424 = (function sablono$core$file_field29424(var_args){
var G__29426 = arguments.length;
switch (G__29426) {
case 1:
return sablono.core.file_field29424.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.file_field29424.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(sablono.core.file_field29424.cljs$core$IFn$_invoke$arity$1 = (function (name__29285__auto__){
return sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$2(cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"file","file",370885649,null)),name__29285__auto__);
}));

(sablono.core.file_field29424.cljs$core$IFn$_invoke$arity$2 = (function (name__29285__auto__,value__29286__auto__){
return sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$3(cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"file","file",370885649,null)),name__29285__auto__,value__29286__auto__);
}));

(sablono.core.file_field29424.cljs$lang$maxFixedArity = 2);


sablono.core.file_field = sablono.core.wrap_attrs(sablono.core.file_field29424);

/**
 * Creates a hidden input field.
 */
sablono.core.hidden_field29427 = (function sablono$core$hidden_field29427(var_args){
var G__29429 = arguments.length;
switch (G__29429) {
case 1:
return sablono.core.hidden_field29427.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.hidden_field29427.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(sablono.core.hidden_field29427.cljs$core$IFn$_invoke$arity$1 = (function (name__29285__auto__){
return sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$2(cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"hidden","hidden",1328025435,null)),name__29285__auto__);
}));

(sablono.core.hidden_field29427.cljs$core$IFn$_invoke$arity$2 = (function (name__29285__auto__,value__29286__auto__){
return sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$3(cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"hidden","hidden",1328025435,null)),name__29285__auto__,value__29286__auto__);
}));

(sablono.core.hidden_field29427.cljs$lang$maxFixedArity = 2);


sablono.core.hidden_field = sablono.core.wrap_attrs(sablono.core.hidden_field29427);

/**
 * Creates a month input field.
 */
sablono.core.month_field29430 = (function sablono$core$month_field29430(var_args){
var G__29433 = arguments.length;
switch (G__29433) {
case 1:
return sablono.core.month_field29430.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.month_field29430.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(sablono.core.month_field29430.cljs$core$IFn$_invoke$arity$1 = (function (name__29285__auto__){
return sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$2(cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"month","month",-319717006,null)),name__29285__auto__);
}));

(sablono.core.month_field29430.cljs$core$IFn$_invoke$arity$2 = (function (name__29285__auto__,value__29286__auto__){
return sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$3(cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"month","month",-319717006,null)),name__29285__auto__,value__29286__auto__);
}));

(sablono.core.month_field29430.cljs$lang$maxFixedArity = 2);


sablono.core.month_field = sablono.core.wrap_attrs(sablono.core.month_field29430);

/**
 * Creates a number input field.
 */
sablono.core.number_field29436 = (function sablono$core$number_field29436(var_args){
var G__29440 = arguments.length;
switch (G__29440) {
case 1:
return sablono.core.number_field29436.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.number_field29436.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(sablono.core.number_field29436.cljs$core$IFn$_invoke$arity$1 = (function (name__29285__auto__){
return sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$2(cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"number","number",-1084057331,null)),name__29285__auto__);
}));

(sablono.core.number_field29436.cljs$core$IFn$_invoke$arity$2 = (function (name__29285__auto__,value__29286__auto__){
return sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$3(cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"number","number",-1084057331,null)),name__29285__auto__,value__29286__auto__);
}));

(sablono.core.number_field29436.cljs$lang$maxFixedArity = 2);


sablono.core.number_field = sablono.core.wrap_attrs(sablono.core.number_field29436);

/**
 * Creates a password input field.
 */
sablono.core.password_field29441 = (function sablono$core$password_field29441(var_args){
var G__29443 = arguments.length;
switch (G__29443) {
case 1:
return sablono.core.password_field29441.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.password_field29441.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(sablono.core.password_field29441.cljs$core$IFn$_invoke$arity$1 = (function (name__29285__auto__){
return sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$2(cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"password","password",2057553998,null)),name__29285__auto__);
}));

(sablono.core.password_field29441.cljs$core$IFn$_invoke$arity$2 = (function (name__29285__auto__,value__29286__auto__){
return sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$3(cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"password","password",2057553998,null)),name__29285__auto__,value__29286__auto__);
}));

(sablono.core.password_field29441.cljs$lang$maxFixedArity = 2);


sablono.core.password_field = sablono.core.wrap_attrs(sablono.core.password_field29441);

/**
 * Creates a range input field.
 */
sablono.core.range_field29445 = (function sablono$core$range_field29445(var_args){
var G__29447 = arguments.length;
switch (G__29447) {
case 1:
return sablono.core.range_field29445.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.range_field29445.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(sablono.core.range_field29445.cljs$core$IFn$_invoke$arity$1 = (function (name__29285__auto__){
return sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$2(cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"range","range",-1014743483,null)),name__29285__auto__);
}));

(sablono.core.range_field29445.cljs$core$IFn$_invoke$arity$2 = (function (name__29285__auto__,value__29286__auto__){
return sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$3(cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"range","range",-1014743483,null)),name__29285__auto__,value__29286__auto__);
}));

(sablono.core.range_field29445.cljs$lang$maxFixedArity = 2);


sablono.core.range_field = sablono.core.wrap_attrs(sablono.core.range_field29445);

/**
 * Creates a search input field.
 */
sablono.core.search_field29448 = (function sablono$core$search_field29448(var_args){
var G__29450 = arguments.length;
switch (G__29450) {
case 1:
return sablono.core.search_field29448.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.search_field29448.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(sablono.core.search_field29448.cljs$core$IFn$_invoke$arity$1 = (function (name__29285__auto__){
return sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$2(cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"search","search",-1089495947,null)),name__29285__auto__);
}));

(sablono.core.search_field29448.cljs$core$IFn$_invoke$arity$2 = (function (name__29285__auto__,value__29286__auto__){
return sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$3(cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"search","search",-1089495947,null)),name__29285__auto__,value__29286__auto__);
}));

(sablono.core.search_field29448.cljs$lang$maxFixedArity = 2);


sablono.core.search_field = sablono.core.wrap_attrs(sablono.core.search_field29448);

/**
 * Creates a tel input field.
 */
sablono.core.tel_field29451 = (function sablono$core$tel_field29451(var_args){
var G__29453 = arguments.length;
switch (G__29453) {
case 1:
return sablono.core.tel_field29451.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.tel_field29451.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(sablono.core.tel_field29451.cljs$core$IFn$_invoke$arity$1 = (function (name__29285__auto__){
return sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$2(cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"tel","tel",1864669686,null)),name__29285__auto__);
}));

(sablono.core.tel_field29451.cljs$core$IFn$_invoke$arity$2 = (function (name__29285__auto__,value__29286__auto__){
return sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$3(cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"tel","tel",1864669686,null)),name__29285__auto__,value__29286__auto__);
}));

(sablono.core.tel_field29451.cljs$lang$maxFixedArity = 2);


sablono.core.tel_field = sablono.core.wrap_attrs(sablono.core.tel_field29451);

/**
 * Creates a text input field.
 */
sablono.core.text_field29454 = (function sablono$core$text_field29454(var_args){
var G__29456 = arguments.length;
switch (G__29456) {
case 1:
return sablono.core.text_field29454.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.text_field29454.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(sablono.core.text_field29454.cljs$core$IFn$_invoke$arity$1 = (function (name__29285__auto__){
return sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$2(cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"text","text",-150030170,null)),name__29285__auto__);
}));

(sablono.core.text_field29454.cljs$core$IFn$_invoke$arity$2 = (function (name__29285__auto__,value__29286__auto__){
return sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$3(cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"text","text",-150030170,null)),name__29285__auto__,value__29286__auto__);
}));

(sablono.core.text_field29454.cljs$lang$maxFixedArity = 2);


sablono.core.text_field = sablono.core.wrap_attrs(sablono.core.text_field29454);

/**
 * Creates a time input field.
 */
sablono.core.time_field29457 = (function sablono$core$time_field29457(var_args){
var G__29459 = arguments.length;
switch (G__29459) {
case 1:
return sablono.core.time_field29457.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.time_field29457.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(sablono.core.time_field29457.cljs$core$IFn$_invoke$arity$1 = (function (name__29285__auto__){
return sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$2(cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"time","time",-1268547887,null)),name__29285__auto__);
}));

(sablono.core.time_field29457.cljs$core$IFn$_invoke$arity$2 = (function (name__29285__auto__,value__29286__auto__){
return sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$3(cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"time","time",-1268547887,null)),name__29285__auto__,value__29286__auto__);
}));

(sablono.core.time_field29457.cljs$lang$maxFixedArity = 2);


sablono.core.time_field = sablono.core.wrap_attrs(sablono.core.time_field29457);

/**
 * Creates a url input field.
 */
sablono.core.url_field29463 = (function sablono$core$url_field29463(var_args){
var G__29465 = arguments.length;
switch (G__29465) {
case 1:
return sablono.core.url_field29463.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.url_field29463.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(sablono.core.url_field29463.cljs$core$IFn$_invoke$arity$1 = (function (name__29285__auto__){
return sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$2(cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"url","url",1916828573,null)),name__29285__auto__);
}));

(sablono.core.url_field29463.cljs$core$IFn$_invoke$arity$2 = (function (name__29285__auto__,value__29286__auto__){
return sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$3(cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"url","url",1916828573,null)),name__29285__auto__,value__29286__auto__);
}));

(sablono.core.url_field29463.cljs$lang$maxFixedArity = 2);


sablono.core.url_field = sablono.core.wrap_attrs(sablono.core.url_field29463);

/**
 * Creates a week input field.
 */
sablono.core.week_field29470 = (function sablono$core$week_field29470(var_args){
var G__29473 = arguments.length;
switch (G__29473) {
case 1:
return sablono.core.week_field29470.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.week_field29470.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(sablono.core.week_field29470.cljs$core$IFn$_invoke$arity$1 = (function (name__29285__auto__){
return sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$2(cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"week","week",314058249,null)),name__29285__auto__);
}));

(sablono.core.week_field29470.cljs$core$IFn$_invoke$arity$2 = (function (name__29285__auto__,value__29286__auto__){
return sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$3(cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"week","week",314058249,null)),name__29285__auto__,value__29286__auto__);
}));

(sablono.core.week_field29470.cljs$lang$maxFixedArity = 2);


sablono.core.week_field = sablono.core.wrap_attrs(sablono.core.week_field29470);
sablono.core.file_upload = sablono.core.file_field;
/**
 * Creates a check box.
 */
sablono.core.check_box29475 = (function sablono$core$check_box29475(var_args){
var G__29477 = arguments.length;
switch (G__29477) {
case 1:
return sablono.core.check_box29475.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.check_box29475.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return sablono.core.check_box29475.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(sablono.core.check_box29475.cljs$core$IFn$_invoke$arity$1 = (function (name){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"type","type",1174270348),"checkbox",new cljs.core.Keyword(null,"name","name",1843675177),sablono.core.make_name(name),new cljs.core.Keyword(null,"id","id",-1388402092),sablono.core.make_id(name)], null)], null);
}));

(sablono.core.check_box29475.cljs$core$IFn$_invoke$arity$2 = (function (name,checked_QMARK_){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"type","type",1174270348),"checkbox",new cljs.core.Keyword(null,"name","name",1843675177),sablono.core.make_name(name),new cljs.core.Keyword(null,"id","id",-1388402092),sablono.core.make_id(name),new cljs.core.Keyword(null,"checked","checked",-50955819),checked_QMARK_], null)], null);
}));

(sablono.core.check_box29475.cljs$core$IFn$_invoke$arity$3 = (function (name,checked_QMARK_,value){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"type","type",1174270348),"checkbox",new cljs.core.Keyword(null,"name","name",1843675177),sablono.core.make_name(name),new cljs.core.Keyword(null,"id","id",-1388402092),sablono.core.make_id(name),new cljs.core.Keyword(null,"value","value",305978217),value,new cljs.core.Keyword(null,"checked","checked",-50955819),checked_QMARK_], null)], null);
}));

(sablono.core.check_box29475.cljs$lang$maxFixedArity = 3);


sablono.core.check_box = sablono.core.wrap_attrs(sablono.core.check_box29475);
/**
 * Creates a radio button.
 */
sablono.core.radio_button29481 = (function sablono$core$radio_button29481(var_args){
var G__29483 = arguments.length;
switch (G__29483) {
case 1:
return sablono.core.radio_button29481.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.radio_button29481.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return sablono.core.radio_button29481.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(sablono.core.radio_button29481.cljs$core$IFn$_invoke$arity$1 = (function (group){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"type","type",1174270348),"radio",new cljs.core.Keyword(null,"name","name",1843675177),sablono.core.make_name(group),new cljs.core.Keyword(null,"id","id",-1388402092),sablono.core.make_id(sablono.util.as_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([group], 0)))], null)], null);
}));

(sablono.core.radio_button29481.cljs$core$IFn$_invoke$arity$2 = (function (group,checked_QMARK_){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"type","type",1174270348),"radio",new cljs.core.Keyword(null,"name","name",1843675177),sablono.core.make_name(group),new cljs.core.Keyword(null,"id","id",-1388402092),sablono.core.make_id(sablono.util.as_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([group], 0))),new cljs.core.Keyword(null,"checked","checked",-50955819),checked_QMARK_], null)], null);
}));

(sablono.core.radio_button29481.cljs$core$IFn$_invoke$arity$3 = (function (group,checked_QMARK_,value){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"type","type",1174270348),"radio",new cljs.core.Keyword(null,"name","name",1843675177),sablono.core.make_name(group),new cljs.core.Keyword(null,"id","id",-1388402092),sablono.core.make_id([sablono.util.as_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([group], 0)),"-",sablono.util.as_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([value], 0))].join('')),new cljs.core.Keyword(null,"value","value",305978217),value,new cljs.core.Keyword(null,"checked","checked",-50955819),checked_QMARK_], null)], null);
}));

(sablono.core.radio_button29481.cljs$lang$maxFixedArity = 3);


sablono.core.radio_button = sablono.core.wrap_attrs(sablono.core.radio_button29481);
sablono.core.hash_key = (function sablono$core$hash_key(x){
return goog.string.hashCode(cljs.core.pr_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([x], 0)));
});
/**
 * Creates a seq of option tags from a collection.
 */
sablono.core.select_options29487 = (function sablono$core$select_options29487(coll){
var iter__4529__auto__ = (function sablono$core$select_options29487_$_iter__29489(s__29490){
return (new cljs.core.LazySeq(null,(function (){
var s__29490__$1 = s__29490;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__29490__$1);
if(temp__5735__auto__){
var s__29490__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__29490__$2)){
var c__4527__auto__ = cljs.core.chunk_first(s__29490__$2);
var size__4528__auto__ = cljs.core.count(c__4527__auto__);
var b__29492 = cljs.core.chunk_buffer(size__4528__auto__);
if((function (){var i__29491 = (0);
while(true){
if((i__29491 < size__4528__auto__)){
var x = cljs.core._nth(c__4527__auto__,i__29491);
cljs.core.chunk_append(b__29492,((cljs.core.sequential_QMARK_(x))?(function (){var vec__29494 = x;
var text = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__29494,(0),null);
var val = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__29494,(1),null);
var disabled_QMARK_ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__29494,(2),null);
var disabled_QMARK___$1 = cljs.core.boolean$(disabled_QMARK_);
if(cljs.core.sequential_QMARK_(val)){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"optgroup","optgroup",1738282218),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"key","key",-1516042587),sablono.core.hash_key(text),new cljs.core.Keyword(null,"label","label",1718410804),text], null),(sablono.core.select_options29487.cljs$core$IFn$_invoke$arity$1 ? sablono.core.select_options29487.cljs$core$IFn$_invoke$arity$1(val) : sablono.core.select_options29487.call(null,val))], null);
} else {
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"option","option",65132272),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"disabled","disabled",-1529784218),disabled_QMARK___$1,new cljs.core.Keyword(null,"key","key",-1516042587),sablono.core.hash_key(val),new cljs.core.Keyword(null,"value","value",305978217),val], null),text], null);
}
})():new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"option","option",65132272),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"key","key",-1516042587),sablono.core.hash_key(x),new cljs.core.Keyword(null,"value","value",305978217),x], null),x], null)));

var G__29714 = (i__29491 + (1));
i__29491 = G__29714;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__29492),sablono$core$select_options29487_$_iter__29489(cljs.core.chunk_rest(s__29490__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__29492),null);
}
} else {
var x = cljs.core.first(s__29490__$2);
return cljs.core.cons(((cljs.core.sequential_QMARK_(x))?(function (){var vec__29497 = x;
var text = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__29497,(0),null);
var val = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__29497,(1),null);
var disabled_QMARK_ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__29497,(2),null);
var disabled_QMARK___$1 = cljs.core.boolean$(disabled_QMARK_);
if(cljs.core.sequential_QMARK_(val)){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"optgroup","optgroup",1738282218),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"key","key",-1516042587),sablono.core.hash_key(text),new cljs.core.Keyword(null,"label","label",1718410804),text], null),(sablono.core.select_options29487.cljs$core$IFn$_invoke$arity$1 ? sablono.core.select_options29487.cljs$core$IFn$_invoke$arity$1(val) : sablono.core.select_options29487.call(null,val))], null);
} else {
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"option","option",65132272),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"disabled","disabled",-1529784218),disabled_QMARK___$1,new cljs.core.Keyword(null,"key","key",-1516042587),sablono.core.hash_key(val),new cljs.core.Keyword(null,"value","value",305978217),val], null),text], null);
}
})():new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"option","option",65132272),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"key","key",-1516042587),sablono.core.hash_key(x),new cljs.core.Keyword(null,"value","value",305978217),x], null),x], null)),sablono$core$select_options29487_$_iter__29489(cljs.core.rest(s__29490__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4529__auto__(coll);
});

sablono.core.select_options = sablono.core.wrap_attrs(sablono.core.select_options29487);
/**
 * Creates a drop-down box using the <select> tag.
 */
sablono.core.drop_down29500 = (function sablono$core$drop_down29500(var_args){
var G__29502 = arguments.length;
switch (G__29502) {
case 2:
return sablono.core.drop_down29500.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return sablono.core.drop_down29500.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(sablono.core.drop_down29500.cljs$core$IFn$_invoke$arity$2 = (function (name,options){
return sablono.core.drop_down29500.cljs$core$IFn$_invoke$arity$3(name,options,null);
}));

(sablono.core.drop_down29500.cljs$core$IFn$_invoke$arity$3 = (function (name,options,selected){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"select","select",1147833503),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"name","name",1843675177),sablono.core.make_name(name),new cljs.core.Keyword(null,"id","id",-1388402092),sablono.core.make_id(name)], null),sablono.core.select_options(options,selected)], null);
}));

(sablono.core.drop_down29500.cljs$lang$maxFixedArity = 3);


sablono.core.drop_down = sablono.core.wrap_attrs(sablono.core.drop_down29500);
/**
 * Creates a text area element.
 */
sablono.core.text_area29504 = (function sablono$core$text_area29504(var_args){
var G__29507 = arguments.length;
switch (G__29507) {
case 1:
return sablono.core.text_area29504.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.text_area29504.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(sablono.core.text_area29504.cljs$core$IFn$_invoke$arity$1 = (function (name){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"textarea","textarea",-650375824),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"name","name",1843675177),sablono.core.make_name(name),new cljs.core.Keyword(null,"id","id",-1388402092),sablono.core.make_id(name)], null)], null);
}));

(sablono.core.text_area29504.cljs$core$IFn$_invoke$arity$2 = (function (name,value){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"textarea","textarea",-650375824),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"name","name",1843675177),sablono.core.make_name(name),new cljs.core.Keyword(null,"id","id",-1388402092),sablono.core.make_id(name),new cljs.core.Keyword(null,"value","value",305978217),(function (){var or__4126__auto__ = value;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return undefined;
}
})()], null)], null);
}));

(sablono.core.text_area29504.cljs$lang$maxFixedArity = 2);


sablono.core.text_area = sablono.core.wrap_attrs(sablono.core.text_area29504);
/**
 * Creates a label for an input field with the supplied name.
 */
sablono.core.label29510 = (function sablono$core$label29510(name,text){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"label","label",1718410804),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"htmlFor","htmlFor",-1050291720),sablono.core.make_id(name)], null),text], null);
});

sablono.core.label = sablono.core.wrap_attrs(sablono.core.label29510);
/**
 * Creates a submit button.
 */
sablono.core.submit_button29512 = (function sablono$core$submit_button29512(text){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1174270348),"submit",new cljs.core.Keyword(null,"value","value",305978217),text], null)], null);
});

sablono.core.submit_button = sablono.core.wrap_attrs(sablono.core.submit_button29512);
/**
 * Creates a form reset button.
 */
sablono.core.reset_button29513 = (function sablono$core$reset_button29513(text){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1174270348),"reset",new cljs.core.Keyword(null,"value","value",305978217),text], null)], null);
});

sablono.core.reset_button = sablono.core.wrap_attrs(sablono.core.reset_button29513);
/**
 * Create a form that points to a particular method and route.
 *   e.g. (form-to [:put "/post"]
 *       ...)
 */
sablono.core.form_to29514 = (function sablono$core$form_to29514(var_args){
var args__4742__auto__ = [];
var len__4736__auto___29751 = arguments.length;
var i__4737__auto___29752 = (0);
while(true){
if((i__4737__auto___29752 < len__4736__auto___29751)){
args__4742__auto__.push((arguments[i__4737__auto___29752]));

var G__29755 = (i__4737__auto___29752 + (1));
i__4737__auto___29752 = G__29755;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((1) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((1)),(0),null)):null);
return sablono.core.form_to29514.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4743__auto__);
});

(sablono.core.form_to29514.cljs$core$IFn$_invoke$arity$variadic = (function (p__29517,body){
var vec__29518 = p__29517;
var method = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__29518,(0),null);
var action = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__29518,(1),null);
var method_str = clojure.string.upper_case(cljs.core.name(method));
var action_uri = sablono.util.to_uri(action);
return cljs.core.vec(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(((cljs.core.contains_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"get","get",1683182755),null,new cljs.core.Keyword(null,"post","post",269697687),null], null), null),method))?new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"form","form",-1624062471),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"method","method",55703592),method_str,new cljs.core.Keyword(null,"action","action",-811238024),action_uri], null)], null):new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"form","form",-1624062471),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"method","method",55703592),"POST",new cljs.core.Keyword(null,"action","action",-811238024),action_uri], null),sablono.core.hidden_field(new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),(3735928559)], null),"_method",method_str)], null)),body));
}));

(sablono.core.form_to29514.cljs$lang$maxFixedArity = (1));

/** @this {Function} */
(sablono.core.form_to29514.cljs$lang$applyTo = (function (seq29515){
var G__29516 = cljs.core.first(seq29515);
var seq29515__$1 = cljs.core.next(seq29515);
var self__4723__auto__ = this;
return self__4723__auto__.cljs$core$IFn$_invoke$arity$variadic(G__29516,seq29515__$1);
}));


sablono.core.form_to = sablono.core.wrap_attrs(sablono.core.form_to29514);

//# sourceMappingURL=sablono.core.js.map
