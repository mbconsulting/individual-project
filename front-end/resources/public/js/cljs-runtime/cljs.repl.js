goog.provide('cljs.repl');
cljs.repl.print_doc = (function cljs$repl$print_doc(p__44609){
var map__44610 = p__44609;
var map__44610__$1 = (((((!((map__44610 == null))))?(((((map__44610.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__44610.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__44610):map__44610);
var m = map__44610__$1;
var n = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__44610__$1,new cljs.core.Keyword(null,"ns","ns",441598760));
var nm = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__44610__$1,new cljs.core.Keyword(null,"name","name",1843675177));
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["-------------------------"], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(function (){var or__4126__auto__ = new cljs.core.Keyword(null,"spec","spec",347520401).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return [(function (){var temp__5735__auto__ = new cljs.core.Keyword(null,"ns","ns",441598760).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(temp__5735__auto__)){
var ns = temp__5735__auto__;
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(ns),"/"].join('');
} else {
return null;
}
})(),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join('');
}
})()], 0));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Protocol"], 0));
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m))){
var seq__44612_44713 = cljs.core.seq(new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m));
var chunk__44613_44714 = null;
var count__44614_44715 = (0);
var i__44615_44716 = (0);
while(true){
if((i__44615_44716 < count__44614_44715)){
var f_44717 = chunk__44613_44714.cljs$core$IIndexed$_nth$arity$2(null,i__44615_44716);
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["  ",f_44717], 0));


var G__44718 = seq__44612_44713;
var G__44719 = chunk__44613_44714;
var G__44720 = count__44614_44715;
var G__44721 = (i__44615_44716 + (1));
seq__44612_44713 = G__44718;
chunk__44613_44714 = G__44719;
count__44614_44715 = G__44720;
i__44615_44716 = G__44721;
continue;
} else {
var temp__5735__auto___44722 = cljs.core.seq(seq__44612_44713);
if(temp__5735__auto___44722){
var seq__44612_44723__$1 = temp__5735__auto___44722;
if(cljs.core.chunked_seq_QMARK_(seq__44612_44723__$1)){
var c__4556__auto___44724 = cljs.core.chunk_first(seq__44612_44723__$1);
var G__44725 = cljs.core.chunk_rest(seq__44612_44723__$1);
var G__44726 = c__4556__auto___44724;
var G__44727 = cljs.core.count(c__4556__auto___44724);
var G__44728 = (0);
seq__44612_44713 = G__44725;
chunk__44613_44714 = G__44726;
count__44614_44715 = G__44727;
i__44615_44716 = G__44728;
continue;
} else {
var f_44729 = cljs.core.first(seq__44612_44723__$1);
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["  ",f_44729], 0));


var G__44730 = cljs.core.next(seq__44612_44723__$1);
var G__44731 = null;
var G__44732 = (0);
var G__44733 = (0);
seq__44612_44713 = G__44730;
chunk__44613_44714 = G__44731;
count__44614_44715 = G__44732;
i__44615_44716 = G__44733;
continue;
}
} else {
}
}
break;
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m))){
var arglists_44734 = new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_((function (){var or__4126__auto__ = new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m);
}
})())){
cljs.core.prn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([arglists_44734], 0));
} else {
cljs.core.prn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.first(arglists_44734)))?cljs.core.second(arglists_44734):arglists_44734)], 0));
}
} else {
}
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"special-form","special-form",-1326536374).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Special Form"], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m)], 0));

if(cljs.core.contains_QMARK_(m,new cljs.core.Keyword(null,"url","url",276297046))){
if(cljs.core.truth_(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))){
return cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n  Please see http://clojure.org/",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))].join('')], 0));
} else {
return null;
}
} else {
return cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n  Please see http://clojure.org/special_forms#",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join('')], 0));
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Macro"], 0));
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"spec","spec",347520401).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Spec"], 0));
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["REPL Special Function"], 0));
} else {
}

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m)], 0));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
var seq__44616_44735 = cljs.core.seq(new cljs.core.Keyword(null,"methods","methods",453930866).cljs$core$IFn$_invoke$arity$1(m));
var chunk__44617_44736 = null;
var count__44618_44737 = (0);
var i__44619_44738 = (0);
while(true){
if((i__44619_44738 < count__44618_44737)){
var vec__44630_44739 = chunk__44617_44736.cljs$core$IIndexed$_nth$arity$2(null,i__44619_44738);
var name_44740 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__44630_44739,(0),null);
var map__44633_44741 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__44630_44739,(1),null);
var map__44633_44742__$1 = (((((!((map__44633_44741 == null))))?(((((map__44633_44741.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__44633_44741.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__44633_44741):map__44633_44741);
var doc_44743 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__44633_44742__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists_44744 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__44633_44742__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println();

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",name_44740], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",arglists_44744], 0));

if(cljs.core.truth_(doc_44743)){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",doc_44743], 0));
} else {
}


var G__44745 = seq__44616_44735;
var G__44746 = chunk__44617_44736;
var G__44747 = count__44618_44737;
var G__44748 = (i__44619_44738 + (1));
seq__44616_44735 = G__44745;
chunk__44617_44736 = G__44746;
count__44618_44737 = G__44747;
i__44619_44738 = G__44748;
continue;
} else {
var temp__5735__auto___44749 = cljs.core.seq(seq__44616_44735);
if(temp__5735__auto___44749){
var seq__44616_44750__$1 = temp__5735__auto___44749;
if(cljs.core.chunked_seq_QMARK_(seq__44616_44750__$1)){
var c__4556__auto___44751 = cljs.core.chunk_first(seq__44616_44750__$1);
var G__44752 = cljs.core.chunk_rest(seq__44616_44750__$1);
var G__44753 = c__4556__auto___44751;
var G__44754 = cljs.core.count(c__4556__auto___44751);
var G__44755 = (0);
seq__44616_44735 = G__44752;
chunk__44617_44736 = G__44753;
count__44618_44737 = G__44754;
i__44619_44738 = G__44755;
continue;
} else {
var vec__44635_44756 = cljs.core.first(seq__44616_44750__$1);
var name_44757 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__44635_44756,(0),null);
var map__44638_44758 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__44635_44756,(1),null);
var map__44638_44759__$1 = (((((!((map__44638_44758 == null))))?(((((map__44638_44758.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__44638_44758.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__44638_44758):map__44638_44758);
var doc_44760 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__44638_44759__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists_44761 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__44638_44759__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println();

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",name_44757], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",arglists_44761], 0));

if(cljs.core.truth_(doc_44760)){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",doc_44760], 0));
} else {
}


var G__44762 = cljs.core.next(seq__44616_44750__$1);
var G__44763 = null;
var G__44764 = (0);
var G__44765 = (0);
seq__44616_44735 = G__44762;
chunk__44617_44736 = G__44763;
count__44618_44737 = G__44764;
i__44619_44738 = G__44765;
continue;
}
} else {
}
}
break;
}
} else {
}

if(cljs.core.truth_(n)){
var temp__5735__auto__ = cljs.spec.alpha.get_spec(cljs.core.symbol.cljs$core$IFn$_invoke$arity$2(cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.ns_name(n)),cljs.core.name(nm)));
if(cljs.core.truth_(temp__5735__auto__)){
var fnspec = temp__5735__auto__;
cljs.core.print.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Spec"], 0));

var seq__44640 = cljs.core.seq(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"args","args",1315556576),new cljs.core.Keyword(null,"ret","ret",-468222814),new cljs.core.Keyword(null,"fn","fn",-1175266204)], null));
var chunk__44641 = null;
var count__44642 = (0);
var i__44643 = (0);
while(true){
if((i__44643 < count__44642)){
var role = chunk__44641.cljs$core$IIndexed$_nth$arity$2(null,i__44643);
var temp__5735__auto___44766__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(fnspec,role);
if(cljs.core.truth_(temp__5735__auto___44766__$1)){
var spec_44767 = temp__5735__auto___44766__$1;
cljs.core.print.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n ",cljs.core.name(role),":"].join(''),cljs.spec.alpha.describe(spec_44767)], 0));
} else {
}


var G__44768 = seq__44640;
var G__44769 = chunk__44641;
var G__44770 = count__44642;
var G__44771 = (i__44643 + (1));
seq__44640 = G__44768;
chunk__44641 = G__44769;
count__44642 = G__44770;
i__44643 = G__44771;
continue;
} else {
var temp__5735__auto____$1 = cljs.core.seq(seq__44640);
if(temp__5735__auto____$1){
var seq__44640__$1 = temp__5735__auto____$1;
if(cljs.core.chunked_seq_QMARK_(seq__44640__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__44640__$1);
var G__44772 = cljs.core.chunk_rest(seq__44640__$1);
var G__44773 = c__4556__auto__;
var G__44774 = cljs.core.count(c__4556__auto__);
var G__44775 = (0);
seq__44640 = G__44772;
chunk__44641 = G__44773;
count__44642 = G__44774;
i__44643 = G__44775;
continue;
} else {
var role = cljs.core.first(seq__44640__$1);
var temp__5735__auto___44776__$2 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(fnspec,role);
if(cljs.core.truth_(temp__5735__auto___44776__$2)){
var spec_44777 = temp__5735__auto___44776__$2;
cljs.core.print.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n ",cljs.core.name(role),":"].join(''),cljs.spec.alpha.describe(spec_44777)], 0));
} else {
}


var G__44778 = cljs.core.next(seq__44640__$1);
var G__44779 = null;
var G__44780 = (0);
var G__44781 = (0);
seq__44640 = G__44778;
chunk__44641 = G__44779;
count__44642 = G__44780;
i__44643 = G__44781;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return null;
}
} else {
return null;
}
}
});
/**
 * Constructs a data representation for a Error with keys:
 *  :cause - root cause message
 *  :phase - error phase
 *  :via - cause chain, with cause keys:
 *           :type - exception class symbol
 *           :message - exception message
 *           :data - ex-data
 *           :at - top stack element
 *  :trace - root cause stack elements
 */
cljs.repl.Error__GT_map = (function cljs$repl$Error__GT_map(o){
var base = (function (t){
return cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"type","type",1174270348),(((t instanceof cljs.core.ExceptionInfo))?new cljs.core.Symbol(null,"ExceptionInfo","ExceptionInfo",294935087,null):(((t instanceof Error))?cljs.core.symbol.cljs$core$IFn$_invoke$arity$2("js",t.name):null
))], null),(function (){var temp__5735__auto__ = cljs.core.ex_message(t);
if(cljs.core.truth_(temp__5735__auto__)){
var msg = temp__5735__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"message","message",-406056002),msg], null);
} else {
return null;
}
})(),(function (){var temp__5735__auto__ = cljs.core.ex_data(t);
if(cljs.core.truth_(temp__5735__auto__)){
var ed = temp__5735__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"data","data",-232669377),ed], null);
} else {
return null;
}
})()], 0));
});
var via = (function (){var via = cljs.core.PersistentVector.EMPTY;
var t = o;
while(true){
if(cljs.core.truth_(t)){
var G__44782 = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(via,t);
var G__44783 = cljs.core.ex_cause(t);
via = G__44782;
t = G__44783;
continue;
} else {
return via;
}
break;
}
})();
var root = cljs.core.peek(via);
return cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"via","via",-1904457336),cljs.core.vec(cljs.core.map.cljs$core$IFn$_invoke$arity$2(base,via)),new cljs.core.Keyword(null,"trace","trace",-1082747415),null], null),(function (){var temp__5735__auto__ = cljs.core.ex_message(root);
if(cljs.core.truth_(temp__5735__auto__)){
var root_msg = temp__5735__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"cause","cause",231901252),root_msg], null);
} else {
return null;
}
})(),(function (){var temp__5735__auto__ = cljs.core.ex_data(root);
if(cljs.core.truth_(temp__5735__auto__)){
var data = temp__5735__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"data","data",-232669377),data], null);
} else {
return null;
}
})(),(function (){var temp__5735__auto__ = new cljs.core.Keyword("clojure.error","phase","clojure.error/phase",275140358).cljs$core$IFn$_invoke$arity$1(cljs.core.ex_data(o));
if(cljs.core.truth_(temp__5735__auto__)){
var phase = temp__5735__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"phase","phase",575722892),phase], null);
} else {
return null;
}
})()], 0));
});
/**
 * Returns an analysis of the phase, error, cause, and location of an error that occurred
 *   based on Throwable data, as returned by Throwable->map. All attributes other than phase
 *   are optional:
 *  :clojure.error/phase - keyword phase indicator, one of:
 *    :read-source :compile-syntax-check :compilation :macro-syntax-check :macroexpansion
 *    :execution :read-eval-result :print-eval-result
 *  :clojure.error/source - file name (no path)
 *  :clojure.error/line - integer line number
 *  :clojure.error/column - integer column number
 *  :clojure.error/symbol - symbol being expanded/compiled/invoked
 *  :clojure.error/class - cause exception class symbol
 *  :clojure.error/cause - cause exception message
 *  :clojure.error/spec - explain-data for spec error
 */
cljs.repl.ex_triage = (function cljs$repl$ex_triage(datafied_throwable){
var map__44646 = datafied_throwable;
var map__44646__$1 = (((((!((map__44646 == null))))?(((((map__44646.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__44646.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__44646):map__44646);
var via = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__44646__$1,new cljs.core.Keyword(null,"via","via",-1904457336));
var trace = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__44646__$1,new cljs.core.Keyword(null,"trace","trace",-1082747415));
var phase = cljs.core.get.cljs$core$IFn$_invoke$arity$3(map__44646__$1,new cljs.core.Keyword(null,"phase","phase",575722892),new cljs.core.Keyword(null,"execution","execution",253283524));
var map__44647 = cljs.core.last(via);
var map__44647__$1 = (((((!((map__44647 == null))))?(((((map__44647.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__44647.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__44647):map__44647);
var type = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__44647__$1,new cljs.core.Keyword(null,"type","type",1174270348));
var message = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__44647__$1,new cljs.core.Keyword(null,"message","message",-406056002));
var data = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__44647__$1,new cljs.core.Keyword(null,"data","data",-232669377));
var map__44648 = data;
var map__44648__$1 = (((((!((map__44648 == null))))?(((((map__44648.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__44648.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__44648):map__44648);
var problems = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__44648__$1,new cljs.core.Keyword("cljs.spec.alpha","problems","cljs.spec.alpha/problems",447400814));
var fn = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__44648__$1,new cljs.core.Keyword("cljs.spec.alpha","fn","cljs.spec.alpha/fn",408600443));
var caller = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__44648__$1,new cljs.core.Keyword("cljs.spec.test.alpha","caller","cljs.spec.test.alpha/caller",-398302390));
var map__44649 = new cljs.core.Keyword(null,"data","data",-232669377).cljs$core$IFn$_invoke$arity$1(cljs.core.first(via));
var map__44649__$1 = (((((!((map__44649 == null))))?(((((map__44649.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__44649.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__44649):map__44649);
var top_data = map__44649__$1;
var source = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__44649__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397));
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3((function (){var G__44654 = phase;
var G__44654__$1 = (((G__44654 instanceof cljs.core.Keyword))?G__44654.fqn:null);
switch (G__44654__$1) {
case "read-source":
var map__44655 = data;
var map__44655__$1 = (((((!((map__44655 == null))))?(((((map__44655.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__44655.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__44655):map__44655);
var line = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__44655__$1,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471));
var column = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__44655__$1,new cljs.core.Keyword("clojure.error","column","clojure.error/column",304721553));
var G__44657 = cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"data","data",-232669377).cljs$core$IFn$_invoke$arity$1(cljs.core.second(via)),top_data], 0));
var G__44657__$1 = (cljs.core.truth_(source)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__44657,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),source):G__44657);
var G__44657__$2 = (cljs.core.truth_((function (){var fexpr__44658 = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["NO_SOURCE_PATH",null,"NO_SOURCE_FILE",null], null), null);
return (fexpr__44658.cljs$core$IFn$_invoke$arity$1 ? fexpr__44658.cljs$core$IFn$_invoke$arity$1(source) : fexpr__44658.call(null,source));
})())?cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(G__44657__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397)):G__44657__$1);
if(cljs.core.truth_(message)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__44657__$2,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message);
} else {
return G__44657__$2;
}

break;
case "compile-syntax-check":
case "compilation":
case "macro-syntax-check":
case "macroexpansion":
var G__44659 = top_data;
var G__44659__$1 = (cljs.core.truth_(source)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__44659,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),source):G__44659);
var G__44659__$2 = (cljs.core.truth_((function (){var fexpr__44660 = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["NO_SOURCE_PATH",null,"NO_SOURCE_FILE",null], null), null);
return (fexpr__44660.cljs$core$IFn$_invoke$arity$1 ? fexpr__44660.cljs$core$IFn$_invoke$arity$1(source) : fexpr__44660.call(null,source));
})())?cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(G__44659__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397)):G__44659__$1);
var G__44659__$3 = (cljs.core.truth_(type)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__44659__$2,new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890),type):G__44659__$2);
var G__44659__$4 = (cljs.core.truth_(message)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__44659__$3,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message):G__44659__$3);
if(cljs.core.truth_(problems)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__44659__$4,new cljs.core.Keyword("clojure.error","spec","clojure.error/spec",2055032595),data);
} else {
return G__44659__$4;
}

break;
case "read-eval-result":
case "print-eval-result":
var vec__44661 = cljs.core.first(trace);
var source__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__44661,(0),null);
var method = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__44661,(1),null);
var file = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__44661,(2),null);
var line = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__44661,(3),null);
var G__44664 = top_data;
var G__44664__$1 = (cljs.core.truth_(line)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__44664,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471),line):G__44664);
var G__44664__$2 = (cljs.core.truth_(file)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__44664__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),file):G__44664__$1);
var G__44664__$3 = (cljs.core.truth_((function (){var and__4115__auto__ = source__$1;
if(cljs.core.truth_(and__4115__auto__)){
return method;
} else {
return and__4115__auto__;
}
})())?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__44664__$2,new cljs.core.Keyword("clojure.error","symbol","clojure.error/symbol",1544821994),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[source__$1,method],null))):G__44664__$2);
var G__44664__$4 = (cljs.core.truth_(type)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__44664__$3,new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890),type):G__44664__$3);
if(cljs.core.truth_(message)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__44664__$4,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message);
} else {
return G__44664__$4;
}

break;
case "execution":
var vec__44665 = cljs.core.first(trace);
var source__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__44665,(0),null);
var method = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__44665,(1),null);
var file = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__44665,(2),null);
var line = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__44665,(3),null);
var file__$1 = cljs.core.first(cljs.core.remove.cljs$core$IFn$_invoke$arity$2((function (p1__44645_SHARP_){
var or__4126__auto__ = (p1__44645_SHARP_ == null);
if(or__4126__auto__){
return or__4126__auto__;
} else {
var fexpr__44669 = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["NO_SOURCE_PATH",null,"NO_SOURCE_FILE",null], null), null);
return (fexpr__44669.cljs$core$IFn$_invoke$arity$1 ? fexpr__44669.cljs$core$IFn$_invoke$arity$1(p1__44645_SHARP_) : fexpr__44669.call(null,p1__44645_SHARP_));
}
}),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(caller),file], null)));
var err_line = (function (){var or__4126__auto__ = new cljs.core.Keyword(null,"line","line",212345235).cljs$core$IFn$_invoke$arity$1(caller);
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return line;
}
})();
var G__44670 = new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890),type], null);
var G__44670__$1 = (cljs.core.truth_(err_line)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__44670,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471),err_line):G__44670);
var G__44670__$2 = (cljs.core.truth_(message)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__44670__$1,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message):G__44670__$1);
var G__44670__$3 = (cljs.core.truth_((function (){var or__4126__auto__ = fn;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
var and__4115__auto__ = source__$1;
if(cljs.core.truth_(and__4115__auto__)){
return method;
} else {
return and__4115__auto__;
}
}
})())?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__44670__$2,new cljs.core.Keyword("clojure.error","symbol","clojure.error/symbol",1544821994),(function (){var or__4126__auto__ = fn;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return (new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[source__$1,method],null));
}
})()):G__44670__$2);
var G__44670__$4 = (cljs.core.truth_(file__$1)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__44670__$3,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),file__$1):G__44670__$3);
if(cljs.core.truth_(problems)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__44670__$4,new cljs.core.Keyword("clojure.error","spec","clojure.error/spec",2055032595),data);
} else {
return G__44670__$4;
}

break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__44654__$1)].join('')));

}
})(),new cljs.core.Keyword("clojure.error","phase","clojure.error/phase",275140358),phase);
});
/**
 * Returns a string from exception data, as produced by ex-triage.
 *   The first line summarizes the exception phase and location.
 *   The subsequent lines describe the cause.
 */
cljs.repl.ex_str = (function cljs$repl$ex_str(p__44673){
var map__44674 = p__44673;
var map__44674__$1 = (((((!((map__44674 == null))))?(((((map__44674.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__44674.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__44674):map__44674);
var triage_data = map__44674__$1;
var phase = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__44674__$1,new cljs.core.Keyword("clojure.error","phase","clojure.error/phase",275140358));
var source = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__44674__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397));
var line = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__44674__$1,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471));
var column = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__44674__$1,new cljs.core.Keyword("clojure.error","column","clojure.error/column",304721553));
var symbol = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__44674__$1,new cljs.core.Keyword("clojure.error","symbol","clojure.error/symbol",1544821994));
var class$ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__44674__$1,new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890));
var cause = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__44674__$1,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742));
var spec = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__44674__$1,new cljs.core.Keyword("clojure.error","spec","clojure.error/spec",2055032595));
var loc = [cljs.core.str.cljs$core$IFn$_invoke$arity$1((function (){var or__4126__auto__ = source;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return "<cljs repl>";
}
})()),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1((function (){var or__4126__auto__ = line;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return (1);
}
})()),(cljs.core.truth_(column)?[":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column)].join(''):"")].join('');
var class_name = cljs.core.name((function (){var or__4126__auto__ = class$;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return "";
}
})());
var simple_class = class_name;
var cause_type = ((cljs.core.contains_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["RuntimeException",null,"Exception",null], null), null),simple_class))?"":[" (",simple_class,")"].join(''));
var format = goog.string.format;
var G__44676 = phase;
var G__44676__$1 = (((G__44676 instanceof cljs.core.Keyword))?G__44676.fqn:null);
switch (G__44676__$1) {
case "read-source":
return (format.cljs$core$IFn$_invoke$arity$3 ? format.cljs$core$IFn$_invoke$arity$3("Syntax error reading source at (%s).\n%s\n",loc,cause) : format.call(null,"Syntax error reading source at (%s).\n%s\n",loc,cause));

break;
case "macro-syntax-check":
var G__44677 = "Syntax error macroexpanding %sat (%s).\n%s";
var G__44678 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__44679 = loc;
var G__44680 = (cljs.core.truth_(spec)?(function (){var sb__4667__auto__ = (new goog.string.StringBuffer());
var _STAR_print_newline_STAR__orig_val__44681_44786 = cljs.core._STAR_print_newline_STAR_;
var _STAR_print_fn_STAR__orig_val__44682_44787 = cljs.core._STAR_print_fn_STAR_;
var _STAR_print_newline_STAR__temp_val__44683_44788 = true;
var _STAR_print_fn_STAR__temp_val__44684_44789 = (function (x__4668__auto__){
return sb__4667__auto__.append(x__4668__auto__);
});
(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__temp_val__44683_44788);

(cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__temp_val__44684_44789);

try{cljs.spec.alpha.explain_out(cljs.core.update.cljs$core$IFn$_invoke$arity$3(spec,new cljs.core.Keyword("cljs.spec.alpha","problems","cljs.spec.alpha/problems",447400814),(function (probs){
return cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p1__44671_SHARP_){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(p1__44671_SHARP_,new cljs.core.Keyword(null,"in","in",-1531184865));
}),probs);
}))
);
}finally {(cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__orig_val__44682_44787);

(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__orig_val__44681_44786);
}
return cljs.core.str.cljs$core$IFn$_invoke$arity$1(sb__4667__auto__);
})():(format.cljs$core$IFn$_invoke$arity$2 ? format.cljs$core$IFn$_invoke$arity$2("%s\n",cause) : format.call(null,"%s\n",cause)));
return (format.cljs$core$IFn$_invoke$arity$4 ? format.cljs$core$IFn$_invoke$arity$4(G__44677,G__44678,G__44679,G__44680) : format.call(null,G__44677,G__44678,G__44679,G__44680));

break;
case "macroexpansion":
var G__44685 = "Unexpected error%s macroexpanding %sat (%s).\n%s\n";
var G__44686 = cause_type;
var G__44687 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__44688 = loc;
var G__44689 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__44685,G__44686,G__44687,G__44688,G__44689) : format.call(null,G__44685,G__44686,G__44687,G__44688,G__44689));

break;
case "compile-syntax-check":
var G__44690 = "Syntax error%s compiling %sat (%s).\n%s\n";
var G__44691 = cause_type;
var G__44692 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__44693 = loc;
var G__44694 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__44690,G__44691,G__44692,G__44693,G__44694) : format.call(null,G__44690,G__44691,G__44692,G__44693,G__44694));

break;
case "compilation":
var G__44695 = "Unexpected error%s compiling %sat (%s).\n%s\n";
var G__44696 = cause_type;
var G__44697 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__44698 = loc;
var G__44699 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__44695,G__44696,G__44697,G__44698,G__44699) : format.call(null,G__44695,G__44696,G__44697,G__44698,G__44699));

break;
case "read-eval-result":
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5("Error reading eval result%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause) : format.call(null,"Error reading eval result%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause));

break;
case "print-eval-result":
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5("Error printing return value%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause) : format.call(null,"Error printing return value%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause));

break;
case "execution":
if(cljs.core.truth_(spec)){
var G__44700 = "Execution error - invalid arguments to %s at (%s).\n%s";
var G__44701 = symbol;
var G__44702 = loc;
var G__44703 = (function (){var sb__4667__auto__ = (new goog.string.StringBuffer());
var _STAR_print_newline_STAR__orig_val__44704_44790 = cljs.core._STAR_print_newline_STAR_;
var _STAR_print_fn_STAR__orig_val__44705_44791 = cljs.core._STAR_print_fn_STAR_;
var _STAR_print_newline_STAR__temp_val__44706_44792 = true;
var _STAR_print_fn_STAR__temp_val__44707_44793 = (function (x__4668__auto__){
return sb__4667__auto__.append(x__4668__auto__);
});
(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__temp_val__44706_44792);

(cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__temp_val__44707_44793);

try{cljs.spec.alpha.explain_out(cljs.core.update.cljs$core$IFn$_invoke$arity$3(spec,new cljs.core.Keyword("cljs.spec.alpha","problems","cljs.spec.alpha/problems",447400814),(function (probs){
return cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p1__44672_SHARP_){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(p1__44672_SHARP_,new cljs.core.Keyword(null,"in","in",-1531184865));
}),probs);
}))
);
}finally {(cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__orig_val__44705_44791);

(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__orig_val__44704_44790);
}
return cljs.core.str.cljs$core$IFn$_invoke$arity$1(sb__4667__auto__);
})();
return (format.cljs$core$IFn$_invoke$arity$4 ? format.cljs$core$IFn$_invoke$arity$4(G__44700,G__44701,G__44702,G__44703) : format.call(null,G__44700,G__44701,G__44702,G__44703));
} else {
var G__44708 = "Execution error%s at %s(%s).\n%s\n";
var G__44709 = cause_type;
var G__44710 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__44711 = loc;
var G__44712 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__44708,G__44709,G__44710,G__44711,G__44712) : format.call(null,G__44708,G__44709,G__44710,G__44711,G__44712));
}

break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__44676__$1)].join('')));

}
});
cljs.repl.error__GT_str = (function cljs$repl$error__GT_str(error){
return cljs.repl.ex_str(cljs.repl.ex_triage(cljs.repl.Error__GT_map(error)));
});

//# sourceMappingURL=cljs.repl.js.map
