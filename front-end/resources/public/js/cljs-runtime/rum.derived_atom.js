goog.provide('rum.derived_atom');
rum.derived_atom.derived_atom = (function rum$derived_atom$derived_atom(var_args){
var G__36011 = arguments.length;
switch (G__36011) {
case 3:
return rum.derived_atom.derived_atom.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return rum.derived_atom.derived_atom.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(rum.derived_atom.derived_atom.cljs$core$IFn$_invoke$arity$3 = (function (refs,key,f){
return rum.derived_atom.derived_atom.cljs$core$IFn$_invoke$arity$4(refs,key,f,cljs.core.PersistentArrayMap.EMPTY);
}));

(rum.derived_atom.derived_atom.cljs$core$IFn$_invoke$arity$4 = (function (refs,key,f,opts){
var map__36021 = opts;
var map__36021__$1 = (((((!((map__36021 == null))))?(((((map__36021.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36021.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__36021):map__36021);
var ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36021__$1,new cljs.core.Keyword(null,"ref","ref",1289896967));
var check_equals_QMARK_ = cljs.core.get.cljs$core$IFn$_invoke$arity$3(map__36021__$1,new cljs.core.Keyword(null,"check-equals?","check-equals?",-2005755315),true);
var recalc = (function (){var G__36029 = cljs.core.count(refs);
switch (G__36029) {
case (1):
var vec__36032 = refs;
var a = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__36032,(0),null);
return (function (){
var G__36039 = cljs.core.deref(a);
return (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(G__36039) : f.call(null,G__36039));
});

break;
case (2):
var vec__36040 = refs;
var a = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__36040,(0),null);
var b = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__36040,(1),null);
return (function (){
var G__36044 = cljs.core.deref(a);
var G__36045 = cljs.core.deref(b);
return (f.cljs$core$IFn$_invoke$arity$2 ? f.cljs$core$IFn$_invoke$arity$2(G__36044,G__36045) : f.call(null,G__36044,G__36045));
});

break;
case (3):
var vec__36050 = refs;
var a = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__36050,(0),null);
var b = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__36050,(1),null);
var c = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__36050,(2),null);
return (function (){
var G__36058 = cljs.core.deref(a);
var G__36059 = cljs.core.deref(b);
var G__36060 = cljs.core.deref(c);
return (f.cljs$core$IFn$_invoke$arity$3 ? f.cljs$core$IFn$_invoke$arity$3(G__36058,G__36059,G__36060) : f.call(null,G__36058,G__36059,G__36060));
});

break;
default:
return (function (){
return cljs.core.apply.cljs$core$IFn$_invoke$arity$2(f,cljs.core.map.cljs$core$IFn$_invoke$arity$2(cljs.core.deref,refs));
});

}
})();
var sink = (cljs.core.truth_(ref)?(function (){var G__36072 = ref;
cljs.core.reset_BANG_(G__36072,(recalc.cljs$core$IFn$_invoke$arity$0 ? recalc.cljs$core$IFn$_invoke$arity$0() : recalc.call(null)));

return G__36072;
})():cljs.core.atom.cljs$core$IFn$_invoke$arity$1((recalc.cljs$core$IFn$_invoke$arity$0 ? recalc.cljs$core$IFn$_invoke$arity$0() : recalc.call(null))));
var watch = (cljs.core.truth_(check_equals_QMARK_)?(function (_,___$1,___$2,___$3){
var new_val = (recalc.cljs$core$IFn$_invoke$arity$0 ? recalc.cljs$core$IFn$_invoke$arity$0() : recalc.call(null));
if(cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(sink),new_val)){
return cljs.core.reset_BANG_(sink,new_val);
} else {
return null;
}
}):(function (_,___$1,___$2,___$3){
return cljs.core.reset_BANG_(sink,(recalc.cljs$core$IFn$_invoke$arity$0 ? recalc.cljs$core$IFn$_invoke$arity$0() : recalc.call(null)));
}));
var seq__36087_36214 = cljs.core.seq(refs);
var chunk__36088_36215 = null;
var count__36089_36216 = (0);
var i__36090_36217 = (0);
while(true){
if((i__36090_36217 < count__36089_36216)){
var ref_36222__$1 = chunk__36088_36215.cljs$core$IIndexed$_nth$arity$2(null,i__36090_36217);
cljs.core.add_watch(ref_36222__$1,key,watch);


var G__36225 = seq__36087_36214;
var G__36226 = chunk__36088_36215;
var G__36227 = count__36089_36216;
var G__36228 = (i__36090_36217 + (1));
seq__36087_36214 = G__36225;
chunk__36088_36215 = G__36226;
count__36089_36216 = G__36227;
i__36090_36217 = G__36228;
continue;
} else {
var temp__5735__auto___36229 = cljs.core.seq(seq__36087_36214);
if(temp__5735__auto___36229){
var seq__36087_36230__$1 = temp__5735__auto___36229;
if(cljs.core.chunked_seq_QMARK_(seq__36087_36230__$1)){
var c__4556__auto___36231 = cljs.core.chunk_first(seq__36087_36230__$1);
var G__36232 = cljs.core.chunk_rest(seq__36087_36230__$1);
var G__36233 = c__4556__auto___36231;
var G__36234 = cljs.core.count(c__4556__auto___36231);
var G__36235 = (0);
seq__36087_36214 = G__36232;
chunk__36088_36215 = G__36233;
count__36089_36216 = G__36234;
i__36090_36217 = G__36235;
continue;
} else {
var ref_36236__$1 = cljs.core.first(seq__36087_36230__$1);
cljs.core.add_watch(ref_36236__$1,key,watch);


var G__36239 = cljs.core.next(seq__36087_36230__$1);
var G__36240 = null;
var G__36241 = (0);
var G__36242 = (0);
seq__36087_36214 = G__36239;
chunk__36088_36215 = G__36240;
count__36089_36216 = G__36241;
i__36090_36217 = G__36242;
continue;
}
} else {
}
}
break;
}

return sink;
}));

(rum.derived_atom.derived_atom.cljs$lang$maxFixedArity = 4);


//# sourceMappingURL=rum.derived_atom.js.map
