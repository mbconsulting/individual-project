goog.provide('cljs.core.async');
cljs.core.async.fn_handler = (function cljs$core$async$fn_handler(var_args){
var G__30811 = arguments.length;
switch (G__30811) {
case 1:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1 = (function (f){
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2(f,true);
}));

(cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2 = (function (f,blockable){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async30812 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async30812 = (function (f,blockable,meta30813){
this.f = f;
this.blockable = blockable;
this.meta30813 = meta30813;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async30812.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_30814,meta30813__$1){
var self__ = this;
var _30814__$1 = this;
return (new cljs.core.async.t_cljs$core$async30812(self__.f,self__.blockable,meta30813__$1));
}));

(cljs.core.async.t_cljs$core$async30812.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_30814){
var self__ = this;
var _30814__$1 = this;
return self__.meta30813;
}));

(cljs.core.async.t_cljs$core$async30812.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async30812.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
}));

(cljs.core.async.t_cljs$core$async30812.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.blockable;
}));

(cljs.core.async.t_cljs$core$async30812.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.f;
}));

(cljs.core.async.t_cljs$core$async30812.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"blockable","blockable",-28395259,null),new cljs.core.Symbol(null,"meta30813","meta30813",-525290835,null)], null);
}));

(cljs.core.async.t_cljs$core$async30812.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async30812.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async30812");

(cljs.core.async.t_cljs$core$async30812.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async30812");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async30812.
 */
cljs.core.async.__GT_t_cljs$core$async30812 = (function cljs$core$async$__GT_t_cljs$core$async30812(f__$1,blockable__$1,meta30813){
return (new cljs.core.async.t_cljs$core$async30812(f__$1,blockable__$1,meta30813));
});

}

return (new cljs.core.async.t_cljs$core$async30812(f,blockable,cljs.core.PersistentArrayMap.EMPTY));
}));

(cljs.core.async.fn_handler.cljs$lang$maxFixedArity = 2);

/**
 * Returns a fixed buffer of size n. When full, puts will block/park.
 */
cljs.core.async.buffer = (function cljs$core$async$buffer(n){
return cljs.core.async.impl.buffers.fixed_buffer(n);
});
/**
 * Returns a buffer of size n. When full, puts will complete but
 *   val will be dropped (no transfer).
 */
cljs.core.async.dropping_buffer = (function cljs$core$async$dropping_buffer(n){
return cljs.core.async.impl.buffers.dropping_buffer(n);
});
/**
 * Returns a buffer of size n. When full, puts will complete, and be
 *   buffered, but oldest elements in buffer will be dropped (not
 *   transferred).
 */
cljs.core.async.sliding_buffer = (function cljs$core$async$sliding_buffer(n){
return cljs.core.async.impl.buffers.sliding_buffer(n);
});
/**
 * Returns true if a channel created with buff will never block. That is to say,
 * puts into this buffer will never cause the buffer to be full. 
 */
cljs.core.async.unblocking_buffer_QMARK_ = (function cljs$core$async$unblocking_buffer_QMARK_(buff){
if((!((buff == null)))){
if(((false) || ((cljs.core.PROTOCOL_SENTINEL === buff.cljs$core$async$impl$protocols$UnblockingBuffer$)))){
return true;
} else {
if((!buff.cljs$lang$protocol_mask$partition$)){
return cljs.core.native_satisfies_QMARK_(cljs.core.async.impl.protocols.UnblockingBuffer,buff);
} else {
return false;
}
}
} else {
return cljs.core.native_satisfies_QMARK_(cljs.core.async.impl.protocols.UnblockingBuffer,buff);
}
});
/**
 * Creates a channel with an optional buffer, an optional transducer (like (map f),
 *   (filter p) etc or a composition thereof), and an optional exception handler.
 *   If buf-or-n is a number, will create and use a fixed buffer of that size. If a
 *   transducer is supplied a buffer must be specified. ex-handler must be a
 *   fn of one argument - if an exception occurs during transformation it will be called
 *   with the thrown value as an argument, and any non-nil return value will be placed
 *   in the channel.
 */
cljs.core.async.chan = (function cljs$core$async$chan(var_args){
var G__30845 = arguments.length;
switch (G__30845) {
case 0:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(null);
}));

(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1 = (function (buf_or_n){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3(buf_or_n,null,null);
}));

(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2 = (function (buf_or_n,xform){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3(buf_or_n,xform,null);
}));

(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3 = (function (buf_or_n,xform,ex_handler){
var buf_or_n__$1 = ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(buf_or_n,(0)))?null:buf_or_n);
if(cljs.core.truth_(xform)){
if(cljs.core.truth_(buf_or_n__$1)){
} else {
throw (new Error(["Assert failed: ","buffer must be supplied when transducer is","\n","buf-or-n"].join('')));
}
} else {
}

return cljs.core.async.impl.channels.chan.cljs$core$IFn$_invoke$arity$3(((typeof buf_or_n__$1 === 'number')?cljs.core.async.buffer(buf_or_n__$1):buf_or_n__$1),xform,ex_handler);
}));

(cljs.core.async.chan.cljs$lang$maxFixedArity = 3);

/**
 * Creates a promise channel with an optional transducer, and an optional
 *   exception-handler. A promise channel can take exactly one value that consumers
 *   will receive. Once full, puts complete but val is dropped (no transfer).
 *   Consumers will block until either a value is placed in the channel or the
 *   channel is closed. See chan for the semantics of xform and ex-handler.
 */
cljs.core.async.promise_chan = (function cljs$core$async$promise_chan(var_args){
var G__30862 = arguments.length;
switch (G__30862) {
case 0:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1(null);
}));

(cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1 = (function (xform){
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2(xform,null);
}));

(cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2 = (function (xform,ex_handler){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3(cljs.core.async.impl.buffers.promise_buffer(),xform,ex_handler);
}));

(cljs.core.async.promise_chan.cljs$lang$maxFixedArity = 2);

/**
 * Returns a channel that will close after msecs
 */
cljs.core.async.timeout = (function cljs$core$async$timeout(msecs){
return cljs.core.async.impl.timers.timeout(msecs);
});
/**
 * takes a val from port. Must be called inside a (go ...) block. Will
 *   return nil if closed. Will park if nothing is available.
 *   Returns true unless port is already closed
 */
cljs.core.async._LT__BANG_ = (function cljs$core$async$_LT__BANG_(port){
throw (new Error("<! used not in (go ...) block"));
});
/**
 * Asynchronously takes a val from port, passing to fn1. Will pass nil
 * if closed. If on-caller? (default true) is true, and value is
 * immediately available, will call fn1 on calling thread.
 * Returns nil.
 */
cljs.core.async.take_BANG_ = (function cljs$core$async$take_BANG_(var_args){
var G__30879 = arguments.length;
switch (G__30879) {
case 2:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,fn1){
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3(port,fn1,true);
}));

(cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,fn1,on_caller_QMARK_){
var ret = cljs.core.async.impl.protocols.take_BANG_(port,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1(fn1));
if(cljs.core.truth_(ret)){
var val_34150 = cljs.core.deref(ret);
if(cljs.core.truth_(on_caller_QMARK_)){
(fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(val_34150) : fn1.call(null,val_34150));
} else {
cljs.core.async.impl.dispatch.run((function (){
return (fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(val_34150) : fn1.call(null,val_34150));
}));
}
} else {
}

return null;
}));

(cljs.core.async.take_BANG_.cljs$lang$maxFixedArity = 3);

cljs.core.async.nop = (function cljs$core$async$nop(_){
return null;
});
cljs.core.async.fhnop = cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1(cljs.core.async.nop);
/**
 * puts a val into port. nil values are not allowed. Must be called
 *   inside a (go ...) block. Will park if no buffer space is available.
 *   Returns true unless port is already closed.
 */
cljs.core.async._GT__BANG_ = (function cljs$core$async$_GT__BANG_(port,val){
throw (new Error(">! used not in (go ...) block"));
});
/**
 * Asynchronously puts a val into port, calling fn1 (if supplied) when
 * complete. nil values are not allowed. Will throw if closed. If
 * on-caller? (default true) is true, and the put is immediately
 * accepted, will call fn1 on calling thread.  Returns nil.
 */
cljs.core.async.put_BANG_ = (function cljs$core$async$put_BANG_(var_args){
var G__30903 = arguments.length;
switch (G__30903) {
case 2:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,val){
var temp__5733__auto__ = cljs.core.async.impl.protocols.put_BANG_(port,val,cljs.core.async.fhnop);
if(cljs.core.truth_(temp__5733__auto__)){
var ret = temp__5733__auto__;
return cljs.core.deref(ret);
} else {
return true;
}
}));

(cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,val,fn1){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4(port,val,fn1,true);
}));

(cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4 = (function (port,val,fn1,on_caller_QMARK_){
var temp__5733__auto__ = cljs.core.async.impl.protocols.put_BANG_(port,val,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1(fn1));
if(cljs.core.truth_(temp__5733__auto__)){
var retb = temp__5733__auto__;
var ret = cljs.core.deref(retb);
if(cljs.core.truth_(on_caller_QMARK_)){
(fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(ret) : fn1.call(null,ret));
} else {
cljs.core.async.impl.dispatch.run((function (){
return (fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(ret) : fn1.call(null,ret));
}));
}

return ret;
} else {
return true;
}
}));

(cljs.core.async.put_BANG_.cljs$lang$maxFixedArity = 4);

cljs.core.async.close_BANG_ = (function cljs$core$async$close_BANG_(port){
return cljs.core.async.impl.protocols.close_BANG_(port);
});
cljs.core.async.random_array = (function cljs$core$async$random_array(n){
var a = (new Array(n));
var n__4613__auto___34172 = n;
var x_34173 = (0);
while(true){
if((x_34173 < n__4613__auto___34172)){
(a[x_34173] = x_34173);

var G__34174 = (x_34173 + (1));
x_34173 = G__34174;
continue;
} else {
}
break;
}

goog.array.shuffle(a);

return a;
});
cljs.core.async.alt_flag = (function cljs$core$async$alt_flag(){
var flag = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(true);
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async30925 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async30925 = (function (flag,meta30926){
this.flag = flag;
this.meta30926 = meta30926;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async30925.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_30927,meta30926__$1){
var self__ = this;
var _30927__$1 = this;
return (new cljs.core.async.t_cljs$core$async30925(self__.flag,meta30926__$1));
}));

(cljs.core.async.t_cljs$core$async30925.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_30927){
var self__ = this;
var _30927__$1 = this;
return self__.meta30926;
}));

(cljs.core.async.t_cljs$core$async30925.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async30925.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.deref(self__.flag);
}));

(cljs.core.async.t_cljs$core$async30925.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
}));

(cljs.core.async.t_cljs$core$async30925.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_(self__.flag,null);

return true;
}));

(cljs.core.async.t_cljs$core$async30925.getBasis = (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"meta30926","meta30926",1893693204,null)], null);
}));

(cljs.core.async.t_cljs$core$async30925.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async30925.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async30925");

(cljs.core.async.t_cljs$core$async30925.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async30925");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async30925.
 */
cljs.core.async.__GT_t_cljs$core$async30925 = (function cljs$core$async$alt_flag_$___GT_t_cljs$core$async30925(flag__$1,meta30926){
return (new cljs.core.async.t_cljs$core$async30925(flag__$1,meta30926));
});

}

return (new cljs.core.async.t_cljs$core$async30925(flag,cljs.core.PersistentArrayMap.EMPTY));
});
cljs.core.async.alt_handler = (function cljs$core$async$alt_handler(flag,cb){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async30938 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async30938 = (function (flag,cb,meta30939){
this.flag = flag;
this.cb = cb;
this.meta30939 = meta30939;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async30938.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_30940,meta30939__$1){
var self__ = this;
var _30940__$1 = this;
return (new cljs.core.async.t_cljs$core$async30938(self__.flag,self__.cb,meta30939__$1));
}));

(cljs.core.async.t_cljs$core$async30938.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_30940){
var self__ = this;
var _30940__$1 = this;
return self__.meta30939;
}));

(cljs.core.async.t_cljs$core$async30938.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async30938.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.active_QMARK_(self__.flag);
}));

(cljs.core.async.t_cljs$core$async30938.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
}));

(cljs.core.async.t_cljs$core$async30938.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.async.impl.protocols.commit(self__.flag);

return self__.cb;
}));

(cljs.core.async.t_cljs$core$async30938.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"cb","cb",-2064487928,null),new cljs.core.Symbol(null,"meta30939","meta30939",17884335,null)], null);
}));

(cljs.core.async.t_cljs$core$async30938.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async30938.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async30938");

(cljs.core.async.t_cljs$core$async30938.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async30938");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async30938.
 */
cljs.core.async.__GT_t_cljs$core$async30938 = (function cljs$core$async$alt_handler_$___GT_t_cljs$core$async30938(flag__$1,cb__$1,meta30939){
return (new cljs.core.async.t_cljs$core$async30938(flag__$1,cb__$1,meta30939));
});

}

return (new cljs.core.async.t_cljs$core$async30938(flag,cb,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * returns derefable [val port] if immediate, nil if enqueued
 */
cljs.core.async.do_alts = (function cljs$core$async$do_alts(fret,ports,opts){
if((cljs.core.count(ports) > (0))){
} else {
throw (new Error(["Assert failed: ","alts must have at least one channel operation","\n","(pos? (count ports))"].join('')));
}

var flag = cljs.core.async.alt_flag();
var n = cljs.core.count(ports);
var idxs = cljs.core.async.random_array(n);
var priority = new cljs.core.Keyword(null,"priority","priority",1431093715).cljs$core$IFn$_invoke$arity$1(opts);
var ret = (function (){var i = (0);
while(true){
if((i < n)){
var idx = (cljs.core.truth_(priority)?i:(idxs[i]));
var port = cljs.core.nth.cljs$core$IFn$_invoke$arity$2(ports,idx);
var wport = ((cljs.core.vector_QMARK_(port))?(port.cljs$core$IFn$_invoke$arity$1 ? port.cljs$core$IFn$_invoke$arity$1((0)) : port.call(null,(0))):null);
var vbox = (cljs.core.truth_(wport)?(function (){var val = (port.cljs$core$IFn$_invoke$arity$1 ? port.cljs$core$IFn$_invoke$arity$1((1)) : port.call(null,(1)));
return cljs.core.async.impl.protocols.put_BANG_(wport,val,cljs.core.async.alt_handler(flag,((function (i,val,idx,port,wport,flag,n,idxs,priority){
return (function (p1__30955_SHARP_){
var G__30960 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__30955_SHARP_,wport], null);
return (fret.cljs$core$IFn$_invoke$arity$1 ? fret.cljs$core$IFn$_invoke$arity$1(G__30960) : fret.call(null,G__30960));
});})(i,val,idx,port,wport,flag,n,idxs,priority))
));
})():cljs.core.async.impl.protocols.take_BANG_(port,cljs.core.async.alt_handler(flag,((function (i,idx,port,wport,flag,n,idxs,priority){
return (function (p1__30956_SHARP_){
var G__30961 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__30956_SHARP_,port], null);
return (fret.cljs$core$IFn$_invoke$arity$1 ? fret.cljs$core$IFn$_invoke$arity$1(G__30961) : fret.call(null,G__30961));
});})(i,idx,port,wport,flag,n,idxs,priority))
)));
if(cljs.core.truth_(vbox)){
return cljs.core.async.impl.channels.box(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.deref(vbox),(function (){var or__4126__auto__ = wport;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return port;
}
})()], null));
} else {
var G__34205 = (i + (1));
i = G__34205;
continue;
}
} else {
return null;
}
break;
}
})();
var or__4126__auto__ = ret;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
if(cljs.core.contains_QMARK_(opts,new cljs.core.Keyword(null,"default","default",-1987822328))){
var temp__5735__auto__ = (function (){var and__4115__auto__ = flag.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1(null);
if(cljs.core.truth_(and__4115__auto__)){
return flag.cljs$core$async$impl$protocols$Handler$commit$arity$1(null);
} else {
return and__4115__auto__;
}
})();
if(cljs.core.truth_(temp__5735__auto__)){
var got = temp__5735__auto__;
return cljs.core.async.impl.channels.box(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"default","default",-1987822328).cljs$core$IFn$_invoke$arity$1(opts),new cljs.core.Keyword(null,"default","default",-1987822328)], null));
} else {
return null;
}
} else {
return null;
}
}
});
/**
 * Completes at most one of several channel operations. Must be called
 * inside a (go ...) block. ports is a vector of channel endpoints,
 * which can be either a channel to take from or a vector of
 *   [channel-to-put-to val-to-put], in any combination. Takes will be
 *   made as if by <!, and puts will be made as if by >!. Unless
 *   the :priority option is true, if more than one port operation is
 *   ready a non-deterministic choice will be made. If no operation is
 *   ready and a :default value is supplied, [default-val :default] will
 *   be returned, otherwise alts! will park until the first operation to
 *   become ready completes. Returns [val port] of the completed
 *   operation, where val is the value taken for takes, and a
 *   boolean (true unless already closed, as per put!) for puts.
 * 
 *   opts are passed as :key val ... Supported options:
 * 
 *   :default val - the value to use if none of the operations are immediately ready
 *   :priority true - (default nil) when true, the operations will be tried in order.
 * 
 *   Note: there is no guarantee that the port exps or val exprs will be
 *   used, nor in what order should they be, so they should not be
 *   depended upon for side effects.
 */
cljs.core.async.alts_BANG_ = (function cljs$core$async$alts_BANG_(var_args){
var args__4742__auto__ = [];
var len__4736__auto___34208 = arguments.length;
var i__4737__auto___34209 = (0);
while(true){
if((i__4737__auto___34209 < len__4736__auto___34208)){
args__4742__auto__.push((arguments[i__4737__auto___34209]));

var G__34210 = (i__4737__auto___34209 + (1));
i__4737__auto___34209 = G__34210;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((1) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((1)),(0),null)):null);
return cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4743__auto__);
});

(cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (ports,p__30978){
var map__30980 = p__30978;
var map__30980__$1 = (((((!((map__30980 == null))))?(((((map__30980.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__30980.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__30980):map__30980);
var opts = map__30980__$1;
throw (new Error("alts! used not in (go ...) block"));
}));

(cljs.core.async.alts_BANG_.cljs$lang$maxFixedArity = (1));

/** @this {Function} */
(cljs.core.async.alts_BANG_.cljs$lang$applyTo = (function (seq30965){
var G__30966 = cljs.core.first(seq30965);
var seq30965__$1 = cljs.core.next(seq30965);
var self__4723__auto__ = this;
return self__4723__auto__.cljs$core$IFn$_invoke$arity$variadic(G__30966,seq30965__$1);
}));

/**
 * Puts a val into port if it's possible to do so immediately.
 *   nil values are not allowed. Never blocks. Returns true if offer succeeds.
 */
cljs.core.async.offer_BANG_ = (function cljs$core$async$offer_BANG_(port,val){
var ret = cljs.core.async.impl.protocols.put_BANG_(port,val,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2(cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref(ret);
} else {
return null;
}
});
/**
 * Takes a val from port if it's possible to do so immediately.
 *   Never blocks. Returns value if successful, nil otherwise.
 */
cljs.core.async.poll_BANG_ = (function cljs$core$async$poll_BANG_(port){
var ret = cljs.core.async.impl.protocols.take_BANG_(port,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2(cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref(ret);
} else {
return null;
}
});
/**
 * Takes elements from the from channel and supplies them to the to
 * channel. By default, the to channel will be closed when the from
 * channel closes, but can be determined by the close?  parameter. Will
 * stop consuming the from channel if the to channel closes
 */
cljs.core.async.pipe = (function cljs$core$async$pipe(var_args){
var G__31005 = arguments.length;
switch (G__31005) {
case 2:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2 = (function (from,to){
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3(from,to,true);
}));

(cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3 = (function (from,to,close_QMARK_){
var c__30703__auto___34229 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30704__auto__ = (function (){var switch__30539__auto__ = (function (state_31064){
var state_val_31068 = (state_31064[(1)]);
if((state_val_31068 === (7))){
var inst_31055 = (state_31064[(2)]);
var state_31064__$1 = state_31064;
var statearr_31084_34232 = state_31064__$1;
(statearr_31084_34232[(2)] = inst_31055);

(statearr_31084_34232[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31068 === (1))){
var state_31064__$1 = state_31064;
var statearr_31086_34235 = state_31064__$1;
(statearr_31086_34235[(2)] = null);

(statearr_31086_34235[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31068 === (4))){
var inst_31036 = (state_31064[(7)]);
var inst_31036__$1 = (state_31064[(2)]);
var inst_31037 = (inst_31036__$1 == null);
var state_31064__$1 = (function (){var statearr_31095 = state_31064;
(statearr_31095[(7)] = inst_31036__$1);

return statearr_31095;
})();
if(cljs.core.truth_(inst_31037)){
var statearr_31096_34239 = state_31064__$1;
(statearr_31096_34239[(1)] = (5));

} else {
var statearr_31098_34240 = state_31064__$1;
(statearr_31098_34240[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31068 === (13))){
var state_31064__$1 = state_31064;
var statearr_31102_34241 = state_31064__$1;
(statearr_31102_34241[(2)] = null);

(statearr_31102_34241[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31068 === (6))){
var inst_31036 = (state_31064[(7)]);
var state_31064__$1 = state_31064;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_31064__$1,(11),to,inst_31036);
} else {
if((state_val_31068 === (3))){
var inst_31060 = (state_31064[(2)]);
var state_31064__$1 = state_31064;
return cljs.core.async.impl.ioc_helpers.return_chan(state_31064__$1,inst_31060);
} else {
if((state_val_31068 === (12))){
var state_31064__$1 = state_31064;
var statearr_31112_34245 = state_31064__$1;
(statearr_31112_34245[(2)] = null);

(statearr_31112_34245[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31068 === (2))){
var state_31064__$1 = state_31064;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_31064__$1,(4),from);
} else {
if((state_val_31068 === (11))){
var inst_31048 = (state_31064[(2)]);
var state_31064__$1 = state_31064;
if(cljs.core.truth_(inst_31048)){
var statearr_31115_34248 = state_31064__$1;
(statearr_31115_34248[(1)] = (12));

} else {
var statearr_31117_34249 = state_31064__$1;
(statearr_31117_34249[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31068 === (9))){
var state_31064__$1 = state_31064;
var statearr_31118_34256 = state_31064__$1;
(statearr_31118_34256[(2)] = null);

(statearr_31118_34256[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31068 === (5))){
var state_31064__$1 = state_31064;
if(cljs.core.truth_(close_QMARK_)){
var statearr_31124_34258 = state_31064__$1;
(statearr_31124_34258[(1)] = (8));

} else {
var statearr_31127_34259 = state_31064__$1;
(statearr_31127_34259[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31068 === (14))){
var inst_31053 = (state_31064[(2)]);
var state_31064__$1 = state_31064;
var statearr_31129_34260 = state_31064__$1;
(statearr_31129_34260[(2)] = inst_31053);

(statearr_31129_34260[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31068 === (10))){
var inst_31045 = (state_31064[(2)]);
var state_31064__$1 = state_31064;
var statearr_31132_34262 = state_31064__$1;
(statearr_31132_34262[(2)] = inst_31045);

(statearr_31132_34262[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31068 === (8))){
var inst_31041 = cljs.core.async.close_BANG_(to);
var state_31064__$1 = state_31064;
var statearr_31137_34263 = state_31064__$1;
(statearr_31137_34263[(2)] = inst_31041);

(statearr_31137_34263[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__30540__auto__ = null;
var cljs$core$async$state_machine__30540__auto____0 = (function (){
var statearr_31143 = [null,null,null,null,null,null,null,null];
(statearr_31143[(0)] = cljs$core$async$state_machine__30540__auto__);

(statearr_31143[(1)] = (1));

return statearr_31143;
});
var cljs$core$async$state_machine__30540__auto____1 = (function (state_31064){
while(true){
var ret_value__30541__auto__ = (function (){try{while(true){
var result__30542__auto__ = switch__30539__auto__(state_31064);
if(cljs.core.keyword_identical_QMARK_(result__30542__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30542__auto__;
}
break;
}
}catch (e31144){var ex__30543__auto__ = e31144;
var statearr_31145_34265 = state_31064;
(statearr_31145_34265[(2)] = ex__30543__auto__);


if(cljs.core.seq((state_31064[(4)]))){
var statearr_31148_34268 = state_31064;
(statearr_31148_34268[(1)] = cljs.core.first((state_31064[(4)])));

} else {
throw ex__30543__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30541__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34274 = state_31064;
state_31064 = G__34274;
continue;
} else {
return ret_value__30541__auto__;
}
break;
}
});
cljs$core$async$state_machine__30540__auto__ = function(state_31064){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__30540__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__30540__auto____1.call(this,state_31064);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__30540__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__30540__auto____0;
cljs$core$async$state_machine__30540__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__30540__auto____1;
return cljs$core$async$state_machine__30540__auto__;
})()
})();
var state__30705__auto__ = (function (){var statearr_31152 = f__30704__auto__();
(statearr_31152[(6)] = c__30703__auto___34229);

return statearr_31152;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30705__auto__);
}));


return to;
}));

(cljs.core.async.pipe.cljs$lang$maxFixedArity = 3);

cljs.core.async.pipeline_STAR_ = (function cljs$core$async$pipeline_STAR_(n,to,xf,from,close_QMARK_,ex_handler,type){
if((n > (0))){
} else {
throw (new Error("Assert failed: (pos? n)"));
}

var jobs = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(n);
var results = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(n);
var process = (function (p__31171){
var vec__31172 = p__31171;
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__31172,(0),null);
var p = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__31172,(1),null);
var job = vec__31172;
if((job == null)){
cljs.core.async.close_BANG_(results);

return null;
} else {
var res = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3((1),xf,ex_handler);
var c__30703__auto___34278 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30704__auto__ = (function (){var switch__30539__auto__ = (function (state_31180){
var state_val_31181 = (state_31180[(1)]);
if((state_val_31181 === (1))){
var state_31180__$1 = state_31180;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_31180__$1,(2),res,v);
} else {
if((state_val_31181 === (2))){
var inst_31177 = (state_31180[(2)]);
var inst_31178 = cljs.core.async.close_BANG_(res);
var state_31180__$1 = (function (){var statearr_31187 = state_31180;
(statearr_31187[(7)] = inst_31177);

return statearr_31187;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_31180__$1,inst_31178);
} else {
return null;
}
}
});
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__30540__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__30540__auto____0 = (function (){
var statearr_31188 = [null,null,null,null,null,null,null,null];
(statearr_31188[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__30540__auto__);

(statearr_31188[(1)] = (1));

return statearr_31188;
});
var cljs$core$async$pipeline_STAR__$_state_machine__30540__auto____1 = (function (state_31180){
while(true){
var ret_value__30541__auto__ = (function (){try{while(true){
var result__30542__auto__ = switch__30539__auto__(state_31180);
if(cljs.core.keyword_identical_QMARK_(result__30542__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30542__auto__;
}
break;
}
}catch (e31193){var ex__30543__auto__ = e31193;
var statearr_31194_34284 = state_31180;
(statearr_31194_34284[(2)] = ex__30543__auto__);


if(cljs.core.seq((state_31180[(4)]))){
var statearr_31195_34285 = state_31180;
(statearr_31195_34285[(1)] = cljs.core.first((state_31180[(4)])));

} else {
throw ex__30543__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30541__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34287 = state_31180;
state_31180 = G__34287;
continue;
} else {
return ret_value__30541__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__30540__auto__ = function(state_31180){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__30540__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__30540__auto____1.call(this,state_31180);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__30540__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__30540__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__30540__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__30540__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__30540__auto__;
})()
})();
var state__30705__auto__ = (function (){var statearr_31205 = f__30704__auto__();
(statearr_31205[(6)] = c__30703__auto___34278);

return statearr_31205;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30705__auto__);
}));


cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(p,res);

return true;
}
});
var async = (function (p__31211){
var vec__31215 = p__31211;
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__31215,(0),null);
var p = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__31215,(1),null);
var job = vec__31215;
if((job == null)){
cljs.core.async.close_BANG_(results);

return null;
} else {
var res = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
(xf.cljs$core$IFn$_invoke$arity$2 ? xf.cljs$core$IFn$_invoke$arity$2(v,res) : xf.call(null,v,res));

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(p,res);

return true;
}
});
var n__4613__auto___34289 = n;
var __34290 = (0);
while(true){
if((__34290 < n__4613__auto___34289)){
var G__31227_34293 = type;
var G__31227_34294__$1 = (((G__31227_34293 instanceof cljs.core.Keyword))?G__31227_34293.fqn:null);
switch (G__31227_34294__$1) {
case "compute":
var c__30703__auto___34300 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (__34290,c__30703__auto___34300,G__31227_34293,G__31227_34294__$1,n__4613__auto___34289,jobs,results,process,async){
return (function (){
var f__30704__auto__ = (function (){var switch__30539__auto__ = ((function (__34290,c__30703__auto___34300,G__31227_34293,G__31227_34294__$1,n__4613__auto___34289,jobs,results,process,async){
return (function (state_31250){
var state_val_31251 = (state_31250[(1)]);
if((state_val_31251 === (1))){
var state_31250__$1 = state_31250;
var statearr_31259_34312 = state_31250__$1;
(statearr_31259_34312[(2)] = null);

(statearr_31259_34312[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31251 === (2))){
var state_31250__$1 = state_31250;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_31250__$1,(4),jobs);
} else {
if((state_val_31251 === (3))){
var inst_31248 = (state_31250[(2)]);
var state_31250__$1 = state_31250;
return cljs.core.async.impl.ioc_helpers.return_chan(state_31250__$1,inst_31248);
} else {
if((state_val_31251 === (4))){
var inst_31238 = (state_31250[(2)]);
var inst_31239 = process(inst_31238);
var state_31250__$1 = state_31250;
if(cljs.core.truth_(inst_31239)){
var statearr_31272_34319 = state_31250__$1;
(statearr_31272_34319[(1)] = (5));

} else {
var statearr_31273_34320 = state_31250__$1;
(statearr_31273_34320[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31251 === (5))){
var state_31250__$1 = state_31250;
var statearr_31274_34324 = state_31250__$1;
(statearr_31274_34324[(2)] = null);

(statearr_31274_34324[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31251 === (6))){
var state_31250__$1 = state_31250;
var statearr_31278_34327 = state_31250__$1;
(statearr_31278_34327[(2)] = null);

(statearr_31278_34327[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31251 === (7))){
var inst_31246 = (state_31250[(2)]);
var state_31250__$1 = state_31250;
var statearr_31286_34328 = state_31250__$1;
(statearr_31286_34328[(2)] = inst_31246);

(statearr_31286_34328[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__34290,c__30703__auto___34300,G__31227_34293,G__31227_34294__$1,n__4613__auto___34289,jobs,results,process,async))
;
return ((function (__34290,switch__30539__auto__,c__30703__auto___34300,G__31227_34293,G__31227_34294__$1,n__4613__auto___34289,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__30540__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__30540__auto____0 = (function (){
var statearr_31296 = [null,null,null,null,null,null,null];
(statearr_31296[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__30540__auto__);

(statearr_31296[(1)] = (1));

return statearr_31296;
});
var cljs$core$async$pipeline_STAR__$_state_machine__30540__auto____1 = (function (state_31250){
while(true){
var ret_value__30541__auto__ = (function (){try{while(true){
var result__30542__auto__ = switch__30539__auto__(state_31250);
if(cljs.core.keyword_identical_QMARK_(result__30542__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30542__auto__;
}
break;
}
}catch (e31301){var ex__30543__auto__ = e31301;
var statearr_31309_34335 = state_31250;
(statearr_31309_34335[(2)] = ex__30543__auto__);


if(cljs.core.seq((state_31250[(4)]))){
var statearr_31312_34340 = state_31250;
(statearr_31312_34340[(1)] = cljs.core.first((state_31250[(4)])));

} else {
throw ex__30543__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30541__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34344 = state_31250;
state_31250 = G__34344;
continue;
} else {
return ret_value__30541__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__30540__auto__ = function(state_31250){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__30540__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__30540__auto____1.call(this,state_31250);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__30540__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__30540__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__30540__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__30540__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__30540__auto__;
})()
;})(__34290,switch__30539__auto__,c__30703__auto___34300,G__31227_34293,G__31227_34294__$1,n__4613__auto___34289,jobs,results,process,async))
})();
var state__30705__auto__ = (function (){var statearr_31316 = f__30704__auto__();
(statearr_31316[(6)] = c__30703__auto___34300);

return statearr_31316;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30705__auto__);
});})(__34290,c__30703__auto___34300,G__31227_34293,G__31227_34294__$1,n__4613__auto___34289,jobs,results,process,async))
);


break;
case "async":
var c__30703__auto___34349 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (__34290,c__30703__auto___34349,G__31227_34293,G__31227_34294__$1,n__4613__auto___34289,jobs,results,process,async){
return (function (){
var f__30704__auto__ = (function (){var switch__30539__auto__ = ((function (__34290,c__30703__auto___34349,G__31227_34293,G__31227_34294__$1,n__4613__auto___34289,jobs,results,process,async){
return (function (state_31340){
var state_val_31341 = (state_31340[(1)]);
if((state_val_31341 === (1))){
var state_31340__$1 = state_31340;
var statearr_31354_34357 = state_31340__$1;
(statearr_31354_34357[(2)] = null);

(statearr_31354_34357[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31341 === (2))){
var state_31340__$1 = state_31340;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_31340__$1,(4),jobs);
} else {
if((state_val_31341 === (3))){
var inst_31337 = (state_31340[(2)]);
var state_31340__$1 = state_31340;
return cljs.core.async.impl.ioc_helpers.return_chan(state_31340__$1,inst_31337);
} else {
if((state_val_31341 === (4))){
var inst_31327 = (state_31340[(2)]);
var inst_31329 = async(inst_31327);
var state_31340__$1 = state_31340;
if(cljs.core.truth_(inst_31329)){
var statearr_31357_34364 = state_31340__$1;
(statearr_31357_34364[(1)] = (5));

} else {
var statearr_31358_34367 = state_31340__$1;
(statearr_31358_34367[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31341 === (5))){
var state_31340__$1 = state_31340;
var statearr_31360_34371 = state_31340__$1;
(statearr_31360_34371[(2)] = null);

(statearr_31360_34371[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31341 === (6))){
var state_31340__$1 = state_31340;
var statearr_31362_34380 = state_31340__$1;
(statearr_31362_34380[(2)] = null);

(statearr_31362_34380[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31341 === (7))){
var inst_31335 = (state_31340[(2)]);
var state_31340__$1 = state_31340;
var statearr_31363_34382 = state_31340__$1;
(statearr_31363_34382[(2)] = inst_31335);

(statearr_31363_34382[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__34290,c__30703__auto___34349,G__31227_34293,G__31227_34294__$1,n__4613__auto___34289,jobs,results,process,async))
;
return ((function (__34290,switch__30539__auto__,c__30703__auto___34349,G__31227_34293,G__31227_34294__$1,n__4613__auto___34289,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__30540__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__30540__auto____0 = (function (){
var statearr_31374 = [null,null,null,null,null,null,null];
(statearr_31374[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__30540__auto__);

(statearr_31374[(1)] = (1));

return statearr_31374;
});
var cljs$core$async$pipeline_STAR__$_state_machine__30540__auto____1 = (function (state_31340){
while(true){
var ret_value__30541__auto__ = (function (){try{while(true){
var result__30542__auto__ = switch__30539__auto__(state_31340);
if(cljs.core.keyword_identical_QMARK_(result__30542__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30542__auto__;
}
break;
}
}catch (e31381){var ex__30543__auto__ = e31381;
var statearr_31382_34388 = state_31340;
(statearr_31382_34388[(2)] = ex__30543__auto__);


if(cljs.core.seq((state_31340[(4)]))){
var statearr_31403_34389 = state_31340;
(statearr_31403_34389[(1)] = cljs.core.first((state_31340[(4)])));

} else {
throw ex__30543__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30541__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34392 = state_31340;
state_31340 = G__34392;
continue;
} else {
return ret_value__30541__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__30540__auto__ = function(state_31340){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__30540__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__30540__auto____1.call(this,state_31340);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__30540__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__30540__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__30540__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__30540__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__30540__auto__;
})()
;})(__34290,switch__30539__auto__,c__30703__auto___34349,G__31227_34293,G__31227_34294__$1,n__4613__auto___34289,jobs,results,process,async))
})();
var state__30705__auto__ = (function (){var statearr_31409 = f__30704__auto__();
(statearr_31409[(6)] = c__30703__auto___34349);

return statearr_31409;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30705__auto__);
});})(__34290,c__30703__auto___34349,G__31227_34293,G__31227_34294__$1,n__4613__auto___34289,jobs,results,process,async))
);


break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__31227_34294__$1)].join('')));

}

var G__34400 = (__34290 + (1));
__34290 = G__34400;
continue;
} else {
}
break;
}

var c__30703__auto___34403 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30704__auto__ = (function (){var switch__30539__auto__ = (function (state_31448){
var state_val_31452 = (state_31448[(1)]);
if((state_val_31452 === (7))){
var inst_31443 = (state_31448[(2)]);
var state_31448__$1 = state_31448;
var statearr_31467_34407 = state_31448__$1;
(statearr_31467_34407[(2)] = inst_31443);

(statearr_31467_34407[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31452 === (1))){
var state_31448__$1 = state_31448;
var statearr_31471_34408 = state_31448__$1;
(statearr_31471_34408[(2)] = null);

(statearr_31471_34408[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31452 === (4))){
var inst_31414 = (state_31448[(7)]);
var inst_31414__$1 = (state_31448[(2)]);
var inst_31415 = (inst_31414__$1 == null);
var state_31448__$1 = (function (){var statearr_31475 = state_31448;
(statearr_31475[(7)] = inst_31414__$1);

return statearr_31475;
})();
if(cljs.core.truth_(inst_31415)){
var statearr_31476_34411 = state_31448__$1;
(statearr_31476_34411[(1)] = (5));

} else {
var statearr_31479_34412 = state_31448__$1;
(statearr_31479_34412[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31452 === (6))){
var inst_31414 = (state_31448[(7)]);
var inst_31424 = (state_31448[(8)]);
var inst_31424__$1 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
var inst_31432 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_31434 = [inst_31414,inst_31424__$1];
var inst_31435 = (new cljs.core.PersistentVector(null,2,(5),inst_31432,inst_31434,null));
var state_31448__$1 = (function (){var statearr_31483 = state_31448;
(statearr_31483[(8)] = inst_31424__$1);

return statearr_31483;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_31448__$1,(8),jobs,inst_31435);
} else {
if((state_val_31452 === (3))){
var inst_31445 = (state_31448[(2)]);
var state_31448__$1 = state_31448;
return cljs.core.async.impl.ioc_helpers.return_chan(state_31448__$1,inst_31445);
} else {
if((state_val_31452 === (2))){
var state_31448__$1 = state_31448;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_31448__$1,(4),from);
} else {
if((state_val_31452 === (9))){
var inst_31440 = (state_31448[(2)]);
var state_31448__$1 = (function (){var statearr_31491 = state_31448;
(statearr_31491[(9)] = inst_31440);

return statearr_31491;
})();
var statearr_31494_34418 = state_31448__$1;
(statearr_31494_34418[(2)] = null);

(statearr_31494_34418[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31452 === (5))){
var inst_31422 = cljs.core.async.close_BANG_(jobs);
var state_31448__$1 = state_31448;
var statearr_31499_34422 = state_31448__$1;
(statearr_31499_34422[(2)] = inst_31422);

(statearr_31499_34422[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31452 === (8))){
var inst_31424 = (state_31448[(8)]);
var inst_31437 = (state_31448[(2)]);
var state_31448__$1 = (function (){var statearr_31502 = state_31448;
(statearr_31502[(10)] = inst_31437);

return statearr_31502;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_31448__$1,(9),results,inst_31424);
} else {
return null;
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__30540__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__30540__auto____0 = (function (){
var statearr_31503 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_31503[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__30540__auto__);

(statearr_31503[(1)] = (1));

return statearr_31503;
});
var cljs$core$async$pipeline_STAR__$_state_machine__30540__auto____1 = (function (state_31448){
while(true){
var ret_value__30541__auto__ = (function (){try{while(true){
var result__30542__auto__ = switch__30539__auto__(state_31448);
if(cljs.core.keyword_identical_QMARK_(result__30542__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30542__auto__;
}
break;
}
}catch (e31505){var ex__30543__auto__ = e31505;
var statearr_31506_34438 = state_31448;
(statearr_31506_34438[(2)] = ex__30543__auto__);


if(cljs.core.seq((state_31448[(4)]))){
var statearr_31507_34443 = state_31448;
(statearr_31507_34443[(1)] = cljs.core.first((state_31448[(4)])));

} else {
throw ex__30543__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30541__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34444 = state_31448;
state_31448 = G__34444;
continue;
} else {
return ret_value__30541__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__30540__auto__ = function(state_31448){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__30540__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__30540__auto____1.call(this,state_31448);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__30540__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__30540__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__30540__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__30540__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__30540__auto__;
})()
})();
var state__30705__auto__ = (function (){var statearr_31512 = f__30704__auto__();
(statearr_31512[(6)] = c__30703__auto___34403);

return statearr_31512;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30705__auto__);
}));


var c__30703__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30704__auto__ = (function (){var switch__30539__auto__ = (function (state_31563){
var state_val_31564 = (state_31563[(1)]);
if((state_val_31564 === (7))){
var inst_31559 = (state_31563[(2)]);
var state_31563__$1 = state_31563;
var statearr_31569_34453 = state_31563__$1;
(statearr_31569_34453[(2)] = inst_31559);

(statearr_31569_34453[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31564 === (20))){
var state_31563__$1 = state_31563;
var statearr_31574_34459 = state_31563__$1;
(statearr_31574_34459[(2)] = null);

(statearr_31574_34459[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31564 === (1))){
var state_31563__$1 = state_31563;
var statearr_31579_34462 = state_31563__$1;
(statearr_31579_34462[(2)] = null);

(statearr_31579_34462[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31564 === (4))){
var inst_31515 = (state_31563[(7)]);
var inst_31515__$1 = (state_31563[(2)]);
var inst_31517 = (inst_31515__$1 == null);
var state_31563__$1 = (function (){var statearr_31583 = state_31563;
(statearr_31583[(7)] = inst_31515__$1);

return statearr_31583;
})();
if(cljs.core.truth_(inst_31517)){
var statearr_31585_34466 = state_31563__$1;
(statearr_31585_34466[(1)] = (5));

} else {
var statearr_31587_34467 = state_31563__$1;
(statearr_31587_34467[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31564 === (15))){
var inst_31531 = (state_31563[(8)]);
var state_31563__$1 = state_31563;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_31563__$1,(18),to,inst_31531);
} else {
if((state_val_31564 === (21))){
var inst_31550 = (state_31563[(2)]);
var state_31563__$1 = state_31563;
var statearr_31591_34479 = state_31563__$1;
(statearr_31591_34479[(2)] = inst_31550);

(statearr_31591_34479[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31564 === (13))){
var inst_31552 = (state_31563[(2)]);
var state_31563__$1 = (function (){var statearr_31596 = state_31563;
(statearr_31596[(9)] = inst_31552);

return statearr_31596;
})();
var statearr_31597_34482 = state_31563__$1;
(statearr_31597_34482[(2)] = null);

(statearr_31597_34482[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31564 === (6))){
var inst_31515 = (state_31563[(7)]);
var state_31563__$1 = state_31563;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_31563__$1,(11),inst_31515);
} else {
if((state_val_31564 === (17))){
var inst_31543 = (state_31563[(2)]);
var state_31563__$1 = state_31563;
if(cljs.core.truth_(inst_31543)){
var statearr_31599_34487 = state_31563__$1;
(statearr_31599_34487[(1)] = (19));

} else {
var statearr_31600_34489 = state_31563__$1;
(statearr_31600_34489[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31564 === (3))){
var inst_31561 = (state_31563[(2)]);
var state_31563__$1 = state_31563;
return cljs.core.async.impl.ioc_helpers.return_chan(state_31563__$1,inst_31561);
} else {
if((state_val_31564 === (12))){
var inst_31526 = (state_31563[(10)]);
var state_31563__$1 = state_31563;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_31563__$1,(14),inst_31526);
} else {
if((state_val_31564 === (2))){
var state_31563__$1 = state_31563;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_31563__$1,(4),results);
} else {
if((state_val_31564 === (19))){
var state_31563__$1 = state_31563;
var statearr_31602_34498 = state_31563__$1;
(statearr_31602_34498[(2)] = null);

(statearr_31602_34498[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31564 === (11))){
var inst_31526 = (state_31563[(2)]);
var state_31563__$1 = (function (){var statearr_31607 = state_31563;
(statearr_31607[(10)] = inst_31526);

return statearr_31607;
})();
var statearr_31609_34500 = state_31563__$1;
(statearr_31609_34500[(2)] = null);

(statearr_31609_34500[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31564 === (9))){
var state_31563__$1 = state_31563;
var statearr_31610_34501 = state_31563__$1;
(statearr_31610_34501[(2)] = null);

(statearr_31610_34501[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31564 === (5))){
var state_31563__$1 = state_31563;
if(cljs.core.truth_(close_QMARK_)){
var statearr_31611_34504 = state_31563__$1;
(statearr_31611_34504[(1)] = (8));

} else {
var statearr_31612_34505 = state_31563__$1;
(statearr_31612_34505[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31564 === (14))){
var inst_31531 = (state_31563[(8)]);
var inst_31531__$1 = (state_31563[(2)]);
var inst_31536 = (inst_31531__$1 == null);
var inst_31537 = cljs.core.not(inst_31536);
var state_31563__$1 = (function (){var statearr_31613 = state_31563;
(statearr_31613[(8)] = inst_31531__$1);

return statearr_31613;
})();
if(inst_31537){
var statearr_31615_34514 = state_31563__$1;
(statearr_31615_34514[(1)] = (15));

} else {
var statearr_31616_34515 = state_31563__$1;
(statearr_31616_34515[(1)] = (16));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31564 === (16))){
var state_31563__$1 = state_31563;
var statearr_31623_34516 = state_31563__$1;
(statearr_31623_34516[(2)] = false);

(statearr_31623_34516[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31564 === (10))){
var inst_31523 = (state_31563[(2)]);
var state_31563__$1 = state_31563;
var statearr_31624_34518 = state_31563__$1;
(statearr_31624_34518[(2)] = inst_31523);

(statearr_31624_34518[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31564 === (18))){
var inst_31540 = (state_31563[(2)]);
var state_31563__$1 = state_31563;
var statearr_31625_34522 = state_31563__$1;
(statearr_31625_34522[(2)] = inst_31540);

(statearr_31625_34522[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31564 === (8))){
var inst_31520 = cljs.core.async.close_BANG_(to);
var state_31563__$1 = state_31563;
var statearr_31626_34527 = state_31563__$1;
(statearr_31626_34527[(2)] = inst_31520);

(statearr_31626_34527[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__30540__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__30540__auto____0 = (function (){
var statearr_31632 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_31632[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__30540__auto__);

(statearr_31632[(1)] = (1));

return statearr_31632;
});
var cljs$core$async$pipeline_STAR__$_state_machine__30540__auto____1 = (function (state_31563){
while(true){
var ret_value__30541__auto__ = (function (){try{while(true){
var result__30542__auto__ = switch__30539__auto__(state_31563);
if(cljs.core.keyword_identical_QMARK_(result__30542__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30542__auto__;
}
break;
}
}catch (e31633){var ex__30543__auto__ = e31633;
var statearr_31634_34535 = state_31563;
(statearr_31634_34535[(2)] = ex__30543__auto__);


if(cljs.core.seq((state_31563[(4)]))){
var statearr_31635_34536 = state_31563;
(statearr_31635_34536[(1)] = cljs.core.first((state_31563[(4)])));

} else {
throw ex__30543__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30541__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34537 = state_31563;
state_31563 = G__34537;
continue;
} else {
return ret_value__30541__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__30540__auto__ = function(state_31563){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__30540__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__30540__auto____1.call(this,state_31563);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__30540__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__30540__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__30540__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__30540__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__30540__auto__;
})()
})();
var state__30705__auto__ = (function (){var statearr_31637 = f__30704__auto__();
(statearr_31637[(6)] = c__30703__auto__);

return statearr_31637;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30705__auto__);
}));

return c__30703__auto__;
});
/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the async function af, with parallelism n. af
 *   must be a function of two arguments, the first an input value and
 *   the second a channel on which to place the result(s). af must close!
 *   the channel before returning.  The presumption is that af will
 *   return immediately, having launched some asynchronous operation
 *   whose completion/callback will manipulate the result channel. Outputs
 *   will be returned in order relative to  the inputs. By default, the to
 *   channel will be closed when the from channel closes, but can be
 *   determined by the close?  parameter. Will stop consuming the from
 *   channel if the to channel closes.
 */
cljs.core.async.pipeline_async = (function cljs$core$async$pipeline_async(var_args){
var G__31642 = arguments.length;
switch (G__31642) {
case 4:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4 = (function (n,to,af,from){
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5(n,to,af,from,true);
}));

(cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5 = (function (n,to,af,from,close_QMARK_){
return cljs.core.async.pipeline_STAR_(n,to,af,from,close_QMARK_,null,new cljs.core.Keyword(null,"async","async",1050769601));
}));

(cljs.core.async.pipeline_async.cljs$lang$maxFixedArity = 5);

/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the transducer xf, with parallelism n. Because
 *   it is parallel, the transducer will be applied independently to each
 *   element, not across elements, and may produce zero or more outputs
 *   per input.  Outputs will be returned in order relative to the
 *   inputs. By default, the to channel will be closed when the from
 *   channel closes, but can be determined by the close?  parameter. Will
 *   stop consuming the from channel if the to channel closes.
 * 
 *   Note this is supplied for API compatibility with the Clojure version.
 *   Values of N > 1 will not result in actual concurrency in a
 *   single-threaded runtime.
 */
cljs.core.async.pipeline = (function cljs$core$async$pipeline(var_args){
var G__31645 = arguments.length;
switch (G__31645) {
case 4:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
case 6:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]),(arguments[(5)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4 = (function (n,to,xf,from){
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5(n,to,xf,from,true);
}));

(cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5 = (function (n,to,xf,from,close_QMARK_){
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6(n,to,xf,from,close_QMARK_,null);
}));

(cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6 = (function (n,to,xf,from,close_QMARK_,ex_handler){
return cljs.core.async.pipeline_STAR_(n,to,xf,from,close_QMARK_,ex_handler,new cljs.core.Keyword(null,"compute","compute",1555393130));
}));

(cljs.core.async.pipeline.cljs$lang$maxFixedArity = 6);

/**
 * Takes a predicate and a source channel and returns a vector of two
 *   channels, the first of which will contain the values for which the
 *   predicate returned true, the second those for which it returned
 *   false.
 * 
 *   The out channels will be unbuffered by default, or two buf-or-ns can
 *   be supplied. The channels will close after the source channel has
 *   closed.
 */
cljs.core.async.split = (function cljs$core$async$split(var_args){
var G__31650 = arguments.length;
switch (G__31650) {
case 2:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 4:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.split.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$4(p,ch,null,null);
}));

(cljs.core.async.split.cljs$core$IFn$_invoke$arity$4 = (function (p,ch,t_buf_or_n,f_buf_or_n){
var tc = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(t_buf_or_n);
var fc = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(f_buf_or_n);
var c__30703__auto___34575 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30704__auto__ = (function (){var switch__30539__auto__ = (function (state_31679){
var state_val_31680 = (state_31679[(1)]);
if((state_val_31680 === (7))){
var inst_31675 = (state_31679[(2)]);
var state_31679__$1 = state_31679;
var statearr_31686_34578 = state_31679__$1;
(statearr_31686_34578[(2)] = inst_31675);

(statearr_31686_34578[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31680 === (1))){
var state_31679__$1 = state_31679;
var statearr_31687_34579 = state_31679__$1;
(statearr_31687_34579[(2)] = null);

(statearr_31687_34579[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31680 === (4))){
var inst_31656 = (state_31679[(7)]);
var inst_31656__$1 = (state_31679[(2)]);
var inst_31657 = (inst_31656__$1 == null);
var state_31679__$1 = (function (){var statearr_31688 = state_31679;
(statearr_31688[(7)] = inst_31656__$1);

return statearr_31688;
})();
if(cljs.core.truth_(inst_31657)){
var statearr_31689_34592 = state_31679__$1;
(statearr_31689_34592[(1)] = (5));

} else {
var statearr_31691_34594 = state_31679__$1;
(statearr_31691_34594[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31680 === (13))){
var state_31679__$1 = state_31679;
var statearr_31695_34599 = state_31679__$1;
(statearr_31695_34599[(2)] = null);

(statearr_31695_34599[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31680 === (6))){
var inst_31656 = (state_31679[(7)]);
var inst_31662 = (p.cljs$core$IFn$_invoke$arity$1 ? p.cljs$core$IFn$_invoke$arity$1(inst_31656) : p.call(null,inst_31656));
var state_31679__$1 = state_31679;
if(cljs.core.truth_(inst_31662)){
var statearr_31698_34606 = state_31679__$1;
(statearr_31698_34606[(1)] = (9));

} else {
var statearr_31699_34609 = state_31679__$1;
(statearr_31699_34609[(1)] = (10));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31680 === (3))){
var inst_31677 = (state_31679[(2)]);
var state_31679__$1 = state_31679;
return cljs.core.async.impl.ioc_helpers.return_chan(state_31679__$1,inst_31677);
} else {
if((state_val_31680 === (12))){
var state_31679__$1 = state_31679;
var statearr_31702_34620 = state_31679__$1;
(statearr_31702_34620[(2)] = null);

(statearr_31702_34620[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31680 === (2))){
var state_31679__$1 = state_31679;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_31679__$1,(4),ch);
} else {
if((state_val_31680 === (11))){
var inst_31656 = (state_31679[(7)]);
var inst_31666 = (state_31679[(2)]);
var state_31679__$1 = state_31679;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_31679__$1,(8),inst_31666,inst_31656);
} else {
if((state_val_31680 === (9))){
var state_31679__$1 = state_31679;
var statearr_31703_34633 = state_31679__$1;
(statearr_31703_34633[(2)] = tc);

(statearr_31703_34633[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31680 === (5))){
var inst_31659 = cljs.core.async.close_BANG_(tc);
var inst_31660 = cljs.core.async.close_BANG_(fc);
var state_31679__$1 = (function (){var statearr_31704 = state_31679;
(statearr_31704[(8)] = inst_31659);

return statearr_31704;
})();
var statearr_31705_34640 = state_31679__$1;
(statearr_31705_34640[(2)] = inst_31660);

(statearr_31705_34640[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31680 === (14))){
var inst_31673 = (state_31679[(2)]);
var state_31679__$1 = state_31679;
var statearr_31706_34642 = state_31679__$1;
(statearr_31706_34642[(2)] = inst_31673);

(statearr_31706_34642[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31680 === (10))){
var state_31679__$1 = state_31679;
var statearr_31707_34650 = state_31679__$1;
(statearr_31707_34650[(2)] = fc);

(statearr_31707_34650[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31680 === (8))){
var inst_31668 = (state_31679[(2)]);
var state_31679__$1 = state_31679;
if(cljs.core.truth_(inst_31668)){
var statearr_31708_34655 = state_31679__$1;
(statearr_31708_34655[(1)] = (12));

} else {
var statearr_31709_34657 = state_31679__$1;
(statearr_31709_34657[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__30540__auto__ = null;
var cljs$core$async$state_machine__30540__auto____0 = (function (){
var statearr_31710 = [null,null,null,null,null,null,null,null,null];
(statearr_31710[(0)] = cljs$core$async$state_machine__30540__auto__);

(statearr_31710[(1)] = (1));

return statearr_31710;
});
var cljs$core$async$state_machine__30540__auto____1 = (function (state_31679){
while(true){
var ret_value__30541__auto__ = (function (){try{while(true){
var result__30542__auto__ = switch__30539__auto__(state_31679);
if(cljs.core.keyword_identical_QMARK_(result__30542__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30542__auto__;
}
break;
}
}catch (e31713){var ex__30543__auto__ = e31713;
var statearr_31714_34686 = state_31679;
(statearr_31714_34686[(2)] = ex__30543__auto__);


if(cljs.core.seq((state_31679[(4)]))){
var statearr_31716_34689 = state_31679;
(statearr_31716_34689[(1)] = cljs.core.first((state_31679[(4)])));

} else {
throw ex__30543__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30541__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34697 = state_31679;
state_31679 = G__34697;
continue;
} else {
return ret_value__30541__auto__;
}
break;
}
});
cljs$core$async$state_machine__30540__auto__ = function(state_31679){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__30540__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__30540__auto____1.call(this,state_31679);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__30540__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__30540__auto____0;
cljs$core$async$state_machine__30540__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__30540__auto____1;
return cljs$core$async$state_machine__30540__auto__;
})()
})();
var state__30705__auto__ = (function (){var statearr_31721 = f__30704__auto__();
(statearr_31721[(6)] = c__30703__auto___34575);

return statearr_31721;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30705__auto__);
}));


return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [tc,fc], null);
}));

(cljs.core.async.split.cljs$lang$maxFixedArity = 4);

/**
 * f should be a function of 2 arguments. Returns a channel containing
 *   the single result of applying f to init and the first item from the
 *   channel, then applying f to that result and the 2nd item, etc. If
 *   the channel closes without yielding items, returns init and f is not
 *   called. ch must close before reduce produces a result.
 */
cljs.core.async.reduce = (function cljs$core$async$reduce(f,init,ch){
var c__30703__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30704__auto__ = (function (){var switch__30539__auto__ = (function (state_31752){
var state_val_31753 = (state_31752[(1)]);
if((state_val_31753 === (7))){
var inst_31748 = (state_31752[(2)]);
var state_31752__$1 = state_31752;
var statearr_31765_34713 = state_31752__$1;
(statearr_31765_34713[(2)] = inst_31748);

(statearr_31765_34713[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31753 === (1))){
var inst_31723 = init;
var inst_31724 = inst_31723;
var state_31752__$1 = (function (){var statearr_31770 = state_31752;
(statearr_31770[(7)] = inst_31724);

return statearr_31770;
})();
var statearr_31772_34715 = state_31752__$1;
(statearr_31772_34715[(2)] = null);

(statearr_31772_34715[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31753 === (4))){
var inst_31731 = (state_31752[(8)]);
var inst_31731__$1 = (state_31752[(2)]);
var inst_31732 = (inst_31731__$1 == null);
var state_31752__$1 = (function (){var statearr_31778 = state_31752;
(statearr_31778[(8)] = inst_31731__$1);

return statearr_31778;
})();
if(cljs.core.truth_(inst_31732)){
var statearr_31786_34722 = state_31752__$1;
(statearr_31786_34722[(1)] = (5));

} else {
var statearr_31787_34724 = state_31752__$1;
(statearr_31787_34724[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31753 === (6))){
var inst_31735 = (state_31752[(9)]);
var inst_31731 = (state_31752[(8)]);
var inst_31724 = (state_31752[(7)]);
var inst_31735__$1 = (f.cljs$core$IFn$_invoke$arity$2 ? f.cljs$core$IFn$_invoke$arity$2(inst_31724,inst_31731) : f.call(null,inst_31724,inst_31731));
var inst_31736 = cljs.core.reduced_QMARK_(inst_31735__$1);
var state_31752__$1 = (function (){var statearr_31791 = state_31752;
(statearr_31791[(9)] = inst_31735__$1);

return statearr_31791;
})();
if(inst_31736){
var statearr_31792_34733 = state_31752__$1;
(statearr_31792_34733[(1)] = (8));

} else {
var statearr_31797_34736 = state_31752__$1;
(statearr_31797_34736[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31753 === (3))){
var inst_31750 = (state_31752[(2)]);
var state_31752__$1 = state_31752;
return cljs.core.async.impl.ioc_helpers.return_chan(state_31752__$1,inst_31750);
} else {
if((state_val_31753 === (2))){
var state_31752__$1 = state_31752;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_31752__$1,(4),ch);
} else {
if((state_val_31753 === (9))){
var inst_31735 = (state_31752[(9)]);
var inst_31724 = inst_31735;
var state_31752__$1 = (function (){var statearr_31805 = state_31752;
(statearr_31805[(7)] = inst_31724);

return statearr_31805;
})();
var statearr_31811_34738 = state_31752__$1;
(statearr_31811_34738[(2)] = null);

(statearr_31811_34738[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31753 === (5))){
var inst_31724 = (state_31752[(7)]);
var state_31752__$1 = state_31752;
var statearr_31816_34745 = state_31752__$1;
(statearr_31816_34745[(2)] = inst_31724);

(statearr_31816_34745[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31753 === (10))){
var inst_31742 = (state_31752[(2)]);
var state_31752__$1 = state_31752;
var statearr_31817_34749 = state_31752__$1;
(statearr_31817_34749[(2)] = inst_31742);

(statearr_31817_34749[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31753 === (8))){
var inst_31735 = (state_31752[(9)]);
var inst_31738 = cljs.core.deref(inst_31735);
var state_31752__$1 = state_31752;
var statearr_31818_34755 = state_31752__$1;
(statearr_31818_34755[(2)] = inst_31738);

(statearr_31818_34755[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$reduce_$_state_machine__30540__auto__ = null;
var cljs$core$async$reduce_$_state_machine__30540__auto____0 = (function (){
var statearr_31820 = [null,null,null,null,null,null,null,null,null,null];
(statearr_31820[(0)] = cljs$core$async$reduce_$_state_machine__30540__auto__);

(statearr_31820[(1)] = (1));

return statearr_31820;
});
var cljs$core$async$reduce_$_state_machine__30540__auto____1 = (function (state_31752){
while(true){
var ret_value__30541__auto__ = (function (){try{while(true){
var result__30542__auto__ = switch__30539__auto__(state_31752);
if(cljs.core.keyword_identical_QMARK_(result__30542__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30542__auto__;
}
break;
}
}catch (e31822){var ex__30543__auto__ = e31822;
var statearr_31824_34768 = state_31752;
(statearr_31824_34768[(2)] = ex__30543__auto__);


if(cljs.core.seq((state_31752[(4)]))){
var statearr_31828_34772 = state_31752;
(statearr_31828_34772[(1)] = cljs.core.first((state_31752[(4)])));

} else {
throw ex__30543__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30541__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34779 = state_31752;
state_31752 = G__34779;
continue;
} else {
return ret_value__30541__auto__;
}
break;
}
});
cljs$core$async$reduce_$_state_machine__30540__auto__ = function(state_31752){
switch(arguments.length){
case 0:
return cljs$core$async$reduce_$_state_machine__30540__auto____0.call(this);
case 1:
return cljs$core$async$reduce_$_state_machine__30540__auto____1.call(this,state_31752);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$reduce_$_state_machine__30540__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$reduce_$_state_machine__30540__auto____0;
cljs$core$async$reduce_$_state_machine__30540__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$reduce_$_state_machine__30540__auto____1;
return cljs$core$async$reduce_$_state_machine__30540__auto__;
})()
})();
var state__30705__auto__ = (function (){var statearr_31829 = f__30704__auto__();
(statearr_31829[(6)] = c__30703__auto__);

return statearr_31829;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30705__auto__);
}));

return c__30703__auto__;
});
/**
 * async/reduces a channel with a transformation (xform f).
 *   Returns a channel containing the result.  ch must close before
 *   transduce produces a result.
 */
cljs.core.async.transduce = (function cljs$core$async$transduce(xform,f,init,ch){
var f__$1 = (xform.cljs$core$IFn$_invoke$arity$1 ? xform.cljs$core$IFn$_invoke$arity$1(f) : xform.call(null,f));
var c__30703__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30704__auto__ = (function (){var switch__30539__auto__ = (function (state_31835){
var state_val_31836 = (state_31835[(1)]);
if((state_val_31836 === (1))){
var inst_31830 = cljs.core.async.reduce(f__$1,init,ch);
var state_31835__$1 = state_31835;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_31835__$1,(2),inst_31830);
} else {
if((state_val_31836 === (2))){
var inst_31832 = (state_31835[(2)]);
var inst_31833 = (f__$1.cljs$core$IFn$_invoke$arity$1 ? f__$1.cljs$core$IFn$_invoke$arity$1(inst_31832) : f__$1.call(null,inst_31832));
var state_31835__$1 = state_31835;
return cljs.core.async.impl.ioc_helpers.return_chan(state_31835__$1,inst_31833);
} else {
return null;
}
}
});
return (function() {
var cljs$core$async$transduce_$_state_machine__30540__auto__ = null;
var cljs$core$async$transduce_$_state_machine__30540__auto____0 = (function (){
var statearr_31839 = [null,null,null,null,null,null,null];
(statearr_31839[(0)] = cljs$core$async$transduce_$_state_machine__30540__auto__);

(statearr_31839[(1)] = (1));

return statearr_31839;
});
var cljs$core$async$transduce_$_state_machine__30540__auto____1 = (function (state_31835){
while(true){
var ret_value__30541__auto__ = (function (){try{while(true){
var result__30542__auto__ = switch__30539__auto__(state_31835);
if(cljs.core.keyword_identical_QMARK_(result__30542__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30542__auto__;
}
break;
}
}catch (e31841){var ex__30543__auto__ = e31841;
var statearr_31842_34792 = state_31835;
(statearr_31842_34792[(2)] = ex__30543__auto__);


if(cljs.core.seq((state_31835[(4)]))){
var statearr_31843_34793 = state_31835;
(statearr_31843_34793[(1)] = cljs.core.first((state_31835[(4)])));

} else {
throw ex__30543__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30541__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34795 = state_31835;
state_31835 = G__34795;
continue;
} else {
return ret_value__30541__auto__;
}
break;
}
});
cljs$core$async$transduce_$_state_machine__30540__auto__ = function(state_31835){
switch(arguments.length){
case 0:
return cljs$core$async$transduce_$_state_machine__30540__auto____0.call(this);
case 1:
return cljs$core$async$transduce_$_state_machine__30540__auto____1.call(this,state_31835);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$transduce_$_state_machine__30540__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$transduce_$_state_machine__30540__auto____0;
cljs$core$async$transduce_$_state_machine__30540__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$transduce_$_state_machine__30540__auto____1;
return cljs$core$async$transduce_$_state_machine__30540__auto__;
})()
})();
var state__30705__auto__ = (function (){var statearr_31844 = f__30704__auto__();
(statearr_31844[(6)] = c__30703__auto__);

return statearr_31844;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30705__auto__);
}));

return c__30703__auto__;
});
/**
 * Puts the contents of coll into the supplied channel.
 * 
 *   By default the channel will be closed after the items are copied,
 *   but can be determined by the close? parameter.
 * 
 *   Returns a channel which will close after the items are copied.
 */
cljs.core.async.onto_chan_BANG_ = (function cljs$core$async$onto_chan_BANG_(var_args){
var G__31849 = arguments.length;
switch (G__31849) {
case 2:
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (ch,coll){
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3(ch,coll,true);
}));

(cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (ch,coll,close_QMARK_){
var c__30703__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30704__auto__ = (function (){var switch__30539__auto__ = (function (state_31874){
var state_val_31875 = (state_31874[(1)]);
if((state_val_31875 === (7))){
var inst_31856 = (state_31874[(2)]);
var state_31874__$1 = state_31874;
var statearr_31876_34805 = state_31874__$1;
(statearr_31876_34805[(2)] = inst_31856);

(statearr_31876_34805[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31875 === (1))){
var inst_31850 = cljs.core.seq(coll);
var inst_31851 = inst_31850;
var state_31874__$1 = (function (){var statearr_31877 = state_31874;
(statearr_31877[(7)] = inst_31851);

return statearr_31877;
})();
var statearr_31880_34807 = state_31874__$1;
(statearr_31880_34807[(2)] = null);

(statearr_31880_34807[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31875 === (4))){
var inst_31851 = (state_31874[(7)]);
var inst_31854 = cljs.core.first(inst_31851);
var state_31874__$1 = state_31874;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_31874__$1,(7),ch,inst_31854);
} else {
if((state_val_31875 === (13))){
var inst_31868 = (state_31874[(2)]);
var state_31874__$1 = state_31874;
var statearr_31881_34808 = state_31874__$1;
(statearr_31881_34808[(2)] = inst_31868);

(statearr_31881_34808[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31875 === (6))){
var inst_31859 = (state_31874[(2)]);
var state_31874__$1 = state_31874;
if(cljs.core.truth_(inst_31859)){
var statearr_31882_34814 = state_31874__$1;
(statearr_31882_34814[(1)] = (8));

} else {
var statearr_31884_34815 = state_31874__$1;
(statearr_31884_34815[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31875 === (3))){
var inst_31872 = (state_31874[(2)]);
var state_31874__$1 = state_31874;
return cljs.core.async.impl.ioc_helpers.return_chan(state_31874__$1,inst_31872);
} else {
if((state_val_31875 === (12))){
var state_31874__$1 = state_31874;
var statearr_31891_34819 = state_31874__$1;
(statearr_31891_34819[(2)] = null);

(statearr_31891_34819[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31875 === (2))){
var inst_31851 = (state_31874[(7)]);
var state_31874__$1 = state_31874;
if(cljs.core.truth_(inst_31851)){
var statearr_31892_34823 = state_31874__$1;
(statearr_31892_34823[(1)] = (4));

} else {
var statearr_31893_34824 = state_31874__$1;
(statearr_31893_34824[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31875 === (11))){
var inst_31865 = cljs.core.async.close_BANG_(ch);
var state_31874__$1 = state_31874;
var statearr_31894_34830 = state_31874__$1;
(statearr_31894_34830[(2)] = inst_31865);

(statearr_31894_34830[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31875 === (9))){
var state_31874__$1 = state_31874;
if(cljs.core.truth_(close_QMARK_)){
var statearr_31895_34835 = state_31874__$1;
(statearr_31895_34835[(1)] = (11));

} else {
var statearr_31896_34837 = state_31874__$1;
(statearr_31896_34837[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31875 === (5))){
var inst_31851 = (state_31874[(7)]);
var state_31874__$1 = state_31874;
var statearr_31899_34838 = state_31874__$1;
(statearr_31899_34838[(2)] = inst_31851);

(statearr_31899_34838[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31875 === (10))){
var inst_31870 = (state_31874[(2)]);
var state_31874__$1 = state_31874;
var statearr_31901_34843 = state_31874__$1;
(statearr_31901_34843[(2)] = inst_31870);

(statearr_31901_34843[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31875 === (8))){
var inst_31851 = (state_31874[(7)]);
var inst_31861 = cljs.core.next(inst_31851);
var inst_31851__$1 = inst_31861;
var state_31874__$1 = (function (){var statearr_31902 = state_31874;
(statearr_31902[(7)] = inst_31851__$1);

return statearr_31902;
})();
var statearr_31903_34856 = state_31874__$1;
(statearr_31903_34856[(2)] = null);

(statearr_31903_34856[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__30540__auto__ = null;
var cljs$core$async$state_machine__30540__auto____0 = (function (){
var statearr_31908 = [null,null,null,null,null,null,null,null];
(statearr_31908[(0)] = cljs$core$async$state_machine__30540__auto__);

(statearr_31908[(1)] = (1));

return statearr_31908;
});
var cljs$core$async$state_machine__30540__auto____1 = (function (state_31874){
while(true){
var ret_value__30541__auto__ = (function (){try{while(true){
var result__30542__auto__ = switch__30539__auto__(state_31874);
if(cljs.core.keyword_identical_QMARK_(result__30542__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30542__auto__;
}
break;
}
}catch (e31909){var ex__30543__auto__ = e31909;
var statearr_31910_34862 = state_31874;
(statearr_31910_34862[(2)] = ex__30543__auto__);


if(cljs.core.seq((state_31874[(4)]))){
var statearr_31911_34867 = state_31874;
(statearr_31911_34867[(1)] = cljs.core.first((state_31874[(4)])));

} else {
throw ex__30543__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30541__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34872 = state_31874;
state_31874 = G__34872;
continue;
} else {
return ret_value__30541__auto__;
}
break;
}
});
cljs$core$async$state_machine__30540__auto__ = function(state_31874){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__30540__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__30540__auto____1.call(this,state_31874);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__30540__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__30540__auto____0;
cljs$core$async$state_machine__30540__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__30540__auto____1;
return cljs$core$async$state_machine__30540__auto__;
})()
})();
var state__30705__auto__ = (function (){var statearr_31912 = f__30704__auto__();
(statearr_31912[(6)] = c__30703__auto__);

return statearr_31912;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30705__auto__);
}));

return c__30703__auto__;
}));

(cljs.core.async.onto_chan_BANG_.cljs$lang$maxFixedArity = 3);

/**
 * Creates and returns a channel which contains the contents of coll,
 *   closing when exhausted.
 */
cljs.core.async.to_chan_BANG_ = (function cljs$core$async$to_chan_BANG_(coll){
var ch = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(cljs.core.bounded_count((100),coll));
cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$2(ch,coll);

return ch;
});
/**
 * Deprecated - use onto-chan!
 */
cljs.core.async.onto_chan = (function cljs$core$async$onto_chan(var_args){
var G__31921 = arguments.length;
switch (G__31921) {
case 2:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2 = (function (ch,coll){
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3(ch,coll,true);
}));

(cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3 = (function (ch,coll,close_QMARK_){
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3(ch,coll,close_QMARK_);
}));

(cljs.core.async.onto_chan.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - use to-chan!
 */
cljs.core.async.to_chan = (function cljs$core$async$to_chan(coll){
return cljs.core.async.to_chan_BANG_(coll);
});

/**
 * @interface
 */
cljs.core.async.Mux = function(){};

var cljs$core$async$Mux$muxch_STAR_$dyn_34879 = (function (_){
var x__4428__auto__ = (((_ == null))?null:_);
var m__4429__auto__ = (cljs.core.async.muxch_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__4429__auto__.call(null,_));
} else {
var m__4426__auto__ = (cljs.core.async.muxch_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__4426__auto__.call(null,_));
} else {
throw cljs.core.missing_protocol("Mux.muxch*",_);
}
}
});
cljs.core.async.muxch_STAR_ = (function cljs$core$async$muxch_STAR_(_){
if((((!((_ == null)))) && ((!((_.cljs$core$async$Mux$muxch_STAR_$arity$1 == null)))))){
return _.cljs$core$async$Mux$muxch_STAR_$arity$1(_);
} else {
return cljs$core$async$Mux$muxch_STAR_$dyn_34879(_);
}
});


/**
 * @interface
 */
cljs.core.async.Mult = function(){};

var cljs$core$async$Mult$tap_STAR_$dyn_34882 = (function (m,ch,close_QMARK_){
var x__4428__auto__ = (((m == null))?null:m);
var m__4429__auto__ = (cljs.core.async.tap_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$3 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$3(m,ch,close_QMARK_) : m__4429__auto__.call(null,m,ch,close_QMARK_));
} else {
var m__4426__auto__ = (cljs.core.async.tap_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$3 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$3(m,ch,close_QMARK_) : m__4426__auto__.call(null,m,ch,close_QMARK_));
} else {
throw cljs.core.missing_protocol("Mult.tap*",m);
}
}
});
cljs.core.async.tap_STAR_ = (function cljs$core$async$tap_STAR_(m,ch,close_QMARK_){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$tap_STAR_$arity$3 == null)))))){
return m.cljs$core$async$Mult$tap_STAR_$arity$3(m,ch,close_QMARK_);
} else {
return cljs$core$async$Mult$tap_STAR_$dyn_34882(m,ch,close_QMARK_);
}
});

var cljs$core$async$Mult$untap_STAR_$dyn_34894 = (function (m,ch){
var x__4428__auto__ = (((m == null))?null:m);
var m__4429__auto__ = (cljs.core.async.untap_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4429__auto__.call(null,m,ch));
} else {
var m__4426__auto__ = (cljs.core.async.untap_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4426__auto__.call(null,m,ch));
} else {
throw cljs.core.missing_protocol("Mult.untap*",m);
}
}
});
cljs.core.async.untap_STAR_ = (function cljs$core$async$untap_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$untap_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mult$untap_STAR_$arity$2(m,ch);
} else {
return cljs$core$async$Mult$untap_STAR_$dyn_34894(m,ch);
}
});

var cljs$core$async$Mult$untap_all_STAR_$dyn_34904 = (function (m){
var x__4428__auto__ = (((m == null))?null:m);
var m__4429__auto__ = (cljs.core.async.untap_all_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__4429__auto__.call(null,m));
} else {
var m__4426__auto__ = (cljs.core.async.untap_all_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__4426__auto__.call(null,m));
} else {
throw cljs.core.missing_protocol("Mult.untap-all*",m);
}
}
});
cljs.core.async.untap_all_STAR_ = (function cljs$core$async$untap_all_STAR_(m){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$untap_all_STAR_$arity$1 == null)))))){
return m.cljs$core$async$Mult$untap_all_STAR_$arity$1(m);
} else {
return cljs$core$async$Mult$untap_all_STAR_$dyn_34904(m);
}
});

/**
 * Creates and returns a mult(iple) of the supplied channel. Channels
 *   containing copies of the channel can be created with 'tap', and
 *   detached with 'untap'.
 * 
 *   Each item is distributed to all taps in parallel and synchronously,
 *   i.e. each tap must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow taps from holding up the mult.
 * 
 *   Items received when there are no taps get dropped.
 * 
 *   If a tap puts to a closed channel, it will be removed from the mult.
 */
cljs.core.async.mult = (function cljs$core$async$mult(ch){
var cs = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var m = (function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async31955 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.Mult}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async31955 = (function (ch,cs,meta31956){
this.ch = ch;
this.cs = cs;
this.meta31956 = meta31956;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async31955.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_31957,meta31956__$1){
var self__ = this;
var _31957__$1 = this;
return (new cljs.core.async.t_cljs$core$async31955(self__.ch,self__.cs,meta31956__$1));
}));

(cljs.core.async.t_cljs$core$async31955.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_31957){
var self__ = this;
var _31957__$1 = this;
return self__.meta31956;
}));

(cljs.core.async.t_cljs$core$async31955.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async31955.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
}));

(cljs.core.async.t_cljs$core$async31955.prototype.cljs$core$async$Mult$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async31955.prototype.cljs$core$async$Mult$tap_STAR_$arity$3 = (function (_,ch__$1,close_QMARK_){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(self__.cs,cljs.core.assoc,ch__$1,close_QMARK_);

return null;
}));

(cljs.core.async.t_cljs$core$async31955.prototype.cljs$core$async$Mult$untap_STAR_$arity$2 = (function (_,ch__$1){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.cs,cljs.core.dissoc,ch__$1);

return null;
}));

(cljs.core.async.t_cljs$core$async31955.prototype.cljs$core$async$Mult$untap_all_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_(self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return null;
}));

(cljs.core.async.t_cljs$core$async31955.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"meta31956","meta31956",-2060337444,null)], null);
}));

(cljs.core.async.t_cljs$core$async31955.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async31955.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async31955");

(cljs.core.async.t_cljs$core$async31955.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async31955");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async31955.
 */
cljs.core.async.__GT_t_cljs$core$async31955 = (function cljs$core$async$mult_$___GT_t_cljs$core$async31955(ch__$1,cs__$1,meta31956){
return (new cljs.core.async.t_cljs$core$async31955(ch__$1,cs__$1,meta31956));
});

}

return (new cljs.core.async.t_cljs$core$async31955(ch,cs,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var dchan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
var dctr = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
var done = (function (_){
if((cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(dchan,true);
} else {
return null;
}
});
var c__30703__auto___34917 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30704__auto__ = (function (){var switch__30539__auto__ = (function (state_32119){
var state_val_32120 = (state_32119[(1)]);
if((state_val_32120 === (7))){
var inst_32115 = (state_32119[(2)]);
var state_32119__$1 = state_32119;
var statearr_32121_34918 = state_32119__$1;
(statearr_32121_34918[(2)] = inst_32115);

(statearr_32121_34918[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32120 === (20))){
var inst_32006 = (state_32119[(7)]);
var inst_32027 = cljs.core.first(inst_32006);
var inst_32028 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_32027,(0),null);
var inst_32029 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_32027,(1),null);
var state_32119__$1 = (function (){var statearr_32122 = state_32119;
(statearr_32122[(8)] = inst_32028);

return statearr_32122;
})();
if(cljs.core.truth_(inst_32029)){
var statearr_32123_34927 = state_32119__$1;
(statearr_32123_34927[(1)] = (22));

} else {
var statearr_32124_34928 = state_32119__$1;
(statearr_32124_34928[(1)] = (23));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32120 === (27))){
var inst_32065 = (state_32119[(9)]);
var inst_32057 = (state_32119[(10)]);
var inst_31971 = (state_32119[(11)]);
var inst_32059 = (state_32119[(12)]);
var inst_32065__$1 = cljs.core._nth(inst_32057,inst_32059);
var inst_32066 = cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3(inst_32065__$1,inst_31971,done);
var state_32119__$1 = (function (){var statearr_32129 = state_32119;
(statearr_32129[(9)] = inst_32065__$1);

return statearr_32129;
})();
if(cljs.core.truth_(inst_32066)){
var statearr_32130_34933 = state_32119__$1;
(statearr_32130_34933[(1)] = (30));

} else {
var statearr_32131_34934 = state_32119__$1;
(statearr_32131_34934[(1)] = (31));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32120 === (1))){
var state_32119__$1 = state_32119;
var statearr_32134_34936 = state_32119__$1;
(statearr_32134_34936[(2)] = null);

(statearr_32134_34936[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32120 === (24))){
var inst_32006 = (state_32119[(7)]);
var inst_32034 = (state_32119[(2)]);
var inst_32035 = cljs.core.next(inst_32006);
var inst_31980 = inst_32035;
var inst_31981 = null;
var inst_31982 = (0);
var inst_31983 = (0);
var state_32119__$1 = (function (){var statearr_32135 = state_32119;
(statearr_32135[(13)] = inst_32034);

(statearr_32135[(14)] = inst_31982);

(statearr_32135[(15)] = inst_31980);

(statearr_32135[(16)] = inst_31981);

(statearr_32135[(17)] = inst_31983);

return statearr_32135;
})();
var statearr_32136_34940 = state_32119__$1;
(statearr_32136_34940[(2)] = null);

(statearr_32136_34940[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32120 === (39))){
var state_32119__$1 = state_32119;
var statearr_32143_34947 = state_32119__$1;
(statearr_32143_34947[(2)] = null);

(statearr_32143_34947[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32120 === (4))){
var inst_31971 = (state_32119[(11)]);
var inst_31971__$1 = (state_32119[(2)]);
var inst_31972 = (inst_31971__$1 == null);
var state_32119__$1 = (function (){var statearr_32144 = state_32119;
(statearr_32144[(11)] = inst_31971__$1);

return statearr_32144;
})();
if(cljs.core.truth_(inst_31972)){
var statearr_32145_34951 = state_32119__$1;
(statearr_32145_34951[(1)] = (5));

} else {
var statearr_32146_34952 = state_32119__$1;
(statearr_32146_34952[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32120 === (15))){
var inst_31982 = (state_32119[(14)]);
var inst_31980 = (state_32119[(15)]);
var inst_31981 = (state_32119[(16)]);
var inst_31983 = (state_32119[(17)]);
var inst_32002 = (state_32119[(2)]);
var inst_32003 = (inst_31983 + (1));
var tmp32137 = inst_31982;
var tmp32138 = inst_31980;
var tmp32139 = inst_31981;
var inst_31980__$1 = tmp32138;
var inst_31981__$1 = tmp32139;
var inst_31982__$1 = tmp32137;
var inst_31983__$1 = inst_32003;
var state_32119__$1 = (function (){var statearr_32150 = state_32119;
(statearr_32150[(14)] = inst_31982__$1);

(statearr_32150[(18)] = inst_32002);

(statearr_32150[(15)] = inst_31980__$1);

(statearr_32150[(16)] = inst_31981__$1);

(statearr_32150[(17)] = inst_31983__$1);

return statearr_32150;
})();
var statearr_32152_34963 = state_32119__$1;
(statearr_32152_34963[(2)] = null);

(statearr_32152_34963[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32120 === (21))){
var inst_32038 = (state_32119[(2)]);
var state_32119__$1 = state_32119;
var statearr_32156_34969 = state_32119__$1;
(statearr_32156_34969[(2)] = inst_32038);

(statearr_32156_34969[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32120 === (31))){
var inst_32065 = (state_32119[(9)]);
var inst_32069 = m.cljs$core$async$Mult$untap_STAR_$arity$2(null,inst_32065);
var state_32119__$1 = state_32119;
var statearr_32159_34977 = state_32119__$1;
(statearr_32159_34977[(2)] = inst_32069);

(statearr_32159_34977[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32120 === (32))){
var inst_32056 = (state_32119[(19)]);
var inst_32057 = (state_32119[(10)]);
var inst_32058 = (state_32119[(20)]);
var inst_32059 = (state_32119[(12)]);
var inst_32071 = (state_32119[(2)]);
var inst_32072 = (inst_32059 + (1));
var tmp32153 = inst_32056;
var tmp32154 = inst_32057;
var tmp32155 = inst_32058;
var inst_32056__$1 = tmp32153;
var inst_32057__$1 = tmp32154;
var inst_32058__$1 = tmp32155;
var inst_32059__$1 = inst_32072;
var state_32119__$1 = (function (){var statearr_32161 = state_32119;
(statearr_32161[(19)] = inst_32056__$1);

(statearr_32161[(10)] = inst_32057__$1);

(statearr_32161[(20)] = inst_32058__$1);

(statearr_32161[(21)] = inst_32071);

(statearr_32161[(12)] = inst_32059__$1);

return statearr_32161;
})();
var statearr_32162_34987 = state_32119__$1;
(statearr_32162_34987[(2)] = null);

(statearr_32162_34987[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32120 === (40))){
var inst_32084 = (state_32119[(22)]);
var inst_32088 = m.cljs$core$async$Mult$untap_STAR_$arity$2(null,inst_32084);
var state_32119__$1 = state_32119;
var statearr_32163_34990 = state_32119__$1;
(statearr_32163_34990[(2)] = inst_32088);

(statearr_32163_34990[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32120 === (33))){
var inst_32075 = (state_32119[(23)]);
var inst_32077 = cljs.core.chunked_seq_QMARK_(inst_32075);
var state_32119__$1 = state_32119;
if(inst_32077){
var statearr_32166_34992 = state_32119__$1;
(statearr_32166_34992[(1)] = (36));

} else {
var statearr_32167_34994 = state_32119__$1;
(statearr_32167_34994[(1)] = (37));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32120 === (13))){
var inst_31992 = (state_32119[(24)]);
var inst_31999 = cljs.core.async.close_BANG_(inst_31992);
var state_32119__$1 = state_32119;
var statearr_32169_34995 = state_32119__$1;
(statearr_32169_34995[(2)] = inst_31999);

(statearr_32169_34995[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32120 === (22))){
var inst_32028 = (state_32119[(8)]);
var inst_32031 = cljs.core.async.close_BANG_(inst_32028);
var state_32119__$1 = state_32119;
var statearr_32170_34997 = state_32119__$1;
(statearr_32170_34997[(2)] = inst_32031);

(statearr_32170_34997[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32120 === (36))){
var inst_32075 = (state_32119[(23)]);
var inst_32079 = cljs.core.chunk_first(inst_32075);
var inst_32080 = cljs.core.chunk_rest(inst_32075);
var inst_32081 = cljs.core.count(inst_32079);
var inst_32056 = inst_32080;
var inst_32057 = inst_32079;
var inst_32058 = inst_32081;
var inst_32059 = (0);
var state_32119__$1 = (function (){var statearr_32171 = state_32119;
(statearr_32171[(19)] = inst_32056);

(statearr_32171[(10)] = inst_32057);

(statearr_32171[(20)] = inst_32058);

(statearr_32171[(12)] = inst_32059);

return statearr_32171;
})();
var statearr_32172_34998 = state_32119__$1;
(statearr_32172_34998[(2)] = null);

(statearr_32172_34998[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32120 === (41))){
var inst_32075 = (state_32119[(23)]);
var inst_32090 = (state_32119[(2)]);
var inst_32091 = cljs.core.next(inst_32075);
var inst_32056 = inst_32091;
var inst_32057 = null;
var inst_32058 = (0);
var inst_32059 = (0);
var state_32119__$1 = (function (){var statearr_32174 = state_32119;
(statearr_32174[(19)] = inst_32056);

(statearr_32174[(10)] = inst_32057);

(statearr_32174[(20)] = inst_32058);

(statearr_32174[(25)] = inst_32090);

(statearr_32174[(12)] = inst_32059);

return statearr_32174;
})();
var statearr_32176_35004 = state_32119__$1;
(statearr_32176_35004[(2)] = null);

(statearr_32176_35004[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32120 === (43))){
var state_32119__$1 = state_32119;
var statearr_32178_35009 = state_32119__$1;
(statearr_32178_35009[(2)] = null);

(statearr_32178_35009[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32120 === (29))){
var inst_32102 = (state_32119[(2)]);
var state_32119__$1 = state_32119;
var statearr_32181_35010 = state_32119__$1;
(statearr_32181_35010[(2)] = inst_32102);

(statearr_32181_35010[(1)] = (26));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32120 === (44))){
var inst_32111 = (state_32119[(2)]);
var state_32119__$1 = (function (){var statearr_32182 = state_32119;
(statearr_32182[(26)] = inst_32111);

return statearr_32182;
})();
var statearr_32183_35015 = state_32119__$1;
(statearr_32183_35015[(2)] = null);

(statearr_32183_35015[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32120 === (6))){
var inst_32048 = (state_32119[(27)]);
var inst_32047 = cljs.core.deref(cs);
var inst_32048__$1 = cljs.core.keys(inst_32047);
var inst_32049 = cljs.core.count(inst_32048__$1);
var inst_32050 = cljs.core.reset_BANG_(dctr,inst_32049);
var inst_32055 = cljs.core.seq(inst_32048__$1);
var inst_32056 = inst_32055;
var inst_32057 = null;
var inst_32058 = (0);
var inst_32059 = (0);
var state_32119__$1 = (function (){var statearr_32186 = state_32119;
(statearr_32186[(28)] = inst_32050);

(statearr_32186[(19)] = inst_32056);

(statearr_32186[(10)] = inst_32057);

(statearr_32186[(20)] = inst_32058);

(statearr_32186[(27)] = inst_32048__$1);

(statearr_32186[(12)] = inst_32059);

return statearr_32186;
})();
var statearr_32190_35026 = state_32119__$1;
(statearr_32190_35026[(2)] = null);

(statearr_32190_35026[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32120 === (28))){
var inst_32056 = (state_32119[(19)]);
var inst_32075 = (state_32119[(23)]);
var inst_32075__$1 = cljs.core.seq(inst_32056);
var state_32119__$1 = (function (){var statearr_32194 = state_32119;
(statearr_32194[(23)] = inst_32075__$1);

return statearr_32194;
})();
if(inst_32075__$1){
var statearr_32196_35031 = state_32119__$1;
(statearr_32196_35031[(1)] = (33));

} else {
var statearr_32198_35032 = state_32119__$1;
(statearr_32198_35032[(1)] = (34));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32120 === (25))){
var inst_32058 = (state_32119[(20)]);
var inst_32059 = (state_32119[(12)]);
var inst_32062 = (inst_32059 < inst_32058);
var inst_32063 = inst_32062;
var state_32119__$1 = state_32119;
if(cljs.core.truth_(inst_32063)){
var statearr_32200_35036 = state_32119__$1;
(statearr_32200_35036[(1)] = (27));

} else {
var statearr_32201_35037 = state_32119__$1;
(statearr_32201_35037[(1)] = (28));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32120 === (34))){
var state_32119__$1 = state_32119;
var statearr_32202_35041 = state_32119__$1;
(statearr_32202_35041[(2)] = null);

(statearr_32202_35041[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32120 === (17))){
var state_32119__$1 = state_32119;
var statearr_32205_35043 = state_32119__$1;
(statearr_32205_35043[(2)] = null);

(statearr_32205_35043[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32120 === (3))){
var inst_32117 = (state_32119[(2)]);
var state_32119__$1 = state_32119;
return cljs.core.async.impl.ioc_helpers.return_chan(state_32119__$1,inst_32117);
} else {
if((state_val_32120 === (12))){
var inst_32043 = (state_32119[(2)]);
var state_32119__$1 = state_32119;
var statearr_32212_35048 = state_32119__$1;
(statearr_32212_35048[(2)] = inst_32043);

(statearr_32212_35048[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32120 === (2))){
var state_32119__$1 = state_32119;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_32119__$1,(4),ch);
} else {
if((state_val_32120 === (23))){
var state_32119__$1 = state_32119;
var statearr_32214_35049 = state_32119__$1;
(statearr_32214_35049[(2)] = null);

(statearr_32214_35049[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32120 === (35))){
var inst_32100 = (state_32119[(2)]);
var state_32119__$1 = state_32119;
var statearr_32215_35055 = state_32119__$1;
(statearr_32215_35055[(2)] = inst_32100);

(statearr_32215_35055[(1)] = (29));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32120 === (19))){
var inst_32006 = (state_32119[(7)]);
var inst_32010 = cljs.core.chunk_first(inst_32006);
var inst_32012 = cljs.core.chunk_rest(inst_32006);
var inst_32013 = cljs.core.count(inst_32010);
var inst_31980 = inst_32012;
var inst_31981 = inst_32010;
var inst_31982 = inst_32013;
var inst_31983 = (0);
var state_32119__$1 = (function (){var statearr_32221 = state_32119;
(statearr_32221[(14)] = inst_31982);

(statearr_32221[(15)] = inst_31980);

(statearr_32221[(16)] = inst_31981);

(statearr_32221[(17)] = inst_31983);

return statearr_32221;
})();
var statearr_32222_35067 = state_32119__$1;
(statearr_32222_35067[(2)] = null);

(statearr_32222_35067[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32120 === (11))){
var inst_31980 = (state_32119[(15)]);
var inst_32006 = (state_32119[(7)]);
var inst_32006__$1 = cljs.core.seq(inst_31980);
var state_32119__$1 = (function (){var statearr_32224 = state_32119;
(statearr_32224[(7)] = inst_32006__$1);

return statearr_32224;
})();
if(inst_32006__$1){
var statearr_32227_35075 = state_32119__$1;
(statearr_32227_35075[(1)] = (16));

} else {
var statearr_32229_35076 = state_32119__$1;
(statearr_32229_35076[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32120 === (9))){
var inst_32045 = (state_32119[(2)]);
var state_32119__$1 = state_32119;
var statearr_32235_35081 = state_32119__$1;
(statearr_32235_35081[(2)] = inst_32045);

(statearr_32235_35081[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32120 === (5))){
var inst_31978 = cljs.core.deref(cs);
var inst_31979 = cljs.core.seq(inst_31978);
var inst_31980 = inst_31979;
var inst_31981 = null;
var inst_31982 = (0);
var inst_31983 = (0);
var state_32119__$1 = (function (){var statearr_32238 = state_32119;
(statearr_32238[(14)] = inst_31982);

(statearr_32238[(15)] = inst_31980);

(statearr_32238[(16)] = inst_31981);

(statearr_32238[(17)] = inst_31983);

return statearr_32238;
})();
var statearr_32239_35085 = state_32119__$1;
(statearr_32239_35085[(2)] = null);

(statearr_32239_35085[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32120 === (14))){
var state_32119__$1 = state_32119;
var statearr_32240_35087 = state_32119__$1;
(statearr_32240_35087[(2)] = null);

(statearr_32240_35087[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32120 === (45))){
var inst_32108 = (state_32119[(2)]);
var state_32119__$1 = state_32119;
var statearr_32241_35089 = state_32119__$1;
(statearr_32241_35089[(2)] = inst_32108);

(statearr_32241_35089[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32120 === (26))){
var inst_32048 = (state_32119[(27)]);
var inst_32104 = (state_32119[(2)]);
var inst_32105 = cljs.core.seq(inst_32048);
var state_32119__$1 = (function (){var statearr_32242 = state_32119;
(statearr_32242[(29)] = inst_32104);

return statearr_32242;
})();
if(inst_32105){
var statearr_32243_35096 = state_32119__$1;
(statearr_32243_35096[(1)] = (42));

} else {
var statearr_32244_35097 = state_32119__$1;
(statearr_32244_35097[(1)] = (43));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32120 === (16))){
var inst_32006 = (state_32119[(7)]);
var inst_32008 = cljs.core.chunked_seq_QMARK_(inst_32006);
var state_32119__$1 = state_32119;
if(inst_32008){
var statearr_32247_35098 = state_32119__$1;
(statearr_32247_35098[(1)] = (19));

} else {
var statearr_32252_35099 = state_32119__$1;
(statearr_32252_35099[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32120 === (38))){
var inst_32097 = (state_32119[(2)]);
var state_32119__$1 = state_32119;
var statearr_32254_35102 = state_32119__$1;
(statearr_32254_35102[(2)] = inst_32097);

(statearr_32254_35102[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32120 === (30))){
var state_32119__$1 = state_32119;
var statearr_32256_35105 = state_32119__$1;
(statearr_32256_35105[(2)] = null);

(statearr_32256_35105[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32120 === (10))){
var inst_31981 = (state_32119[(16)]);
var inst_31983 = (state_32119[(17)]);
var inst_31991 = cljs.core._nth(inst_31981,inst_31983);
var inst_31992 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_31991,(0),null);
var inst_31993 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_31991,(1),null);
var state_32119__$1 = (function (){var statearr_32259 = state_32119;
(statearr_32259[(24)] = inst_31992);

return statearr_32259;
})();
if(cljs.core.truth_(inst_31993)){
var statearr_32264_35114 = state_32119__$1;
(statearr_32264_35114[(1)] = (13));

} else {
var statearr_32266_35115 = state_32119__$1;
(statearr_32266_35115[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32120 === (18))){
var inst_32041 = (state_32119[(2)]);
var state_32119__$1 = state_32119;
var statearr_32268_35118 = state_32119__$1;
(statearr_32268_35118[(2)] = inst_32041);

(statearr_32268_35118[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32120 === (42))){
var state_32119__$1 = state_32119;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_32119__$1,(45),dchan);
} else {
if((state_val_32120 === (37))){
var inst_32084 = (state_32119[(22)]);
var inst_32075 = (state_32119[(23)]);
var inst_31971 = (state_32119[(11)]);
var inst_32084__$1 = cljs.core.first(inst_32075);
var inst_32085 = cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3(inst_32084__$1,inst_31971,done);
var state_32119__$1 = (function (){var statearr_32269 = state_32119;
(statearr_32269[(22)] = inst_32084__$1);

return statearr_32269;
})();
if(cljs.core.truth_(inst_32085)){
var statearr_32271_35127 = state_32119__$1;
(statearr_32271_35127[(1)] = (39));

} else {
var statearr_32272_35128 = state_32119__$1;
(statearr_32272_35128[(1)] = (40));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32120 === (8))){
var inst_31982 = (state_32119[(14)]);
var inst_31983 = (state_32119[(17)]);
var inst_31985 = (inst_31983 < inst_31982);
var inst_31986 = inst_31985;
var state_32119__$1 = state_32119;
if(cljs.core.truth_(inst_31986)){
var statearr_32277_35132 = state_32119__$1;
(statearr_32277_35132[(1)] = (10));

} else {
var statearr_32278_35139 = state_32119__$1;
(statearr_32278_35139[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$mult_$_state_machine__30540__auto__ = null;
var cljs$core$async$mult_$_state_machine__30540__auto____0 = (function (){
var statearr_32279 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_32279[(0)] = cljs$core$async$mult_$_state_machine__30540__auto__);

(statearr_32279[(1)] = (1));

return statearr_32279;
});
var cljs$core$async$mult_$_state_machine__30540__auto____1 = (function (state_32119){
while(true){
var ret_value__30541__auto__ = (function (){try{while(true){
var result__30542__auto__ = switch__30539__auto__(state_32119);
if(cljs.core.keyword_identical_QMARK_(result__30542__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30542__auto__;
}
break;
}
}catch (e32280){var ex__30543__auto__ = e32280;
var statearr_32281_35144 = state_32119;
(statearr_32281_35144[(2)] = ex__30543__auto__);


if(cljs.core.seq((state_32119[(4)]))){
var statearr_32282_35146 = state_32119;
(statearr_32282_35146[(1)] = cljs.core.first((state_32119[(4)])));

} else {
throw ex__30543__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30541__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35147 = state_32119;
state_32119 = G__35147;
continue;
} else {
return ret_value__30541__auto__;
}
break;
}
});
cljs$core$async$mult_$_state_machine__30540__auto__ = function(state_32119){
switch(arguments.length){
case 0:
return cljs$core$async$mult_$_state_machine__30540__auto____0.call(this);
case 1:
return cljs$core$async$mult_$_state_machine__30540__auto____1.call(this,state_32119);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mult_$_state_machine__30540__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mult_$_state_machine__30540__auto____0;
cljs$core$async$mult_$_state_machine__30540__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mult_$_state_machine__30540__auto____1;
return cljs$core$async$mult_$_state_machine__30540__auto__;
})()
})();
var state__30705__auto__ = (function (){var statearr_32286 = f__30704__auto__();
(statearr_32286[(6)] = c__30703__auto___34917);

return statearr_32286;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30705__auto__);
}));


return m;
});
/**
 * Copies the mult source onto the supplied channel.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.tap = (function cljs$core$async$tap(var_args){
var G__32291 = arguments.length;
switch (G__32291) {
case 2:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2 = (function (mult,ch){
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3(mult,ch,true);
}));

(cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3 = (function (mult,ch,close_QMARK_){
cljs.core.async.tap_STAR_(mult,ch,close_QMARK_);

return ch;
}));

(cljs.core.async.tap.cljs$lang$maxFixedArity = 3);

/**
 * Disconnects a target channel from a mult
 */
cljs.core.async.untap = (function cljs$core$async$untap(mult,ch){
return cljs.core.async.untap_STAR_(mult,ch);
});
/**
 * Disconnects all target channels from a mult
 */
cljs.core.async.untap_all = (function cljs$core$async$untap_all(mult){
return cljs.core.async.untap_all_STAR_(mult);
});

/**
 * @interface
 */
cljs.core.async.Mix = function(){};

var cljs$core$async$Mix$admix_STAR_$dyn_35157 = (function (m,ch){
var x__4428__auto__ = (((m == null))?null:m);
var m__4429__auto__ = (cljs.core.async.admix_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4429__auto__.call(null,m,ch));
} else {
var m__4426__auto__ = (cljs.core.async.admix_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4426__auto__.call(null,m,ch));
} else {
throw cljs.core.missing_protocol("Mix.admix*",m);
}
}
});
cljs.core.async.admix_STAR_ = (function cljs$core$async$admix_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$admix_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$admix_STAR_$arity$2(m,ch);
} else {
return cljs$core$async$Mix$admix_STAR_$dyn_35157(m,ch);
}
});

var cljs$core$async$Mix$unmix_STAR_$dyn_35162 = (function (m,ch){
var x__4428__auto__ = (((m == null))?null:m);
var m__4429__auto__ = (cljs.core.async.unmix_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4429__auto__.call(null,m,ch));
} else {
var m__4426__auto__ = (cljs.core.async.unmix_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4426__auto__.call(null,m,ch));
} else {
throw cljs.core.missing_protocol("Mix.unmix*",m);
}
}
});
cljs.core.async.unmix_STAR_ = (function cljs$core$async$unmix_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$unmix_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$unmix_STAR_$arity$2(m,ch);
} else {
return cljs$core$async$Mix$unmix_STAR_$dyn_35162(m,ch);
}
});

var cljs$core$async$Mix$unmix_all_STAR_$dyn_35171 = (function (m){
var x__4428__auto__ = (((m == null))?null:m);
var m__4429__auto__ = (cljs.core.async.unmix_all_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__4429__auto__.call(null,m));
} else {
var m__4426__auto__ = (cljs.core.async.unmix_all_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__4426__auto__.call(null,m));
} else {
throw cljs.core.missing_protocol("Mix.unmix-all*",m);
}
}
});
cljs.core.async.unmix_all_STAR_ = (function cljs$core$async$unmix_all_STAR_(m){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$unmix_all_STAR_$arity$1 == null)))))){
return m.cljs$core$async$Mix$unmix_all_STAR_$arity$1(m);
} else {
return cljs$core$async$Mix$unmix_all_STAR_$dyn_35171(m);
}
});

var cljs$core$async$Mix$toggle_STAR_$dyn_35174 = (function (m,state_map){
var x__4428__auto__ = (((m == null))?null:m);
var m__4429__auto__ = (cljs.core.async.toggle_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$2(m,state_map) : m__4429__auto__.call(null,m,state_map));
} else {
var m__4426__auto__ = (cljs.core.async.toggle_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$2(m,state_map) : m__4426__auto__.call(null,m,state_map));
} else {
throw cljs.core.missing_protocol("Mix.toggle*",m);
}
}
});
cljs.core.async.toggle_STAR_ = (function cljs$core$async$toggle_STAR_(m,state_map){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$toggle_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$toggle_STAR_$arity$2(m,state_map);
} else {
return cljs$core$async$Mix$toggle_STAR_$dyn_35174(m,state_map);
}
});

var cljs$core$async$Mix$solo_mode_STAR_$dyn_35191 = (function (m,mode){
var x__4428__auto__ = (((m == null))?null:m);
var m__4429__auto__ = (cljs.core.async.solo_mode_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$2(m,mode) : m__4429__auto__.call(null,m,mode));
} else {
var m__4426__auto__ = (cljs.core.async.solo_mode_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$2(m,mode) : m__4426__auto__.call(null,m,mode));
} else {
throw cljs.core.missing_protocol("Mix.solo-mode*",m);
}
}
});
cljs.core.async.solo_mode_STAR_ = (function cljs$core$async$solo_mode_STAR_(m,mode){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$solo_mode_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$solo_mode_STAR_$arity$2(m,mode);
} else {
return cljs$core$async$Mix$solo_mode_STAR_$dyn_35191(m,mode);
}
});

cljs.core.async.ioc_alts_BANG_ = (function cljs$core$async$ioc_alts_BANG_(var_args){
var args__4742__auto__ = [];
var len__4736__auto___35202 = arguments.length;
var i__4737__auto___35203 = (0);
while(true){
if((i__4737__auto___35203 < len__4736__auto___35202)){
args__4742__auto__.push((arguments[i__4737__auto___35203]));

var G__35205 = (i__4737__auto___35203 + (1));
i__4737__auto___35203 = G__35205;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((3) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((3)),(0),null)):null);
return cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),argseq__4743__auto__);
});

(cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (state,cont_block,ports,p__32318){
var map__32319 = p__32318;
var map__32319__$1 = (((((!((map__32319 == null))))?(((((map__32319.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__32319.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__32319):map__32319);
var opts = map__32319__$1;
var statearr_32322_35210 = state;
(statearr_32322_35210[(1)] = cont_block);


var temp__5735__auto__ = cljs.core.async.do_alts((function (val){
var statearr_32325_35211 = state;
(statearr_32325_35211[(2)] = val);


return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state);
}),ports,opts);
if(cljs.core.truth_(temp__5735__auto__)){
var cb = temp__5735__auto__;
var statearr_32326_35212 = state;
(statearr_32326_35212[(2)] = cljs.core.deref(cb));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}));

(cljs.core.async.ioc_alts_BANG_.cljs$lang$maxFixedArity = (3));

/** @this {Function} */
(cljs.core.async.ioc_alts_BANG_.cljs$lang$applyTo = (function (seq32314){
var G__32315 = cljs.core.first(seq32314);
var seq32314__$1 = cljs.core.next(seq32314);
var G__32316 = cljs.core.first(seq32314__$1);
var seq32314__$2 = cljs.core.next(seq32314__$1);
var G__32317 = cljs.core.first(seq32314__$2);
var seq32314__$3 = cljs.core.next(seq32314__$2);
var self__4723__auto__ = this;
return self__4723__auto__.cljs$core$IFn$_invoke$arity$variadic(G__32315,G__32316,G__32317,seq32314__$3);
}));

/**
 * Creates and returns a mix of one or more input channels which will
 *   be put on the supplied out channel. Input sources can be added to
 *   the mix with 'admix', and removed with 'unmix'. A mix supports
 *   soloing, muting and pausing multiple inputs atomically using
 *   'toggle', and can solo using either muting or pausing as determined
 *   by 'solo-mode'.
 * 
 *   Each channel can have zero or more boolean modes set via 'toggle':
 * 
 *   :solo - when true, only this (ond other soloed) channel(s) will appear
 *        in the mix output channel. :mute and :pause states of soloed
 *        channels are ignored. If solo-mode is :mute, non-soloed
 *        channels are muted, if :pause, non-soloed channels are
 *        paused.
 * 
 *   :mute - muted channels will have their contents consumed but not included in the mix
 *   :pause - paused channels will not have their contents consumed (and thus also not included in the mix)
 */
cljs.core.async.mix = (function cljs$core$async$mix(out){
var cs = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var solo_modes = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"pause","pause",-2095325672),null,new cljs.core.Keyword(null,"mute","mute",1151223646),null], null), null);
var attrs = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(solo_modes,new cljs.core.Keyword(null,"solo","solo",-316350075));
var solo_mode = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"mute","mute",1151223646));
var change = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(cljs.core.async.sliding_buffer((1)));
var changed = (function (){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(change,true);
});
var pick = (function (attr,chs){
return cljs.core.reduce_kv((function (ret,c,v){
if(cljs.core.truth_((attr.cljs$core$IFn$_invoke$arity$1 ? attr.cljs$core$IFn$_invoke$arity$1(v) : attr.call(null,v)))){
return cljs.core.conj.cljs$core$IFn$_invoke$arity$2(ret,c);
} else {
return ret;
}
}),cljs.core.PersistentHashSet.EMPTY,chs);
});
var calc_state = (function (){
var chs = cljs.core.deref(cs);
var mode = cljs.core.deref(solo_mode);
var solos = pick(new cljs.core.Keyword(null,"solo","solo",-316350075),chs);
var pauses = pick(new cljs.core.Keyword(null,"pause","pause",-2095325672),chs);
return new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"solos","solos",1441458643),solos,new cljs.core.Keyword(null,"mutes","mutes",1068806309),pick(new cljs.core.Keyword(null,"mute","mute",1151223646),chs),new cljs.core.Keyword(null,"reads","reads",-1215067361),cljs.core.conj.cljs$core$IFn$_invoke$arity$2(((((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(mode,new cljs.core.Keyword(null,"pause","pause",-2095325672))) && ((!(cljs.core.empty_QMARK_(solos))))))?cljs.core.vec(solos):cljs.core.vec(cljs.core.remove.cljs$core$IFn$_invoke$arity$2(pauses,cljs.core.keys(chs)))),change)], null);
});
var m = (function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async32341 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mix}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async32341 = (function (change,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,meta32342){
this.change = change;
this.solo_mode = solo_mode;
this.pick = pick;
this.cs = cs;
this.calc_state = calc_state;
this.out = out;
this.changed = changed;
this.solo_modes = solo_modes;
this.attrs = attrs;
this.meta32342 = meta32342;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async32341.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_32343,meta32342__$1){
var self__ = this;
var _32343__$1 = this;
return (new cljs.core.async.t_cljs$core$async32341(self__.change,self__.solo_mode,self__.pick,self__.cs,self__.calc_state,self__.out,self__.changed,self__.solo_modes,self__.attrs,meta32342__$1));
}));

(cljs.core.async.t_cljs$core$async32341.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_32343){
var self__ = this;
var _32343__$1 = this;
return self__.meta32342;
}));

(cljs.core.async.t_cljs$core$async32341.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async32341.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.out;
}));

(cljs.core.async.t_cljs$core$async32341.prototype.cljs$core$async$Mix$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async32341.prototype.cljs$core$async$Mix$admix_STAR_$arity$2 = (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(self__.cs,cljs.core.assoc,ch,cljs.core.PersistentArrayMap.EMPTY);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async32341.prototype.cljs$core$async$Mix$unmix_STAR_$arity$2 = (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.cs,cljs.core.dissoc,ch);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async32341.prototype.cljs$core$async$Mix$unmix_all_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_(self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async32341.prototype.cljs$core$async$Mix$toggle_STAR_$arity$2 = (function (_,state_map){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.cs,cljs.core.partial.cljs$core$IFn$_invoke$arity$2(cljs.core.merge_with,cljs.core.merge),state_map);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async32341.prototype.cljs$core$async$Mix$solo_mode_STAR_$arity$2 = (function (_,mode){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_((self__.solo_modes.cljs$core$IFn$_invoke$arity$1 ? self__.solo_modes.cljs$core$IFn$_invoke$arity$1(mode) : self__.solo_modes.call(null,mode)))){
} else {
throw (new Error(["Assert failed: ",["mode must be one of: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(self__.solo_modes)].join(''),"\n","(solo-modes mode)"].join('')));
}

cljs.core.reset_BANG_(self__.solo_mode,mode);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async32341.getBasis = (function (){
return new cljs.core.PersistentVector(null, 10, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"change","change",477485025,null),new cljs.core.Symbol(null,"solo-mode","solo-mode",2031788074,null),new cljs.core.Symbol(null,"pick","pick",1300068175,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"calc-state","calc-state",-349968968,null),new cljs.core.Symbol(null,"out","out",729986010,null),new cljs.core.Symbol(null,"changed","changed",-2083710852,null),new cljs.core.Symbol(null,"solo-modes","solo-modes",882180540,null),new cljs.core.Symbol(null,"attrs","attrs",-450137186,null),new cljs.core.Symbol(null,"meta32342","meta32342",1110378798,null)], null);
}));

(cljs.core.async.t_cljs$core$async32341.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async32341.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async32341");

(cljs.core.async.t_cljs$core$async32341.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async32341");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async32341.
 */
cljs.core.async.__GT_t_cljs$core$async32341 = (function cljs$core$async$mix_$___GT_t_cljs$core$async32341(change__$1,solo_mode__$1,pick__$1,cs__$1,calc_state__$1,out__$1,changed__$1,solo_modes__$1,attrs__$1,meta32342){
return (new cljs.core.async.t_cljs$core$async32341(change__$1,solo_mode__$1,pick__$1,cs__$1,calc_state__$1,out__$1,changed__$1,solo_modes__$1,attrs__$1,meta32342));
});

}

return (new cljs.core.async.t_cljs$core$async32341(change,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var c__30703__auto___35242 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30704__auto__ = (function (){var switch__30539__auto__ = (function (state_32508){
var state_val_32509 = (state_32508[(1)]);
if((state_val_32509 === (7))){
var inst_32413 = (state_32508[(2)]);
var state_32508__$1 = state_32508;
var statearr_32514_35245 = state_32508__$1;
(statearr_32514_35245[(2)] = inst_32413);

(statearr_32514_35245[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32509 === (20))){
var inst_32426 = (state_32508[(7)]);
var state_32508__$1 = state_32508;
var statearr_32515_35246 = state_32508__$1;
(statearr_32515_35246[(2)] = inst_32426);

(statearr_32515_35246[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32509 === (27))){
var state_32508__$1 = state_32508;
var statearr_32516_35247 = state_32508__$1;
(statearr_32516_35247[(2)] = null);

(statearr_32516_35247[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32509 === (1))){
var inst_32398 = (state_32508[(8)]);
var inst_32398__$1 = calc_state();
var inst_32401 = (inst_32398__$1 == null);
var inst_32402 = cljs.core.not(inst_32401);
var state_32508__$1 = (function (){var statearr_32517 = state_32508;
(statearr_32517[(8)] = inst_32398__$1);

return statearr_32517;
})();
if(inst_32402){
var statearr_32518_35252 = state_32508__$1;
(statearr_32518_35252[(1)] = (2));

} else {
var statearr_32519_35253 = state_32508__$1;
(statearr_32519_35253[(1)] = (3));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32509 === (24))){
var inst_32466 = (state_32508[(9)]);
var inst_32481 = (state_32508[(10)]);
var inst_32450 = (state_32508[(11)]);
var inst_32481__$1 = (inst_32450.cljs$core$IFn$_invoke$arity$1 ? inst_32450.cljs$core$IFn$_invoke$arity$1(inst_32466) : inst_32450.call(null,inst_32466));
var state_32508__$1 = (function (){var statearr_32520 = state_32508;
(statearr_32520[(10)] = inst_32481__$1);

return statearr_32520;
})();
if(cljs.core.truth_(inst_32481__$1)){
var statearr_32521_35255 = state_32508__$1;
(statearr_32521_35255[(1)] = (29));

} else {
var statearr_32522_35256 = state_32508__$1;
(statearr_32522_35256[(1)] = (30));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32509 === (4))){
var inst_32417 = (state_32508[(2)]);
var state_32508__$1 = state_32508;
if(cljs.core.truth_(inst_32417)){
var statearr_32523_35259 = state_32508__$1;
(statearr_32523_35259[(1)] = (8));

} else {
var statearr_32524_35260 = state_32508__$1;
(statearr_32524_35260[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32509 === (15))){
var inst_32444 = (state_32508[(2)]);
var state_32508__$1 = state_32508;
if(cljs.core.truth_(inst_32444)){
var statearr_32526_35262 = state_32508__$1;
(statearr_32526_35262[(1)] = (19));

} else {
var statearr_32528_35263 = state_32508__$1;
(statearr_32528_35263[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32509 === (21))){
var inst_32449 = (state_32508[(12)]);
var inst_32449__$1 = (state_32508[(2)]);
var inst_32450 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_32449__$1,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_32451 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_32449__$1,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_32452 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_32449__$1,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var state_32508__$1 = (function (){var statearr_32530 = state_32508;
(statearr_32530[(12)] = inst_32449__$1);

(statearr_32530[(13)] = inst_32451);

(statearr_32530[(11)] = inst_32450);

return statearr_32530;
})();
return cljs.core.async.ioc_alts_BANG_(state_32508__$1,(22),inst_32452);
} else {
if((state_val_32509 === (31))){
var inst_32489 = (state_32508[(2)]);
var state_32508__$1 = state_32508;
if(cljs.core.truth_(inst_32489)){
var statearr_32531_35267 = state_32508__$1;
(statearr_32531_35267[(1)] = (32));

} else {
var statearr_32532_35268 = state_32508__$1;
(statearr_32532_35268[(1)] = (33));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32509 === (32))){
var inst_32465 = (state_32508[(14)]);
var state_32508__$1 = state_32508;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_32508__$1,(35),out,inst_32465);
} else {
if((state_val_32509 === (33))){
var inst_32449 = (state_32508[(12)]);
var inst_32426 = inst_32449;
var state_32508__$1 = (function (){var statearr_32533 = state_32508;
(statearr_32533[(7)] = inst_32426);

return statearr_32533;
})();
var statearr_32534_35270 = state_32508__$1;
(statearr_32534_35270[(2)] = null);

(statearr_32534_35270[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32509 === (13))){
var inst_32426 = (state_32508[(7)]);
var inst_32433 = inst_32426.cljs$lang$protocol_mask$partition0$;
var inst_32434 = (inst_32433 & (64));
var inst_32435 = inst_32426.cljs$core$ISeq$;
var inst_32436 = (cljs.core.PROTOCOL_SENTINEL === inst_32435);
var inst_32437 = ((inst_32434) || (inst_32436));
var state_32508__$1 = state_32508;
if(cljs.core.truth_(inst_32437)){
var statearr_32535_35282 = state_32508__$1;
(statearr_32535_35282[(1)] = (16));

} else {
var statearr_32536_35283 = state_32508__$1;
(statearr_32536_35283[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32509 === (22))){
var inst_32465 = (state_32508[(14)]);
var inst_32466 = (state_32508[(9)]);
var inst_32463 = (state_32508[(2)]);
var inst_32465__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_32463,(0),null);
var inst_32466__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_32463,(1),null);
var inst_32468 = (inst_32465__$1 == null);
var inst_32469 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_32466__$1,change);
var inst_32470 = ((inst_32468) || (inst_32469));
var state_32508__$1 = (function (){var statearr_32537 = state_32508;
(statearr_32537[(14)] = inst_32465__$1);

(statearr_32537[(9)] = inst_32466__$1);

return statearr_32537;
})();
if(cljs.core.truth_(inst_32470)){
var statearr_32538_35290 = state_32508__$1;
(statearr_32538_35290[(1)] = (23));

} else {
var statearr_32539_35291 = state_32508__$1;
(statearr_32539_35291[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32509 === (36))){
var inst_32449 = (state_32508[(12)]);
var inst_32426 = inst_32449;
var state_32508__$1 = (function (){var statearr_32540 = state_32508;
(statearr_32540[(7)] = inst_32426);

return statearr_32540;
})();
var statearr_32541_35292 = state_32508__$1;
(statearr_32541_35292[(2)] = null);

(statearr_32541_35292[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32509 === (29))){
var inst_32481 = (state_32508[(10)]);
var state_32508__$1 = state_32508;
var statearr_32542_35294 = state_32508__$1;
(statearr_32542_35294[(2)] = inst_32481);

(statearr_32542_35294[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32509 === (6))){
var state_32508__$1 = state_32508;
var statearr_32543_35295 = state_32508__$1;
(statearr_32543_35295[(2)] = false);

(statearr_32543_35295[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32509 === (28))){
var inst_32477 = (state_32508[(2)]);
var inst_32478 = calc_state();
var inst_32426 = inst_32478;
var state_32508__$1 = (function (){var statearr_32544 = state_32508;
(statearr_32544[(7)] = inst_32426);

(statearr_32544[(15)] = inst_32477);

return statearr_32544;
})();
var statearr_32546_35299 = state_32508__$1;
(statearr_32546_35299[(2)] = null);

(statearr_32546_35299[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32509 === (25))){
var inst_32503 = (state_32508[(2)]);
var state_32508__$1 = state_32508;
var statearr_32551_35304 = state_32508__$1;
(statearr_32551_35304[(2)] = inst_32503);

(statearr_32551_35304[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32509 === (34))){
var inst_32501 = (state_32508[(2)]);
var state_32508__$1 = state_32508;
var statearr_32555_35306 = state_32508__$1;
(statearr_32555_35306[(2)] = inst_32501);

(statearr_32555_35306[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32509 === (17))){
var state_32508__$1 = state_32508;
var statearr_32556_35316 = state_32508__$1;
(statearr_32556_35316[(2)] = false);

(statearr_32556_35316[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32509 === (3))){
var state_32508__$1 = state_32508;
var statearr_32559_35322 = state_32508__$1;
(statearr_32559_35322[(2)] = false);

(statearr_32559_35322[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32509 === (12))){
var inst_32505 = (state_32508[(2)]);
var state_32508__$1 = state_32508;
return cljs.core.async.impl.ioc_helpers.return_chan(state_32508__$1,inst_32505);
} else {
if((state_val_32509 === (2))){
var inst_32398 = (state_32508[(8)]);
var inst_32405 = inst_32398.cljs$lang$protocol_mask$partition0$;
var inst_32406 = (inst_32405 & (64));
var inst_32407 = inst_32398.cljs$core$ISeq$;
var inst_32408 = (cljs.core.PROTOCOL_SENTINEL === inst_32407);
var inst_32409 = ((inst_32406) || (inst_32408));
var state_32508__$1 = state_32508;
if(cljs.core.truth_(inst_32409)){
var statearr_32562_35324 = state_32508__$1;
(statearr_32562_35324[(1)] = (5));

} else {
var statearr_32564_35330 = state_32508__$1;
(statearr_32564_35330[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32509 === (23))){
var inst_32465 = (state_32508[(14)]);
var inst_32472 = (inst_32465 == null);
var state_32508__$1 = state_32508;
if(cljs.core.truth_(inst_32472)){
var statearr_32565_35333 = state_32508__$1;
(statearr_32565_35333[(1)] = (26));

} else {
var statearr_32567_35338 = state_32508__$1;
(statearr_32567_35338[(1)] = (27));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32509 === (35))){
var inst_32492 = (state_32508[(2)]);
var state_32508__$1 = state_32508;
if(cljs.core.truth_(inst_32492)){
var statearr_32568_35340 = state_32508__$1;
(statearr_32568_35340[(1)] = (36));

} else {
var statearr_32569_35341 = state_32508__$1;
(statearr_32569_35341[(1)] = (37));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32509 === (19))){
var inst_32426 = (state_32508[(7)]);
var inst_32446 = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,inst_32426);
var state_32508__$1 = state_32508;
var statearr_32571_35346 = state_32508__$1;
(statearr_32571_35346[(2)] = inst_32446);

(statearr_32571_35346[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32509 === (11))){
var inst_32426 = (state_32508[(7)]);
var inst_32430 = (inst_32426 == null);
var inst_32431 = cljs.core.not(inst_32430);
var state_32508__$1 = state_32508;
if(inst_32431){
var statearr_32574_35356 = state_32508__$1;
(statearr_32574_35356[(1)] = (13));

} else {
var statearr_32575_35358 = state_32508__$1;
(statearr_32575_35358[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32509 === (9))){
var inst_32398 = (state_32508[(8)]);
var state_32508__$1 = state_32508;
var statearr_32576_35360 = state_32508__$1;
(statearr_32576_35360[(2)] = inst_32398);

(statearr_32576_35360[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32509 === (5))){
var state_32508__$1 = state_32508;
var statearr_32577_35365 = state_32508__$1;
(statearr_32577_35365[(2)] = true);

(statearr_32577_35365[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32509 === (14))){
var state_32508__$1 = state_32508;
var statearr_32580_35366 = state_32508__$1;
(statearr_32580_35366[(2)] = false);

(statearr_32580_35366[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32509 === (26))){
var inst_32466 = (state_32508[(9)]);
var inst_32474 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(cs,cljs.core.dissoc,inst_32466);
var state_32508__$1 = state_32508;
var statearr_32590_35368 = state_32508__$1;
(statearr_32590_35368[(2)] = inst_32474);

(statearr_32590_35368[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32509 === (16))){
var state_32508__$1 = state_32508;
var statearr_32598_35369 = state_32508__$1;
(statearr_32598_35369[(2)] = true);

(statearr_32598_35369[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32509 === (38))){
var inst_32497 = (state_32508[(2)]);
var state_32508__$1 = state_32508;
var statearr_32605_35373 = state_32508__$1;
(statearr_32605_35373[(2)] = inst_32497);

(statearr_32605_35373[(1)] = (34));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32509 === (30))){
var inst_32451 = (state_32508[(13)]);
var inst_32466 = (state_32508[(9)]);
var inst_32450 = (state_32508[(11)]);
var inst_32484 = cljs.core.empty_QMARK_(inst_32450);
var inst_32485 = (inst_32451.cljs$core$IFn$_invoke$arity$1 ? inst_32451.cljs$core$IFn$_invoke$arity$1(inst_32466) : inst_32451.call(null,inst_32466));
var inst_32486 = cljs.core.not(inst_32485);
var inst_32487 = ((inst_32484) && (inst_32486));
var state_32508__$1 = state_32508;
var statearr_32612_35379 = state_32508__$1;
(statearr_32612_35379[(2)] = inst_32487);

(statearr_32612_35379[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32509 === (10))){
var inst_32398 = (state_32508[(8)]);
var inst_32422 = (state_32508[(2)]);
var inst_32423 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_32422,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_32424 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_32422,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_32425 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_32422,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var inst_32426 = inst_32398;
var state_32508__$1 = (function (){var statearr_32618 = state_32508;
(statearr_32618[(7)] = inst_32426);

(statearr_32618[(16)] = inst_32424);

(statearr_32618[(17)] = inst_32425);

(statearr_32618[(18)] = inst_32423);

return statearr_32618;
})();
var statearr_32619_35386 = state_32508__$1;
(statearr_32619_35386[(2)] = null);

(statearr_32619_35386[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32509 === (18))){
var inst_32441 = (state_32508[(2)]);
var state_32508__$1 = state_32508;
var statearr_32625_35388 = state_32508__$1;
(statearr_32625_35388[(2)] = inst_32441);

(statearr_32625_35388[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32509 === (37))){
var state_32508__$1 = state_32508;
var statearr_32627_35389 = state_32508__$1;
(statearr_32627_35389[(2)] = null);

(statearr_32627_35389[(1)] = (38));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32509 === (8))){
var inst_32398 = (state_32508[(8)]);
var inst_32419 = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,inst_32398);
var state_32508__$1 = state_32508;
var statearr_32630_35390 = state_32508__$1;
(statearr_32630_35390[(2)] = inst_32419);

(statearr_32630_35390[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$mix_$_state_machine__30540__auto__ = null;
var cljs$core$async$mix_$_state_machine__30540__auto____0 = (function (){
var statearr_32633 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_32633[(0)] = cljs$core$async$mix_$_state_machine__30540__auto__);

(statearr_32633[(1)] = (1));

return statearr_32633;
});
var cljs$core$async$mix_$_state_machine__30540__auto____1 = (function (state_32508){
while(true){
var ret_value__30541__auto__ = (function (){try{while(true){
var result__30542__auto__ = switch__30539__auto__(state_32508);
if(cljs.core.keyword_identical_QMARK_(result__30542__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30542__auto__;
}
break;
}
}catch (e32635){var ex__30543__auto__ = e32635;
var statearr_32636_35396 = state_32508;
(statearr_32636_35396[(2)] = ex__30543__auto__);


if(cljs.core.seq((state_32508[(4)]))){
var statearr_32637_35398 = state_32508;
(statearr_32637_35398[(1)] = cljs.core.first((state_32508[(4)])));

} else {
throw ex__30543__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30541__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35400 = state_32508;
state_32508 = G__35400;
continue;
} else {
return ret_value__30541__auto__;
}
break;
}
});
cljs$core$async$mix_$_state_machine__30540__auto__ = function(state_32508){
switch(arguments.length){
case 0:
return cljs$core$async$mix_$_state_machine__30540__auto____0.call(this);
case 1:
return cljs$core$async$mix_$_state_machine__30540__auto____1.call(this,state_32508);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mix_$_state_machine__30540__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mix_$_state_machine__30540__auto____0;
cljs$core$async$mix_$_state_machine__30540__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mix_$_state_machine__30540__auto____1;
return cljs$core$async$mix_$_state_machine__30540__auto__;
})()
})();
var state__30705__auto__ = (function (){var statearr_32647 = f__30704__auto__();
(statearr_32647[(6)] = c__30703__auto___35242);

return statearr_32647;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30705__auto__);
}));


return m;
});
/**
 * Adds ch as an input to the mix
 */
cljs.core.async.admix = (function cljs$core$async$admix(mix,ch){
return cljs.core.async.admix_STAR_(mix,ch);
});
/**
 * Removes ch as an input to the mix
 */
cljs.core.async.unmix = (function cljs$core$async$unmix(mix,ch){
return cljs.core.async.unmix_STAR_(mix,ch);
});
/**
 * removes all inputs from the mix
 */
cljs.core.async.unmix_all = (function cljs$core$async$unmix_all(mix){
return cljs.core.async.unmix_all_STAR_(mix);
});
/**
 * Atomically sets the state(s) of one or more channels in a mix. The
 *   state map is a map of channels -> channel-state-map. A
 *   channel-state-map is a map of attrs -> boolean, where attr is one or
 *   more of :mute, :pause or :solo. Any states supplied are merged with
 *   the current state.
 * 
 *   Note that channels can be added to a mix via toggle, which can be
 *   used to add channels in a particular (e.g. paused) state.
 */
cljs.core.async.toggle = (function cljs$core$async$toggle(mix,state_map){
return cljs.core.async.toggle_STAR_(mix,state_map);
});
/**
 * Sets the solo mode of the mix. mode must be one of :mute or :pause
 */
cljs.core.async.solo_mode = (function cljs$core$async$solo_mode(mix,mode){
return cljs.core.async.solo_mode_STAR_(mix,mode);
});

/**
 * @interface
 */
cljs.core.async.Pub = function(){};

var cljs$core$async$Pub$sub_STAR_$dyn_35411 = (function (p,v,ch,close_QMARK_){
var x__4428__auto__ = (((p == null))?null:p);
var m__4429__auto__ = (cljs.core.async.sub_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$4 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$4(p,v,ch,close_QMARK_) : m__4429__auto__.call(null,p,v,ch,close_QMARK_));
} else {
var m__4426__auto__ = (cljs.core.async.sub_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$4 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$4(p,v,ch,close_QMARK_) : m__4426__auto__.call(null,p,v,ch,close_QMARK_));
} else {
throw cljs.core.missing_protocol("Pub.sub*",p);
}
}
});
cljs.core.async.sub_STAR_ = (function cljs$core$async$sub_STAR_(p,v,ch,close_QMARK_){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$sub_STAR_$arity$4 == null)))))){
return p.cljs$core$async$Pub$sub_STAR_$arity$4(p,v,ch,close_QMARK_);
} else {
return cljs$core$async$Pub$sub_STAR_$dyn_35411(p,v,ch,close_QMARK_);
}
});

var cljs$core$async$Pub$unsub_STAR_$dyn_35418 = (function (p,v,ch){
var x__4428__auto__ = (((p == null))?null:p);
var m__4429__auto__ = (cljs.core.async.unsub_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$3 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$3(p,v,ch) : m__4429__auto__.call(null,p,v,ch));
} else {
var m__4426__auto__ = (cljs.core.async.unsub_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$3 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$3(p,v,ch) : m__4426__auto__.call(null,p,v,ch));
} else {
throw cljs.core.missing_protocol("Pub.unsub*",p);
}
}
});
cljs.core.async.unsub_STAR_ = (function cljs$core$async$unsub_STAR_(p,v,ch){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_STAR_$arity$3 == null)))))){
return p.cljs$core$async$Pub$unsub_STAR_$arity$3(p,v,ch);
} else {
return cljs$core$async$Pub$unsub_STAR_$dyn_35418(p,v,ch);
}
});

var cljs$core$async$Pub$unsub_all_STAR_$dyn_35440 = (function() {
var G__35441 = null;
var G__35441__1 = (function (p){
var x__4428__auto__ = (((p == null))?null:p);
var m__4429__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$1(p) : m__4429__auto__.call(null,p));
} else {
var m__4426__auto__ = (cljs.core.async.unsub_all_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$1(p) : m__4426__auto__.call(null,p));
} else {
throw cljs.core.missing_protocol("Pub.unsub-all*",p);
}
}
});
var G__35441__2 = (function (p,v){
var x__4428__auto__ = (((p == null))?null:p);
var m__4429__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$2(p,v) : m__4429__auto__.call(null,p,v));
} else {
var m__4426__auto__ = (cljs.core.async.unsub_all_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$2(p,v) : m__4426__auto__.call(null,p,v));
} else {
throw cljs.core.missing_protocol("Pub.unsub-all*",p);
}
}
});
G__35441 = function(p,v){
switch(arguments.length){
case 1:
return G__35441__1.call(this,p);
case 2:
return G__35441__2.call(this,p,v);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
G__35441.cljs$core$IFn$_invoke$arity$1 = G__35441__1;
G__35441.cljs$core$IFn$_invoke$arity$2 = G__35441__2;
return G__35441;
})()
;
cljs.core.async.unsub_all_STAR_ = (function cljs$core$async$unsub_all_STAR_(var_args){
var G__32706 = arguments.length;
switch (G__32706) {
case 1:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1 = (function (p){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$1 == null)))))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$1(p);
} else {
return cljs$core$async$Pub$unsub_all_STAR_$dyn_35440(p);
}
}));

(cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2 = (function (p,v){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$2 == null)))))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$2(p,v);
} else {
return cljs$core$async$Pub$unsub_all_STAR_$dyn_35440(p,v);
}
}));

(cljs.core.async.unsub_all_STAR_.cljs$lang$maxFixedArity = 2);


/**
 * Creates and returns a pub(lication) of the supplied channel,
 *   partitioned into topics by the topic-fn. topic-fn will be applied to
 *   each value on the channel and the result will determine the 'topic'
 *   on which that value will be put. Channels can be subscribed to
 *   receive copies of topics using 'sub', and unsubscribed using
 *   'unsub'. Each topic will be handled by an internal mult on a
 *   dedicated channel. By default these internal channels are
 *   unbuffered, but a buf-fn can be supplied which, given a topic,
 *   creates a buffer with desired properties.
 * 
 *   Each item is distributed to all subs in parallel and synchronously,
 *   i.e. each sub must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow subs from holding up the pub.
 * 
 *   Items received when there are no matching subs get dropped.
 * 
 *   Note that if buf-fns are used then each topic is handled
 *   asynchronously, i.e. if a channel is subscribed to more than one
 *   topic it should not expect them to be interleaved identically with
 *   the source.
 */
cljs.core.async.pub = (function cljs$core$async$pub(var_args){
var G__32731 = arguments.length;
switch (G__32731) {
case 2:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2 = (function (ch,topic_fn){
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3(ch,topic_fn,cljs.core.constantly(null));
}));

(cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3 = (function (ch,topic_fn,buf_fn){
var mults = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var ensure_mult = (function (topic){
var or__4126__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(mults),topic);
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(mults,(function (p1__32712_SHARP_){
if(cljs.core.truth_((p1__32712_SHARP_.cljs$core$IFn$_invoke$arity$1 ? p1__32712_SHARP_.cljs$core$IFn$_invoke$arity$1(topic) : p1__32712_SHARP_.call(null,topic)))){
return p1__32712_SHARP_;
} else {
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(p1__32712_SHARP_,topic,cljs.core.async.mult(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((buf_fn.cljs$core$IFn$_invoke$arity$1 ? buf_fn.cljs$core$IFn$_invoke$arity$1(topic) : buf_fn.call(null,topic)))));
}
})),topic);
}
});
var p = (function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async32754 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.Pub}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async32754 = (function (ch,topic_fn,buf_fn,mults,ensure_mult,meta32755){
this.ch = ch;
this.topic_fn = topic_fn;
this.buf_fn = buf_fn;
this.mults = mults;
this.ensure_mult = ensure_mult;
this.meta32755 = meta32755;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async32754.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_32756,meta32755__$1){
var self__ = this;
var _32756__$1 = this;
return (new cljs.core.async.t_cljs$core$async32754(self__.ch,self__.topic_fn,self__.buf_fn,self__.mults,self__.ensure_mult,meta32755__$1));
}));

(cljs.core.async.t_cljs$core$async32754.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_32756){
var self__ = this;
var _32756__$1 = this;
return self__.meta32755;
}));

(cljs.core.async.t_cljs$core$async32754.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async32754.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
}));

(cljs.core.async.t_cljs$core$async32754.prototype.cljs$core$async$Pub$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async32754.prototype.cljs$core$async$Pub$sub_STAR_$arity$4 = (function (p,topic,ch__$1,close_QMARK_){
var self__ = this;
var p__$1 = this;
var m = (self__.ensure_mult.cljs$core$IFn$_invoke$arity$1 ? self__.ensure_mult.cljs$core$IFn$_invoke$arity$1(topic) : self__.ensure_mult.call(null,topic));
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3(m,ch__$1,close_QMARK_);
}));

(cljs.core.async.t_cljs$core$async32754.prototype.cljs$core$async$Pub$unsub_STAR_$arity$3 = (function (p,topic,ch__$1){
var self__ = this;
var p__$1 = this;
var temp__5735__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(self__.mults),topic);
if(cljs.core.truth_(temp__5735__auto__)){
var m = temp__5735__auto__;
return cljs.core.async.untap(m,ch__$1);
} else {
return null;
}
}));

(cljs.core.async.t_cljs$core$async32754.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.reset_BANG_(self__.mults,cljs.core.PersistentArrayMap.EMPTY);
}));

(cljs.core.async.t_cljs$core$async32754.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$2 = (function (_,topic){
var self__ = this;
var ___$1 = this;
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.mults,cljs.core.dissoc,topic);
}));

(cljs.core.async.t_cljs$core$async32754.getBasis = (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"topic-fn","topic-fn",-862449736,null),new cljs.core.Symbol(null,"buf-fn","buf-fn",-1200281591,null),new cljs.core.Symbol(null,"mults","mults",-461114485,null),new cljs.core.Symbol(null,"ensure-mult","ensure-mult",1796584816,null),new cljs.core.Symbol(null,"meta32755","meta32755",207785327,null)], null);
}));

(cljs.core.async.t_cljs$core$async32754.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async32754.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async32754");

(cljs.core.async.t_cljs$core$async32754.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async32754");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async32754.
 */
cljs.core.async.__GT_t_cljs$core$async32754 = (function cljs$core$async$__GT_t_cljs$core$async32754(ch__$1,topic_fn__$1,buf_fn__$1,mults__$1,ensure_mult__$1,meta32755){
return (new cljs.core.async.t_cljs$core$async32754(ch__$1,topic_fn__$1,buf_fn__$1,mults__$1,ensure_mult__$1,meta32755));
});

}

return (new cljs.core.async.t_cljs$core$async32754(ch,topic_fn,buf_fn,mults,ensure_mult,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var c__30703__auto___35575 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30704__auto__ = (function (){var switch__30539__auto__ = (function (state_32882){
var state_val_32883 = (state_32882[(1)]);
if((state_val_32883 === (7))){
var inst_32877 = (state_32882[(2)]);
var state_32882__$1 = state_32882;
var statearr_32889_35583 = state_32882__$1;
(statearr_32889_35583[(2)] = inst_32877);

(statearr_32889_35583[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32883 === (20))){
var state_32882__$1 = state_32882;
var statearr_32891_35589 = state_32882__$1;
(statearr_32891_35589[(2)] = null);

(statearr_32891_35589[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32883 === (1))){
var state_32882__$1 = state_32882;
var statearr_32894_35591 = state_32882__$1;
(statearr_32894_35591[(2)] = null);

(statearr_32894_35591[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32883 === (24))){
var inst_32846 = (state_32882[(7)]);
var inst_32865 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(mults,cljs.core.dissoc,inst_32846);
var state_32882__$1 = state_32882;
var statearr_32895_35598 = state_32882__$1;
(statearr_32895_35598[(2)] = inst_32865);

(statearr_32895_35598[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32883 === (4))){
var inst_32777 = (state_32882[(8)]);
var inst_32777__$1 = (state_32882[(2)]);
var inst_32782 = (inst_32777__$1 == null);
var state_32882__$1 = (function (){var statearr_32902 = state_32882;
(statearr_32902[(8)] = inst_32777__$1);

return statearr_32902;
})();
if(cljs.core.truth_(inst_32782)){
var statearr_32903_35607 = state_32882__$1;
(statearr_32903_35607[(1)] = (5));

} else {
var statearr_32905_35611 = state_32882__$1;
(statearr_32905_35611[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32883 === (15))){
var inst_32836 = (state_32882[(2)]);
var state_32882__$1 = state_32882;
var statearr_32911_35613 = state_32882__$1;
(statearr_32911_35613[(2)] = inst_32836);

(statearr_32911_35613[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32883 === (21))){
var inst_32874 = (state_32882[(2)]);
var state_32882__$1 = (function (){var statearr_32913 = state_32882;
(statearr_32913[(9)] = inst_32874);

return statearr_32913;
})();
var statearr_32914_35614 = state_32882__$1;
(statearr_32914_35614[(2)] = null);

(statearr_32914_35614[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32883 === (13))){
var inst_32809 = (state_32882[(10)]);
var inst_32818 = cljs.core.chunked_seq_QMARK_(inst_32809);
var state_32882__$1 = state_32882;
if(inst_32818){
var statearr_32919_35615 = state_32882__$1;
(statearr_32919_35615[(1)] = (16));

} else {
var statearr_32920_35619 = state_32882__$1;
(statearr_32920_35619[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32883 === (22))){
var inst_32858 = (state_32882[(2)]);
var state_32882__$1 = state_32882;
if(cljs.core.truth_(inst_32858)){
var statearr_32922_35623 = state_32882__$1;
(statearr_32922_35623[(1)] = (23));

} else {
var statearr_32924_35625 = state_32882__$1;
(statearr_32924_35625[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32883 === (6))){
var inst_32846 = (state_32882[(7)]);
var inst_32777 = (state_32882[(8)]);
var inst_32853 = (state_32882[(11)]);
var inst_32846__$1 = (topic_fn.cljs$core$IFn$_invoke$arity$1 ? topic_fn.cljs$core$IFn$_invoke$arity$1(inst_32777) : topic_fn.call(null,inst_32777));
var inst_32852 = cljs.core.deref(mults);
var inst_32853__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_32852,inst_32846__$1);
var state_32882__$1 = (function (){var statearr_32928 = state_32882;
(statearr_32928[(7)] = inst_32846__$1);

(statearr_32928[(11)] = inst_32853__$1);

return statearr_32928;
})();
if(cljs.core.truth_(inst_32853__$1)){
var statearr_32929_35636 = state_32882__$1;
(statearr_32929_35636[(1)] = (19));

} else {
var statearr_32930_35637 = state_32882__$1;
(statearr_32930_35637[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32883 === (25))){
var inst_32871 = (state_32882[(2)]);
var state_32882__$1 = state_32882;
var statearr_32931_35643 = state_32882__$1;
(statearr_32931_35643[(2)] = inst_32871);

(statearr_32931_35643[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32883 === (17))){
var inst_32809 = (state_32882[(10)]);
var inst_32826 = cljs.core.first(inst_32809);
var inst_32827 = cljs.core.async.muxch_STAR_(inst_32826);
var inst_32828 = cljs.core.async.close_BANG_(inst_32827);
var inst_32829 = cljs.core.next(inst_32809);
var inst_32791 = inst_32829;
var inst_32792 = null;
var inst_32793 = (0);
var inst_32794 = (0);
var state_32882__$1 = (function (){var statearr_32933 = state_32882;
(statearr_32933[(12)] = inst_32793);

(statearr_32933[(13)] = inst_32791);

(statearr_32933[(14)] = inst_32794);

(statearr_32933[(15)] = inst_32828);

(statearr_32933[(16)] = inst_32792);

return statearr_32933;
})();
var statearr_32935_35662 = state_32882__$1;
(statearr_32935_35662[(2)] = null);

(statearr_32935_35662[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32883 === (3))){
var inst_32879 = (state_32882[(2)]);
var state_32882__$1 = state_32882;
return cljs.core.async.impl.ioc_helpers.return_chan(state_32882__$1,inst_32879);
} else {
if((state_val_32883 === (12))){
var inst_32838 = (state_32882[(2)]);
var state_32882__$1 = state_32882;
var statearr_32937_35675 = state_32882__$1;
(statearr_32937_35675[(2)] = inst_32838);

(statearr_32937_35675[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32883 === (2))){
var state_32882__$1 = state_32882;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_32882__$1,(4),ch);
} else {
if((state_val_32883 === (23))){
var state_32882__$1 = state_32882;
var statearr_32941_35688 = state_32882__$1;
(statearr_32941_35688[(2)] = null);

(statearr_32941_35688[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32883 === (19))){
var inst_32777 = (state_32882[(8)]);
var inst_32853 = (state_32882[(11)]);
var inst_32856 = cljs.core.async.muxch_STAR_(inst_32853);
var state_32882__$1 = state_32882;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_32882__$1,(22),inst_32856,inst_32777);
} else {
if((state_val_32883 === (11))){
var inst_32791 = (state_32882[(13)]);
var inst_32809 = (state_32882[(10)]);
var inst_32809__$1 = cljs.core.seq(inst_32791);
var state_32882__$1 = (function (){var statearr_32947 = state_32882;
(statearr_32947[(10)] = inst_32809__$1);

return statearr_32947;
})();
if(inst_32809__$1){
var statearr_32949_35724 = state_32882__$1;
(statearr_32949_35724[(1)] = (13));

} else {
var statearr_32950_35725 = state_32882__$1;
(statearr_32950_35725[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32883 === (9))){
var inst_32840 = (state_32882[(2)]);
var state_32882__$1 = state_32882;
var statearr_32954_35734 = state_32882__$1;
(statearr_32954_35734[(2)] = inst_32840);

(statearr_32954_35734[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32883 === (5))){
var inst_32788 = cljs.core.deref(mults);
var inst_32789 = cljs.core.vals(inst_32788);
var inst_32790 = cljs.core.seq(inst_32789);
var inst_32791 = inst_32790;
var inst_32792 = null;
var inst_32793 = (0);
var inst_32794 = (0);
var state_32882__$1 = (function (){var statearr_32957 = state_32882;
(statearr_32957[(12)] = inst_32793);

(statearr_32957[(13)] = inst_32791);

(statearr_32957[(14)] = inst_32794);

(statearr_32957[(16)] = inst_32792);

return statearr_32957;
})();
var statearr_32959_35738 = state_32882__$1;
(statearr_32959_35738[(2)] = null);

(statearr_32959_35738[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32883 === (14))){
var state_32882__$1 = state_32882;
var statearr_32963_35743 = state_32882__$1;
(statearr_32963_35743[(2)] = null);

(statearr_32963_35743[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32883 === (16))){
var inst_32809 = (state_32882[(10)]);
var inst_32820 = cljs.core.chunk_first(inst_32809);
var inst_32822 = cljs.core.chunk_rest(inst_32809);
var inst_32823 = cljs.core.count(inst_32820);
var inst_32791 = inst_32822;
var inst_32792 = inst_32820;
var inst_32793 = inst_32823;
var inst_32794 = (0);
var state_32882__$1 = (function (){var statearr_32964 = state_32882;
(statearr_32964[(12)] = inst_32793);

(statearr_32964[(13)] = inst_32791);

(statearr_32964[(14)] = inst_32794);

(statearr_32964[(16)] = inst_32792);

return statearr_32964;
})();
var statearr_32966_35751 = state_32882__$1;
(statearr_32966_35751[(2)] = null);

(statearr_32966_35751[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32883 === (10))){
var inst_32793 = (state_32882[(12)]);
var inst_32791 = (state_32882[(13)]);
var inst_32794 = (state_32882[(14)]);
var inst_32792 = (state_32882[(16)]);
var inst_32800 = cljs.core._nth(inst_32792,inst_32794);
var inst_32801 = cljs.core.async.muxch_STAR_(inst_32800);
var inst_32802 = cljs.core.async.close_BANG_(inst_32801);
var inst_32806 = (inst_32794 + (1));
var tmp32960 = inst_32793;
var tmp32961 = inst_32791;
var tmp32962 = inst_32792;
var inst_32791__$1 = tmp32961;
var inst_32792__$1 = tmp32962;
var inst_32793__$1 = tmp32960;
var inst_32794__$1 = inst_32806;
var state_32882__$1 = (function (){var statearr_32969 = state_32882;
(statearr_32969[(12)] = inst_32793__$1);

(statearr_32969[(17)] = inst_32802);

(statearr_32969[(13)] = inst_32791__$1);

(statearr_32969[(14)] = inst_32794__$1);

(statearr_32969[(16)] = inst_32792__$1);

return statearr_32969;
})();
var statearr_32970_35757 = state_32882__$1;
(statearr_32970_35757[(2)] = null);

(statearr_32970_35757[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32883 === (18))){
var inst_32832 = (state_32882[(2)]);
var state_32882__$1 = state_32882;
var statearr_32971_35763 = state_32882__$1;
(statearr_32971_35763[(2)] = inst_32832);

(statearr_32971_35763[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32883 === (8))){
var inst_32793 = (state_32882[(12)]);
var inst_32794 = (state_32882[(14)]);
var inst_32796 = (inst_32794 < inst_32793);
var inst_32797 = inst_32796;
var state_32882__$1 = state_32882;
if(cljs.core.truth_(inst_32797)){
var statearr_32975_35772 = state_32882__$1;
(statearr_32975_35772[(1)] = (10));

} else {
var statearr_32980_35773 = state_32882__$1;
(statearr_32980_35773[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__30540__auto__ = null;
var cljs$core$async$state_machine__30540__auto____0 = (function (){
var statearr_32981 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_32981[(0)] = cljs$core$async$state_machine__30540__auto__);

(statearr_32981[(1)] = (1));

return statearr_32981;
});
var cljs$core$async$state_machine__30540__auto____1 = (function (state_32882){
while(true){
var ret_value__30541__auto__ = (function (){try{while(true){
var result__30542__auto__ = switch__30539__auto__(state_32882);
if(cljs.core.keyword_identical_QMARK_(result__30542__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30542__auto__;
}
break;
}
}catch (e32982){var ex__30543__auto__ = e32982;
var statearr_32984_35786 = state_32882;
(statearr_32984_35786[(2)] = ex__30543__auto__);


if(cljs.core.seq((state_32882[(4)]))){
var statearr_32988_35788 = state_32882;
(statearr_32988_35788[(1)] = cljs.core.first((state_32882[(4)])));

} else {
throw ex__30543__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30541__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35792 = state_32882;
state_32882 = G__35792;
continue;
} else {
return ret_value__30541__auto__;
}
break;
}
});
cljs$core$async$state_machine__30540__auto__ = function(state_32882){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__30540__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__30540__auto____1.call(this,state_32882);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__30540__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__30540__auto____0;
cljs$core$async$state_machine__30540__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__30540__auto____1;
return cljs$core$async$state_machine__30540__auto__;
})()
})();
var state__30705__auto__ = (function (){var statearr_33001 = f__30704__auto__();
(statearr_33001[(6)] = c__30703__auto___35575);

return statearr_33001;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30705__auto__);
}));


return p;
}));

(cljs.core.async.pub.cljs$lang$maxFixedArity = 3);

/**
 * Subscribes a channel to a topic of a pub.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.sub = (function cljs$core$async$sub(var_args){
var G__33006 = arguments.length;
switch (G__33006) {
case 3:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3 = (function (p,topic,ch){
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4(p,topic,ch,true);
}));

(cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4 = (function (p,topic,ch,close_QMARK_){
return cljs.core.async.sub_STAR_(p,topic,ch,close_QMARK_);
}));

(cljs.core.async.sub.cljs$lang$maxFixedArity = 4);

/**
 * Unsubscribes a channel from a topic of a pub
 */
cljs.core.async.unsub = (function cljs$core$async$unsub(p,topic,ch){
return cljs.core.async.unsub_STAR_(p,topic,ch);
});
/**
 * Unsubscribes all channels from a pub, or a topic of a pub
 */
cljs.core.async.unsub_all = (function cljs$core$async$unsub_all(var_args){
var G__33023 = arguments.length;
switch (G__33023) {
case 1:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1 = (function (p){
return cljs.core.async.unsub_all_STAR_(p);
}));

(cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2 = (function (p,topic){
return cljs.core.async.unsub_all_STAR_(p,topic);
}));

(cljs.core.async.unsub_all.cljs$lang$maxFixedArity = 2);

/**
 * Takes a function and a collection of source channels, and returns a
 *   channel which contains the values produced by applying f to the set
 *   of first items taken from each source channel, followed by applying
 *   f to the set of second items from each channel, until any one of the
 *   channels is closed, at which point the output channel will be
 *   closed. The returned channel will be unbuffered by default, or a
 *   buf-or-n can be supplied
 */
cljs.core.async.map = (function cljs$core$async$map(var_args){
var G__33055 = arguments.length;
switch (G__33055) {
case 2:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.map.cljs$core$IFn$_invoke$arity$2 = (function (f,chs){
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$3(f,chs,null);
}));

(cljs.core.async.map.cljs$core$IFn$_invoke$arity$3 = (function (f,chs,buf_or_n){
var chs__$1 = cljs.core.vec(chs);
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var cnt = cljs.core.count(chs__$1);
var rets = cljs.core.object_array.cljs$core$IFn$_invoke$arity$1(cnt);
var dchan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
var dctr = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
var done = cljs.core.mapv.cljs$core$IFn$_invoke$arity$2((function (i){
return (function (ret){
(rets[i] = ret);

if((cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(dchan,rets.slice((0)));
} else {
return null;
}
});
}),cljs.core.range.cljs$core$IFn$_invoke$arity$1(cnt));
var c__30703__auto___35817 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30704__auto__ = (function (){var switch__30539__auto__ = (function (state_33124){
var state_val_33125 = (state_33124[(1)]);
if((state_val_33125 === (7))){
var state_33124__$1 = state_33124;
var statearr_33130_35820 = state_33124__$1;
(statearr_33130_35820[(2)] = null);

(statearr_33130_35820[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33125 === (1))){
var state_33124__$1 = state_33124;
var statearr_33131_35823 = state_33124__$1;
(statearr_33131_35823[(2)] = null);

(statearr_33131_35823[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33125 === (4))){
var inst_33071 = (state_33124[(7)]);
var inst_33069 = (state_33124[(8)]);
var inst_33074 = (inst_33071 < inst_33069);
var state_33124__$1 = state_33124;
if(cljs.core.truth_(inst_33074)){
var statearr_33137_35824 = state_33124__$1;
(statearr_33137_35824[(1)] = (6));

} else {
var statearr_33138_35828 = state_33124__$1;
(statearr_33138_35828[(1)] = (7));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33125 === (15))){
var inst_33105 = (state_33124[(9)]);
var inst_33111 = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(f,inst_33105);
var state_33124__$1 = state_33124;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33124__$1,(17),out,inst_33111);
} else {
if((state_val_33125 === (13))){
var inst_33105 = (state_33124[(9)]);
var inst_33105__$1 = (state_33124[(2)]);
var inst_33106 = cljs.core.some(cljs.core.nil_QMARK_,inst_33105__$1);
var state_33124__$1 = (function (){var statearr_33147 = state_33124;
(statearr_33147[(9)] = inst_33105__$1);

return statearr_33147;
})();
if(cljs.core.truth_(inst_33106)){
var statearr_33149_35829 = state_33124__$1;
(statearr_33149_35829[(1)] = (14));

} else {
var statearr_33151_35830 = state_33124__$1;
(statearr_33151_35830[(1)] = (15));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33125 === (6))){
var state_33124__$1 = state_33124;
var statearr_33152_35834 = state_33124__$1;
(statearr_33152_35834[(2)] = null);

(statearr_33152_35834[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33125 === (17))){
var inst_33113 = (state_33124[(2)]);
var state_33124__$1 = (function (){var statearr_33165 = state_33124;
(statearr_33165[(10)] = inst_33113);

return statearr_33165;
})();
var statearr_33166_35838 = state_33124__$1;
(statearr_33166_35838[(2)] = null);

(statearr_33166_35838[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33125 === (3))){
var inst_33118 = (state_33124[(2)]);
var state_33124__$1 = state_33124;
return cljs.core.async.impl.ioc_helpers.return_chan(state_33124__$1,inst_33118);
} else {
if((state_val_33125 === (12))){
var _ = (function (){var statearr_33170 = state_33124;
(statearr_33170[(4)] = cljs.core.rest((state_33124[(4)])));

return statearr_33170;
})();
var state_33124__$1 = state_33124;
var ex33160 = (state_33124__$1[(2)]);
var statearr_33172_35841 = state_33124__$1;
(statearr_33172_35841[(5)] = ex33160);


if((ex33160 instanceof Object)){
var statearr_33176_35842 = state_33124__$1;
(statearr_33176_35842[(1)] = (11));

(statearr_33176_35842[(5)] = null);

} else {
throw ex33160;

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33125 === (2))){
var inst_33067 = cljs.core.reset_BANG_(dctr,cnt);
var inst_33069 = cnt;
var inst_33071 = (0);
var state_33124__$1 = (function (){var statearr_33182 = state_33124;
(statearr_33182[(7)] = inst_33071);

(statearr_33182[(11)] = inst_33067);

(statearr_33182[(8)] = inst_33069);

return statearr_33182;
})();
var statearr_33183_35845 = state_33124__$1;
(statearr_33183_35845[(2)] = null);

(statearr_33183_35845[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33125 === (11))){
var inst_33082 = (state_33124[(2)]);
var inst_33083 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(dctr,cljs.core.dec);
var state_33124__$1 = (function (){var statearr_33185 = state_33124;
(statearr_33185[(12)] = inst_33082);

return statearr_33185;
})();
var statearr_33188_35846 = state_33124__$1;
(statearr_33188_35846[(2)] = inst_33083);

(statearr_33188_35846[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33125 === (9))){
var inst_33071 = (state_33124[(7)]);
var _ = (function (){var statearr_33192 = state_33124;
(statearr_33192[(4)] = cljs.core.cons((12),(state_33124[(4)])));

return statearr_33192;
})();
var inst_33091 = (chs__$1.cljs$core$IFn$_invoke$arity$1 ? chs__$1.cljs$core$IFn$_invoke$arity$1(inst_33071) : chs__$1.call(null,inst_33071));
var inst_33092 = (done.cljs$core$IFn$_invoke$arity$1 ? done.cljs$core$IFn$_invoke$arity$1(inst_33071) : done.call(null,inst_33071));
var inst_33093 = cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2(inst_33091,inst_33092);
var ___$1 = (function (){var statearr_33197 = state_33124;
(statearr_33197[(4)] = cljs.core.rest((state_33124[(4)])));

return statearr_33197;
})();
var state_33124__$1 = state_33124;
var statearr_33198_35861 = state_33124__$1;
(statearr_33198_35861[(2)] = inst_33093);

(statearr_33198_35861[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33125 === (5))){
var inst_33103 = (state_33124[(2)]);
var state_33124__$1 = (function (){var statearr_33199 = state_33124;
(statearr_33199[(13)] = inst_33103);

return statearr_33199;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_33124__$1,(13),dchan);
} else {
if((state_val_33125 === (14))){
var inst_33109 = cljs.core.async.close_BANG_(out);
var state_33124__$1 = state_33124;
var statearr_33202_35862 = state_33124__$1;
(statearr_33202_35862[(2)] = inst_33109);

(statearr_33202_35862[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33125 === (16))){
var inst_33116 = (state_33124[(2)]);
var state_33124__$1 = state_33124;
var statearr_33209_35866 = state_33124__$1;
(statearr_33209_35866[(2)] = inst_33116);

(statearr_33209_35866[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33125 === (10))){
var inst_33071 = (state_33124[(7)]);
var inst_33096 = (state_33124[(2)]);
var inst_33097 = (inst_33071 + (1));
var inst_33071__$1 = inst_33097;
var state_33124__$1 = (function (){var statearr_33213 = state_33124;
(statearr_33213[(7)] = inst_33071__$1);

(statearr_33213[(14)] = inst_33096);

return statearr_33213;
})();
var statearr_33215_35873 = state_33124__$1;
(statearr_33215_35873[(2)] = null);

(statearr_33215_35873[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33125 === (8))){
var inst_33101 = (state_33124[(2)]);
var state_33124__$1 = state_33124;
var statearr_33217_35878 = state_33124__$1;
(statearr_33217_35878[(2)] = inst_33101);

(statearr_33217_35878[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__30540__auto__ = null;
var cljs$core$async$state_machine__30540__auto____0 = (function (){
var statearr_33220 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_33220[(0)] = cljs$core$async$state_machine__30540__auto__);

(statearr_33220[(1)] = (1));

return statearr_33220;
});
var cljs$core$async$state_machine__30540__auto____1 = (function (state_33124){
while(true){
var ret_value__30541__auto__ = (function (){try{while(true){
var result__30542__auto__ = switch__30539__auto__(state_33124);
if(cljs.core.keyword_identical_QMARK_(result__30542__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30542__auto__;
}
break;
}
}catch (e33221){var ex__30543__auto__ = e33221;
var statearr_33222_35891 = state_33124;
(statearr_33222_35891[(2)] = ex__30543__auto__);


if(cljs.core.seq((state_33124[(4)]))){
var statearr_33223_35892 = state_33124;
(statearr_33223_35892[(1)] = cljs.core.first((state_33124[(4)])));

} else {
throw ex__30543__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30541__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35893 = state_33124;
state_33124 = G__35893;
continue;
} else {
return ret_value__30541__auto__;
}
break;
}
});
cljs$core$async$state_machine__30540__auto__ = function(state_33124){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__30540__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__30540__auto____1.call(this,state_33124);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__30540__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__30540__auto____0;
cljs$core$async$state_machine__30540__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__30540__auto____1;
return cljs$core$async$state_machine__30540__auto__;
})()
})();
var state__30705__auto__ = (function (){var statearr_33228 = f__30704__auto__();
(statearr_33228[(6)] = c__30703__auto___35817);

return statearr_33228;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30705__auto__);
}));


return out;
}));

(cljs.core.async.map.cljs$lang$maxFixedArity = 3);

/**
 * Takes a collection of source channels and returns a channel which
 *   contains all values taken from them. The returned channel will be
 *   unbuffered by default, or a buf-or-n can be supplied. The channel
 *   will close after all the source channels have closed.
 */
cljs.core.async.merge = (function cljs$core$async$merge(var_args){
var G__33244 = arguments.length;
switch (G__33244) {
case 1:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1 = (function (chs){
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2(chs,null);
}));

(cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2 = (function (chs,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__30703__auto___35907 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30704__auto__ = (function (){var switch__30539__auto__ = (function (state_33288){
var state_val_33289 = (state_33288[(1)]);
if((state_val_33289 === (7))){
var inst_33263 = (state_33288[(7)]);
var inst_33264 = (state_33288[(8)]);
var inst_33263__$1 = (state_33288[(2)]);
var inst_33264__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_33263__$1,(0),null);
var inst_33265 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_33263__$1,(1),null);
var inst_33266 = (inst_33264__$1 == null);
var state_33288__$1 = (function (){var statearr_33292 = state_33288;
(statearr_33292[(7)] = inst_33263__$1);

(statearr_33292[(8)] = inst_33264__$1);

(statearr_33292[(9)] = inst_33265);

return statearr_33292;
})();
if(cljs.core.truth_(inst_33266)){
var statearr_33293_35914 = state_33288__$1;
(statearr_33293_35914[(1)] = (8));

} else {
var statearr_33294_35916 = state_33288__$1;
(statearr_33294_35916[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33289 === (1))){
var inst_33253 = cljs.core.vec(chs);
var inst_33254 = inst_33253;
var state_33288__$1 = (function (){var statearr_33296 = state_33288;
(statearr_33296[(10)] = inst_33254);

return statearr_33296;
})();
var statearr_33297_35918 = state_33288__$1;
(statearr_33297_35918[(2)] = null);

(statearr_33297_35918[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33289 === (4))){
var inst_33254 = (state_33288[(10)]);
var state_33288__$1 = state_33288;
return cljs.core.async.ioc_alts_BANG_(state_33288__$1,(7),inst_33254);
} else {
if((state_val_33289 === (6))){
var inst_33284 = (state_33288[(2)]);
var state_33288__$1 = state_33288;
var statearr_33300_35923 = state_33288__$1;
(statearr_33300_35923[(2)] = inst_33284);

(statearr_33300_35923[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33289 === (3))){
var inst_33286 = (state_33288[(2)]);
var state_33288__$1 = state_33288;
return cljs.core.async.impl.ioc_helpers.return_chan(state_33288__$1,inst_33286);
} else {
if((state_val_33289 === (2))){
var inst_33254 = (state_33288[(10)]);
var inst_33256 = cljs.core.count(inst_33254);
var inst_33257 = (inst_33256 > (0));
var state_33288__$1 = state_33288;
if(cljs.core.truth_(inst_33257)){
var statearr_33306_35932 = state_33288__$1;
(statearr_33306_35932[(1)] = (4));

} else {
var statearr_33309_35933 = state_33288__$1;
(statearr_33309_35933[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33289 === (11))){
var inst_33254 = (state_33288[(10)]);
var inst_33277 = (state_33288[(2)]);
var tmp33305 = inst_33254;
var inst_33254__$1 = tmp33305;
var state_33288__$1 = (function (){var statearr_33310 = state_33288;
(statearr_33310[(11)] = inst_33277);

(statearr_33310[(10)] = inst_33254__$1);

return statearr_33310;
})();
var statearr_33311_35937 = state_33288__$1;
(statearr_33311_35937[(2)] = null);

(statearr_33311_35937[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33289 === (9))){
var inst_33264 = (state_33288[(8)]);
var state_33288__$1 = state_33288;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33288__$1,(11),out,inst_33264);
} else {
if((state_val_33289 === (5))){
var inst_33282 = cljs.core.async.close_BANG_(out);
var state_33288__$1 = state_33288;
var statearr_33315_35943 = state_33288__$1;
(statearr_33315_35943[(2)] = inst_33282);

(statearr_33315_35943[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33289 === (10))){
var inst_33280 = (state_33288[(2)]);
var state_33288__$1 = state_33288;
var statearr_33316_35944 = state_33288__$1;
(statearr_33316_35944[(2)] = inst_33280);

(statearr_33316_35944[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33289 === (8))){
var inst_33263 = (state_33288[(7)]);
var inst_33264 = (state_33288[(8)]);
var inst_33254 = (state_33288[(10)]);
var inst_33265 = (state_33288[(9)]);
var inst_33272 = (function (){var cs = inst_33254;
var vec__33259 = inst_33263;
var v = inst_33264;
var c = inst_33265;
return (function (p1__33236_SHARP_){
return cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2(c,p1__33236_SHARP_);
});
})();
var inst_33273 = cljs.core.filterv(inst_33272,inst_33254);
var inst_33254__$1 = inst_33273;
var state_33288__$1 = (function (){var statearr_33317 = state_33288;
(statearr_33317[(10)] = inst_33254__$1);

return statearr_33317;
})();
var statearr_33318_35950 = state_33288__$1;
(statearr_33318_35950[(2)] = null);

(statearr_33318_35950[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__30540__auto__ = null;
var cljs$core$async$state_machine__30540__auto____0 = (function (){
var statearr_33320 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_33320[(0)] = cljs$core$async$state_machine__30540__auto__);

(statearr_33320[(1)] = (1));

return statearr_33320;
});
var cljs$core$async$state_machine__30540__auto____1 = (function (state_33288){
while(true){
var ret_value__30541__auto__ = (function (){try{while(true){
var result__30542__auto__ = switch__30539__auto__(state_33288);
if(cljs.core.keyword_identical_QMARK_(result__30542__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30542__auto__;
}
break;
}
}catch (e33321){var ex__30543__auto__ = e33321;
var statearr_33322_35958 = state_33288;
(statearr_33322_35958[(2)] = ex__30543__auto__);


if(cljs.core.seq((state_33288[(4)]))){
var statearr_33323_35959 = state_33288;
(statearr_33323_35959[(1)] = cljs.core.first((state_33288[(4)])));

} else {
throw ex__30543__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30541__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35970 = state_33288;
state_33288 = G__35970;
continue;
} else {
return ret_value__30541__auto__;
}
break;
}
});
cljs$core$async$state_machine__30540__auto__ = function(state_33288){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__30540__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__30540__auto____1.call(this,state_33288);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__30540__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__30540__auto____0;
cljs$core$async$state_machine__30540__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__30540__auto____1;
return cljs$core$async$state_machine__30540__auto__;
})()
})();
var state__30705__auto__ = (function (){var statearr_33324 = f__30704__auto__();
(statearr_33324[(6)] = c__30703__auto___35907);

return statearr_33324;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30705__auto__);
}));


return out;
}));

(cljs.core.async.merge.cljs$lang$maxFixedArity = 2);

/**
 * Returns a channel containing the single (collection) result of the
 *   items taken from the channel conjoined to the supplied
 *   collection. ch must close before into produces a result.
 */
cljs.core.async.into = (function cljs$core$async$into(coll,ch){
return cljs.core.async.reduce(cljs.core.conj,coll,ch);
});
/**
 * Returns a channel that will return, at most, n items from ch. After n items
 * have been returned, or ch has been closed, the return chanel will close.
 * 
 *   The output channel is unbuffered by default, unless buf-or-n is given.
 */
cljs.core.async.take = (function cljs$core$async$take(var_args){
var G__33329 = arguments.length;
switch (G__33329) {
case 2:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.take.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$3(n,ch,null);
}));

(cljs.core.async.take.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__30703__auto___36009 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30704__auto__ = (function (){var switch__30539__auto__ = (function (state_33363){
var state_val_33364 = (state_33363[(1)]);
if((state_val_33364 === (7))){
var inst_33339 = (state_33363[(7)]);
var inst_33339__$1 = (state_33363[(2)]);
var inst_33340 = (inst_33339__$1 == null);
var inst_33341 = cljs.core.not(inst_33340);
var state_33363__$1 = (function (){var statearr_33373 = state_33363;
(statearr_33373[(7)] = inst_33339__$1);

return statearr_33373;
})();
if(inst_33341){
var statearr_33383_36019 = state_33363__$1;
(statearr_33383_36019[(1)] = (8));

} else {
var statearr_33384_36022 = state_33363__$1;
(statearr_33384_36022[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33364 === (1))){
var inst_33330 = (0);
var state_33363__$1 = (function (){var statearr_33390 = state_33363;
(statearr_33390[(8)] = inst_33330);

return statearr_33390;
})();
var statearr_33391_36030 = state_33363__$1;
(statearr_33391_36030[(2)] = null);

(statearr_33391_36030[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33364 === (4))){
var state_33363__$1 = state_33363;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_33363__$1,(7),ch);
} else {
if((state_val_33364 === (6))){
var inst_33358 = (state_33363[(2)]);
var state_33363__$1 = state_33363;
var statearr_33393_36046 = state_33363__$1;
(statearr_33393_36046[(2)] = inst_33358);

(statearr_33393_36046[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33364 === (3))){
var inst_33360 = (state_33363[(2)]);
var inst_33361 = cljs.core.async.close_BANG_(out);
var state_33363__$1 = (function (){var statearr_33394 = state_33363;
(statearr_33394[(9)] = inst_33360);

return statearr_33394;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_33363__$1,inst_33361);
} else {
if((state_val_33364 === (2))){
var inst_33330 = (state_33363[(8)]);
var inst_33336 = (inst_33330 < n);
var state_33363__$1 = state_33363;
if(cljs.core.truth_(inst_33336)){
var statearr_33396_36073 = state_33363__$1;
(statearr_33396_36073[(1)] = (4));

} else {
var statearr_33397_36079 = state_33363__$1;
(statearr_33397_36079[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33364 === (11))){
var inst_33330 = (state_33363[(8)]);
var inst_33348 = (state_33363[(2)]);
var inst_33351 = (inst_33330 + (1));
var inst_33330__$1 = inst_33351;
var state_33363__$1 = (function (){var statearr_33398 = state_33363;
(statearr_33398[(8)] = inst_33330__$1);

(statearr_33398[(10)] = inst_33348);

return statearr_33398;
})();
var statearr_33399_36095 = state_33363__$1;
(statearr_33399_36095[(2)] = null);

(statearr_33399_36095[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33364 === (9))){
var state_33363__$1 = state_33363;
var statearr_33400_36111 = state_33363__$1;
(statearr_33400_36111[(2)] = null);

(statearr_33400_36111[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33364 === (5))){
var state_33363__$1 = state_33363;
var statearr_33405_36120 = state_33363__$1;
(statearr_33405_36120[(2)] = null);

(statearr_33405_36120[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33364 === (10))){
var inst_33355 = (state_33363[(2)]);
var state_33363__$1 = state_33363;
var statearr_33406_36125 = state_33363__$1;
(statearr_33406_36125[(2)] = inst_33355);

(statearr_33406_36125[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33364 === (8))){
var inst_33339 = (state_33363[(7)]);
var state_33363__$1 = state_33363;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33363__$1,(11),out,inst_33339);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__30540__auto__ = null;
var cljs$core$async$state_machine__30540__auto____0 = (function (){
var statearr_33408 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_33408[(0)] = cljs$core$async$state_machine__30540__auto__);

(statearr_33408[(1)] = (1));

return statearr_33408;
});
var cljs$core$async$state_machine__30540__auto____1 = (function (state_33363){
while(true){
var ret_value__30541__auto__ = (function (){try{while(true){
var result__30542__auto__ = switch__30539__auto__(state_33363);
if(cljs.core.keyword_identical_QMARK_(result__30542__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30542__auto__;
}
break;
}
}catch (e33409){var ex__30543__auto__ = e33409;
var statearr_33410_36155 = state_33363;
(statearr_33410_36155[(2)] = ex__30543__auto__);


if(cljs.core.seq((state_33363[(4)]))){
var statearr_33411_36157 = state_33363;
(statearr_33411_36157[(1)] = cljs.core.first((state_33363[(4)])));

} else {
throw ex__30543__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30541__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__36169 = state_33363;
state_33363 = G__36169;
continue;
} else {
return ret_value__30541__auto__;
}
break;
}
});
cljs$core$async$state_machine__30540__auto__ = function(state_33363){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__30540__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__30540__auto____1.call(this,state_33363);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__30540__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__30540__auto____0;
cljs$core$async$state_machine__30540__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__30540__auto____1;
return cljs$core$async$state_machine__30540__auto__;
})()
})();
var state__30705__auto__ = (function (){var statearr_33412 = f__30704__auto__();
(statearr_33412[(6)] = c__30703__auto___36009);

return statearr_33412;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30705__auto__);
}));


return out;
}));

(cljs.core.async.take.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_LT_ = (function cljs$core$async$map_LT_(f,ch){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async33416 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async33416 = (function (f,ch,meta33417){
this.f = f;
this.ch = ch;
this.meta33417 = meta33417;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async33416.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_33418,meta33417__$1){
var self__ = this;
var _33418__$1 = this;
return (new cljs.core.async.t_cljs$core$async33416(self__.f,self__.ch,meta33417__$1));
}));

(cljs.core.async.t_cljs$core$async33416.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_33418){
var self__ = this;
var _33418__$1 = this;
return self__.meta33417;
}));

(cljs.core.async.t_cljs$core$async33416.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async33416.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async33416.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async33416.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async33416.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
var ret = cljs.core.async.impl.protocols.take_BANG_(self__.ch,(function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async33427 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async33427 = (function (f,ch,meta33417,_,fn1,meta33428){
this.f = f;
this.ch = ch;
this.meta33417 = meta33417;
this._ = _;
this.fn1 = fn1;
this.meta33428 = meta33428;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async33427.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_33429,meta33428__$1){
var self__ = this;
var _33429__$1 = this;
return (new cljs.core.async.t_cljs$core$async33427(self__.f,self__.ch,self__.meta33417,self__._,self__.fn1,meta33428__$1));
}));

(cljs.core.async.t_cljs$core$async33427.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_33429){
var self__ = this;
var _33429__$1 = this;
return self__.meta33428;
}));

(cljs.core.async.t_cljs$core$async33427.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async33427.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (___$1){
var self__ = this;
var ___$2 = this;
return cljs.core.async.impl.protocols.active_QMARK_(self__.fn1);
}));

(cljs.core.async.t_cljs$core$async33427.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (___$1){
var self__ = this;
var ___$2 = this;
return true;
}));

(cljs.core.async.t_cljs$core$async33427.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (___$1){
var self__ = this;
var ___$2 = this;
var f1 = cljs.core.async.impl.protocols.commit(self__.fn1);
return (function (p1__33413_SHARP_){
var G__33442 = (((p1__33413_SHARP_ == null))?null:(self__.f.cljs$core$IFn$_invoke$arity$1 ? self__.f.cljs$core$IFn$_invoke$arity$1(p1__33413_SHARP_) : self__.f.call(null,p1__33413_SHARP_)));
return (f1.cljs$core$IFn$_invoke$arity$1 ? f1.cljs$core$IFn$_invoke$arity$1(G__33442) : f1.call(null,G__33442));
});
}));

(cljs.core.async.t_cljs$core$async33427.getBasis = (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta33417","meta33417",963248721,null),cljs.core.with_meta(new cljs.core.Symbol(null,"_","_",-1201019570,null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"tag","tag",-1290361223),new cljs.core.Symbol("cljs.core.async","t_cljs$core$async33416","cljs.core.async/t_cljs$core$async33416",-1775354034,null)], null)),new cljs.core.Symbol(null,"fn1","fn1",895834444,null),new cljs.core.Symbol(null,"meta33428","meta33428",-265303591,null)], null);
}));

(cljs.core.async.t_cljs$core$async33427.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async33427.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async33427");

(cljs.core.async.t_cljs$core$async33427.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async33427");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async33427.
 */
cljs.core.async.__GT_t_cljs$core$async33427 = (function cljs$core$async$map_LT__$___GT_t_cljs$core$async33427(f__$1,ch__$1,meta33417__$1,___$2,fn1__$1,meta33428){
return (new cljs.core.async.t_cljs$core$async33427(f__$1,ch__$1,meta33417__$1,___$2,fn1__$1,meta33428));
});

}

return (new cljs.core.async.t_cljs$core$async33427(self__.f,self__.ch,self__.meta33417,___$1,fn1,cljs.core.PersistentArrayMap.EMPTY));
})()
);
if(cljs.core.truth_((function (){var and__4115__auto__ = ret;
if(cljs.core.truth_(and__4115__auto__)){
return (!((cljs.core.deref(ret) == null)));
} else {
return and__4115__auto__;
}
})())){
return cljs.core.async.impl.channels.box((function (){var G__33444 = cljs.core.deref(ret);
return (self__.f.cljs$core$IFn$_invoke$arity$1 ? self__.f.cljs$core$IFn$_invoke$arity$1(G__33444) : self__.f.call(null,G__33444));
})());
} else {
return ret;
}
}));

(cljs.core.async.t_cljs$core$async33416.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async33416.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_(self__.ch,val,fn1);
}));

(cljs.core.async.t_cljs$core$async33416.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta33417","meta33417",963248721,null)], null);
}));

(cljs.core.async.t_cljs$core$async33416.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async33416.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async33416");

(cljs.core.async.t_cljs$core$async33416.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async33416");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async33416.
 */
cljs.core.async.__GT_t_cljs$core$async33416 = (function cljs$core$async$map_LT__$___GT_t_cljs$core$async33416(f__$1,ch__$1,meta33417){
return (new cljs.core.async.t_cljs$core$async33416(f__$1,ch__$1,meta33417));
});

}

return (new cljs.core.async.t_cljs$core$async33416(f,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_GT_ = (function cljs$core$async$map_GT_(f,ch){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async33445 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async33445 = (function (f,ch,meta33446){
this.f = f;
this.ch = ch;
this.meta33446 = meta33446;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async33445.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_33447,meta33446__$1){
var self__ = this;
var _33447__$1 = this;
return (new cljs.core.async.t_cljs$core$async33445(self__.f,self__.ch,meta33446__$1));
}));

(cljs.core.async.t_cljs$core$async33445.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_33447){
var self__ = this;
var _33447__$1 = this;
return self__.meta33446;
}));

(cljs.core.async.t_cljs$core$async33445.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async33445.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async33445.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async33445.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_(self__.ch,fn1);
}));

(cljs.core.async.t_cljs$core$async33445.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async33445.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_(self__.ch,(self__.f.cljs$core$IFn$_invoke$arity$1 ? self__.f.cljs$core$IFn$_invoke$arity$1(val) : self__.f.call(null,val)),fn1);
}));

(cljs.core.async.t_cljs$core$async33445.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta33446","meta33446",1371555806,null)], null);
}));

(cljs.core.async.t_cljs$core$async33445.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async33445.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async33445");

(cljs.core.async.t_cljs$core$async33445.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async33445");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async33445.
 */
cljs.core.async.__GT_t_cljs$core$async33445 = (function cljs$core$async$map_GT__$___GT_t_cljs$core$async33445(f__$1,ch__$1,meta33446){
return (new cljs.core.async.t_cljs$core$async33445(f__$1,ch__$1,meta33446));
});

}

return (new cljs.core.async.t_cljs$core$async33445(f,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_GT_ = (function cljs$core$async$filter_GT_(p,ch){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async33464 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async33464 = (function (p,ch,meta33465){
this.p = p;
this.ch = ch;
this.meta33465 = meta33465;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async33464.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_33466,meta33465__$1){
var self__ = this;
var _33466__$1 = this;
return (new cljs.core.async.t_cljs$core$async33464(self__.p,self__.ch,meta33465__$1));
}));

(cljs.core.async.t_cljs$core$async33464.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_33466){
var self__ = this;
var _33466__$1 = this;
return self__.meta33465;
}));

(cljs.core.async.t_cljs$core$async33464.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async33464.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async33464.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async33464.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async33464.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_(self__.ch,fn1);
}));

(cljs.core.async.t_cljs$core$async33464.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async33464.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_((self__.p.cljs$core$IFn$_invoke$arity$1 ? self__.p.cljs$core$IFn$_invoke$arity$1(val) : self__.p.call(null,val)))){
return cljs.core.async.impl.protocols.put_BANG_(self__.ch,val,fn1);
} else {
return cljs.core.async.impl.channels.box(cljs.core.not(cljs.core.async.impl.protocols.closed_QMARK_(self__.ch)));
}
}));

(cljs.core.async.t_cljs$core$async33464.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"p","p",1791580836,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta33465","meta33465",872409545,null)], null);
}));

(cljs.core.async.t_cljs$core$async33464.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async33464.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async33464");

(cljs.core.async.t_cljs$core$async33464.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async33464");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async33464.
 */
cljs.core.async.__GT_t_cljs$core$async33464 = (function cljs$core$async$filter_GT__$___GT_t_cljs$core$async33464(p__$1,ch__$1,meta33465){
return (new cljs.core.async.t_cljs$core$async33464(p__$1,ch__$1,meta33465));
});

}

return (new cljs.core.async.t_cljs$core$async33464(p,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_GT_ = (function cljs$core$async$remove_GT_(p,ch){
return cljs.core.async.filter_GT_(cljs.core.complement(p),ch);
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_LT_ = (function cljs$core$async$filter_LT_(var_args){
var G__33510 = arguments.length;
switch (G__33510) {
case 2:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3(p,ch,null);
}));

(cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__30703__auto___36288 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30704__auto__ = (function (){var switch__30539__auto__ = (function (state_33536){
var state_val_33537 = (state_33536[(1)]);
if((state_val_33537 === (7))){
var inst_33531 = (state_33536[(2)]);
var state_33536__$1 = state_33536;
var statearr_33540_36289 = state_33536__$1;
(statearr_33540_36289[(2)] = inst_33531);

(statearr_33540_36289[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33537 === (1))){
var state_33536__$1 = state_33536;
var statearr_33541_36293 = state_33536__$1;
(statearr_33541_36293[(2)] = null);

(statearr_33541_36293[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33537 === (4))){
var inst_33517 = (state_33536[(7)]);
var inst_33517__$1 = (state_33536[(2)]);
var inst_33518 = (inst_33517__$1 == null);
var state_33536__$1 = (function (){var statearr_33542 = state_33536;
(statearr_33542[(7)] = inst_33517__$1);

return statearr_33542;
})();
if(cljs.core.truth_(inst_33518)){
var statearr_33543_36298 = state_33536__$1;
(statearr_33543_36298[(1)] = (5));

} else {
var statearr_33545_36303 = state_33536__$1;
(statearr_33545_36303[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33537 === (6))){
var inst_33517 = (state_33536[(7)]);
var inst_33522 = (p.cljs$core$IFn$_invoke$arity$1 ? p.cljs$core$IFn$_invoke$arity$1(inst_33517) : p.call(null,inst_33517));
var state_33536__$1 = state_33536;
if(cljs.core.truth_(inst_33522)){
var statearr_33547_36308 = state_33536__$1;
(statearr_33547_36308[(1)] = (8));

} else {
var statearr_33548_36309 = state_33536__$1;
(statearr_33548_36309[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33537 === (3))){
var inst_33533 = (state_33536[(2)]);
var state_33536__$1 = state_33536;
return cljs.core.async.impl.ioc_helpers.return_chan(state_33536__$1,inst_33533);
} else {
if((state_val_33537 === (2))){
var state_33536__$1 = state_33536;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_33536__$1,(4),ch);
} else {
if((state_val_33537 === (11))){
var inst_33525 = (state_33536[(2)]);
var state_33536__$1 = state_33536;
var statearr_33551_36311 = state_33536__$1;
(statearr_33551_36311[(2)] = inst_33525);

(statearr_33551_36311[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33537 === (9))){
var state_33536__$1 = state_33536;
var statearr_33554_36315 = state_33536__$1;
(statearr_33554_36315[(2)] = null);

(statearr_33554_36315[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33537 === (5))){
var inst_33520 = cljs.core.async.close_BANG_(out);
var state_33536__$1 = state_33536;
var statearr_33555_36317 = state_33536__$1;
(statearr_33555_36317[(2)] = inst_33520);

(statearr_33555_36317[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33537 === (10))){
var inst_33528 = (state_33536[(2)]);
var state_33536__$1 = (function (){var statearr_33557 = state_33536;
(statearr_33557[(8)] = inst_33528);

return statearr_33557;
})();
var statearr_33559_36318 = state_33536__$1;
(statearr_33559_36318[(2)] = null);

(statearr_33559_36318[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33537 === (8))){
var inst_33517 = (state_33536[(7)]);
var state_33536__$1 = state_33536;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33536__$1,(11),out,inst_33517);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__30540__auto__ = null;
var cljs$core$async$state_machine__30540__auto____0 = (function (){
var statearr_33561 = [null,null,null,null,null,null,null,null,null];
(statearr_33561[(0)] = cljs$core$async$state_machine__30540__auto__);

(statearr_33561[(1)] = (1));

return statearr_33561;
});
var cljs$core$async$state_machine__30540__auto____1 = (function (state_33536){
while(true){
var ret_value__30541__auto__ = (function (){try{while(true){
var result__30542__auto__ = switch__30539__auto__(state_33536);
if(cljs.core.keyword_identical_QMARK_(result__30542__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30542__auto__;
}
break;
}
}catch (e33562){var ex__30543__auto__ = e33562;
var statearr_33563_36326 = state_33536;
(statearr_33563_36326[(2)] = ex__30543__auto__);


if(cljs.core.seq((state_33536[(4)]))){
var statearr_33566_36328 = state_33536;
(statearr_33566_36328[(1)] = cljs.core.first((state_33536[(4)])));

} else {
throw ex__30543__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30541__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__36333 = state_33536;
state_33536 = G__36333;
continue;
} else {
return ret_value__30541__auto__;
}
break;
}
});
cljs$core$async$state_machine__30540__auto__ = function(state_33536){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__30540__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__30540__auto____1.call(this,state_33536);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__30540__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__30540__auto____0;
cljs$core$async$state_machine__30540__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__30540__auto____1;
return cljs$core$async$state_machine__30540__auto__;
})()
})();
var state__30705__auto__ = (function (){var statearr_33568 = f__30704__auto__();
(statearr_33568[(6)] = c__30703__auto___36288);

return statearr_33568;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30705__auto__);
}));


return out;
}));

(cljs.core.async.filter_LT_.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_LT_ = (function cljs$core$async$remove_LT_(var_args){
var G__33570 = arguments.length;
switch (G__33570) {
case 2:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3(p,ch,null);
}));

(cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3(cljs.core.complement(p),ch,buf_or_n);
}));

(cljs.core.async.remove_LT_.cljs$lang$maxFixedArity = 3);

cljs.core.async.mapcat_STAR_ = (function cljs$core$async$mapcat_STAR_(f,in$,out){
var c__30703__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30704__auto__ = (function (){var switch__30539__auto__ = (function (state_33648){
var state_val_33649 = (state_33648[(1)]);
if((state_val_33649 === (7))){
var inst_33644 = (state_33648[(2)]);
var state_33648__$1 = state_33648;
var statearr_33652_36364 = state_33648__$1;
(statearr_33652_36364[(2)] = inst_33644);

(statearr_33652_36364[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33649 === (20))){
var inst_33612 = (state_33648[(7)]);
var inst_33625 = (state_33648[(2)]);
var inst_33626 = cljs.core.next(inst_33612);
var inst_33596 = inst_33626;
var inst_33597 = null;
var inst_33598 = (0);
var inst_33599 = (0);
var state_33648__$1 = (function (){var statearr_33654 = state_33648;
(statearr_33654[(8)] = inst_33596);

(statearr_33654[(9)] = inst_33625);

(statearr_33654[(10)] = inst_33598);

(statearr_33654[(11)] = inst_33597);

(statearr_33654[(12)] = inst_33599);

return statearr_33654;
})();
var statearr_33655_36375 = state_33648__$1;
(statearr_33655_36375[(2)] = null);

(statearr_33655_36375[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33649 === (1))){
var state_33648__$1 = state_33648;
var statearr_33657_36376 = state_33648__$1;
(statearr_33657_36376[(2)] = null);

(statearr_33657_36376[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33649 === (4))){
var inst_33583 = (state_33648[(13)]);
var inst_33583__$1 = (state_33648[(2)]);
var inst_33584 = (inst_33583__$1 == null);
var state_33648__$1 = (function (){var statearr_33661 = state_33648;
(statearr_33661[(13)] = inst_33583__$1);

return statearr_33661;
})();
if(cljs.core.truth_(inst_33584)){
var statearr_33662_36381 = state_33648__$1;
(statearr_33662_36381[(1)] = (5));

} else {
var statearr_33663_36382 = state_33648__$1;
(statearr_33663_36382[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33649 === (15))){
var state_33648__$1 = state_33648;
var statearr_33667_36383 = state_33648__$1;
(statearr_33667_36383[(2)] = null);

(statearr_33667_36383[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33649 === (21))){
var state_33648__$1 = state_33648;
var statearr_33671_36386 = state_33648__$1;
(statearr_33671_36386[(2)] = null);

(statearr_33671_36386[(1)] = (23));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33649 === (13))){
var inst_33596 = (state_33648[(8)]);
var inst_33598 = (state_33648[(10)]);
var inst_33597 = (state_33648[(11)]);
var inst_33599 = (state_33648[(12)]);
var inst_33607 = (state_33648[(2)]);
var inst_33608 = (inst_33599 + (1));
var tmp33664 = inst_33596;
var tmp33665 = inst_33598;
var tmp33666 = inst_33597;
var inst_33596__$1 = tmp33664;
var inst_33597__$1 = tmp33666;
var inst_33598__$1 = tmp33665;
var inst_33599__$1 = inst_33608;
var state_33648__$1 = (function (){var statearr_33672 = state_33648;
(statearr_33672[(8)] = inst_33596__$1);

(statearr_33672[(10)] = inst_33598__$1);

(statearr_33672[(11)] = inst_33597__$1);

(statearr_33672[(12)] = inst_33599__$1);

(statearr_33672[(14)] = inst_33607);

return statearr_33672;
})();
var statearr_33677_36387 = state_33648__$1;
(statearr_33677_36387[(2)] = null);

(statearr_33677_36387[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33649 === (22))){
var state_33648__$1 = state_33648;
var statearr_33679_36388 = state_33648__$1;
(statearr_33679_36388[(2)] = null);

(statearr_33679_36388[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33649 === (6))){
var inst_33583 = (state_33648[(13)]);
var inst_33593 = (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(inst_33583) : f.call(null,inst_33583));
var inst_33594 = cljs.core.seq(inst_33593);
var inst_33596 = inst_33594;
var inst_33597 = null;
var inst_33598 = (0);
var inst_33599 = (0);
var state_33648__$1 = (function (){var statearr_33680 = state_33648;
(statearr_33680[(8)] = inst_33596);

(statearr_33680[(10)] = inst_33598);

(statearr_33680[(11)] = inst_33597);

(statearr_33680[(12)] = inst_33599);

return statearr_33680;
})();
var statearr_33682_36395 = state_33648__$1;
(statearr_33682_36395[(2)] = null);

(statearr_33682_36395[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33649 === (17))){
var inst_33612 = (state_33648[(7)]);
var inst_33616 = cljs.core.chunk_first(inst_33612);
var inst_33618 = cljs.core.chunk_rest(inst_33612);
var inst_33619 = cljs.core.count(inst_33616);
var inst_33596 = inst_33618;
var inst_33597 = inst_33616;
var inst_33598 = inst_33619;
var inst_33599 = (0);
var state_33648__$1 = (function (){var statearr_33684 = state_33648;
(statearr_33684[(8)] = inst_33596);

(statearr_33684[(10)] = inst_33598);

(statearr_33684[(11)] = inst_33597);

(statearr_33684[(12)] = inst_33599);

return statearr_33684;
})();
var statearr_33685_36408 = state_33648__$1;
(statearr_33685_36408[(2)] = null);

(statearr_33685_36408[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33649 === (3))){
var inst_33646 = (state_33648[(2)]);
var state_33648__$1 = state_33648;
return cljs.core.async.impl.ioc_helpers.return_chan(state_33648__$1,inst_33646);
} else {
if((state_val_33649 === (12))){
var inst_33634 = (state_33648[(2)]);
var state_33648__$1 = state_33648;
var statearr_33686_36418 = state_33648__$1;
(statearr_33686_36418[(2)] = inst_33634);

(statearr_33686_36418[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33649 === (2))){
var state_33648__$1 = state_33648;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_33648__$1,(4),in$);
} else {
if((state_val_33649 === (23))){
var inst_33642 = (state_33648[(2)]);
var state_33648__$1 = state_33648;
var statearr_33688_36425 = state_33648__$1;
(statearr_33688_36425[(2)] = inst_33642);

(statearr_33688_36425[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33649 === (19))){
var inst_33629 = (state_33648[(2)]);
var state_33648__$1 = state_33648;
var statearr_33692_36431 = state_33648__$1;
(statearr_33692_36431[(2)] = inst_33629);

(statearr_33692_36431[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33649 === (11))){
var inst_33596 = (state_33648[(8)]);
var inst_33612 = (state_33648[(7)]);
var inst_33612__$1 = cljs.core.seq(inst_33596);
var state_33648__$1 = (function (){var statearr_33694 = state_33648;
(statearr_33694[(7)] = inst_33612__$1);

return statearr_33694;
})();
if(inst_33612__$1){
var statearr_33695_36438 = state_33648__$1;
(statearr_33695_36438[(1)] = (14));

} else {
var statearr_33696_36443 = state_33648__$1;
(statearr_33696_36443[(1)] = (15));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33649 === (9))){
var inst_33636 = (state_33648[(2)]);
var inst_33637 = cljs.core.async.impl.protocols.closed_QMARK_(out);
var state_33648__$1 = (function (){var statearr_33697 = state_33648;
(statearr_33697[(15)] = inst_33636);

return statearr_33697;
})();
if(cljs.core.truth_(inst_33637)){
var statearr_33699_36450 = state_33648__$1;
(statearr_33699_36450[(1)] = (21));

} else {
var statearr_33701_36451 = state_33648__$1;
(statearr_33701_36451[(1)] = (22));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33649 === (5))){
var inst_33586 = cljs.core.async.close_BANG_(out);
var state_33648__$1 = state_33648;
var statearr_33703_36452 = state_33648__$1;
(statearr_33703_36452[(2)] = inst_33586);

(statearr_33703_36452[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33649 === (14))){
var inst_33612 = (state_33648[(7)]);
var inst_33614 = cljs.core.chunked_seq_QMARK_(inst_33612);
var state_33648__$1 = state_33648;
if(inst_33614){
var statearr_33704_36455 = state_33648__$1;
(statearr_33704_36455[(1)] = (17));

} else {
var statearr_33705_36457 = state_33648__$1;
(statearr_33705_36457[(1)] = (18));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33649 === (16))){
var inst_33632 = (state_33648[(2)]);
var state_33648__$1 = state_33648;
var statearr_33706_36458 = state_33648__$1;
(statearr_33706_36458[(2)] = inst_33632);

(statearr_33706_36458[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33649 === (10))){
var inst_33597 = (state_33648[(11)]);
var inst_33599 = (state_33648[(12)]);
var inst_33605 = cljs.core._nth(inst_33597,inst_33599);
var state_33648__$1 = state_33648;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33648__$1,(13),out,inst_33605);
} else {
if((state_val_33649 === (18))){
var inst_33612 = (state_33648[(7)]);
var inst_33623 = cljs.core.first(inst_33612);
var state_33648__$1 = state_33648;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33648__$1,(20),out,inst_33623);
} else {
if((state_val_33649 === (8))){
var inst_33598 = (state_33648[(10)]);
var inst_33599 = (state_33648[(12)]);
var inst_33602 = (inst_33599 < inst_33598);
var inst_33603 = inst_33602;
var state_33648__$1 = state_33648;
if(cljs.core.truth_(inst_33603)){
var statearr_33712_36468 = state_33648__$1;
(statearr_33712_36468[(1)] = (10));

} else {
var statearr_33713_36470 = state_33648__$1;
(statearr_33713_36470[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$mapcat_STAR__$_state_machine__30540__auto__ = null;
var cljs$core$async$mapcat_STAR__$_state_machine__30540__auto____0 = (function (){
var statearr_33714 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_33714[(0)] = cljs$core$async$mapcat_STAR__$_state_machine__30540__auto__);

(statearr_33714[(1)] = (1));

return statearr_33714;
});
var cljs$core$async$mapcat_STAR__$_state_machine__30540__auto____1 = (function (state_33648){
while(true){
var ret_value__30541__auto__ = (function (){try{while(true){
var result__30542__auto__ = switch__30539__auto__(state_33648);
if(cljs.core.keyword_identical_QMARK_(result__30542__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30542__auto__;
}
break;
}
}catch (e33715){var ex__30543__auto__ = e33715;
var statearr_33716_36475 = state_33648;
(statearr_33716_36475[(2)] = ex__30543__auto__);


if(cljs.core.seq((state_33648[(4)]))){
var statearr_33717_36479 = state_33648;
(statearr_33717_36479[(1)] = cljs.core.first((state_33648[(4)])));

} else {
throw ex__30543__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30541__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__36480 = state_33648;
state_33648 = G__36480;
continue;
} else {
return ret_value__30541__auto__;
}
break;
}
});
cljs$core$async$mapcat_STAR__$_state_machine__30540__auto__ = function(state_33648){
switch(arguments.length){
case 0:
return cljs$core$async$mapcat_STAR__$_state_machine__30540__auto____0.call(this);
case 1:
return cljs$core$async$mapcat_STAR__$_state_machine__30540__auto____1.call(this,state_33648);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mapcat_STAR__$_state_machine__30540__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mapcat_STAR__$_state_machine__30540__auto____0;
cljs$core$async$mapcat_STAR__$_state_machine__30540__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mapcat_STAR__$_state_machine__30540__auto____1;
return cljs$core$async$mapcat_STAR__$_state_machine__30540__auto__;
})()
})();
var state__30705__auto__ = (function (){var statearr_33719 = f__30704__auto__();
(statearr_33719[(6)] = c__30703__auto__);

return statearr_33719;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30705__auto__);
}));

return c__30703__auto__;
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_LT_ = (function cljs$core$async$mapcat_LT_(var_args){
var G__33725 = arguments.length;
switch (G__33725) {
case 2:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2 = (function (f,in$){
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3(f,in$,null);
}));

(cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3 = (function (f,in$,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
cljs.core.async.mapcat_STAR_(f,in$,out);

return out;
}));

(cljs.core.async.mapcat_LT_.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_GT_ = (function cljs$core$async$mapcat_GT_(var_args){
var G__33743 = arguments.length;
switch (G__33743) {
case 2:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2 = (function (f,out){
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3(f,out,null);
}));

(cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3 = (function (f,out,buf_or_n){
var in$ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
cljs.core.async.mapcat_STAR_(f,in$,out);

return in$;
}));

(cljs.core.async.mapcat_GT_.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.unique = (function cljs$core$async$unique(var_args){
var G__33747 = arguments.length;
switch (G__33747) {
case 1:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1 = (function (ch){
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2(ch,null);
}));

(cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2 = (function (ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__30703__auto___36531 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30704__auto__ = (function (){var switch__30539__auto__ = (function (state_33774){
var state_val_33775 = (state_33774[(1)]);
if((state_val_33775 === (7))){
var inst_33769 = (state_33774[(2)]);
var state_33774__$1 = state_33774;
var statearr_33779_36534 = state_33774__$1;
(statearr_33779_36534[(2)] = inst_33769);

(statearr_33779_36534[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33775 === (1))){
var inst_33750 = null;
var state_33774__$1 = (function (){var statearr_33780 = state_33774;
(statearr_33780[(7)] = inst_33750);

return statearr_33780;
})();
var statearr_33781_36536 = state_33774__$1;
(statearr_33781_36536[(2)] = null);

(statearr_33781_36536[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33775 === (4))){
var inst_33754 = (state_33774[(8)]);
var inst_33754__$1 = (state_33774[(2)]);
var inst_33755 = (inst_33754__$1 == null);
var inst_33756 = cljs.core.not(inst_33755);
var state_33774__$1 = (function (){var statearr_33782 = state_33774;
(statearr_33782[(8)] = inst_33754__$1);

return statearr_33782;
})();
if(inst_33756){
var statearr_33785_36539 = state_33774__$1;
(statearr_33785_36539[(1)] = (5));

} else {
var statearr_33786_36541 = state_33774__$1;
(statearr_33786_36541[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33775 === (6))){
var state_33774__$1 = state_33774;
var statearr_33788_36543 = state_33774__$1;
(statearr_33788_36543[(2)] = null);

(statearr_33788_36543[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33775 === (3))){
var inst_33771 = (state_33774[(2)]);
var inst_33772 = cljs.core.async.close_BANG_(out);
var state_33774__$1 = (function (){var statearr_33791 = state_33774;
(statearr_33791[(9)] = inst_33771);

return statearr_33791;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_33774__$1,inst_33772);
} else {
if((state_val_33775 === (2))){
var state_33774__$1 = state_33774;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_33774__$1,(4),ch);
} else {
if((state_val_33775 === (11))){
var inst_33754 = (state_33774[(8)]);
var inst_33763 = (state_33774[(2)]);
var inst_33750 = inst_33754;
var state_33774__$1 = (function (){var statearr_33793 = state_33774;
(statearr_33793[(10)] = inst_33763);

(statearr_33793[(7)] = inst_33750);

return statearr_33793;
})();
var statearr_33794_36552 = state_33774__$1;
(statearr_33794_36552[(2)] = null);

(statearr_33794_36552[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33775 === (9))){
var inst_33754 = (state_33774[(8)]);
var state_33774__$1 = state_33774;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33774__$1,(11),out,inst_33754);
} else {
if((state_val_33775 === (5))){
var inst_33754 = (state_33774[(8)]);
var inst_33750 = (state_33774[(7)]);
var inst_33758 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_33754,inst_33750);
var state_33774__$1 = state_33774;
if(inst_33758){
var statearr_33796_36559 = state_33774__$1;
(statearr_33796_36559[(1)] = (8));

} else {
var statearr_33798_36560 = state_33774__$1;
(statearr_33798_36560[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33775 === (10))){
var inst_33766 = (state_33774[(2)]);
var state_33774__$1 = state_33774;
var statearr_33803_36563 = state_33774__$1;
(statearr_33803_36563[(2)] = inst_33766);

(statearr_33803_36563[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33775 === (8))){
var inst_33750 = (state_33774[(7)]);
var tmp33795 = inst_33750;
var inst_33750__$1 = tmp33795;
var state_33774__$1 = (function (){var statearr_33804 = state_33774;
(statearr_33804[(7)] = inst_33750__$1);

return statearr_33804;
})();
var statearr_33805_36565 = state_33774__$1;
(statearr_33805_36565[(2)] = null);

(statearr_33805_36565[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__30540__auto__ = null;
var cljs$core$async$state_machine__30540__auto____0 = (function (){
var statearr_33806 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_33806[(0)] = cljs$core$async$state_machine__30540__auto__);

(statearr_33806[(1)] = (1));

return statearr_33806;
});
var cljs$core$async$state_machine__30540__auto____1 = (function (state_33774){
while(true){
var ret_value__30541__auto__ = (function (){try{while(true){
var result__30542__auto__ = switch__30539__auto__(state_33774);
if(cljs.core.keyword_identical_QMARK_(result__30542__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30542__auto__;
}
break;
}
}catch (e33807){var ex__30543__auto__ = e33807;
var statearr_33808_36582 = state_33774;
(statearr_33808_36582[(2)] = ex__30543__auto__);


if(cljs.core.seq((state_33774[(4)]))){
var statearr_33813_36586 = state_33774;
(statearr_33813_36586[(1)] = cljs.core.first((state_33774[(4)])));

} else {
throw ex__30543__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30541__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__36593 = state_33774;
state_33774 = G__36593;
continue;
} else {
return ret_value__30541__auto__;
}
break;
}
});
cljs$core$async$state_machine__30540__auto__ = function(state_33774){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__30540__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__30540__auto____1.call(this,state_33774);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__30540__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__30540__auto____0;
cljs$core$async$state_machine__30540__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__30540__auto____1;
return cljs$core$async$state_machine__30540__auto__;
})()
})();
var state__30705__auto__ = (function (){var statearr_33818 = f__30704__auto__();
(statearr_33818[(6)] = c__30703__auto___36531);

return statearr_33818;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30705__auto__);
}));


return out;
}));

(cljs.core.async.unique.cljs$lang$maxFixedArity = 2);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition = (function cljs$core$async$partition(var_args){
var G__33821 = arguments.length;
switch (G__33821) {
case 2:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3(n,ch,null);
}));

(cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__30703__auto___36624 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30704__auto__ = (function (){var switch__30539__auto__ = (function (state_33872){
var state_val_33873 = (state_33872[(1)]);
if((state_val_33873 === (7))){
var inst_33868 = (state_33872[(2)]);
var state_33872__$1 = state_33872;
var statearr_33881_36639 = state_33872__$1;
(statearr_33881_36639[(2)] = inst_33868);

(statearr_33881_36639[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33873 === (1))){
var inst_33827 = (new Array(n));
var inst_33830 = inst_33827;
var inst_33831 = (0);
var state_33872__$1 = (function (){var statearr_33887 = state_33872;
(statearr_33887[(7)] = inst_33830);

(statearr_33887[(8)] = inst_33831);

return statearr_33887;
})();
var statearr_33888_36646 = state_33872__$1;
(statearr_33888_36646[(2)] = null);

(statearr_33888_36646[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33873 === (4))){
var inst_33835 = (state_33872[(9)]);
var inst_33835__$1 = (state_33872[(2)]);
var inst_33836 = (inst_33835__$1 == null);
var inst_33837 = cljs.core.not(inst_33836);
var state_33872__$1 = (function (){var statearr_33895 = state_33872;
(statearr_33895[(9)] = inst_33835__$1);

return statearr_33895;
})();
if(inst_33837){
var statearr_33896_36653 = state_33872__$1;
(statearr_33896_36653[(1)] = (5));

} else {
var statearr_33897_36654 = state_33872__$1;
(statearr_33897_36654[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33873 === (15))){
var inst_33862 = (state_33872[(2)]);
var state_33872__$1 = state_33872;
var statearr_33899_36655 = state_33872__$1;
(statearr_33899_36655[(2)] = inst_33862);

(statearr_33899_36655[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33873 === (13))){
var state_33872__$1 = state_33872;
var statearr_33900_36660 = state_33872__$1;
(statearr_33900_36660[(2)] = null);

(statearr_33900_36660[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33873 === (6))){
var inst_33831 = (state_33872[(8)]);
var inst_33853 = (inst_33831 > (0));
var state_33872__$1 = state_33872;
if(cljs.core.truth_(inst_33853)){
var statearr_33901_36666 = state_33872__$1;
(statearr_33901_36666[(1)] = (12));

} else {
var statearr_33902_36672 = state_33872__$1;
(statearr_33902_36672[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33873 === (3))){
var inst_33870 = (state_33872[(2)]);
var state_33872__$1 = state_33872;
return cljs.core.async.impl.ioc_helpers.return_chan(state_33872__$1,inst_33870);
} else {
if((state_val_33873 === (12))){
var inst_33830 = (state_33872[(7)]);
var inst_33860 = cljs.core.vec(inst_33830);
var state_33872__$1 = state_33872;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33872__$1,(15),out,inst_33860);
} else {
if((state_val_33873 === (2))){
var state_33872__$1 = state_33872;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_33872__$1,(4),ch);
} else {
if((state_val_33873 === (11))){
var inst_33847 = (state_33872[(2)]);
var inst_33848 = (new Array(n));
var inst_33830 = inst_33848;
var inst_33831 = (0);
var state_33872__$1 = (function (){var statearr_33909 = state_33872;
(statearr_33909[(7)] = inst_33830);

(statearr_33909[(8)] = inst_33831);

(statearr_33909[(10)] = inst_33847);

return statearr_33909;
})();
var statearr_33910_36678 = state_33872__$1;
(statearr_33910_36678[(2)] = null);

(statearr_33910_36678[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33873 === (9))){
var inst_33830 = (state_33872[(7)]);
var inst_33845 = cljs.core.vec(inst_33830);
var state_33872__$1 = state_33872;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33872__$1,(11),out,inst_33845);
} else {
if((state_val_33873 === (5))){
var inst_33840 = (state_33872[(11)]);
var inst_33830 = (state_33872[(7)]);
var inst_33831 = (state_33872[(8)]);
var inst_33835 = (state_33872[(9)]);
var inst_33839 = (inst_33830[inst_33831] = inst_33835);
var inst_33840__$1 = (inst_33831 + (1));
var inst_33841 = (inst_33840__$1 < n);
var state_33872__$1 = (function (){var statearr_33913 = state_33872;
(statearr_33913[(12)] = inst_33839);

(statearr_33913[(11)] = inst_33840__$1);

return statearr_33913;
})();
if(cljs.core.truth_(inst_33841)){
var statearr_33915_36694 = state_33872__$1;
(statearr_33915_36694[(1)] = (8));

} else {
var statearr_33916_36695 = state_33872__$1;
(statearr_33916_36695[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33873 === (14))){
var inst_33865 = (state_33872[(2)]);
var inst_33866 = cljs.core.async.close_BANG_(out);
var state_33872__$1 = (function (){var statearr_33918 = state_33872;
(statearr_33918[(13)] = inst_33865);

return statearr_33918;
})();
var statearr_33919_36700 = state_33872__$1;
(statearr_33919_36700[(2)] = inst_33866);

(statearr_33919_36700[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33873 === (10))){
var inst_33851 = (state_33872[(2)]);
var state_33872__$1 = state_33872;
var statearr_33920_36707 = state_33872__$1;
(statearr_33920_36707[(2)] = inst_33851);

(statearr_33920_36707[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33873 === (8))){
var inst_33840 = (state_33872[(11)]);
var inst_33830 = (state_33872[(7)]);
var tmp33917 = inst_33830;
var inst_33830__$1 = tmp33917;
var inst_33831 = inst_33840;
var state_33872__$1 = (function (){var statearr_33925 = state_33872;
(statearr_33925[(7)] = inst_33830__$1);

(statearr_33925[(8)] = inst_33831);

return statearr_33925;
})();
var statearr_33926_36711 = state_33872__$1;
(statearr_33926_36711[(2)] = null);

(statearr_33926_36711[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__30540__auto__ = null;
var cljs$core$async$state_machine__30540__auto____0 = (function (){
var statearr_33932 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_33932[(0)] = cljs$core$async$state_machine__30540__auto__);

(statearr_33932[(1)] = (1));

return statearr_33932;
});
var cljs$core$async$state_machine__30540__auto____1 = (function (state_33872){
while(true){
var ret_value__30541__auto__ = (function (){try{while(true){
var result__30542__auto__ = switch__30539__auto__(state_33872);
if(cljs.core.keyword_identical_QMARK_(result__30542__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30542__auto__;
}
break;
}
}catch (e33933){var ex__30543__auto__ = e33933;
var statearr_33934_36733 = state_33872;
(statearr_33934_36733[(2)] = ex__30543__auto__);


if(cljs.core.seq((state_33872[(4)]))){
var statearr_33935_36741 = state_33872;
(statearr_33935_36741[(1)] = cljs.core.first((state_33872[(4)])));

} else {
throw ex__30543__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30541__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__36749 = state_33872;
state_33872 = G__36749;
continue;
} else {
return ret_value__30541__auto__;
}
break;
}
});
cljs$core$async$state_machine__30540__auto__ = function(state_33872){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__30540__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__30540__auto____1.call(this,state_33872);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__30540__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__30540__auto____0;
cljs$core$async$state_machine__30540__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__30540__auto____1;
return cljs$core$async$state_machine__30540__auto__;
})()
})();
var state__30705__auto__ = (function (){var statearr_33937 = f__30704__auto__();
(statearr_33937[(6)] = c__30703__auto___36624);

return statearr_33937;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30705__auto__);
}));


return out;
}));

(cljs.core.async.partition.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition_by = (function cljs$core$async$partition_by(var_args){
var G__33948 = arguments.length;
switch (G__33948) {
case 2:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2 = (function (f,ch){
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3(f,ch,null);
}));

(cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3 = (function (f,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__30703__auto___36774 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30704__auto__ = (function (){var switch__30539__auto__ = (function (state_33994){
var state_val_33995 = (state_33994[(1)]);
if((state_val_33995 === (7))){
var inst_33990 = (state_33994[(2)]);
var state_33994__$1 = state_33994;
var statearr_34001_36782 = state_33994__$1;
(statearr_34001_36782[(2)] = inst_33990);

(statearr_34001_36782[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33995 === (1))){
var inst_33950 = [];
var inst_33951 = inst_33950;
var inst_33952 = new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123);
var state_33994__$1 = (function (){var statearr_34002 = state_33994;
(statearr_34002[(7)] = inst_33952);

(statearr_34002[(8)] = inst_33951);

return statearr_34002;
})();
var statearr_34005_36792 = state_33994__$1;
(statearr_34005_36792[(2)] = null);

(statearr_34005_36792[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33995 === (4))){
var inst_33955 = (state_33994[(9)]);
var inst_33955__$1 = (state_33994[(2)]);
var inst_33956 = (inst_33955__$1 == null);
var inst_33958 = cljs.core.not(inst_33956);
var state_33994__$1 = (function (){var statearr_34006 = state_33994;
(statearr_34006[(9)] = inst_33955__$1);

return statearr_34006;
})();
if(inst_33958){
var statearr_34007_36799 = state_33994__$1;
(statearr_34007_36799[(1)] = (5));

} else {
var statearr_34009_36802 = state_33994__$1;
(statearr_34009_36802[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33995 === (15))){
var inst_33984 = (state_33994[(2)]);
var state_33994__$1 = state_33994;
var statearr_34010_36806 = state_33994__$1;
(statearr_34010_36806[(2)] = inst_33984);

(statearr_34010_36806[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33995 === (13))){
var state_33994__$1 = state_33994;
var statearr_34015_36807 = state_33994__$1;
(statearr_34015_36807[(2)] = null);

(statearr_34015_36807[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33995 === (6))){
var inst_33951 = (state_33994[(8)]);
var inst_33979 = inst_33951.length;
var inst_33980 = (inst_33979 > (0));
var state_33994__$1 = state_33994;
if(cljs.core.truth_(inst_33980)){
var statearr_34016_36819 = state_33994__$1;
(statearr_34016_36819[(1)] = (12));

} else {
var statearr_34017_36820 = state_33994__$1;
(statearr_34017_36820[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33995 === (3))){
var inst_33992 = (state_33994[(2)]);
var state_33994__$1 = state_33994;
return cljs.core.async.impl.ioc_helpers.return_chan(state_33994__$1,inst_33992);
} else {
if((state_val_33995 === (12))){
var inst_33951 = (state_33994[(8)]);
var inst_33982 = cljs.core.vec(inst_33951);
var state_33994__$1 = state_33994;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33994__$1,(15),out,inst_33982);
} else {
if((state_val_33995 === (2))){
var state_33994__$1 = state_33994;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_33994__$1,(4),ch);
} else {
if((state_val_33995 === (11))){
var inst_33960 = (state_33994[(10)]);
var inst_33955 = (state_33994[(9)]);
var inst_33972 = (state_33994[(2)]);
var inst_33973 = [];
var inst_33974 = inst_33973.push(inst_33955);
var inst_33951 = inst_33973;
var inst_33952 = inst_33960;
var state_33994__$1 = (function (){var statearr_34021 = state_33994;
(statearr_34021[(11)] = inst_33974);

(statearr_34021[(7)] = inst_33952);

(statearr_34021[(8)] = inst_33951);

(statearr_34021[(12)] = inst_33972);

return statearr_34021;
})();
var statearr_34022_36829 = state_33994__$1;
(statearr_34022_36829[(2)] = null);

(statearr_34022_36829[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33995 === (9))){
var inst_33951 = (state_33994[(8)]);
var inst_33970 = cljs.core.vec(inst_33951);
var state_33994__$1 = state_33994;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33994__$1,(11),out,inst_33970);
} else {
if((state_val_33995 === (5))){
var inst_33960 = (state_33994[(10)]);
var inst_33955 = (state_33994[(9)]);
var inst_33952 = (state_33994[(7)]);
var inst_33960__$1 = (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(inst_33955) : f.call(null,inst_33955));
var inst_33963 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_33960__$1,inst_33952);
var inst_33964 = cljs.core.keyword_identical_QMARK_(inst_33952,new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123));
var inst_33965 = ((inst_33963) || (inst_33964));
var state_33994__$1 = (function (){var statearr_34023 = state_33994;
(statearr_34023[(10)] = inst_33960__$1);

return statearr_34023;
})();
if(cljs.core.truth_(inst_33965)){
var statearr_34025_36838 = state_33994__$1;
(statearr_34025_36838[(1)] = (8));

} else {
var statearr_34026_36839 = state_33994__$1;
(statearr_34026_36839[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33995 === (14))){
var inst_33987 = (state_33994[(2)]);
var inst_33988 = cljs.core.async.close_BANG_(out);
var state_33994__$1 = (function (){var statearr_34028 = state_33994;
(statearr_34028[(13)] = inst_33987);

return statearr_34028;
})();
var statearr_34029_36843 = state_33994__$1;
(statearr_34029_36843[(2)] = inst_33988);

(statearr_34029_36843[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33995 === (10))){
var inst_33977 = (state_33994[(2)]);
var state_33994__$1 = state_33994;
var statearr_34030_36845 = state_33994__$1;
(statearr_34030_36845[(2)] = inst_33977);

(statearr_34030_36845[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33995 === (8))){
var inst_33960 = (state_33994[(10)]);
var inst_33955 = (state_33994[(9)]);
var inst_33951 = (state_33994[(8)]);
var inst_33967 = inst_33951.push(inst_33955);
var tmp34027 = inst_33951;
var inst_33951__$1 = tmp34027;
var inst_33952 = inst_33960;
var state_33994__$1 = (function (){var statearr_34031 = state_33994;
(statearr_34031[(7)] = inst_33952);

(statearr_34031[(8)] = inst_33951__$1);

(statearr_34031[(14)] = inst_33967);

return statearr_34031;
})();
var statearr_34034_36853 = state_33994__$1;
(statearr_34034_36853[(2)] = null);

(statearr_34034_36853[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__30540__auto__ = null;
var cljs$core$async$state_machine__30540__auto____0 = (function (){
var statearr_34039 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_34039[(0)] = cljs$core$async$state_machine__30540__auto__);

(statearr_34039[(1)] = (1));

return statearr_34039;
});
var cljs$core$async$state_machine__30540__auto____1 = (function (state_33994){
while(true){
var ret_value__30541__auto__ = (function (){try{while(true){
var result__30542__auto__ = switch__30539__auto__(state_33994);
if(cljs.core.keyword_identical_QMARK_(result__30542__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30542__auto__;
}
break;
}
}catch (e34040){var ex__30543__auto__ = e34040;
var statearr_34041_36860 = state_33994;
(statearr_34041_36860[(2)] = ex__30543__auto__);


if(cljs.core.seq((state_33994[(4)]))){
var statearr_34042_36866 = state_33994;
(statearr_34042_36866[(1)] = cljs.core.first((state_33994[(4)])));

} else {
throw ex__30543__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30541__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__36875 = state_33994;
state_33994 = G__36875;
continue;
} else {
return ret_value__30541__auto__;
}
break;
}
});
cljs$core$async$state_machine__30540__auto__ = function(state_33994){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__30540__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__30540__auto____1.call(this,state_33994);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__30540__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__30540__auto____0;
cljs$core$async$state_machine__30540__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__30540__auto____1;
return cljs$core$async$state_machine__30540__auto__;
})()
})();
var state__30705__auto__ = (function (){var statearr_34044 = f__30704__auto__();
(statearr_34044[(6)] = c__30703__auto___36774);

return statearr_34044;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30705__auto__);
}));


return out;
}));

(cljs.core.async.partition_by.cljs$lang$maxFixedArity = 3);


//# sourceMappingURL=cljs.core.async.js.map
