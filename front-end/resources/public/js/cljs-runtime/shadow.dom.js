goog.provide('shadow.dom');
shadow.dom.transition_supported_QMARK_ = (((typeof window !== 'undefined'))?goog.style.transition.isSupported():null);

/**
 * @interface
 */
shadow.dom.IElement = function(){};

var shadow$dom$IElement$_to_dom$dyn_35213 = (function (this$){
var x__4428__auto__ = (((this$ == null))?null:this$);
var m__4429__auto__ = (shadow.dom._to_dom[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4429__auto__.call(null,this$));
} else {
var m__4426__auto__ = (shadow.dom._to_dom["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4426__auto__.call(null,this$));
} else {
throw cljs.core.missing_protocol("IElement.-to-dom",this$);
}
}
});
shadow.dom._to_dom = (function shadow$dom$_to_dom(this$){
if((((!((this$ == null)))) && ((!((this$.shadow$dom$IElement$_to_dom$arity$1 == null)))))){
return this$.shadow$dom$IElement$_to_dom$arity$1(this$);
} else {
return shadow$dom$IElement$_to_dom$dyn_35213(this$);
}
});


/**
 * @interface
 */
shadow.dom.SVGElement = function(){};

var shadow$dom$SVGElement$_to_svg$dyn_35220 = (function (this$){
var x__4428__auto__ = (((this$ == null))?null:this$);
var m__4429__auto__ = (shadow.dom._to_svg[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4429__auto__.call(null,this$));
} else {
var m__4426__auto__ = (shadow.dom._to_svg["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4426__auto__.call(null,this$));
} else {
throw cljs.core.missing_protocol("SVGElement.-to-svg",this$);
}
}
});
shadow.dom._to_svg = (function shadow$dom$_to_svg(this$){
if((((!((this$ == null)))) && ((!((this$.shadow$dom$SVGElement$_to_svg$arity$1 == null)))))){
return this$.shadow$dom$SVGElement$_to_svg$arity$1(this$);
} else {
return shadow$dom$SVGElement$_to_svg$dyn_35220(this$);
}
});

shadow.dom.lazy_native_coll_seq = (function shadow$dom$lazy_native_coll_seq(coll,idx){
if((idx < coll.length)){
return (new cljs.core.LazySeq(null,(function (){
return cljs.core.cons((coll[idx]),(function (){var G__34196 = coll;
var G__34197 = (idx + (1));
return (shadow.dom.lazy_native_coll_seq.cljs$core$IFn$_invoke$arity$2 ? shadow.dom.lazy_native_coll_seq.cljs$core$IFn$_invoke$arity$2(G__34196,G__34197) : shadow.dom.lazy_native_coll_seq.call(null,G__34196,G__34197));
})());
}),null,null));
} else {
return null;
}
});

/**
* @constructor
 * @implements {cljs.core.IIndexed}
 * @implements {cljs.core.ICounted}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IDeref}
 * @implements {shadow.dom.IElement}
*/
shadow.dom.NativeColl = (function (coll){
this.coll = coll;
this.cljs$lang$protocol_mask$partition0$ = 8421394;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(shadow.dom.NativeColl.prototype.cljs$core$IDeref$_deref$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return self__.coll;
}));

(shadow.dom.NativeColl.prototype.cljs$core$IIndexed$_nth$arity$2 = (function (this$,n){
var self__ = this;
var this$__$1 = this;
return (self__.coll[n]);
}));

(shadow.dom.NativeColl.prototype.cljs$core$IIndexed$_nth$arity$3 = (function (this$,n,not_found){
var self__ = this;
var this$__$1 = this;
var or__4126__auto__ = (self__.coll[n]);
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return not_found;
}
}));

(shadow.dom.NativeColl.prototype.cljs$core$ICounted$_count$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return self__.coll.length;
}));

(shadow.dom.NativeColl.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return shadow.dom.lazy_native_coll_seq(self__.coll,(0));
}));

(shadow.dom.NativeColl.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(shadow.dom.NativeColl.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return self__.coll;
}));

(shadow.dom.NativeColl.getBasis = (function (){
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"coll","coll",-1006698606,null)], null);
}));

(shadow.dom.NativeColl.cljs$lang$type = true);

(shadow.dom.NativeColl.cljs$lang$ctorStr = "shadow.dom/NativeColl");

(shadow.dom.NativeColl.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"shadow.dom/NativeColl");
}));

/**
 * Positional factory function for shadow.dom/NativeColl.
 */
shadow.dom.__GT_NativeColl = (function shadow$dom$__GT_NativeColl(coll){
return (new shadow.dom.NativeColl(coll));
});

shadow.dom.native_coll = (function shadow$dom$native_coll(coll){
return (new shadow.dom.NativeColl(coll));
});
shadow.dom.dom_node = (function shadow$dom$dom_node(el){
if((el == null)){
return null;
} else {
if((((!((el == null))))?((((false) || ((cljs.core.PROTOCOL_SENTINEL === el.shadow$dom$IElement$))))?true:false):false)){
return el.shadow$dom$IElement$_to_dom$arity$1(null);
} else {
if(typeof el === 'string'){
return document.createTextNode(el);
} else {
if(typeof el === 'number'){
return document.createTextNode(cljs.core.str.cljs$core$IFn$_invoke$arity$1(el));
} else {
return el;

}
}
}
}
});
shadow.dom.query_one = (function shadow$dom$query_one(var_args){
var G__34207 = arguments.length;
switch (G__34207) {
case 1:
return shadow.dom.query_one.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.query_one.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.query_one.cljs$core$IFn$_invoke$arity$1 = (function (sel){
return document.querySelector(sel);
}));

(shadow.dom.query_one.cljs$core$IFn$_invoke$arity$2 = (function (sel,root){
return shadow.dom.dom_node(root).querySelector(sel);
}));

(shadow.dom.query_one.cljs$lang$maxFixedArity = 2);

shadow.dom.query = (function shadow$dom$query(var_args){
var G__34213 = arguments.length;
switch (G__34213) {
case 1:
return shadow.dom.query.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.query.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.query.cljs$core$IFn$_invoke$arity$1 = (function (sel){
return (new shadow.dom.NativeColl(document.querySelectorAll(sel)));
}));

(shadow.dom.query.cljs$core$IFn$_invoke$arity$2 = (function (sel,root){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(root).querySelectorAll(sel)));
}));

(shadow.dom.query.cljs$lang$maxFixedArity = 2);

shadow.dom.by_id = (function shadow$dom$by_id(var_args){
var G__34218 = arguments.length;
switch (G__34218) {
case 2:
return shadow.dom.by_id.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 1:
return shadow.dom.by_id.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.by_id.cljs$core$IFn$_invoke$arity$2 = (function (id,el){
return shadow.dom.dom_node(el).getElementById(id);
}));

(shadow.dom.by_id.cljs$core$IFn$_invoke$arity$1 = (function (id){
return document.getElementById(id);
}));

(shadow.dom.by_id.cljs$lang$maxFixedArity = 2);

shadow.dom.build = shadow.dom.dom_node;
shadow.dom.ev_stop = (function shadow$dom$ev_stop(var_args){
var G__34222 = arguments.length;
switch (G__34222) {
case 1:
return shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 4:
return shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1 = (function (e){
if(cljs.core.truth_(e.stopPropagation)){
e.stopPropagation();

e.preventDefault();
} else {
(e.cancelBubble = true);

(e.returnValue = false);
}

return e;
}));

(shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$2 = (function (e,el){
shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1(e);

return el;
}));

(shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$4 = (function (e,el,scope,owner){
shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1(e);

return el;
}));

(shadow.dom.ev_stop.cljs$lang$maxFixedArity = 4);

/**
 * check wether a parent node (or the document) contains the child
 */
shadow.dom.contains_QMARK_ = (function shadow$dom$contains_QMARK_(var_args){
var G__34234 = arguments.length;
switch (G__34234) {
case 1:
return shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$1 = (function (el){
return goog.dom.contains(document,shadow.dom.dom_node(el));
}));

(shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$2 = (function (parent,el){
return goog.dom.contains(shadow.dom.dom_node(parent),shadow.dom.dom_node(el));
}));

(shadow.dom.contains_QMARK_.cljs$lang$maxFixedArity = 2);

shadow.dom.add_class = (function shadow$dom$add_class(el,cls){
return goog.dom.classlist.add(shadow.dom.dom_node(el),cls);
});
shadow.dom.remove_class = (function shadow$dom$remove_class(el,cls){
return goog.dom.classlist.remove(shadow.dom.dom_node(el),cls);
});
shadow.dom.toggle_class = (function shadow$dom$toggle_class(var_args){
var G__34247 = arguments.length;
switch (G__34247) {
case 2:
return shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$2 = (function (el,cls){
return goog.dom.classlist.toggle(shadow.dom.dom_node(el),cls);
}));

(shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$3 = (function (el,cls,v){
if(cljs.core.truth_(v)){
return shadow.dom.add_class(el,cls);
} else {
return shadow.dom.remove_class(el,cls);
}
}));

(shadow.dom.toggle_class.cljs$lang$maxFixedArity = 3);

shadow.dom.dom_listen = (cljs.core.truth_((function (){var or__4126__auto__ = (!((typeof document !== 'undefined')));
if(or__4126__auto__){
return or__4126__auto__;
} else {
return document.addEventListener;
}
})())?(function shadow$dom$dom_listen_good(el,ev,handler){
return el.addEventListener(ev,handler,false);
}):(function shadow$dom$dom_listen_ie(el,ev,handler){
try{return el.attachEvent(["on",cljs.core.str.cljs$core$IFn$_invoke$arity$1(ev)].join(''),(function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
}));
}catch (e34261){if((e34261 instanceof Object)){
var e = e34261;
return console.log("didnt support attachEvent",el,e);
} else {
throw e34261;

}
}}));
shadow.dom.dom_listen_remove = (cljs.core.truth_((function (){var or__4126__auto__ = (!((typeof document !== 'undefined')));
if(or__4126__auto__){
return or__4126__auto__;
} else {
return document.removeEventListener;
}
})())?(function shadow$dom$dom_listen_remove_good(el,ev,handler){
return el.removeEventListener(ev,handler,false);
}):(function shadow$dom$dom_listen_remove_ie(el,ev,handler){
return el.detachEvent(["on",cljs.core.str.cljs$core$IFn$_invoke$arity$1(ev)].join(''),handler);
}));
shadow.dom.on_query = (function shadow$dom$on_query(root_el,ev,selector,handler){
var seq__34269 = cljs.core.seq(shadow.dom.query.cljs$core$IFn$_invoke$arity$2(selector,root_el));
var chunk__34270 = null;
var count__34271 = (0);
var i__34272 = (0);
while(true){
if((i__34272 < count__34271)){
var el = chunk__34270.cljs$core$IIndexed$_nth$arity$2(null,i__34272);
var handler_35269__$1 = ((function (seq__34269,chunk__34270,count__34271,i__34272,el){
return (function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
});})(seq__34269,chunk__34270,count__34271,i__34272,el))
;
shadow.dom.dom_listen(el,cljs.core.name(ev),handler_35269__$1);


var G__35271 = seq__34269;
var G__35272 = chunk__34270;
var G__35273 = count__34271;
var G__35274 = (i__34272 + (1));
seq__34269 = G__35271;
chunk__34270 = G__35272;
count__34271 = G__35273;
i__34272 = G__35274;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__34269);
if(temp__5735__auto__){
var seq__34269__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__34269__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__34269__$1);
var G__35278 = cljs.core.chunk_rest(seq__34269__$1);
var G__35279 = c__4556__auto__;
var G__35280 = cljs.core.count(c__4556__auto__);
var G__35281 = (0);
seq__34269 = G__35278;
chunk__34270 = G__35279;
count__34271 = G__35280;
i__34272 = G__35281;
continue;
} else {
var el = cljs.core.first(seq__34269__$1);
var handler_35284__$1 = ((function (seq__34269,chunk__34270,count__34271,i__34272,el,seq__34269__$1,temp__5735__auto__){
return (function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
});})(seq__34269,chunk__34270,count__34271,i__34272,el,seq__34269__$1,temp__5735__auto__))
;
shadow.dom.dom_listen(el,cljs.core.name(ev),handler_35284__$1);


var G__35285 = cljs.core.next(seq__34269__$1);
var G__35286 = null;
var G__35287 = (0);
var G__35288 = (0);
seq__34269 = G__35285;
chunk__34270 = G__35286;
count__34271 = G__35287;
i__34272 = G__35288;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.dom.on = (function shadow$dom$on(var_args){
var G__34286 = arguments.length;
switch (G__34286) {
case 3:
return shadow.dom.on.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return shadow.dom.on.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.on.cljs$core$IFn$_invoke$arity$3 = (function (el,ev,handler){
return shadow.dom.on.cljs$core$IFn$_invoke$arity$4(el,ev,handler,false);
}));

(shadow.dom.on.cljs$core$IFn$_invoke$arity$4 = (function (el,ev,handler,capture){
if(cljs.core.vector_QMARK_(ev)){
return shadow.dom.on_query(el,cljs.core.first(ev),cljs.core.second(ev),handler);
} else {
var handler__$1 = (function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
});
return shadow.dom.dom_listen(shadow.dom.dom_node(el),cljs.core.name(ev),handler__$1);
}
}));

(shadow.dom.on.cljs$lang$maxFixedArity = 4);

shadow.dom.remove_event_handler = (function shadow$dom$remove_event_handler(el,ev,handler){
return shadow.dom.dom_listen_remove(shadow.dom.dom_node(el),cljs.core.name(ev),handler);
});
shadow.dom.add_event_listeners = (function shadow$dom$add_event_listeners(el,events){
var seq__34301 = cljs.core.seq(events);
var chunk__34302 = null;
var count__34303 = (0);
var i__34304 = (0);
while(true){
if((i__34304 < count__34303)){
var vec__34321 = chunk__34302.cljs$core$IIndexed$_nth$arity$2(null,i__34304);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34321,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34321,(1),null);
shadow.dom.on.cljs$core$IFn$_invoke$arity$3(el,k,v);


var G__35300 = seq__34301;
var G__35301 = chunk__34302;
var G__35302 = count__34303;
var G__35303 = (i__34304 + (1));
seq__34301 = G__35300;
chunk__34302 = G__35301;
count__34303 = G__35302;
i__34304 = G__35303;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__34301);
if(temp__5735__auto__){
var seq__34301__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__34301__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__34301__$1);
var G__35308 = cljs.core.chunk_rest(seq__34301__$1);
var G__35309 = c__4556__auto__;
var G__35310 = cljs.core.count(c__4556__auto__);
var G__35311 = (0);
seq__34301 = G__35308;
chunk__34302 = G__35309;
count__34303 = G__35310;
i__34304 = G__35311;
continue;
} else {
var vec__34329 = cljs.core.first(seq__34301__$1);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34329,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34329,(1),null);
shadow.dom.on.cljs$core$IFn$_invoke$arity$3(el,k,v);


var G__35318 = cljs.core.next(seq__34301__$1);
var G__35319 = null;
var G__35320 = (0);
var G__35321 = (0);
seq__34301 = G__35318;
chunk__34302 = G__35319;
count__34303 = G__35320;
i__34304 = G__35321;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.dom.set_style = (function shadow$dom$set_style(el,styles){
var dom = shadow.dom.dom_node(el);
var seq__34336 = cljs.core.seq(styles);
var chunk__34337 = null;
var count__34338 = (0);
var i__34339 = (0);
while(true){
if((i__34339 < count__34338)){
var vec__34361 = chunk__34337.cljs$core$IIndexed$_nth$arity$2(null,i__34339);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34361,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34361,(1),null);
goog.style.setStyle(dom,cljs.core.name(k),(((v == null))?"":v));


var G__35326 = seq__34336;
var G__35327 = chunk__34337;
var G__35328 = count__34338;
var G__35329 = (i__34339 + (1));
seq__34336 = G__35326;
chunk__34337 = G__35327;
count__34338 = G__35328;
i__34339 = G__35329;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__34336);
if(temp__5735__auto__){
var seq__34336__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__34336__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__34336__$1);
var G__35334 = cljs.core.chunk_rest(seq__34336__$1);
var G__35335 = c__4556__auto__;
var G__35336 = cljs.core.count(c__4556__auto__);
var G__35337 = (0);
seq__34336 = G__35334;
chunk__34337 = G__35335;
count__34338 = G__35336;
i__34339 = G__35337;
continue;
} else {
var vec__34376 = cljs.core.first(seq__34336__$1);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34376,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34376,(1),null);
goog.style.setStyle(dom,cljs.core.name(k),(((v == null))?"":v));


var G__35342 = cljs.core.next(seq__34336__$1);
var G__35343 = null;
var G__35344 = (0);
var G__35345 = (0);
seq__34336 = G__35342;
chunk__34337 = G__35343;
count__34338 = G__35344;
i__34339 = G__35345;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.dom.set_attr_STAR_ = (function shadow$dom$set_attr_STAR_(el,key,value){
var G__34385_35352 = key;
var G__34385_35353__$1 = (((G__34385_35352 instanceof cljs.core.Keyword))?G__34385_35352.fqn:null);
switch (G__34385_35353__$1) {
case "id":
(el.id = cljs.core.str.cljs$core$IFn$_invoke$arity$1(value));

break;
case "class":
(el.className = cljs.core.str.cljs$core$IFn$_invoke$arity$1(value));

break;
case "for":
(el.htmlFor = value);

break;
case "cellpadding":
el.setAttribute("cellPadding",value);

break;
case "cellspacing":
el.setAttribute("cellSpacing",value);

break;
case "colspan":
el.setAttribute("colSpan",value);

break;
case "frameborder":
el.setAttribute("frameBorder",value);

break;
case "height":
el.setAttribute("height",value);

break;
case "maxlength":
el.setAttribute("maxLength",value);

break;
case "role":
el.setAttribute("role",value);

break;
case "rowspan":
el.setAttribute("rowSpan",value);

break;
case "type":
el.setAttribute("type",value);

break;
case "usemap":
el.setAttribute("useMap",value);

break;
case "valign":
el.setAttribute("vAlign",value);

break;
case "width":
el.setAttribute("width",value);

break;
case "on":
shadow.dom.add_event_listeners(el,value);

break;
case "style":
if((value == null)){
} else {
if(typeof value === 'string'){
el.setAttribute("style",value);
} else {
if(cljs.core.map_QMARK_(value)){
shadow.dom.set_style(el,value);
} else {
goog.style.setStyle(el,value);

}
}
}

break;
default:
var ks_35370 = cljs.core.name(key);
if(cljs.core.truth_((function (){var or__4126__auto__ = goog.string.startsWith(ks_35370,"data-");
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return goog.string.startsWith(ks_35370,"aria-");
}
})())){
el.setAttribute(ks_35370,value);
} else {
(el[ks_35370] = value);
}

}

return el;
});
shadow.dom.set_attrs = (function shadow$dom$set_attrs(el,attrs){
return cljs.core.reduce_kv((function (el__$1,key,value){
shadow.dom.set_attr_STAR_(el__$1,key,value);

return el__$1;
}),shadow.dom.dom_node(el),attrs);
});
shadow.dom.set_attr = (function shadow$dom$set_attr(el,key,value){
return shadow.dom.set_attr_STAR_(shadow.dom.dom_node(el),key,value);
});
shadow.dom.has_class_QMARK_ = (function shadow$dom$has_class_QMARK_(el,cls){
return goog.dom.classlist.contains(shadow.dom.dom_node(el),cls);
});
shadow.dom.merge_class_string = (function shadow$dom$merge_class_string(current,extra_class){
if(cljs.core.seq(current)){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(current)," ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(extra_class)].join('');
} else {
return extra_class;
}
});
shadow.dom.parse_tag = (function shadow$dom$parse_tag(spec){
var spec__$1 = cljs.core.name(spec);
var fdot = spec__$1.indexOf(".");
var fhash = spec__$1.indexOf("#");
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fdot)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fhash)))){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1,null,null], null);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fhash)){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1.substring((0),fdot),null,clojure.string.replace(spec__$1.substring((fdot + (1))),/\./," ")], null);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fdot)){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1.substring((0),fhash),spec__$1.substring((fhash + (1))),null], null);
} else {
if((fhash > fdot)){
throw ["cant have id after class?",spec__$1].join('');
} else {
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1.substring((0),fhash),spec__$1.substring((fhash + (1)),fdot),clojure.string.replace(spec__$1.substring((fdot + (1))),/\./," ")], null);

}
}
}
}
});
shadow.dom.create_dom_node = (function shadow$dom$create_dom_node(tag_def,p__34415){
var map__34416 = p__34415;
var map__34416__$1 = (((((!((map__34416 == null))))?(((((map__34416.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__34416.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__34416):map__34416);
var props = map__34416__$1;
var class$ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__34416__$1,new cljs.core.Keyword(null,"class","class",-2030961996));
var tag_props = ({});
var vec__34419 = shadow.dom.parse_tag(tag_def);
var tag_name = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34419,(0),null);
var tag_id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34419,(1),null);
var tag_classes = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34419,(2),null);
if(cljs.core.truth_(tag_id)){
(tag_props["id"] = tag_id);
} else {
}

if(cljs.core.truth_(tag_classes)){
(tag_props["class"] = shadow.dom.merge_class_string(class$,tag_classes));
} else {
}

var G__34423 = goog.dom.createDom(tag_name,tag_props);
shadow.dom.set_attrs(G__34423,cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(props,new cljs.core.Keyword(null,"class","class",-2030961996)));

return G__34423;
});
shadow.dom.append = (function shadow$dom$append(var_args){
var G__34426 = arguments.length;
switch (G__34426) {
case 1:
return shadow.dom.append.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.append.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.append.cljs$core$IFn$_invoke$arity$1 = (function (node){
if(cljs.core.truth_(node)){
var temp__5735__auto__ = shadow.dom.dom_node(node);
if(cljs.core.truth_(temp__5735__auto__)){
var n = temp__5735__auto__;
document.body.appendChild(n);

return n;
} else {
return null;
}
} else {
return null;
}
}));

(shadow.dom.append.cljs$core$IFn$_invoke$arity$2 = (function (el,node){
if(cljs.core.truth_(node)){
var temp__5735__auto__ = shadow.dom.dom_node(node);
if(cljs.core.truth_(temp__5735__auto__)){
var n = temp__5735__auto__;
shadow.dom.dom_node(el).appendChild(n);

return n;
} else {
return null;
}
} else {
return null;
}
}));

(shadow.dom.append.cljs$lang$maxFixedArity = 2);

shadow.dom.destructure_node = (function shadow$dom$destructure_node(create_fn,p__34449){
var vec__34450 = p__34449;
var seq__34451 = cljs.core.seq(vec__34450);
var first__34452 = cljs.core.first(seq__34451);
var seq__34451__$1 = cljs.core.next(seq__34451);
var nn = first__34452;
var first__34452__$1 = cljs.core.first(seq__34451__$1);
var seq__34451__$2 = cljs.core.next(seq__34451__$1);
var np = first__34452__$1;
var nc = seq__34451__$2;
var node = vec__34450;
if((nn instanceof cljs.core.Keyword)){
} else {
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("invalid dom node",new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"node","node",581201198),node], null));
}

if((((np == null)) && ((nc == null)))){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(function (){var G__34457 = nn;
var G__34458 = cljs.core.PersistentArrayMap.EMPTY;
return (create_fn.cljs$core$IFn$_invoke$arity$2 ? create_fn.cljs$core$IFn$_invoke$arity$2(G__34457,G__34458) : create_fn.call(null,G__34457,G__34458));
})(),cljs.core.List.EMPTY], null);
} else {
if(cljs.core.map_QMARK_(np)){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(create_fn.cljs$core$IFn$_invoke$arity$2 ? create_fn.cljs$core$IFn$_invoke$arity$2(nn,np) : create_fn.call(null,nn,np)),nc], null);
} else {
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(function (){var G__34460 = nn;
var G__34461 = cljs.core.PersistentArrayMap.EMPTY;
return (create_fn.cljs$core$IFn$_invoke$arity$2 ? create_fn.cljs$core$IFn$_invoke$arity$2(G__34460,G__34461) : create_fn.call(null,G__34460,G__34461));
})(),cljs.core.conj.cljs$core$IFn$_invoke$arity$2(nc,np)], null);

}
}
});
shadow.dom.make_dom_node = (function shadow$dom$make_dom_node(structure){
var vec__34463 = shadow.dom.destructure_node(shadow.dom.create_dom_node,structure);
var node = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34463,(0),null);
var node_children = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34463,(1),null);
var seq__34468_35423 = cljs.core.seq(node_children);
var chunk__34469_35424 = null;
var count__34470_35425 = (0);
var i__34471_35426 = (0);
while(true){
if((i__34471_35426 < count__34470_35425)){
var child_struct_35428 = chunk__34469_35424.cljs$core$IIndexed$_nth$arity$2(null,i__34471_35426);
var children_35429 = shadow.dom.dom_node(child_struct_35428);
if(cljs.core.seq_QMARK_(children_35429)){
var seq__34508_35430 = cljs.core.seq(cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom.dom_node,children_35429));
var chunk__34510_35431 = null;
var count__34511_35432 = (0);
var i__34512_35433 = (0);
while(true){
if((i__34512_35433 < count__34511_35432)){
var child_35435 = chunk__34510_35431.cljs$core$IIndexed$_nth$arity$2(null,i__34512_35433);
if(cljs.core.truth_(child_35435)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_35435);


var G__35436 = seq__34508_35430;
var G__35437 = chunk__34510_35431;
var G__35438 = count__34511_35432;
var G__35439 = (i__34512_35433 + (1));
seq__34508_35430 = G__35436;
chunk__34510_35431 = G__35437;
count__34511_35432 = G__35438;
i__34512_35433 = G__35439;
continue;
} else {
var G__35442 = seq__34508_35430;
var G__35443 = chunk__34510_35431;
var G__35444 = count__34511_35432;
var G__35445 = (i__34512_35433 + (1));
seq__34508_35430 = G__35442;
chunk__34510_35431 = G__35443;
count__34511_35432 = G__35444;
i__34512_35433 = G__35445;
continue;
}
} else {
var temp__5735__auto___35446 = cljs.core.seq(seq__34508_35430);
if(temp__5735__auto___35446){
var seq__34508_35447__$1 = temp__5735__auto___35446;
if(cljs.core.chunked_seq_QMARK_(seq__34508_35447__$1)){
var c__4556__auto___35448 = cljs.core.chunk_first(seq__34508_35447__$1);
var G__35449 = cljs.core.chunk_rest(seq__34508_35447__$1);
var G__35450 = c__4556__auto___35448;
var G__35451 = cljs.core.count(c__4556__auto___35448);
var G__35452 = (0);
seq__34508_35430 = G__35449;
chunk__34510_35431 = G__35450;
count__34511_35432 = G__35451;
i__34512_35433 = G__35452;
continue;
} else {
var child_35457 = cljs.core.first(seq__34508_35447__$1);
if(cljs.core.truth_(child_35457)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_35457);


var G__35458 = cljs.core.next(seq__34508_35447__$1);
var G__35459 = null;
var G__35460 = (0);
var G__35461 = (0);
seq__34508_35430 = G__35458;
chunk__34510_35431 = G__35459;
count__34511_35432 = G__35460;
i__34512_35433 = G__35461;
continue;
} else {
var G__35463 = cljs.core.next(seq__34508_35447__$1);
var G__35464 = null;
var G__35465 = (0);
var G__35466 = (0);
seq__34508_35430 = G__35463;
chunk__34510_35431 = G__35464;
count__34511_35432 = G__35465;
i__34512_35433 = G__35466;
continue;
}
}
} else {
}
}
break;
}
} else {
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,children_35429);
}


var G__35467 = seq__34468_35423;
var G__35468 = chunk__34469_35424;
var G__35469 = count__34470_35425;
var G__35470 = (i__34471_35426 + (1));
seq__34468_35423 = G__35467;
chunk__34469_35424 = G__35468;
count__34470_35425 = G__35469;
i__34471_35426 = G__35470;
continue;
} else {
var temp__5735__auto___35471 = cljs.core.seq(seq__34468_35423);
if(temp__5735__auto___35471){
var seq__34468_35472__$1 = temp__5735__auto___35471;
if(cljs.core.chunked_seq_QMARK_(seq__34468_35472__$1)){
var c__4556__auto___35473 = cljs.core.chunk_first(seq__34468_35472__$1);
var G__35475 = cljs.core.chunk_rest(seq__34468_35472__$1);
var G__35476 = c__4556__auto___35473;
var G__35477 = cljs.core.count(c__4556__auto___35473);
var G__35478 = (0);
seq__34468_35423 = G__35475;
chunk__34469_35424 = G__35476;
count__34470_35425 = G__35477;
i__34471_35426 = G__35478;
continue;
} else {
var child_struct_35480 = cljs.core.first(seq__34468_35472__$1);
var children_35481 = shadow.dom.dom_node(child_struct_35480);
if(cljs.core.seq_QMARK_(children_35481)){
var seq__34529_35482 = cljs.core.seq(cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom.dom_node,children_35481));
var chunk__34531_35483 = null;
var count__34532_35484 = (0);
var i__34533_35485 = (0);
while(true){
if((i__34533_35485 < count__34532_35484)){
var child_35486 = chunk__34531_35483.cljs$core$IIndexed$_nth$arity$2(null,i__34533_35485);
if(cljs.core.truth_(child_35486)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_35486);


var G__35490 = seq__34529_35482;
var G__35491 = chunk__34531_35483;
var G__35492 = count__34532_35484;
var G__35493 = (i__34533_35485 + (1));
seq__34529_35482 = G__35490;
chunk__34531_35483 = G__35491;
count__34532_35484 = G__35492;
i__34533_35485 = G__35493;
continue;
} else {
var G__35494 = seq__34529_35482;
var G__35495 = chunk__34531_35483;
var G__35496 = count__34532_35484;
var G__35497 = (i__34533_35485 + (1));
seq__34529_35482 = G__35494;
chunk__34531_35483 = G__35495;
count__34532_35484 = G__35496;
i__34533_35485 = G__35497;
continue;
}
} else {
var temp__5735__auto___35499__$1 = cljs.core.seq(seq__34529_35482);
if(temp__5735__auto___35499__$1){
var seq__34529_35502__$1 = temp__5735__auto___35499__$1;
if(cljs.core.chunked_seq_QMARK_(seq__34529_35502__$1)){
var c__4556__auto___35504 = cljs.core.chunk_first(seq__34529_35502__$1);
var G__35505 = cljs.core.chunk_rest(seq__34529_35502__$1);
var G__35506 = c__4556__auto___35504;
var G__35507 = cljs.core.count(c__4556__auto___35504);
var G__35508 = (0);
seq__34529_35482 = G__35505;
chunk__34531_35483 = G__35506;
count__34532_35484 = G__35507;
i__34533_35485 = G__35508;
continue;
} else {
var child_35510 = cljs.core.first(seq__34529_35502__$1);
if(cljs.core.truth_(child_35510)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_35510);


var G__35512 = cljs.core.next(seq__34529_35502__$1);
var G__35513 = null;
var G__35514 = (0);
var G__35515 = (0);
seq__34529_35482 = G__35512;
chunk__34531_35483 = G__35513;
count__34532_35484 = G__35514;
i__34533_35485 = G__35515;
continue;
} else {
var G__35517 = cljs.core.next(seq__34529_35502__$1);
var G__35518 = null;
var G__35519 = (0);
var G__35520 = (0);
seq__34529_35482 = G__35517;
chunk__34531_35483 = G__35518;
count__34532_35484 = G__35519;
i__34533_35485 = G__35520;
continue;
}
}
} else {
}
}
break;
}
} else {
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,children_35481);
}


var G__35523 = cljs.core.next(seq__34468_35472__$1);
var G__35524 = null;
var G__35525 = (0);
var G__35526 = (0);
seq__34468_35423 = G__35523;
chunk__34469_35424 = G__35524;
count__34470_35425 = G__35525;
i__34471_35426 = G__35526;
continue;
}
} else {
}
}
break;
}

return node;
});
(cljs.core.Keyword.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.Keyword.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return shadow.dom.make_dom_node(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [this$__$1], null));
}));

(cljs.core.PersistentVector.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.PersistentVector.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return shadow.dom.make_dom_node(this$__$1);
}));

(cljs.core.LazySeq.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.LazySeq.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom._to_dom,this$__$1);
}));
if(cljs.core.truth_(((typeof HTMLElement) != 'undefined'))){
(HTMLElement.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(HTMLElement.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1;
}));
} else {
}
if(cljs.core.truth_(((typeof DocumentFragment) != 'undefined'))){
(DocumentFragment.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(DocumentFragment.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1;
}));
} else {
}
/**
 * clear node children
 */
shadow.dom.reset = (function shadow$dom$reset(node){
return goog.dom.removeChildren(shadow.dom.dom_node(node));
});
shadow.dom.remove = (function shadow$dom$remove(node){
if((((!((node == null))))?(((((node.cljs$lang$protocol_mask$partition0$ & (8388608))) || ((cljs.core.PROTOCOL_SENTINEL === node.cljs$core$ISeqable$))))?true:false):false)){
var seq__34568 = cljs.core.seq(node);
var chunk__34569 = null;
var count__34570 = (0);
var i__34571 = (0);
while(true){
if((i__34571 < count__34570)){
var n = chunk__34569.cljs$core$IIndexed$_nth$arity$2(null,i__34571);
(shadow.dom.remove.cljs$core$IFn$_invoke$arity$1 ? shadow.dom.remove.cljs$core$IFn$_invoke$arity$1(n) : shadow.dom.remove.call(null,n));


var G__35550 = seq__34568;
var G__35551 = chunk__34569;
var G__35552 = count__34570;
var G__35553 = (i__34571 + (1));
seq__34568 = G__35550;
chunk__34569 = G__35551;
count__34570 = G__35552;
i__34571 = G__35553;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__34568);
if(temp__5735__auto__){
var seq__34568__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__34568__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__34568__$1);
var G__35557 = cljs.core.chunk_rest(seq__34568__$1);
var G__35558 = c__4556__auto__;
var G__35559 = cljs.core.count(c__4556__auto__);
var G__35560 = (0);
seq__34568 = G__35557;
chunk__34569 = G__35558;
count__34570 = G__35559;
i__34571 = G__35560;
continue;
} else {
var n = cljs.core.first(seq__34568__$1);
(shadow.dom.remove.cljs$core$IFn$_invoke$arity$1 ? shadow.dom.remove.cljs$core$IFn$_invoke$arity$1(n) : shadow.dom.remove.call(null,n));


var G__35568 = cljs.core.next(seq__34568__$1);
var G__35569 = null;
var G__35570 = (0);
var G__35571 = (0);
seq__34568 = G__35568;
chunk__34569 = G__35569;
count__34570 = G__35570;
i__34571 = G__35571;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return goog.dom.removeNode(node);
}
});
shadow.dom.replace_node = (function shadow$dom$replace_node(old,new$){
return goog.dom.replaceNode(shadow.dom.dom_node(new$),shadow.dom.dom_node(old));
});
shadow.dom.text = (function shadow$dom$text(var_args){
var G__34608 = arguments.length;
switch (G__34608) {
case 2:
return shadow.dom.text.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 1:
return shadow.dom.text.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.text.cljs$core$IFn$_invoke$arity$2 = (function (el,new_text){
return (shadow.dom.dom_node(el).innerText = new_text);
}));

(shadow.dom.text.cljs$core$IFn$_invoke$arity$1 = (function (el){
return shadow.dom.dom_node(el).innerText;
}));

(shadow.dom.text.cljs$lang$maxFixedArity = 2);

shadow.dom.check = (function shadow$dom$check(var_args){
var G__34627 = arguments.length;
switch (G__34627) {
case 1:
return shadow.dom.check.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.check.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.check.cljs$core$IFn$_invoke$arity$1 = (function (el){
return shadow.dom.check.cljs$core$IFn$_invoke$arity$2(el,true);
}));

(shadow.dom.check.cljs$core$IFn$_invoke$arity$2 = (function (el,checked){
return (shadow.dom.dom_node(el).checked = checked);
}));

(shadow.dom.check.cljs$lang$maxFixedArity = 2);

shadow.dom.checked_QMARK_ = (function shadow$dom$checked_QMARK_(el){
return shadow.dom.dom_node(el).checked;
});
shadow.dom.form_elements = (function shadow$dom$form_elements(el){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(el).elements));
});
shadow.dom.children = (function shadow$dom$children(el){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(el).children));
});
shadow.dom.child_nodes = (function shadow$dom$child_nodes(el){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(el).childNodes));
});
shadow.dom.attr = (function shadow$dom$attr(var_args){
var G__34681 = arguments.length;
switch (G__34681) {
case 2:
return shadow.dom.attr.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.attr.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.attr.cljs$core$IFn$_invoke$arity$2 = (function (el,key){
return shadow.dom.dom_node(el).getAttribute(cljs.core.name(key));
}));

(shadow.dom.attr.cljs$core$IFn$_invoke$arity$3 = (function (el,key,default$){
var or__4126__auto__ = shadow.dom.dom_node(el).getAttribute(cljs.core.name(key));
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return default$;
}
}));

(shadow.dom.attr.cljs$lang$maxFixedArity = 3);

shadow.dom.del_attr = (function shadow$dom$del_attr(el,key){
return shadow.dom.dom_node(el).removeAttribute(cljs.core.name(key));
});
shadow.dom.data = (function shadow$dom$data(el,key){
return shadow.dom.dom_node(el).getAttribute(["data-",cljs.core.name(key)].join(''));
});
shadow.dom.set_data = (function shadow$dom$set_data(el,key,value){
return shadow.dom.dom_node(el).setAttribute(["data-",cljs.core.name(key)].join(''),cljs.core.str.cljs$core$IFn$_invoke$arity$1(value));
});
shadow.dom.set_html = (function shadow$dom$set_html(node,text){
return (shadow.dom.dom_node(node).innerHTML = text);
});
shadow.dom.get_html = (function shadow$dom$get_html(node){
return shadow.dom.dom_node(node).innerHTML;
});
shadow.dom.fragment = (function shadow$dom$fragment(var_args){
var args__4742__auto__ = [];
var len__4736__auto___35628 = arguments.length;
var i__4737__auto___35630 = (0);
while(true){
if((i__4737__auto___35630 < len__4736__auto___35628)){
args__4742__auto__.push((arguments[i__4737__auto___35630]));

var G__35632 = (i__4737__auto___35630 + (1));
i__4737__auto___35630 = G__35632;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((0) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((0)),(0),null)):null);
return shadow.dom.fragment.cljs$core$IFn$_invoke$arity$variadic(argseq__4743__auto__);
});

(shadow.dom.fragment.cljs$core$IFn$_invoke$arity$variadic = (function (nodes){
var fragment = document.createDocumentFragment();
var seq__34725_35639 = cljs.core.seq(nodes);
var chunk__34726_35640 = null;
var count__34727_35641 = (0);
var i__34728_35642 = (0);
while(true){
if((i__34728_35642 < count__34727_35641)){
var node_35644 = chunk__34726_35640.cljs$core$IIndexed$_nth$arity$2(null,i__34728_35642);
fragment.appendChild(shadow.dom._to_dom(node_35644));


var G__35645 = seq__34725_35639;
var G__35646 = chunk__34726_35640;
var G__35647 = count__34727_35641;
var G__35648 = (i__34728_35642 + (1));
seq__34725_35639 = G__35645;
chunk__34726_35640 = G__35646;
count__34727_35641 = G__35647;
i__34728_35642 = G__35648;
continue;
} else {
var temp__5735__auto___35649 = cljs.core.seq(seq__34725_35639);
if(temp__5735__auto___35649){
var seq__34725_35651__$1 = temp__5735__auto___35649;
if(cljs.core.chunked_seq_QMARK_(seq__34725_35651__$1)){
var c__4556__auto___35652 = cljs.core.chunk_first(seq__34725_35651__$1);
var G__35653 = cljs.core.chunk_rest(seq__34725_35651__$1);
var G__35654 = c__4556__auto___35652;
var G__35655 = cljs.core.count(c__4556__auto___35652);
var G__35656 = (0);
seq__34725_35639 = G__35653;
chunk__34726_35640 = G__35654;
count__34727_35641 = G__35655;
i__34728_35642 = G__35656;
continue;
} else {
var node_35661 = cljs.core.first(seq__34725_35651__$1);
fragment.appendChild(shadow.dom._to_dom(node_35661));


var G__35663 = cljs.core.next(seq__34725_35651__$1);
var G__35664 = null;
var G__35665 = (0);
var G__35666 = (0);
seq__34725_35639 = G__35663;
chunk__34726_35640 = G__35664;
count__34727_35641 = G__35665;
i__34728_35642 = G__35666;
continue;
}
} else {
}
}
break;
}

return (new shadow.dom.NativeColl(fragment));
}));

(shadow.dom.fragment.cljs$lang$maxFixedArity = (0));

/** @this {Function} */
(shadow.dom.fragment.cljs$lang$applyTo = (function (seq34716){
var self__4724__auto__ = this;
return self__4724__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq34716));
}));

/**
 * given a html string, eval all <script> tags and return the html without the scripts
 * don't do this for everything, only content you trust.
 */
shadow.dom.eval_scripts = (function shadow$dom$eval_scripts(s){
var scripts = cljs.core.re_seq(/<script[^>]*?>(.+?)<\/script>/,s);
var seq__34739_35676 = cljs.core.seq(scripts);
var chunk__34740_35677 = null;
var count__34741_35678 = (0);
var i__34742_35679 = (0);
while(true){
if((i__34742_35679 < count__34741_35678)){
var vec__34757_35685 = chunk__34740_35677.cljs$core$IIndexed$_nth$arity$2(null,i__34742_35679);
var script_tag_35686 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34757_35685,(0),null);
var script_body_35687 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34757_35685,(1),null);
eval(script_body_35687);


var G__35698 = seq__34739_35676;
var G__35699 = chunk__34740_35677;
var G__35700 = count__34741_35678;
var G__35701 = (i__34742_35679 + (1));
seq__34739_35676 = G__35698;
chunk__34740_35677 = G__35699;
count__34741_35678 = G__35700;
i__34742_35679 = G__35701;
continue;
} else {
var temp__5735__auto___35705 = cljs.core.seq(seq__34739_35676);
if(temp__5735__auto___35705){
var seq__34739_35708__$1 = temp__5735__auto___35705;
if(cljs.core.chunked_seq_QMARK_(seq__34739_35708__$1)){
var c__4556__auto___35713 = cljs.core.chunk_first(seq__34739_35708__$1);
var G__35714 = cljs.core.chunk_rest(seq__34739_35708__$1);
var G__35715 = c__4556__auto___35713;
var G__35716 = cljs.core.count(c__4556__auto___35713);
var G__35717 = (0);
seq__34739_35676 = G__35714;
chunk__34740_35677 = G__35715;
count__34741_35678 = G__35716;
i__34742_35679 = G__35717;
continue;
} else {
var vec__34765_35721 = cljs.core.first(seq__34739_35708__$1);
var script_tag_35722 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34765_35721,(0),null);
var script_body_35723 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34765_35721,(1),null);
eval(script_body_35723);


var G__35730 = cljs.core.next(seq__34739_35708__$1);
var G__35731 = null;
var G__35732 = (0);
var G__35733 = (0);
seq__34739_35676 = G__35730;
chunk__34740_35677 = G__35731;
count__34741_35678 = G__35732;
i__34742_35679 = G__35733;
continue;
}
} else {
}
}
break;
}

return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (s__$1,p__34769){
var vec__34773 = p__34769;
var script_tag = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34773,(0),null);
var script_body = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34773,(1),null);
return clojure.string.replace(s__$1,script_tag,"");
}),s,scripts);
});
shadow.dom.str__GT_fragment = (function shadow$dom$str__GT_fragment(s){
var el = document.createElement("div");
(el.innerHTML = s);

return (new shadow.dom.NativeColl(goog.dom.childrenToNode_(document,el)));
});
shadow.dom.node_name = (function shadow$dom$node_name(el){
return shadow.dom.dom_node(el).nodeName;
});
shadow.dom.ancestor_by_class = (function shadow$dom$ancestor_by_class(el,cls){
return goog.dom.getAncestorByClass(shadow.dom.dom_node(el),cls);
});
shadow.dom.ancestor_by_tag = (function shadow$dom$ancestor_by_tag(var_args){
var G__34787 = arguments.length;
switch (G__34787) {
case 2:
return shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$2 = (function (el,tag){
return goog.dom.getAncestorByTagNameAndClass(shadow.dom.dom_node(el),cljs.core.name(tag));
}));

(shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$3 = (function (el,tag,cls){
return goog.dom.getAncestorByTagNameAndClass(shadow.dom.dom_node(el),cljs.core.name(tag),cljs.core.name(cls));
}));

(shadow.dom.ancestor_by_tag.cljs$lang$maxFixedArity = 3);

shadow.dom.get_value = (function shadow$dom$get_value(dom){
return goog.dom.forms.getValue(shadow.dom.dom_node(dom));
});
shadow.dom.set_value = (function shadow$dom$set_value(dom,value){
return goog.dom.forms.setValue(shadow.dom.dom_node(dom),value);
});
shadow.dom.px = (function shadow$dom$px(value){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1((value | (0))),"px"].join('');
});
shadow.dom.pct = (function shadow$dom$pct(value){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(value),"%"].join('');
});
shadow.dom.remove_style_STAR_ = (function shadow$dom$remove_style_STAR_(el,style){
return el.style.removeProperty(cljs.core.name(style));
});
shadow.dom.remove_style = (function shadow$dom$remove_style(el,style){
var el__$1 = shadow.dom.dom_node(el);
return shadow.dom.remove_style_STAR_(el__$1,style);
});
shadow.dom.remove_styles = (function shadow$dom$remove_styles(el,style_keys){
var el__$1 = shadow.dom.dom_node(el);
var seq__34801 = cljs.core.seq(style_keys);
var chunk__34802 = null;
var count__34803 = (0);
var i__34804 = (0);
while(true){
if((i__34804 < count__34803)){
var it = chunk__34802.cljs$core$IIndexed$_nth$arity$2(null,i__34804);
shadow.dom.remove_style_STAR_(el__$1,it);


var G__35753 = seq__34801;
var G__35754 = chunk__34802;
var G__35755 = count__34803;
var G__35756 = (i__34804 + (1));
seq__34801 = G__35753;
chunk__34802 = G__35754;
count__34803 = G__35755;
i__34804 = G__35756;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__34801);
if(temp__5735__auto__){
var seq__34801__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__34801__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__34801__$1);
var G__35758 = cljs.core.chunk_rest(seq__34801__$1);
var G__35759 = c__4556__auto__;
var G__35760 = cljs.core.count(c__4556__auto__);
var G__35761 = (0);
seq__34801 = G__35758;
chunk__34802 = G__35759;
count__34803 = G__35760;
i__34804 = G__35761;
continue;
} else {
var it = cljs.core.first(seq__34801__$1);
shadow.dom.remove_style_STAR_(el__$1,it);


var G__35767 = cljs.core.next(seq__34801__$1);
var G__35768 = null;
var G__35769 = (0);
var G__35770 = (0);
seq__34801 = G__35767;
chunk__34802 = G__35768;
count__34803 = G__35769;
i__34804 = G__35770;
continue;
}
} else {
return null;
}
}
break;
}
});

/**
* @constructor
 * @implements {cljs.core.IRecord}
 * @implements {cljs.core.IKVReduce}
 * @implements {cljs.core.IEquiv}
 * @implements {cljs.core.IHash}
 * @implements {cljs.core.ICollection}
 * @implements {cljs.core.ICounted}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.ICloneable}
 * @implements {cljs.core.IPrintWithWriter}
 * @implements {cljs.core.IIterable}
 * @implements {cljs.core.IWithMeta}
 * @implements {cljs.core.IAssociative}
 * @implements {cljs.core.IMap}
 * @implements {cljs.core.ILookup}
*/
shadow.dom.Coordinate = (function (x,y,__meta,__extmap,__hash){
this.x = x;
this.y = y;
this.__meta = __meta;
this.__extmap = __extmap;
this.__hash = __hash;
this.cljs$lang$protocol_mask$partition0$ = 2230716170;
this.cljs$lang$protocol_mask$partition1$ = 139264;
});
(shadow.dom.Coordinate.prototype.cljs$core$ILookup$_lookup$arity$2 = (function (this__4380__auto__,k__4381__auto__){
var self__ = this;
var this__4380__auto____$1 = this;
return this__4380__auto____$1.cljs$core$ILookup$_lookup$arity$3(null,k__4381__auto__,null);
}));

(shadow.dom.Coordinate.prototype.cljs$core$ILookup$_lookup$arity$3 = (function (this__4382__auto__,k34811,else__4383__auto__){
var self__ = this;
var this__4382__auto____$1 = this;
var G__34820 = k34811;
var G__34820__$1 = (((G__34820 instanceof cljs.core.Keyword))?G__34820.fqn:null);
switch (G__34820__$1) {
case "x":
return self__.x;

break;
case "y":
return self__.y;

break;
default:
return cljs.core.get.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k34811,else__4383__auto__);

}
}));

(shadow.dom.Coordinate.prototype.cljs$core$IKVReduce$_kv_reduce$arity$3 = (function (this__4399__auto__,f__4400__auto__,init__4401__auto__){
var self__ = this;
var this__4399__auto____$1 = this;
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (ret__4402__auto__,p__34826){
var vec__34827 = p__34826;
var k__4403__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34827,(0),null);
var v__4404__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34827,(1),null);
return (f__4400__auto__.cljs$core$IFn$_invoke$arity$3 ? f__4400__auto__.cljs$core$IFn$_invoke$arity$3(ret__4402__auto__,k__4403__auto__,v__4404__auto__) : f__4400__auto__.call(null,ret__4402__auto__,k__4403__auto__,v__4404__auto__));
}),init__4401__auto__,this__4399__auto____$1);
}));

(shadow.dom.Coordinate.prototype.cljs$core$IPrintWithWriter$_pr_writer$arity$3 = (function (this__4394__auto__,writer__4395__auto__,opts__4396__auto__){
var self__ = this;
var this__4394__auto____$1 = this;
var pr_pair__4397__auto__ = (function (keyval__4398__auto__){
return cljs.core.pr_sequential_writer(writer__4395__auto__,cljs.core.pr_writer,""," ","",opts__4396__auto__,keyval__4398__auto__);
});
return cljs.core.pr_sequential_writer(writer__4395__auto__,pr_pair__4397__auto__,"#shadow.dom.Coordinate{",", ","}",opts__4396__auto__,cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"x","x",2099068185),self__.x],null)),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"y","y",-1757859776),self__.y],null))], null),self__.__extmap));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IIterable$_iterator$arity$1 = (function (G__34810){
var self__ = this;
var G__34810__$1 = this;
return (new cljs.core.RecordIter((0),G__34810__$1,2,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"x","x",2099068185),new cljs.core.Keyword(null,"y","y",-1757859776)], null),(cljs.core.truth_(self__.__extmap)?cljs.core._iterator(self__.__extmap):cljs.core.nil_iter())));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IMeta$_meta$arity$1 = (function (this__4378__auto__){
var self__ = this;
var this__4378__auto____$1 = this;
return self__.__meta;
}));

(shadow.dom.Coordinate.prototype.cljs$core$ICloneable$_clone$arity$1 = (function (this__4375__auto__){
var self__ = this;
var this__4375__auto____$1 = this;
return (new shadow.dom.Coordinate(self__.x,self__.y,self__.__meta,self__.__extmap,self__.__hash));
}));

(shadow.dom.Coordinate.prototype.cljs$core$ICounted$_count$arity$1 = (function (this__4384__auto__){
var self__ = this;
var this__4384__auto____$1 = this;
return (2 + cljs.core.count(self__.__extmap));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IHash$_hash$arity$1 = (function (this__4376__auto__){
var self__ = this;
var this__4376__auto____$1 = this;
var h__4238__auto__ = self__.__hash;
if((!((h__4238__auto__ == null)))){
return h__4238__auto__;
} else {
var h__4238__auto____$1 = (function (coll__4377__auto__){
return (145542109 ^ cljs.core.hash_unordered_coll(coll__4377__auto__));
})(this__4376__auto____$1);
(self__.__hash = h__4238__auto____$1);

return h__4238__auto____$1;
}
}));

(shadow.dom.Coordinate.prototype.cljs$core$IEquiv$_equiv$arity$2 = (function (this34812,other34813){
var self__ = this;
var this34812__$1 = this;
return (((!((other34813 == null)))) && ((this34812__$1.constructor === other34813.constructor)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this34812__$1.x,other34813.x)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this34812__$1.y,other34813.y)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this34812__$1.__extmap,other34813.__extmap)));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IMap$_dissoc$arity$2 = (function (this__4389__auto__,k__4390__auto__){
var self__ = this;
var this__4389__auto____$1 = this;
if(cljs.core.contains_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"y","y",-1757859776),null,new cljs.core.Keyword(null,"x","x",2099068185),null], null), null),k__4390__auto__)){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(cljs.core._with_meta(cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,this__4389__auto____$1),self__.__meta),k__4390__auto__);
} else {
return (new shadow.dom.Coordinate(self__.x,self__.y,self__.__meta,cljs.core.not_empty(cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(self__.__extmap,k__4390__auto__)),null));
}
}));

(shadow.dom.Coordinate.prototype.cljs$core$IAssociative$_assoc$arity$3 = (function (this__4387__auto__,k__4388__auto__,G__34810){
var self__ = this;
var this__4387__auto____$1 = this;
var pred__34863 = cljs.core.keyword_identical_QMARK_;
var expr__34864 = k__4388__auto__;
if(cljs.core.truth_((pred__34863.cljs$core$IFn$_invoke$arity$2 ? pred__34863.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"x","x",2099068185),expr__34864) : pred__34863.call(null,new cljs.core.Keyword(null,"x","x",2099068185),expr__34864)))){
return (new shadow.dom.Coordinate(G__34810,self__.y,self__.__meta,self__.__extmap,null));
} else {
if(cljs.core.truth_((pred__34863.cljs$core$IFn$_invoke$arity$2 ? pred__34863.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"y","y",-1757859776),expr__34864) : pred__34863.call(null,new cljs.core.Keyword(null,"y","y",-1757859776),expr__34864)))){
return (new shadow.dom.Coordinate(self__.x,G__34810,self__.__meta,self__.__extmap,null));
} else {
return (new shadow.dom.Coordinate(self__.x,self__.y,self__.__meta,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k__4388__auto__,G__34810),null));
}
}
}));

(shadow.dom.Coordinate.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this__4392__auto__){
var self__ = this;
var this__4392__auto____$1 = this;
return cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.MapEntry(new cljs.core.Keyword(null,"x","x",2099068185),self__.x,null)),(new cljs.core.MapEntry(new cljs.core.Keyword(null,"y","y",-1757859776),self__.y,null))], null),self__.__extmap));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (this__4379__auto__,G__34810){
var self__ = this;
var this__4379__auto____$1 = this;
return (new shadow.dom.Coordinate(self__.x,self__.y,G__34810,self__.__extmap,self__.__hash));
}));

(shadow.dom.Coordinate.prototype.cljs$core$ICollection$_conj$arity$2 = (function (this__4385__auto__,entry__4386__auto__){
var self__ = this;
var this__4385__auto____$1 = this;
if(cljs.core.vector_QMARK_(entry__4386__auto__)){
return this__4385__auto____$1.cljs$core$IAssociative$_assoc$arity$3(null,cljs.core._nth(entry__4386__auto__,(0)),cljs.core._nth(entry__4386__auto__,(1)));
} else {
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(cljs.core._conj,this__4385__auto____$1,entry__4386__auto__);
}
}));

(shadow.dom.Coordinate.getBasis = (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"x","x",-555367584,null),new cljs.core.Symbol(null,"y","y",-117328249,null)], null);
}));

(shadow.dom.Coordinate.cljs$lang$type = true);

(shadow.dom.Coordinate.cljs$lang$ctorPrSeq = (function (this__4423__auto__){
return (new cljs.core.List(null,"shadow.dom/Coordinate",null,(1),null));
}));

(shadow.dom.Coordinate.cljs$lang$ctorPrWriter = (function (this__4423__auto__,writer__4424__auto__){
return cljs.core._write(writer__4424__auto__,"shadow.dom/Coordinate");
}));

/**
 * Positional factory function for shadow.dom/Coordinate.
 */
shadow.dom.__GT_Coordinate = (function shadow$dom$__GT_Coordinate(x,y){
return (new shadow.dom.Coordinate(x,y,null,null,null));
});

/**
 * Factory function for shadow.dom/Coordinate, taking a map of keywords to field values.
 */
shadow.dom.map__GT_Coordinate = (function shadow$dom$map__GT_Coordinate(G__34816){
var extmap__4419__auto__ = (function (){var G__34878 = cljs.core.dissoc.cljs$core$IFn$_invoke$arity$variadic(G__34816,new cljs.core.Keyword(null,"x","x",2099068185),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"y","y",-1757859776)], 0));
if(cljs.core.record_QMARK_(G__34816)){
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,G__34878);
} else {
return G__34878;
}
})();
return (new shadow.dom.Coordinate(new cljs.core.Keyword(null,"x","x",2099068185).cljs$core$IFn$_invoke$arity$1(G__34816),new cljs.core.Keyword(null,"y","y",-1757859776).cljs$core$IFn$_invoke$arity$1(G__34816),null,cljs.core.not_empty(extmap__4419__auto__),null));
});

shadow.dom.get_position = (function shadow$dom$get_position(el){
var pos = goog.style.getPosition(shadow.dom.dom_node(el));
return shadow.dom.__GT_Coordinate(pos.x,pos.y);
});
shadow.dom.get_client_position = (function shadow$dom$get_client_position(el){
var pos = goog.style.getClientPosition(shadow.dom.dom_node(el));
return shadow.dom.__GT_Coordinate(pos.x,pos.y);
});
shadow.dom.get_page_offset = (function shadow$dom$get_page_offset(el){
var pos = goog.style.getPageOffset(shadow.dom.dom_node(el));
return shadow.dom.__GT_Coordinate(pos.x,pos.y);
});

/**
* @constructor
 * @implements {cljs.core.IRecord}
 * @implements {cljs.core.IKVReduce}
 * @implements {cljs.core.IEquiv}
 * @implements {cljs.core.IHash}
 * @implements {cljs.core.ICollection}
 * @implements {cljs.core.ICounted}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.ICloneable}
 * @implements {cljs.core.IPrintWithWriter}
 * @implements {cljs.core.IIterable}
 * @implements {cljs.core.IWithMeta}
 * @implements {cljs.core.IAssociative}
 * @implements {cljs.core.IMap}
 * @implements {cljs.core.ILookup}
*/
shadow.dom.Size = (function (w,h,__meta,__extmap,__hash){
this.w = w;
this.h = h;
this.__meta = __meta;
this.__extmap = __extmap;
this.__hash = __hash;
this.cljs$lang$protocol_mask$partition0$ = 2230716170;
this.cljs$lang$protocol_mask$partition1$ = 139264;
});
(shadow.dom.Size.prototype.cljs$core$ILookup$_lookup$arity$2 = (function (this__4380__auto__,k__4381__auto__){
var self__ = this;
var this__4380__auto____$1 = this;
return this__4380__auto____$1.cljs$core$ILookup$_lookup$arity$3(null,k__4381__auto__,null);
}));

(shadow.dom.Size.prototype.cljs$core$ILookup$_lookup$arity$3 = (function (this__4382__auto__,k34889,else__4383__auto__){
var self__ = this;
var this__4382__auto____$1 = this;
var G__34899 = k34889;
var G__34899__$1 = (((G__34899 instanceof cljs.core.Keyword))?G__34899.fqn:null);
switch (G__34899__$1) {
case "w":
return self__.w;

break;
case "h":
return self__.h;

break;
default:
return cljs.core.get.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k34889,else__4383__auto__);

}
}));

(shadow.dom.Size.prototype.cljs$core$IKVReduce$_kv_reduce$arity$3 = (function (this__4399__auto__,f__4400__auto__,init__4401__auto__){
var self__ = this;
var this__4399__auto____$1 = this;
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (ret__4402__auto__,p__34910){
var vec__34912 = p__34910;
var k__4403__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34912,(0),null);
var v__4404__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34912,(1),null);
return (f__4400__auto__.cljs$core$IFn$_invoke$arity$3 ? f__4400__auto__.cljs$core$IFn$_invoke$arity$3(ret__4402__auto__,k__4403__auto__,v__4404__auto__) : f__4400__auto__.call(null,ret__4402__auto__,k__4403__auto__,v__4404__auto__));
}),init__4401__auto__,this__4399__auto____$1);
}));

(shadow.dom.Size.prototype.cljs$core$IPrintWithWriter$_pr_writer$arity$3 = (function (this__4394__auto__,writer__4395__auto__,opts__4396__auto__){
var self__ = this;
var this__4394__auto____$1 = this;
var pr_pair__4397__auto__ = (function (keyval__4398__auto__){
return cljs.core.pr_sequential_writer(writer__4395__auto__,cljs.core.pr_writer,""," ","",opts__4396__auto__,keyval__4398__auto__);
});
return cljs.core.pr_sequential_writer(writer__4395__auto__,pr_pair__4397__auto__,"#shadow.dom.Size{",", ","}",opts__4396__auto__,cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"w","w",354169001),self__.w],null)),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"h","h",1109658740),self__.h],null))], null),self__.__extmap));
}));

(shadow.dom.Size.prototype.cljs$core$IIterable$_iterator$arity$1 = (function (G__34888){
var self__ = this;
var G__34888__$1 = this;
return (new cljs.core.RecordIter((0),G__34888__$1,2,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"w","w",354169001),new cljs.core.Keyword(null,"h","h",1109658740)], null),(cljs.core.truth_(self__.__extmap)?cljs.core._iterator(self__.__extmap):cljs.core.nil_iter())));
}));

(shadow.dom.Size.prototype.cljs$core$IMeta$_meta$arity$1 = (function (this__4378__auto__){
var self__ = this;
var this__4378__auto____$1 = this;
return self__.__meta;
}));

(shadow.dom.Size.prototype.cljs$core$ICloneable$_clone$arity$1 = (function (this__4375__auto__){
var self__ = this;
var this__4375__auto____$1 = this;
return (new shadow.dom.Size(self__.w,self__.h,self__.__meta,self__.__extmap,self__.__hash));
}));

(shadow.dom.Size.prototype.cljs$core$ICounted$_count$arity$1 = (function (this__4384__auto__){
var self__ = this;
var this__4384__auto____$1 = this;
return (2 + cljs.core.count(self__.__extmap));
}));

(shadow.dom.Size.prototype.cljs$core$IHash$_hash$arity$1 = (function (this__4376__auto__){
var self__ = this;
var this__4376__auto____$1 = this;
var h__4238__auto__ = self__.__hash;
if((!((h__4238__auto__ == null)))){
return h__4238__auto__;
} else {
var h__4238__auto____$1 = (function (coll__4377__auto__){
return (-1228019642 ^ cljs.core.hash_unordered_coll(coll__4377__auto__));
})(this__4376__auto____$1);
(self__.__hash = h__4238__auto____$1);

return h__4238__auto____$1;
}
}));

(shadow.dom.Size.prototype.cljs$core$IEquiv$_equiv$arity$2 = (function (this34891,other34892){
var self__ = this;
var this34891__$1 = this;
return (((!((other34892 == null)))) && ((this34891__$1.constructor === other34892.constructor)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this34891__$1.w,other34892.w)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this34891__$1.h,other34892.h)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this34891__$1.__extmap,other34892.__extmap)));
}));

(shadow.dom.Size.prototype.cljs$core$IMap$_dissoc$arity$2 = (function (this__4389__auto__,k__4390__auto__){
var self__ = this;
var this__4389__auto____$1 = this;
if(cljs.core.contains_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"w","w",354169001),null,new cljs.core.Keyword(null,"h","h",1109658740),null], null), null),k__4390__auto__)){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(cljs.core._with_meta(cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,this__4389__auto____$1),self__.__meta),k__4390__auto__);
} else {
return (new shadow.dom.Size(self__.w,self__.h,self__.__meta,cljs.core.not_empty(cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(self__.__extmap,k__4390__auto__)),null));
}
}));

(shadow.dom.Size.prototype.cljs$core$IAssociative$_assoc$arity$3 = (function (this__4387__auto__,k__4388__auto__,G__34888){
var self__ = this;
var this__4387__auto____$1 = this;
var pred__34919 = cljs.core.keyword_identical_QMARK_;
var expr__34920 = k__4388__auto__;
if(cljs.core.truth_((pred__34919.cljs$core$IFn$_invoke$arity$2 ? pred__34919.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"w","w",354169001),expr__34920) : pred__34919.call(null,new cljs.core.Keyword(null,"w","w",354169001),expr__34920)))){
return (new shadow.dom.Size(G__34888,self__.h,self__.__meta,self__.__extmap,null));
} else {
if(cljs.core.truth_((pred__34919.cljs$core$IFn$_invoke$arity$2 ? pred__34919.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"h","h",1109658740),expr__34920) : pred__34919.call(null,new cljs.core.Keyword(null,"h","h",1109658740),expr__34920)))){
return (new shadow.dom.Size(self__.w,G__34888,self__.__meta,self__.__extmap,null));
} else {
return (new shadow.dom.Size(self__.w,self__.h,self__.__meta,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k__4388__auto__,G__34888),null));
}
}
}));

(shadow.dom.Size.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this__4392__auto__){
var self__ = this;
var this__4392__auto____$1 = this;
return cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.MapEntry(new cljs.core.Keyword(null,"w","w",354169001),self__.w,null)),(new cljs.core.MapEntry(new cljs.core.Keyword(null,"h","h",1109658740),self__.h,null))], null),self__.__extmap));
}));

(shadow.dom.Size.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (this__4379__auto__,G__34888){
var self__ = this;
var this__4379__auto____$1 = this;
return (new shadow.dom.Size(self__.w,self__.h,G__34888,self__.__extmap,self__.__hash));
}));

(shadow.dom.Size.prototype.cljs$core$ICollection$_conj$arity$2 = (function (this__4385__auto__,entry__4386__auto__){
var self__ = this;
var this__4385__auto____$1 = this;
if(cljs.core.vector_QMARK_(entry__4386__auto__)){
return this__4385__auto____$1.cljs$core$IAssociative$_assoc$arity$3(null,cljs.core._nth(entry__4386__auto__,(0)),cljs.core._nth(entry__4386__auto__,(1)));
} else {
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(cljs.core._conj,this__4385__auto____$1,entry__4386__auto__);
}
}));

(shadow.dom.Size.getBasis = (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"w","w",1994700528,null),new cljs.core.Symbol(null,"h","h",-1544777029,null)], null);
}));

(shadow.dom.Size.cljs$lang$type = true);

(shadow.dom.Size.cljs$lang$ctorPrSeq = (function (this__4423__auto__){
return (new cljs.core.List(null,"shadow.dom/Size",null,(1),null));
}));

(shadow.dom.Size.cljs$lang$ctorPrWriter = (function (this__4423__auto__,writer__4424__auto__){
return cljs.core._write(writer__4424__auto__,"shadow.dom/Size");
}));

/**
 * Positional factory function for shadow.dom/Size.
 */
shadow.dom.__GT_Size = (function shadow$dom$__GT_Size(w,h){
return (new shadow.dom.Size(w,h,null,null,null));
});

/**
 * Factory function for shadow.dom/Size, taking a map of keywords to field values.
 */
shadow.dom.map__GT_Size = (function shadow$dom$map__GT_Size(G__34893){
var extmap__4419__auto__ = (function (){var G__34935 = cljs.core.dissoc.cljs$core$IFn$_invoke$arity$variadic(G__34893,new cljs.core.Keyword(null,"w","w",354169001),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"h","h",1109658740)], 0));
if(cljs.core.record_QMARK_(G__34893)){
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,G__34935);
} else {
return G__34935;
}
})();
return (new shadow.dom.Size(new cljs.core.Keyword(null,"w","w",354169001).cljs$core$IFn$_invoke$arity$1(G__34893),new cljs.core.Keyword(null,"h","h",1109658740).cljs$core$IFn$_invoke$arity$1(G__34893),null,cljs.core.not_empty(extmap__4419__auto__),null));
});

shadow.dom.size__GT_clj = (function shadow$dom$size__GT_clj(size){
return (new shadow.dom.Size(size.width,size.height,null,null,null));
});
shadow.dom.get_size = (function shadow$dom$get_size(el){
return shadow.dom.size__GT_clj(goog.style.getSize(shadow.dom.dom_node(el)));
});
shadow.dom.get_height = (function shadow$dom$get_height(el){
return shadow.dom.get_size(el).h;
});
shadow.dom.get_viewport_size = (function shadow$dom$get_viewport_size(){
return shadow.dom.size__GT_clj(goog.dom.getViewportSize());
});
shadow.dom.first_child = (function shadow$dom$first_child(el){
return (shadow.dom.dom_node(el).children[(0)]);
});
shadow.dom.select_option_values = (function shadow$dom$select_option_values(el){
var native$ = shadow.dom.dom_node(el);
var opts = (native$["options"]);
var a__4610__auto__ = opts;
var l__4611__auto__ = a__4610__auto__.length;
var i = (0);
var ret = cljs.core.PersistentVector.EMPTY;
while(true){
if((i < l__4611__auto__)){
var G__35920 = (i + (1));
var G__35921 = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(ret,(opts[i]["value"]));
i = G__35920;
ret = G__35921;
continue;
} else {
return ret;
}
break;
}
});
shadow.dom.build_url = (function shadow$dom$build_url(path,query_params){
if(cljs.core.empty_QMARK_(query_params)){
return path;
} else {
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(path),"?",clojure.string.join.cljs$core$IFn$_invoke$arity$2("&",cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p__34972){
var vec__34973 = p__34972;
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34973,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34973,(1),null);
return [cljs.core.name(k),"=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(encodeURIComponent(cljs.core.str.cljs$core$IFn$_invoke$arity$1(v)))].join('');
}),query_params))].join('');
}
});
shadow.dom.redirect = (function shadow$dom$redirect(var_args){
var G__34980 = arguments.length;
switch (G__34980) {
case 1:
return shadow.dom.redirect.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.redirect.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.redirect.cljs$core$IFn$_invoke$arity$1 = (function (path){
return shadow.dom.redirect.cljs$core$IFn$_invoke$arity$2(path,cljs.core.PersistentArrayMap.EMPTY);
}));

(shadow.dom.redirect.cljs$core$IFn$_invoke$arity$2 = (function (path,query_params){
return (document["location"]["href"] = shadow.dom.build_url(path,query_params));
}));

(shadow.dom.redirect.cljs$lang$maxFixedArity = 2);

shadow.dom.reload_BANG_ = (function shadow$dom$reload_BANG_(){
return (document.location.href = document.location.href);
});
shadow.dom.tag_name = (function shadow$dom$tag_name(el){
var dom = shadow.dom.dom_node(el);
return dom.tagName;
});
shadow.dom.insert_after = (function shadow$dom$insert_after(ref,new$){
var new_node = shadow.dom.dom_node(new$);
goog.dom.insertSiblingAfter(new_node,shadow.dom.dom_node(ref));

return new_node;
});
shadow.dom.insert_before = (function shadow$dom$insert_before(ref,new$){
var new_node = shadow.dom.dom_node(new$);
goog.dom.insertSiblingBefore(new_node,shadow.dom.dom_node(ref));

return new_node;
});
shadow.dom.insert_first = (function shadow$dom$insert_first(ref,new$){
var temp__5733__auto__ = shadow.dom.dom_node(ref).firstChild;
if(cljs.core.truth_(temp__5733__auto__)){
var child = temp__5733__auto__;
return shadow.dom.insert_before(child,new$);
} else {
return shadow.dom.append.cljs$core$IFn$_invoke$arity$2(ref,new$);
}
});
shadow.dom.index_of = (function shadow$dom$index_of(el){
var el__$1 = shadow.dom.dom_node(el);
var i = (0);
while(true){
var ps = el__$1.previousSibling;
if((ps == null)){
return i;
} else {
var G__35945 = ps;
var G__35946 = (i + (1));
el__$1 = G__35945;
i = G__35946;
continue;
}
break;
}
});
shadow.dom.get_parent = (function shadow$dom$get_parent(el){
return goog.dom.getParentElement(shadow.dom.dom_node(el));
});
shadow.dom.parents = (function shadow$dom$parents(el){
var parent = shadow.dom.get_parent(el);
if(cljs.core.truth_(parent)){
return cljs.core.cons(parent,(new cljs.core.LazySeq(null,(function (){
return (shadow.dom.parents.cljs$core$IFn$_invoke$arity$1 ? shadow.dom.parents.cljs$core$IFn$_invoke$arity$1(parent) : shadow.dom.parents.call(null,parent));
}),null,null)));
} else {
return null;
}
});
shadow.dom.matches = (function shadow$dom$matches(el,sel){
return shadow.dom.dom_node(el).matches(sel);
});
shadow.dom.get_next_sibling = (function shadow$dom$get_next_sibling(el){
return goog.dom.getNextElementSibling(shadow.dom.dom_node(el));
});
shadow.dom.get_previous_sibling = (function shadow$dom$get_previous_sibling(el){
return goog.dom.getPreviousElementSibling(shadow.dom.dom_node(el));
});
shadow.dom.xmlns = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentArrayMap(null, 2, ["svg","http://www.w3.org/2000/svg","xlink","http://www.w3.org/1999/xlink"], null));
shadow.dom.create_svg_node = (function shadow$dom$create_svg_node(tag_def,props){
var vec__35019 = shadow.dom.parse_tag(tag_def);
var tag_name = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35019,(0),null);
var tag_id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35019,(1),null);
var tag_classes = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35019,(2),null);
var el = document.createElementNS("http://www.w3.org/2000/svg",tag_name);
if(cljs.core.truth_(tag_id)){
el.setAttribute("id",tag_id);
} else {
}

if(cljs.core.truth_(tag_classes)){
el.setAttribute("class",shadow.dom.merge_class_string(new cljs.core.Keyword(null,"class","class",-2030961996).cljs$core$IFn$_invoke$arity$1(props),tag_classes));
} else {
}

var seq__35022_35965 = cljs.core.seq(props);
var chunk__35023_35966 = null;
var count__35024_35967 = (0);
var i__35025_35968 = (0);
while(true){
if((i__35025_35968 < count__35024_35967)){
var vec__35038_35975 = chunk__35023_35966.cljs$core$IIndexed$_nth$arity$2(null,i__35025_35968);
var k_35976 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35038_35975,(0),null);
var v_35977 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35038_35975,(1),null);
el.setAttributeNS((function (){var temp__5735__auto__ = cljs.core.namespace(k_35976);
if(cljs.core.truth_(temp__5735__auto__)){
var ns = temp__5735__auto__;
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(shadow.dom.xmlns),ns);
} else {
return null;
}
})(),cljs.core.name(k_35976),v_35977);


var G__35987 = seq__35022_35965;
var G__35988 = chunk__35023_35966;
var G__35989 = count__35024_35967;
var G__35990 = (i__35025_35968 + (1));
seq__35022_35965 = G__35987;
chunk__35023_35966 = G__35988;
count__35024_35967 = G__35989;
i__35025_35968 = G__35990;
continue;
} else {
var temp__5735__auto___35996 = cljs.core.seq(seq__35022_35965);
if(temp__5735__auto___35996){
var seq__35022_35997__$1 = temp__5735__auto___35996;
if(cljs.core.chunked_seq_QMARK_(seq__35022_35997__$1)){
var c__4556__auto___35998 = cljs.core.chunk_first(seq__35022_35997__$1);
var G__35999 = cljs.core.chunk_rest(seq__35022_35997__$1);
var G__36000 = c__4556__auto___35998;
var G__36001 = cljs.core.count(c__4556__auto___35998);
var G__36002 = (0);
seq__35022_35965 = G__35999;
chunk__35023_35966 = G__36000;
count__35024_35967 = G__36001;
i__35025_35968 = G__36002;
continue;
} else {
var vec__35045_36005 = cljs.core.first(seq__35022_35997__$1);
var k_36006 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35045_36005,(0),null);
var v_36007 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35045_36005,(1),null);
el.setAttributeNS((function (){var temp__5735__auto____$1 = cljs.core.namespace(k_36006);
if(cljs.core.truth_(temp__5735__auto____$1)){
var ns = temp__5735__auto____$1;
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(shadow.dom.xmlns),ns);
} else {
return null;
}
})(),cljs.core.name(k_36006),v_36007);


var G__36014 = cljs.core.next(seq__35022_35997__$1);
var G__36015 = null;
var G__36016 = (0);
var G__36017 = (0);
seq__35022_35965 = G__36014;
chunk__35023_35966 = G__36015;
count__35024_35967 = G__36016;
i__35025_35968 = G__36017;
continue;
}
} else {
}
}
break;
}

return el;
});
shadow.dom.svg_node = (function shadow$dom$svg_node(el){
if((el == null)){
return null;
} else {
if((((!((el == null))))?((((false) || ((cljs.core.PROTOCOL_SENTINEL === el.shadow$dom$SVGElement$))))?true:false):false)){
return el.shadow$dom$SVGElement$_to_svg$arity$1(null);
} else {
return el;

}
}
});
shadow.dom.make_svg_node = (function shadow$dom$make_svg_node(structure){
var vec__35057 = shadow.dom.destructure_node(shadow.dom.create_svg_node,structure);
var node = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35057,(0),null);
var node_children = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35057,(1),null);
var seq__35060_36035 = cljs.core.seq(node_children);
var chunk__35062_36036 = null;
var count__35063_36037 = (0);
var i__35064_36038 = (0);
while(true){
if((i__35064_36038 < count__35063_36037)){
var child_struct_36043 = chunk__35062_36036.cljs$core$IIndexed$_nth$arity$2(null,i__35064_36038);
if((!((child_struct_36043 == null)))){
if(typeof child_struct_36043 === 'string'){
var text_36047 = (node["textContent"]);
(node["textContent"] = [cljs.core.str.cljs$core$IFn$_invoke$arity$1(text_36047),child_struct_36043].join(''));
} else {
var children_36048 = shadow.dom.svg_node(child_struct_36043);
if(cljs.core.seq_QMARK_(children_36048)){
var seq__35106_36054 = cljs.core.seq(children_36048);
var chunk__35108_36055 = null;
var count__35109_36056 = (0);
var i__35110_36057 = (0);
while(true){
if((i__35110_36057 < count__35109_36056)){
var child_36064 = chunk__35108_36055.cljs$core$IIndexed$_nth$arity$2(null,i__35110_36057);
if(cljs.core.truth_(child_36064)){
node.appendChild(child_36064);


var G__36068 = seq__35106_36054;
var G__36069 = chunk__35108_36055;
var G__36070 = count__35109_36056;
var G__36071 = (i__35110_36057 + (1));
seq__35106_36054 = G__36068;
chunk__35108_36055 = G__36069;
count__35109_36056 = G__36070;
i__35110_36057 = G__36071;
continue;
} else {
var G__36075 = seq__35106_36054;
var G__36076 = chunk__35108_36055;
var G__36077 = count__35109_36056;
var G__36078 = (i__35110_36057 + (1));
seq__35106_36054 = G__36075;
chunk__35108_36055 = G__36076;
count__35109_36056 = G__36077;
i__35110_36057 = G__36078;
continue;
}
} else {
var temp__5735__auto___36080 = cljs.core.seq(seq__35106_36054);
if(temp__5735__auto___36080){
var seq__35106_36081__$1 = temp__5735__auto___36080;
if(cljs.core.chunked_seq_QMARK_(seq__35106_36081__$1)){
var c__4556__auto___36082 = cljs.core.chunk_first(seq__35106_36081__$1);
var G__36083 = cljs.core.chunk_rest(seq__35106_36081__$1);
var G__36084 = c__4556__auto___36082;
var G__36085 = cljs.core.count(c__4556__auto___36082);
var G__36086 = (0);
seq__35106_36054 = G__36083;
chunk__35108_36055 = G__36084;
count__35109_36056 = G__36085;
i__35110_36057 = G__36086;
continue;
} else {
var child_36093 = cljs.core.first(seq__35106_36081__$1);
if(cljs.core.truth_(child_36093)){
node.appendChild(child_36093);


var G__36097 = cljs.core.next(seq__35106_36081__$1);
var G__36098 = null;
var G__36099 = (0);
var G__36100 = (0);
seq__35106_36054 = G__36097;
chunk__35108_36055 = G__36098;
count__35109_36056 = G__36099;
i__35110_36057 = G__36100;
continue;
} else {
var G__36104 = cljs.core.next(seq__35106_36081__$1);
var G__36105 = null;
var G__36106 = (0);
var G__36110 = (0);
seq__35106_36054 = G__36104;
chunk__35108_36055 = G__36105;
count__35109_36056 = G__36106;
i__35110_36057 = G__36110;
continue;
}
}
} else {
}
}
break;
}
} else {
node.appendChild(children_36048);
}
}


var G__36116 = seq__35060_36035;
var G__36117 = chunk__35062_36036;
var G__36118 = count__35063_36037;
var G__36119 = (i__35064_36038 + (1));
seq__35060_36035 = G__36116;
chunk__35062_36036 = G__36117;
count__35063_36037 = G__36118;
i__35064_36038 = G__36119;
continue;
} else {
var G__36121 = seq__35060_36035;
var G__36122 = chunk__35062_36036;
var G__36123 = count__35063_36037;
var G__36124 = (i__35064_36038 + (1));
seq__35060_36035 = G__36121;
chunk__35062_36036 = G__36122;
count__35063_36037 = G__36123;
i__35064_36038 = G__36124;
continue;
}
} else {
var temp__5735__auto___36126 = cljs.core.seq(seq__35060_36035);
if(temp__5735__auto___36126){
var seq__35060_36130__$1 = temp__5735__auto___36126;
if(cljs.core.chunked_seq_QMARK_(seq__35060_36130__$1)){
var c__4556__auto___36131 = cljs.core.chunk_first(seq__35060_36130__$1);
var G__36133 = cljs.core.chunk_rest(seq__35060_36130__$1);
var G__36134 = c__4556__auto___36131;
var G__36135 = cljs.core.count(c__4556__auto___36131);
var G__36136 = (0);
seq__35060_36035 = G__36133;
chunk__35062_36036 = G__36134;
count__35063_36037 = G__36135;
i__35064_36038 = G__36136;
continue;
} else {
var child_struct_36138 = cljs.core.first(seq__35060_36130__$1);
if((!((child_struct_36138 == null)))){
if(typeof child_struct_36138 === 'string'){
var text_36145 = (node["textContent"]);
(node["textContent"] = [cljs.core.str.cljs$core$IFn$_invoke$arity$1(text_36145),child_struct_36138].join(''));
} else {
var children_36148 = shadow.dom.svg_node(child_struct_36138);
if(cljs.core.seq_QMARK_(children_36148)){
var seq__35133_36151 = cljs.core.seq(children_36148);
var chunk__35135_36152 = null;
var count__35136_36153 = (0);
var i__35137_36154 = (0);
while(true){
if((i__35137_36154 < count__35136_36153)){
var child_36156 = chunk__35135_36152.cljs$core$IIndexed$_nth$arity$2(null,i__35137_36154);
if(cljs.core.truth_(child_36156)){
node.appendChild(child_36156);


var G__36158 = seq__35133_36151;
var G__36159 = chunk__35135_36152;
var G__36160 = count__35136_36153;
var G__36161 = (i__35137_36154 + (1));
seq__35133_36151 = G__36158;
chunk__35135_36152 = G__36159;
count__35136_36153 = G__36160;
i__35137_36154 = G__36161;
continue;
} else {
var G__36165 = seq__35133_36151;
var G__36166 = chunk__35135_36152;
var G__36167 = count__35136_36153;
var G__36168 = (i__35137_36154 + (1));
seq__35133_36151 = G__36165;
chunk__35135_36152 = G__36166;
count__35136_36153 = G__36167;
i__35137_36154 = G__36168;
continue;
}
} else {
var temp__5735__auto___36170__$1 = cljs.core.seq(seq__35133_36151);
if(temp__5735__auto___36170__$1){
var seq__35133_36177__$1 = temp__5735__auto___36170__$1;
if(cljs.core.chunked_seq_QMARK_(seq__35133_36177__$1)){
var c__4556__auto___36178 = cljs.core.chunk_first(seq__35133_36177__$1);
var G__36179 = cljs.core.chunk_rest(seq__35133_36177__$1);
var G__36180 = c__4556__auto___36178;
var G__36181 = cljs.core.count(c__4556__auto___36178);
var G__36182 = (0);
seq__35133_36151 = G__36179;
chunk__35135_36152 = G__36180;
count__35136_36153 = G__36181;
i__35137_36154 = G__36182;
continue;
} else {
var child_36187 = cljs.core.first(seq__35133_36177__$1);
if(cljs.core.truth_(child_36187)){
node.appendChild(child_36187);


var G__36188 = cljs.core.next(seq__35133_36177__$1);
var G__36189 = null;
var G__36190 = (0);
var G__36191 = (0);
seq__35133_36151 = G__36188;
chunk__35135_36152 = G__36189;
count__35136_36153 = G__36190;
i__35137_36154 = G__36191;
continue;
} else {
var G__36194 = cljs.core.next(seq__35133_36177__$1);
var G__36195 = null;
var G__36196 = (0);
var G__36197 = (0);
seq__35133_36151 = G__36194;
chunk__35135_36152 = G__36195;
count__35136_36153 = G__36196;
i__35137_36154 = G__36197;
continue;
}
}
} else {
}
}
break;
}
} else {
node.appendChild(children_36148);
}
}


var G__36199 = cljs.core.next(seq__35060_36130__$1);
var G__36200 = null;
var G__36201 = (0);
var G__36202 = (0);
seq__35060_36035 = G__36199;
chunk__35062_36036 = G__36200;
count__35063_36037 = G__36201;
i__35064_36038 = G__36202;
continue;
} else {
var G__36203 = cljs.core.next(seq__35060_36130__$1);
var G__36204 = null;
var G__36205 = (0);
var G__36206 = (0);
seq__35060_36035 = G__36203;
chunk__35062_36036 = G__36204;
count__35063_36037 = G__36205;
i__35064_36038 = G__36206;
continue;
}
}
} else {
}
}
break;
}

return node;
});
goog.object.set(shadow.dom.SVGElement,"string",true);

goog.object.set(shadow.dom._to_svg,"string",(function (this$){
if((this$ instanceof cljs.core.Keyword)){
return shadow.dom.make_svg_node(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [this$], null));
} else {
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("strings cannot be in svgs",new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"this","this",-611633625),this$], null));
}
}));

(cljs.core.PersistentVector.prototype.shadow$dom$SVGElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.PersistentVector.prototype.shadow$dom$SVGElement$_to_svg$arity$1 = (function (this$){
var this$__$1 = this;
return shadow.dom.make_svg_node(this$__$1);
}));

(cljs.core.LazySeq.prototype.shadow$dom$SVGElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.LazySeq.prototype.shadow$dom$SVGElement$_to_svg$arity$1 = (function (this$){
var this$__$1 = this;
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom._to_svg,this$__$1);
}));

goog.object.set(shadow.dom.SVGElement,"null",true);

goog.object.set(shadow.dom._to_svg,"null",(function (_){
return null;
}));
shadow.dom.svg = (function shadow$dom$svg(var_args){
var args__4742__auto__ = [];
var len__4736__auto___36220 = arguments.length;
var i__4737__auto___36221 = (0);
while(true){
if((i__4737__auto___36221 < len__4736__auto___36220)){
args__4742__auto__.push((arguments[i__4737__auto___36221]));

var G__36224 = (i__4737__auto___36221 + (1));
i__4737__auto___36221 = G__36224;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((1) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((1)),(0),null)):null);
return shadow.dom.svg.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4743__auto__);
});

(shadow.dom.svg.cljs$core$IFn$_invoke$arity$variadic = (function (attrs,children){
return shadow.dom._to_svg(cljs.core.vec(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"svg","svg",856789142),attrs], null),children)));
}));

(shadow.dom.svg.cljs$lang$maxFixedArity = (1));

/** @this {Function} */
(shadow.dom.svg.cljs$lang$applyTo = (function (seq35155){
var G__35156 = cljs.core.first(seq35155);
var seq35155__$1 = cljs.core.next(seq35155);
var self__4723__auto__ = this;
return self__4723__auto__.cljs$core$IFn$_invoke$arity$variadic(G__35156,seq35155__$1);
}));

/**
 * returns a channel for events on el
 * transform-fn should be a (fn [e el] some-val) where some-val will be put on the chan
 * once-or-cleanup handles the removal of the event handler
 * - true: remove after one event
 * - false: never removed
 * - chan: remove on msg/close
 */
shadow.dom.event_chan = (function shadow$dom$event_chan(var_args){
var G__35167 = arguments.length;
switch (G__35167) {
case 2:
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$2 = (function (el,event){
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4(el,event,null,false);
}));

(shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$3 = (function (el,event,xf){
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4(el,event,xf,false);
}));

(shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4 = (function (el,event,xf,once_or_cleanup){
var buf = cljs.core.async.sliding_buffer((1));
var chan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2(buf,xf);
var event_fn = (function shadow$dom$event_fn(e){
cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(chan,e);

if(once_or_cleanup === true){
shadow.dom.remove_event_handler(el,event,shadow$dom$event_fn);

return cljs.core.async.close_BANG_(chan);
} else {
return null;
}
});
shadow.dom.dom_listen(shadow.dom.dom_node(el),cljs.core.name(event),event_fn);

if(cljs.core.truth_((function (){var and__4115__auto__ = once_or_cleanup;
if(cljs.core.truth_(and__4115__auto__)){
return (!(once_or_cleanup === true));
} else {
return and__4115__auto__;
}
})())){
var c__30703__auto___36255 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30704__auto__ = (function (){var switch__30539__auto__ = (function (state_35180){
var state_val_35181 = (state_35180[(1)]);
if((state_val_35181 === (1))){
var state_35180__$1 = state_35180;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_35180__$1,(2),once_or_cleanup);
} else {
if((state_val_35181 === (2))){
var inst_35177 = (state_35180[(2)]);
var inst_35178 = shadow.dom.remove_event_handler(el,event,event_fn);
var state_35180__$1 = (function (){var statearr_35188 = state_35180;
(statearr_35188[(7)] = inst_35177);

return statearr_35188;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_35180__$1,inst_35178);
} else {
return null;
}
}
});
return (function() {
var shadow$dom$state_machine__30540__auto__ = null;
var shadow$dom$state_machine__30540__auto____0 = (function (){
var statearr_35193 = [null,null,null,null,null,null,null,null];
(statearr_35193[(0)] = shadow$dom$state_machine__30540__auto__);

(statearr_35193[(1)] = (1));

return statearr_35193;
});
var shadow$dom$state_machine__30540__auto____1 = (function (state_35180){
while(true){
var ret_value__30541__auto__ = (function (){try{while(true){
var result__30542__auto__ = switch__30539__auto__(state_35180);
if(cljs.core.keyword_identical_QMARK_(result__30542__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30542__auto__;
}
break;
}
}catch (e35194){var ex__30543__auto__ = e35194;
var statearr_35195_36268 = state_35180;
(statearr_35195_36268[(2)] = ex__30543__auto__);


if(cljs.core.seq((state_35180[(4)]))){
var statearr_35196_36269 = state_35180;
(statearr_35196_36269[(1)] = cljs.core.first((state_35180[(4)])));

} else {
throw ex__30543__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30541__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__36270 = state_35180;
state_35180 = G__36270;
continue;
} else {
return ret_value__30541__auto__;
}
break;
}
});
shadow$dom$state_machine__30540__auto__ = function(state_35180){
switch(arguments.length){
case 0:
return shadow$dom$state_machine__30540__auto____0.call(this);
case 1:
return shadow$dom$state_machine__30540__auto____1.call(this,state_35180);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
shadow$dom$state_machine__30540__auto__.cljs$core$IFn$_invoke$arity$0 = shadow$dom$state_machine__30540__auto____0;
shadow$dom$state_machine__30540__auto__.cljs$core$IFn$_invoke$arity$1 = shadow$dom$state_machine__30540__auto____1;
return shadow$dom$state_machine__30540__auto__;
})()
})();
var state__30705__auto__ = (function (){var statearr_35201 = f__30704__auto__();
(statearr_35201[(6)] = c__30703__auto___36255);

return statearr_35201;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30705__auto__);
}));

} else {
}

return chan;
}));

(shadow.dom.event_chan.cljs$lang$maxFixedArity = 4);


//# sourceMappingURL=shadow.dom.js.map
