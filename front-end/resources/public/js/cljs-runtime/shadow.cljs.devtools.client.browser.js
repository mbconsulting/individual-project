goog.provide('shadow.cljs.devtools.client.browser');
shadow.cljs.devtools.client.browser.devtools_msg = (function shadow$cljs$devtools$client$browser$devtools_msg(var_args){
var args__4742__auto__ = [];
var len__4736__auto___39193 = arguments.length;
var i__4737__auto___39194 = (0);
while(true){
if((i__4737__auto___39194 < len__4736__auto___39193)){
args__4742__auto__.push((arguments[i__4737__auto___39194]));

var G__39195 = (i__4737__auto___39194 + (1));
i__4737__auto___39194 = G__39195;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((1) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((1)),(0),null)):null);
return shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4743__auto__);
});

(shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic = (function (msg,args){
if(shadow.cljs.devtools.client.env.log){
if(cljs.core.seq(shadow.cljs.devtools.client.env.log_style)){
return console.log.apply(console,cljs.core.into_array.cljs$core$IFn$_invoke$arity$1(cljs.core.into.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [["%cshadow-cljs: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg)].join(''),shadow.cljs.devtools.client.env.log_style], null),args)));
} else {
return console.log.apply(console,cljs.core.into_array.cljs$core$IFn$_invoke$arity$1(cljs.core.into.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [["shadow-cljs: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg)].join('')], null),args)));
}
} else {
return null;
}
}));

(shadow.cljs.devtools.client.browser.devtools_msg.cljs$lang$maxFixedArity = (1));

/** @this {Function} */
(shadow.cljs.devtools.client.browser.devtools_msg.cljs$lang$applyTo = (function (seq38775){
var G__38776 = cljs.core.first(seq38775);
var seq38775__$1 = cljs.core.next(seq38775);
var self__4723__auto__ = this;
return self__4723__auto__.cljs$core$IFn$_invoke$arity$variadic(G__38776,seq38775__$1);
}));

shadow.cljs.devtools.client.browser.script_eval = (function shadow$cljs$devtools$client$browser$script_eval(code){
return goog.globalEval(code);
});
shadow.cljs.devtools.client.browser.do_js_load = (function shadow$cljs$devtools$client$browser$do_js_load(sources){
var seq__38786 = cljs.core.seq(sources);
var chunk__38787 = null;
var count__38788 = (0);
var i__38789 = (0);
while(true){
if((i__38789 < count__38788)){
var map__38812 = chunk__38787.cljs$core$IIndexed$_nth$arity$2(null,i__38789);
var map__38812__$1 = (((((!((map__38812 == null))))?(((((map__38812.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__38812.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__38812):map__38812);
var src = map__38812__$1;
var resource_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__38812__$1,new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582));
var output_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__38812__$1,new cljs.core.Keyword(null,"output-name","output-name",-1769107767));
var resource_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__38812__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__38812__$1,new cljs.core.Keyword(null,"js","js",1768080579));
$CLJS.SHADOW_ENV.setLoaded(output_name);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load JS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([resource_name], 0));

shadow.cljs.devtools.client.env.before_load_src(src);

try{shadow.cljs.devtools.client.browser.script_eval([cljs.core.str.cljs$core$IFn$_invoke$arity$1(js),"\n//# sourceURL=",cljs.core.str.cljs$core$IFn$_invoke$arity$1($CLJS.SHADOW_ENV.scriptBase),cljs.core.str.cljs$core$IFn$_invoke$arity$1(output_name)].join(''));
}catch (e38820){var e_39199 = e38820;
if(shadow.cljs.devtools.client.env.log){
console.error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name)].join(''),e_39199);
} else {
}

throw (new Error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name),": ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(e_39199.message)].join('')));
}

var G__39200 = seq__38786;
var G__39201 = chunk__38787;
var G__39202 = count__38788;
var G__39203 = (i__38789 + (1));
seq__38786 = G__39200;
chunk__38787 = G__39201;
count__38788 = G__39202;
i__38789 = G__39203;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__38786);
if(temp__5735__auto__){
var seq__38786__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__38786__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__38786__$1);
var G__39211 = cljs.core.chunk_rest(seq__38786__$1);
var G__39212 = c__4556__auto__;
var G__39213 = cljs.core.count(c__4556__auto__);
var G__39214 = (0);
seq__38786 = G__39211;
chunk__38787 = G__39212;
count__38788 = G__39213;
i__38789 = G__39214;
continue;
} else {
var map__38827 = cljs.core.first(seq__38786__$1);
var map__38827__$1 = (((((!((map__38827 == null))))?(((((map__38827.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__38827.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__38827):map__38827);
var src = map__38827__$1;
var resource_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__38827__$1,new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582));
var output_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__38827__$1,new cljs.core.Keyword(null,"output-name","output-name",-1769107767));
var resource_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__38827__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__38827__$1,new cljs.core.Keyword(null,"js","js",1768080579));
$CLJS.SHADOW_ENV.setLoaded(output_name);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load JS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([resource_name], 0));

shadow.cljs.devtools.client.env.before_load_src(src);

try{shadow.cljs.devtools.client.browser.script_eval([cljs.core.str.cljs$core$IFn$_invoke$arity$1(js),"\n//# sourceURL=",cljs.core.str.cljs$core$IFn$_invoke$arity$1($CLJS.SHADOW_ENV.scriptBase),cljs.core.str.cljs$core$IFn$_invoke$arity$1(output_name)].join(''));
}catch (e38831){var e_39227 = e38831;
if(shadow.cljs.devtools.client.env.log){
console.error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name)].join(''),e_39227);
} else {
}

throw (new Error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name),": ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(e_39227.message)].join('')));
}

var G__39228 = cljs.core.next(seq__38786__$1);
var G__39229 = null;
var G__39230 = (0);
var G__39231 = (0);
seq__38786 = G__39228;
chunk__38787 = G__39229;
count__38788 = G__39230;
i__38789 = G__39231;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.cljs.devtools.client.browser.do_js_reload = (function shadow$cljs$devtools$client$browser$do_js_reload(msg,sources,complete_fn,failure_fn){
return shadow.cljs.devtools.client.env.do_js_reload.cljs$core$IFn$_invoke$arity$4(cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(msg,new cljs.core.Keyword(null,"log-missing-fn","log-missing-fn",732676765),(function (fn_sym){
return null;
}),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"log-call-async","log-call-async",183826192),(function (fn_sym){
return shadow.cljs.devtools.client.browser.devtools_msg(["call async ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym)].join(''));
}),new cljs.core.Keyword(null,"log-call","log-call",412404391),(function (fn_sym){
return shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym)].join(''));
})], 0)),(function (){
return shadow.cljs.devtools.client.browser.do_js_load(sources);
}),complete_fn,failure_fn);
});
/**
 * when (require '["some-str" :as x]) is done at the REPL we need to manually call the shadow.js.require for it
 * since the file only adds the shadow$provide. only need to do this for shadow-js.
 */
shadow.cljs.devtools.client.browser.do_js_requires = (function shadow$cljs$devtools$client$browser$do_js_requires(js_requires){
var seq__38842 = cljs.core.seq(js_requires);
var chunk__38843 = null;
var count__38844 = (0);
var i__38845 = (0);
while(true){
if((i__38845 < count__38844)){
var js_ns = chunk__38843.cljs$core$IIndexed$_nth$arity$2(null,i__38845);
var require_str_39242 = ["var ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns)," = shadow.js.require(\"",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns),"\");"].join('');
shadow.cljs.devtools.client.browser.script_eval(require_str_39242);


var G__39243 = seq__38842;
var G__39244 = chunk__38843;
var G__39245 = count__38844;
var G__39246 = (i__38845 + (1));
seq__38842 = G__39243;
chunk__38843 = G__39244;
count__38844 = G__39245;
i__38845 = G__39246;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__38842);
if(temp__5735__auto__){
var seq__38842__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__38842__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__38842__$1);
var G__39250 = cljs.core.chunk_rest(seq__38842__$1);
var G__39251 = c__4556__auto__;
var G__39252 = cljs.core.count(c__4556__auto__);
var G__39253 = (0);
seq__38842 = G__39250;
chunk__38843 = G__39251;
count__38844 = G__39252;
i__38845 = G__39253;
continue;
} else {
var js_ns = cljs.core.first(seq__38842__$1);
var require_str_39254 = ["var ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns)," = shadow.js.require(\"",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns),"\");"].join('');
shadow.cljs.devtools.client.browser.script_eval(require_str_39254);


var G__39255 = cljs.core.next(seq__38842__$1);
var G__39256 = null;
var G__39257 = (0);
var G__39258 = (0);
seq__38842 = G__39255;
chunk__38843 = G__39256;
count__38844 = G__39257;
i__38845 = G__39258;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.cljs.devtools.client.browser.handle_build_complete = (function shadow$cljs$devtools$client$browser$handle_build_complete(runtime,p__38853){
var map__38854 = p__38853;
var map__38854__$1 = (((((!((map__38854 == null))))?(((((map__38854.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__38854.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__38854):map__38854);
var msg = map__38854__$1;
var info = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__38854__$1,new cljs.core.Keyword(null,"info","info",-317069002));
var reload_info = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__38854__$1,new cljs.core.Keyword(null,"reload-info","reload-info",1648088086));
var warnings = cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.distinct.cljs$core$IFn$_invoke$arity$1((function (){var iter__4529__auto__ = (function shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__38857(s__38858){
return (new cljs.core.LazySeq(null,(function (){
var s__38858__$1 = s__38858;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__38858__$1);
if(temp__5735__auto__){
var xs__6292__auto__ = temp__5735__auto__;
var map__38863 = cljs.core.first(xs__6292__auto__);
var map__38863__$1 = (((((!((map__38863 == null))))?(((((map__38863.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__38863.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__38863):map__38863);
var src = map__38863__$1;
var resource_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__38863__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
var warnings = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__38863__$1,new cljs.core.Keyword(null,"warnings","warnings",-735437651));
if(cljs.core.not(new cljs.core.Keyword(null,"from-jar","from-jar",1050932827).cljs$core$IFn$_invoke$arity$1(src))){
var iterys__4525__auto__ = ((function (s__38858__$1,map__38863,map__38863__$1,src,resource_name,warnings,xs__6292__auto__,temp__5735__auto__,map__38854,map__38854__$1,msg,info,reload_info){
return (function shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__38857_$_iter__38859(s__38860){
return (new cljs.core.LazySeq(null,((function (s__38858__$1,map__38863,map__38863__$1,src,resource_name,warnings,xs__6292__auto__,temp__5735__auto__,map__38854,map__38854__$1,msg,info,reload_info){
return (function (){
var s__38860__$1 = s__38860;
while(true){
var temp__5735__auto____$1 = cljs.core.seq(s__38860__$1);
if(temp__5735__auto____$1){
var s__38860__$2 = temp__5735__auto____$1;
if(cljs.core.chunked_seq_QMARK_(s__38860__$2)){
var c__4527__auto__ = cljs.core.chunk_first(s__38860__$2);
var size__4528__auto__ = cljs.core.count(c__4527__auto__);
var b__38862 = cljs.core.chunk_buffer(size__4528__auto__);
if((function (){var i__38861 = (0);
while(true){
if((i__38861 < size__4528__auto__)){
var warning = cljs.core._nth(c__4527__auto__,i__38861);
cljs.core.chunk_append(b__38862,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(warning,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100),resource_name));

var G__39263 = (i__38861 + (1));
i__38861 = G__39263;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__38862),shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__38857_$_iter__38859(cljs.core.chunk_rest(s__38860__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__38862),null);
}
} else {
var warning = cljs.core.first(s__38860__$2);
return cljs.core.cons(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(warning,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100),resource_name),shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__38857_$_iter__38859(cljs.core.rest(s__38860__$2)));
}
} else {
return null;
}
break;
}
});})(s__38858__$1,map__38863,map__38863__$1,src,resource_name,warnings,xs__6292__auto__,temp__5735__auto__,map__38854,map__38854__$1,msg,info,reload_info))
,null,null));
});})(s__38858__$1,map__38863,map__38863__$1,src,resource_name,warnings,xs__6292__auto__,temp__5735__auto__,map__38854,map__38854__$1,msg,info,reload_info))
;
var fs__4526__auto__ = cljs.core.seq(iterys__4525__auto__(warnings));
if(fs__4526__auto__){
return cljs.core.concat.cljs$core$IFn$_invoke$arity$2(fs__4526__auto__,shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__38857(cljs.core.rest(s__38858__$1)));
} else {
var G__39264 = cljs.core.rest(s__38858__$1);
s__38858__$1 = G__39264;
continue;
}
} else {
var G__39265 = cljs.core.rest(s__38858__$1);
s__38858__$1 = G__39265;
continue;
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4529__auto__(new cljs.core.Keyword(null,"sources","sources",-321166424).cljs$core$IFn$_invoke$arity$1(info));
})()));
if(shadow.cljs.devtools.client.env.log){
var seq__38870_39266 = cljs.core.seq(warnings);
var chunk__38871_39267 = null;
var count__38872_39268 = (0);
var i__38873_39269 = (0);
while(true){
if((i__38873_39269 < count__38872_39268)){
var map__38896_39271 = chunk__38871_39267.cljs$core$IIndexed$_nth$arity$2(null,i__38873_39269);
var map__38896_39272__$1 = (((((!((map__38896_39271 == null))))?(((((map__38896_39271.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__38896_39271.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__38896_39271):map__38896_39271);
var w_39273 = map__38896_39272__$1;
var msg_39274__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__38896_39272__$1,new cljs.core.Keyword(null,"msg","msg",-1386103444));
var line_39275 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__38896_39272__$1,new cljs.core.Keyword(null,"line","line",212345235));
var column_39276 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__38896_39272__$1,new cljs.core.Keyword(null,"column","column",2078222095));
var resource_name_39277 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__38896_39272__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
console.warn(["BUILD-WARNING in ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name_39277)," at [",cljs.core.str.cljs$core$IFn$_invoke$arity$1(line_39275),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column_39276),"]\n\t",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg_39274__$1)].join(''));


var G__39283 = seq__38870_39266;
var G__39284 = chunk__38871_39267;
var G__39285 = count__38872_39268;
var G__39286 = (i__38873_39269 + (1));
seq__38870_39266 = G__39283;
chunk__38871_39267 = G__39284;
count__38872_39268 = G__39285;
i__38873_39269 = G__39286;
continue;
} else {
var temp__5735__auto___39287 = cljs.core.seq(seq__38870_39266);
if(temp__5735__auto___39287){
var seq__38870_39288__$1 = temp__5735__auto___39287;
if(cljs.core.chunked_seq_QMARK_(seq__38870_39288__$1)){
var c__4556__auto___39289 = cljs.core.chunk_first(seq__38870_39288__$1);
var G__39290 = cljs.core.chunk_rest(seq__38870_39288__$1);
var G__39291 = c__4556__auto___39289;
var G__39292 = cljs.core.count(c__4556__auto___39289);
var G__39293 = (0);
seq__38870_39266 = G__39290;
chunk__38871_39267 = G__39291;
count__38872_39268 = G__39292;
i__38873_39269 = G__39293;
continue;
} else {
var map__38903_39295 = cljs.core.first(seq__38870_39288__$1);
var map__38903_39296__$1 = (((((!((map__38903_39295 == null))))?(((((map__38903_39295.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__38903_39295.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__38903_39295):map__38903_39295);
var w_39297 = map__38903_39296__$1;
var msg_39298__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__38903_39296__$1,new cljs.core.Keyword(null,"msg","msg",-1386103444));
var line_39299 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__38903_39296__$1,new cljs.core.Keyword(null,"line","line",212345235));
var column_39300 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__38903_39296__$1,new cljs.core.Keyword(null,"column","column",2078222095));
var resource_name_39301 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__38903_39296__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
console.warn(["BUILD-WARNING in ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name_39301)," at [",cljs.core.str.cljs$core$IFn$_invoke$arity$1(line_39299),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column_39300),"]\n\t",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg_39298__$1)].join(''));


var G__39303 = cljs.core.next(seq__38870_39288__$1);
var G__39304 = null;
var G__39305 = (0);
var G__39306 = (0);
seq__38870_39266 = G__39303;
chunk__38871_39267 = G__39304;
count__38872_39268 = G__39305;
i__38873_39269 = G__39306;
continue;
}
} else {
}
}
break;
}
} else {
}

if((!(shadow.cljs.devtools.client.env.autoload))){
return shadow.cljs.devtools.client.hud.load_end_success();
} else {
if(((cljs.core.empty_QMARK_(warnings)) || (shadow.cljs.devtools.client.env.ignore_warnings))){
var sources_to_get = shadow.cljs.devtools.client.env.filter_reload_sources(info,reload_info);
if(cljs.core.not(cljs.core.seq(sources_to_get))){
return shadow.cljs.devtools.client.hud.load_end_success();
} else {
if(cljs.core.seq(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(msg,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"reload-info","reload-info",1648088086),new cljs.core.Keyword(null,"after-load","after-load",-1278503285)], null)))){
} else {
shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("reloading code but no :after-load hooks are configured!",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["https://shadow-cljs.github.io/docs/UsersGuide.html#_lifecycle_hooks"], 0));
}

return shadow.cljs.devtools.client.shared.load_sources(runtime,sources_to_get,(function (p1__38852_SHARP_){
return shadow.cljs.devtools.client.browser.do_js_reload(msg,p1__38852_SHARP_,shadow.cljs.devtools.client.hud.load_end_success,shadow.cljs.devtools.client.hud.load_failure);
}));
}
} else {
return null;
}
}
});
shadow.cljs.devtools.client.browser.page_load_uri = (cljs.core.truth_(goog.global.document)?goog.Uri.parse(document.location.href):null);
shadow.cljs.devtools.client.browser.match_paths = (function shadow$cljs$devtools$client$browser$match_paths(old,new$){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2("file",shadow.cljs.devtools.client.browser.page_load_uri.getScheme())){
var rel_new = cljs.core.subs.cljs$core$IFn$_invoke$arity$2(new$,(1));
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(old,rel_new)) || (clojure.string.starts_with_QMARK_(old,[rel_new,"?"].join(''))))){
return rel_new;
} else {
return null;
}
} else {
var node_uri = goog.Uri.parse(old);
var node_uri_resolved = shadow.cljs.devtools.client.browser.page_load_uri.resolve(node_uri);
var node_abs = node_uri_resolved.getPath();
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$1(shadow.cljs.devtools.client.browser.page_load_uri.hasSameDomainAs(node_uri))) || (cljs.core.not(node_uri.hasDomain())))){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(node_abs,new$)){
return new$;
} else {
return false;
}
} else {
return false;
}
}
});
shadow.cljs.devtools.client.browser.handle_asset_update = (function shadow$cljs$devtools$client$browser$handle_asset_update(p__38931){
var map__38932 = p__38931;
var map__38932__$1 = (((((!((map__38932 == null))))?(((((map__38932.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__38932.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__38932):map__38932);
var msg = map__38932__$1;
var updates = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__38932__$1,new cljs.core.Keyword(null,"updates","updates",2013983452));
var seq__38936 = cljs.core.seq(updates);
var chunk__38938 = null;
var count__38939 = (0);
var i__38940 = (0);
while(true){
if((i__38940 < count__38939)){
var path = chunk__38938.cljs$core$IIndexed$_nth$arity$2(null,i__38940);
if(clojure.string.ends_with_QMARK_(path,"css")){
var seq__39056_39309 = cljs.core.seq(cljs.core.array_seq.cljs$core$IFn$_invoke$arity$1(document.querySelectorAll("link[rel=\"stylesheet\"]")));
var chunk__39060_39310 = null;
var count__39061_39311 = (0);
var i__39062_39312 = (0);
while(true){
if((i__39062_39312 < count__39061_39311)){
var node_39313 = chunk__39060_39310.cljs$core$IIndexed$_nth$arity$2(null,i__39062_39312);
if(cljs.core.not(node_39313.shadow$old)){
var path_match_39314 = shadow.cljs.devtools.client.browser.match_paths(node_39313.getAttribute("href"),path);
if(cljs.core.truth_(path_match_39314)){
var new_link_39315 = (function (){var G__39086 = node_39313.cloneNode(true);
G__39086.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_39314),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__39086;
})();
(node_39313.shadow$old = true);

(new_link_39315.onload = ((function (seq__39056_39309,chunk__39060_39310,count__39061_39311,i__39062_39312,seq__38936,chunk__38938,count__38939,i__38940,new_link_39315,path_match_39314,node_39313,path,map__38932,map__38932__$1,msg,updates){
return (function (e){
return goog.dom.removeNode(node_39313);
});})(seq__39056_39309,chunk__39060_39310,count__39061_39311,i__39062_39312,seq__38936,chunk__38938,count__38939,i__38940,new_link_39315,path_match_39314,node_39313,path,map__38932,map__38932__$1,msg,updates))
);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_39314], 0));

goog.dom.insertSiblingAfter(new_link_39315,node_39313);


var G__39317 = seq__39056_39309;
var G__39318 = chunk__39060_39310;
var G__39319 = count__39061_39311;
var G__39320 = (i__39062_39312 + (1));
seq__39056_39309 = G__39317;
chunk__39060_39310 = G__39318;
count__39061_39311 = G__39319;
i__39062_39312 = G__39320;
continue;
} else {
var G__39321 = seq__39056_39309;
var G__39322 = chunk__39060_39310;
var G__39323 = count__39061_39311;
var G__39324 = (i__39062_39312 + (1));
seq__39056_39309 = G__39321;
chunk__39060_39310 = G__39322;
count__39061_39311 = G__39323;
i__39062_39312 = G__39324;
continue;
}
} else {
var G__39326 = seq__39056_39309;
var G__39327 = chunk__39060_39310;
var G__39328 = count__39061_39311;
var G__39329 = (i__39062_39312 + (1));
seq__39056_39309 = G__39326;
chunk__39060_39310 = G__39327;
count__39061_39311 = G__39328;
i__39062_39312 = G__39329;
continue;
}
} else {
var temp__5735__auto___39333 = cljs.core.seq(seq__39056_39309);
if(temp__5735__auto___39333){
var seq__39056_39334__$1 = temp__5735__auto___39333;
if(cljs.core.chunked_seq_QMARK_(seq__39056_39334__$1)){
var c__4556__auto___39335 = cljs.core.chunk_first(seq__39056_39334__$1);
var G__39336 = cljs.core.chunk_rest(seq__39056_39334__$1);
var G__39337 = c__4556__auto___39335;
var G__39338 = cljs.core.count(c__4556__auto___39335);
var G__39339 = (0);
seq__39056_39309 = G__39336;
chunk__39060_39310 = G__39337;
count__39061_39311 = G__39338;
i__39062_39312 = G__39339;
continue;
} else {
var node_39340 = cljs.core.first(seq__39056_39334__$1);
if(cljs.core.not(node_39340.shadow$old)){
var path_match_39341 = shadow.cljs.devtools.client.browser.match_paths(node_39340.getAttribute("href"),path);
if(cljs.core.truth_(path_match_39341)){
var new_link_39342 = (function (){var G__39095 = node_39340.cloneNode(true);
G__39095.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_39341),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__39095;
})();
(node_39340.shadow$old = true);

(new_link_39342.onload = ((function (seq__39056_39309,chunk__39060_39310,count__39061_39311,i__39062_39312,seq__38936,chunk__38938,count__38939,i__38940,new_link_39342,path_match_39341,node_39340,seq__39056_39334__$1,temp__5735__auto___39333,path,map__38932,map__38932__$1,msg,updates){
return (function (e){
return goog.dom.removeNode(node_39340);
});})(seq__39056_39309,chunk__39060_39310,count__39061_39311,i__39062_39312,seq__38936,chunk__38938,count__38939,i__38940,new_link_39342,path_match_39341,node_39340,seq__39056_39334__$1,temp__5735__auto___39333,path,map__38932,map__38932__$1,msg,updates))
);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_39341], 0));

goog.dom.insertSiblingAfter(new_link_39342,node_39340);


var G__39343 = cljs.core.next(seq__39056_39334__$1);
var G__39344 = null;
var G__39345 = (0);
var G__39346 = (0);
seq__39056_39309 = G__39343;
chunk__39060_39310 = G__39344;
count__39061_39311 = G__39345;
i__39062_39312 = G__39346;
continue;
} else {
var G__39347 = cljs.core.next(seq__39056_39334__$1);
var G__39348 = null;
var G__39349 = (0);
var G__39350 = (0);
seq__39056_39309 = G__39347;
chunk__39060_39310 = G__39348;
count__39061_39311 = G__39349;
i__39062_39312 = G__39350;
continue;
}
} else {
var G__39351 = cljs.core.next(seq__39056_39334__$1);
var G__39352 = null;
var G__39353 = (0);
var G__39354 = (0);
seq__39056_39309 = G__39351;
chunk__39060_39310 = G__39352;
count__39061_39311 = G__39353;
i__39062_39312 = G__39354;
continue;
}
}
} else {
}
}
break;
}


var G__39355 = seq__38936;
var G__39356 = chunk__38938;
var G__39357 = count__38939;
var G__39358 = (i__38940 + (1));
seq__38936 = G__39355;
chunk__38938 = G__39356;
count__38939 = G__39357;
i__38940 = G__39358;
continue;
} else {
var G__39359 = seq__38936;
var G__39360 = chunk__38938;
var G__39361 = count__38939;
var G__39362 = (i__38940 + (1));
seq__38936 = G__39359;
chunk__38938 = G__39360;
count__38939 = G__39361;
i__38940 = G__39362;
continue;
}
} else {
var temp__5735__auto__ = cljs.core.seq(seq__38936);
if(temp__5735__auto__){
var seq__38936__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__38936__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__38936__$1);
var G__39363 = cljs.core.chunk_rest(seq__38936__$1);
var G__39364 = c__4556__auto__;
var G__39365 = cljs.core.count(c__4556__auto__);
var G__39366 = (0);
seq__38936 = G__39363;
chunk__38938 = G__39364;
count__38939 = G__39365;
i__38940 = G__39366;
continue;
} else {
var path = cljs.core.first(seq__38936__$1);
if(clojure.string.ends_with_QMARK_(path,"css")){
var seq__39101_39367 = cljs.core.seq(cljs.core.array_seq.cljs$core$IFn$_invoke$arity$1(document.querySelectorAll("link[rel=\"stylesheet\"]")));
var chunk__39105_39368 = null;
var count__39106_39369 = (0);
var i__39107_39370 = (0);
while(true){
if((i__39107_39370 < count__39106_39369)){
var node_39371 = chunk__39105_39368.cljs$core$IIndexed$_nth$arity$2(null,i__39107_39370);
if(cljs.core.not(node_39371.shadow$old)){
var path_match_39372 = shadow.cljs.devtools.client.browser.match_paths(node_39371.getAttribute("href"),path);
if(cljs.core.truth_(path_match_39372)){
var new_link_39373 = (function (){var G__39123 = node_39371.cloneNode(true);
G__39123.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_39372),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__39123;
})();
(node_39371.shadow$old = true);

(new_link_39373.onload = ((function (seq__39101_39367,chunk__39105_39368,count__39106_39369,i__39107_39370,seq__38936,chunk__38938,count__38939,i__38940,new_link_39373,path_match_39372,node_39371,path,seq__38936__$1,temp__5735__auto__,map__38932,map__38932__$1,msg,updates){
return (function (e){
return goog.dom.removeNode(node_39371);
});})(seq__39101_39367,chunk__39105_39368,count__39106_39369,i__39107_39370,seq__38936,chunk__38938,count__38939,i__38940,new_link_39373,path_match_39372,node_39371,path,seq__38936__$1,temp__5735__auto__,map__38932,map__38932__$1,msg,updates))
);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_39372], 0));

goog.dom.insertSiblingAfter(new_link_39373,node_39371);


var G__39374 = seq__39101_39367;
var G__39375 = chunk__39105_39368;
var G__39376 = count__39106_39369;
var G__39377 = (i__39107_39370 + (1));
seq__39101_39367 = G__39374;
chunk__39105_39368 = G__39375;
count__39106_39369 = G__39376;
i__39107_39370 = G__39377;
continue;
} else {
var G__39378 = seq__39101_39367;
var G__39379 = chunk__39105_39368;
var G__39380 = count__39106_39369;
var G__39381 = (i__39107_39370 + (1));
seq__39101_39367 = G__39378;
chunk__39105_39368 = G__39379;
count__39106_39369 = G__39380;
i__39107_39370 = G__39381;
continue;
}
} else {
var G__39382 = seq__39101_39367;
var G__39383 = chunk__39105_39368;
var G__39384 = count__39106_39369;
var G__39385 = (i__39107_39370 + (1));
seq__39101_39367 = G__39382;
chunk__39105_39368 = G__39383;
count__39106_39369 = G__39384;
i__39107_39370 = G__39385;
continue;
}
} else {
var temp__5735__auto___39386__$1 = cljs.core.seq(seq__39101_39367);
if(temp__5735__auto___39386__$1){
var seq__39101_39387__$1 = temp__5735__auto___39386__$1;
if(cljs.core.chunked_seq_QMARK_(seq__39101_39387__$1)){
var c__4556__auto___39388 = cljs.core.chunk_first(seq__39101_39387__$1);
var G__39389 = cljs.core.chunk_rest(seq__39101_39387__$1);
var G__39390 = c__4556__auto___39388;
var G__39391 = cljs.core.count(c__4556__auto___39388);
var G__39392 = (0);
seq__39101_39367 = G__39389;
chunk__39105_39368 = G__39390;
count__39106_39369 = G__39391;
i__39107_39370 = G__39392;
continue;
} else {
var node_39393 = cljs.core.first(seq__39101_39387__$1);
if(cljs.core.not(node_39393.shadow$old)){
var path_match_39394 = shadow.cljs.devtools.client.browser.match_paths(node_39393.getAttribute("href"),path);
if(cljs.core.truth_(path_match_39394)){
var new_link_39395 = (function (){var G__39128 = node_39393.cloneNode(true);
G__39128.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_39394),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__39128;
})();
(node_39393.shadow$old = true);

(new_link_39395.onload = ((function (seq__39101_39367,chunk__39105_39368,count__39106_39369,i__39107_39370,seq__38936,chunk__38938,count__38939,i__38940,new_link_39395,path_match_39394,node_39393,seq__39101_39387__$1,temp__5735__auto___39386__$1,path,seq__38936__$1,temp__5735__auto__,map__38932,map__38932__$1,msg,updates){
return (function (e){
return goog.dom.removeNode(node_39393);
});})(seq__39101_39367,chunk__39105_39368,count__39106_39369,i__39107_39370,seq__38936,chunk__38938,count__38939,i__38940,new_link_39395,path_match_39394,node_39393,seq__39101_39387__$1,temp__5735__auto___39386__$1,path,seq__38936__$1,temp__5735__auto__,map__38932,map__38932__$1,msg,updates))
);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_39394], 0));

goog.dom.insertSiblingAfter(new_link_39395,node_39393);


var G__39401 = cljs.core.next(seq__39101_39387__$1);
var G__39402 = null;
var G__39403 = (0);
var G__39404 = (0);
seq__39101_39367 = G__39401;
chunk__39105_39368 = G__39402;
count__39106_39369 = G__39403;
i__39107_39370 = G__39404;
continue;
} else {
var G__39405 = cljs.core.next(seq__39101_39387__$1);
var G__39406 = null;
var G__39407 = (0);
var G__39408 = (0);
seq__39101_39367 = G__39405;
chunk__39105_39368 = G__39406;
count__39106_39369 = G__39407;
i__39107_39370 = G__39408;
continue;
}
} else {
var G__39410 = cljs.core.next(seq__39101_39387__$1);
var G__39411 = null;
var G__39412 = (0);
var G__39413 = (0);
seq__39101_39367 = G__39410;
chunk__39105_39368 = G__39411;
count__39106_39369 = G__39412;
i__39107_39370 = G__39413;
continue;
}
}
} else {
}
}
break;
}


var G__39414 = cljs.core.next(seq__38936__$1);
var G__39415 = null;
var G__39416 = (0);
var G__39417 = (0);
seq__38936 = G__39414;
chunk__38938 = G__39415;
count__38939 = G__39416;
i__38940 = G__39417;
continue;
} else {
var G__39419 = cljs.core.next(seq__38936__$1);
var G__39420 = null;
var G__39421 = (0);
var G__39422 = (0);
seq__38936 = G__39419;
chunk__38938 = G__39420;
count__38939 = G__39421;
i__38940 = G__39422;
continue;
}
}
} else {
return null;
}
}
break;
}
});
shadow.cljs.devtools.client.browser.global_eval = (function shadow$cljs$devtools$client$browser$global_eval(js){
if(cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2("undefined",typeof(module))){
return eval(js);
} else {
return (0,eval)(js);;
}
});
shadow.cljs.devtools.client.browser.repl_init = (function shadow$cljs$devtools$client$browser$repl_init(runtime,p__39136){
var map__39137 = p__39136;
var map__39137__$1 = (((((!((map__39137 == null))))?(((((map__39137.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__39137.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__39137):map__39137);
var repl_state = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__39137__$1,new cljs.core.Keyword(null,"repl-state","repl-state",-1733780387));
return shadow.cljs.devtools.client.shared.load_sources(runtime,cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.remove.cljs$core$IFn$_invoke$arity$2(shadow.cljs.devtools.client.env.src_is_loaded_QMARK_,new cljs.core.Keyword(null,"repl-sources","repl-sources",723867535).cljs$core$IFn$_invoke$arity$1(repl_state))),(function (sources){
shadow.cljs.devtools.client.browser.do_js_load(sources);

return shadow.cljs.devtools.client.browser.devtools_msg("ready!");
}));
});
shadow.cljs.devtools.client.browser.client_info = new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"host","host",-1558485167),(cljs.core.truth_(goog.global.document)?new cljs.core.Keyword(null,"browser","browser",828191719):new cljs.core.Keyword(null,"browser-worker","browser-worker",1638998282)),new cljs.core.Keyword(null,"user-agent","user-agent",1220426212),[(cljs.core.truth_(goog.userAgent.OPERA)?"Opera":(cljs.core.truth_(goog.userAgent.product.CHROME)?"Chrome":(cljs.core.truth_(goog.userAgent.IE)?"MSIE":(cljs.core.truth_(goog.userAgent.EDGE)?"Edge":(cljs.core.truth_(goog.userAgent.GECKO)?"Firefox":(cljs.core.truth_(goog.userAgent.SAFARI)?"Safari":(cljs.core.truth_(goog.userAgent.WEBKIT)?"Webkit":null)))))))," ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(goog.userAgent.VERSION)," [",cljs.core.str.cljs$core$IFn$_invoke$arity$1(goog.userAgent.PLATFORM),"]"].join(''),new cljs.core.Keyword(null,"dom","dom",-1236537922),(!((goog.global.document == null)))], null);
if((typeof shadow !== 'undefined') && (typeof shadow.cljs !== 'undefined') && (typeof shadow.cljs.devtools !== 'undefined') && (typeof shadow.cljs.devtools.client !== 'undefined') && (typeof shadow.cljs.devtools.client.browser !== 'undefined') && (typeof shadow.cljs.devtools.client.browser.ws_was_welcome_ref !== 'undefined')){
} else {
shadow.cljs.devtools.client.browser.ws_was_welcome_ref = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(false);
}
if(((shadow.cljs.devtools.client.env.enabled) && ((shadow.cljs.devtools.client.env.worker_client_id > (0))))){
(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$remote$runtime$api$IEvalJS$ = cljs.core.PROTOCOL_SENTINEL);

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$remote$runtime$api$IEvalJS$_js_eval$arity$2 = (function (this$,code){
var this$__$1 = this;
return shadow.cljs.devtools.client.browser.global_eval(code);
}));

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$cljs$devtools$client$shared$IHostSpecific$ = cljs.core.PROTOCOL_SENTINEL);

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$cljs$devtools$client$shared$IHostSpecific$do_invoke$arity$2 = (function (this$,p__39148){
var map__39149 = p__39148;
var map__39149__$1 = (((((!((map__39149 == null))))?(((((map__39149.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__39149.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__39149):map__39149);
var _ = map__39149__$1;
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__39149__$1,new cljs.core.Keyword(null,"js","js",1768080579));
var this$__$1 = this;
return shadow.cljs.devtools.client.browser.global_eval(js);
}));

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$cljs$devtools$client$shared$IHostSpecific$do_repl_init$arity$4 = (function (runtime,p__39151,done,error){
var map__39152 = p__39151;
var map__39152__$1 = (((((!((map__39152 == null))))?(((((map__39152.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__39152.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__39152):map__39152);
var repl_sources = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__39152__$1,new cljs.core.Keyword(null,"repl-sources","repl-sources",723867535));
var runtime__$1 = this;
return shadow.cljs.devtools.client.shared.load_sources(runtime__$1,cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.remove.cljs$core$IFn$_invoke$arity$2(shadow.cljs.devtools.client.env.src_is_loaded_QMARK_,repl_sources)),(function (sources){
shadow.cljs.devtools.client.browser.do_js_load(sources);

return (done.cljs$core$IFn$_invoke$arity$0 ? done.cljs$core$IFn$_invoke$arity$0() : done.call(null));
}));
}));

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$cljs$devtools$client$shared$IHostSpecific$do_repl_require$arity$4 = (function (runtime,p__39155,done,error){
var map__39157 = p__39155;
var map__39157__$1 = (((((!((map__39157 == null))))?(((((map__39157.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__39157.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__39157):map__39157);
var msg = map__39157__$1;
var sources = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__39157__$1,new cljs.core.Keyword(null,"sources","sources",-321166424));
var reload_namespaces = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__39157__$1,new cljs.core.Keyword(null,"reload-namespaces","reload-namespaces",250210134));
var js_requires = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__39157__$1,new cljs.core.Keyword(null,"js-requires","js-requires",-1311472051));
var runtime__$1 = this;
var sources_to_load = cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.remove.cljs$core$IFn$_invoke$arity$2((function (p__39159){
var map__39160 = p__39159;
var map__39160__$1 = (((((!((map__39160 == null))))?(((((map__39160.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__39160.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__39160):map__39160);
var src = map__39160__$1;
var provides = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__39160__$1,new cljs.core.Keyword(null,"provides","provides",-1634397992));
var and__4115__auto__ = shadow.cljs.devtools.client.env.src_is_loaded_QMARK_(src);
if(cljs.core.truth_(and__4115__auto__)){
return cljs.core.not(cljs.core.some(reload_namespaces,provides));
} else {
return and__4115__auto__;
}
}),sources));
if(cljs.core.not(cljs.core.seq(sources_to_load))){
var G__39163 = cljs.core.PersistentVector.EMPTY;
return (done.cljs$core$IFn$_invoke$arity$1 ? done.cljs$core$IFn$_invoke$arity$1(G__39163) : done.call(null,G__39163));
} else {
return shadow.remote.runtime.shared.call.cljs$core$IFn$_invoke$arity$3(runtime__$1,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"cljs-load-sources","cljs-load-sources",-1458295962),new cljs.core.Keyword(null,"to","to",192099007),shadow.cljs.devtools.client.env.worker_client_id,new cljs.core.Keyword(null,"sources","sources",-321166424),cljs.core.into.cljs$core$IFn$_invoke$arity$3(cljs.core.PersistentVector.EMPTY,cljs.core.map.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582)),sources_to_load)], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"cljs-sources","cljs-sources",31121610),(function (p__39165){
var map__39166 = p__39165;
var map__39166__$1 = (((((!((map__39166 == null))))?(((((map__39166.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__39166.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__39166):map__39166);
var msg__$1 = map__39166__$1;
var sources__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__39166__$1,new cljs.core.Keyword(null,"sources","sources",-321166424));
try{shadow.cljs.devtools.client.browser.do_js_load(sources__$1);

if(cljs.core.seq(js_requires)){
shadow.cljs.devtools.client.browser.do_js_requires(js_requires);
} else {
}

return (done.cljs$core$IFn$_invoke$arity$1 ? done.cljs$core$IFn$_invoke$arity$1(sources_to_load) : done.call(null,sources_to_load));
}catch (e39168){var ex = e39168;
return (error.cljs$core$IFn$_invoke$arity$1 ? error.cljs$core$IFn$_invoke$arity$1(ex) : error.call(null,ex));
}})], null));
}
}));

shadow.cljs.devtools.client.shared.add_plugin_BANG_(new cljs.core.Keyword("shadow.cljs.devtools.client.browser","client","shadow.cljs.devtools.client.browser/client",-1461019282),cljs.core.PersistentHashSet.EMPTY,(function (p__39169){
var map__39170 = p__39169;
var map__39170__$1 = (((((!((map__39170 == null))))?(((((map__39170.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__39170.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__39170):map__39170);
var env = map__39170__$1;
var runtime = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__39170__$1,new cljs.core.Keyword(null,"runtime","runtime",-1331573996));
var svc = new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"runtime","runtime",-1331573996),runtime], null);
shadow.remote.runtime.api.add_extension(runtime,new cljs.core.Keyword("shadow.cljs.devtools.client.browser","client","shadow.cljs.devtools.client.browser/client",-1461019282),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"on-welcome","on-welcome",1895317125),(function (){
cljs.core.reset_BANG_(shadow.cljs.devtools.client.browser.ws_was_welcome_ref,true);

shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

shadow.cljs.devtools.client.env.patch_goog_BANG_();

return shadow.cljs.devtools.client.browser.devtools_msg(["#",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"client-id","client-id",-464622140).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(new cljs.core.Keyword(null,"state-ref","state-ref",2127874952).cljs$core$IFn$_invoke$arity$1(runtime))))," ready!"].join(''));
}),new cljs.core.Keyword(null,"on-disconnect","on-disconnect",-809021814),(function (e){
if(cljs.core.truth_(cljs.core.deref(shadow.cljs.devtools.client.browser.ws_was_welcome_ref))){
shadow.cljs.devtools.client.hud.connection_error("The Websocket connection was closed!");

return cljs.core.reset_BANG_(shadow.cljs.devtools.client.browser.ws_was_welcome_ref,false);
} else {
return null;
}
}),new cljs.core.Keyword(null,"on-reconnect","on-reconnect",1239988702),(function (e){
return shadow.cljs.devtools.client.hud.connection_error("Reconnecting ...");
}),new cljs.core.Keyword(null,"ops","ops",1237330063),new cljs.core.PersistentArrayMap(null, 8, [new cljs.core.Keyword(null,"access-denied","access-denied",959449406),(function (msg){
cljs.core.reset_BANG_(shadow.cljs.devtools.client.browser.ws_was_welcome_ref,false);

return shadow.cljs.devtools.client.hud.connection_error(["Stale Output! Your loaded JS was not produced by the running shadow-cljs instance."," Is the watch for this build running?"].join(''));
}),new cljs.core.Keyword(null,"cljs-runtime-init","cljs-runtime-init",1305890232),(function (msg){
return shadow.cljs.devtools.client.browser.repl_init(runtime,msg);
}),new cljs.core.Keyword(null,"cljs-asset-update","cljs-asset-update",1224093028),(function (p__39172){
var map__39173 = p__39172;
var map__39173__$1 = (((((!((map__39173 == null))))?(((((map__39173.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__39173.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__39173):map__39173);
var msg = map__39173__$1;
var updates = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__39173__$1,new cljs.core.Keyword(null,"updates","updates",2013983452));
return shadow.cljs.devtools.client.browser.handle_asset_update(msg);
}),new cljs.core.Keyword(null,"cljs-build-configure","cljs-build-configure",-2089891268),(function (msg){
return null;
}),new cljs.core.Keyword(null,"cljs-build-start","cljs-build-start",-725781241),(function (msg){
shadow.cljs.devtools.client.hud.hud_hide();

shadow.cljs.devtools.client.hud.load_start();

return shadow.cljs.devtools.client.env.run_custom_notify_BANG_(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(msg,new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"build-start","build-start",-959649480)));
}),new cljs.core.Keyword(null,"cljs-build-complete","cljs-build-complete",273626153),(function (msg){
var msg__$1 = shadow.cljs.devtools.client.env.add_warnings_to_info(msg);
shadow.cljs.devtools.client.hud.hud_warnings(msg__$1);

shadow.cljs.devtools.client.browser.handle_build_complete(runtime,msg__$1);

return shadow.cljs.devtools.client.env.run_custom_notify_BANG_(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(msg__$1,new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"build-complete","build-complete",-501868472)));
}),new cljs.core.Keyword(null,"cljs-build-failure","cljs-build-failure",1718154990),(function (msg){
shadow.cljs.devtools.client.hud.load_end();

shadow.cljs.devtools.client.hud.hud_error(msg);

return shadow.cljs.devtools.client.env.run_custom_notify_BANG_(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(msg,new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"build-failure","build-failure",-2107487466)));
}),new cljs.core.Keyword("shadow.cljs.devtools.client.env","worker-notify","shadow.cljs.devtools.client.env/worker-notify",-1456820670),(function (p__39177){
var map__39178 = p__39177;
var map__39178__$1 = (((((!((map__39178 == null))))?(((((map__39178.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__39178.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__39178):map__39178);
var event_op = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__39178__$1,new cljs.core.Keyword(null,"event-op","event-op",200358057));
var client_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__39178__$1,new cljs.core.Keyword(null,"client-id","client-id",-464622140));
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"client-disconnect","client-disconnect",640227957),event_op)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(client_id,shadow.cljs.devtools.client.env.worker_client_id)))){
shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

return shadow.cljs.devtools.client.hud.connection_error("The watch for this build was stopped!");
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"client-connect","client-connect",-1113973888),event_op)){
shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

return shadow.cljs.devtools.client.hud.connection_error("The watch for this build was restarted. Reload required!");
} else {
return null;
}
}
})], null)], null));

return svc;
}),(function (p__39184){
var map__39185 = p__39184;
var map__39185__$1 = (((((!((map__39185 == null))))?(((((map__39185.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__39185.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__39185):map__39185);
var svc = map__39185__$1;
var runtime = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__39185__$1,new cljs.core.Keyword(null,"runtime","runtime",-1331573996));
return shadow.remote.runtime.api.del_extension(runtime,new cljs.core.Keyword("shadow.cljs.devtools.client.browser","client","shadow.cljs.devtools.client.browser/client",-1461019282));
}));

shadow.cljs.devtools.client.shared.init_runtime_BANG_(shadow.cljs.devtools.client.browser.client_info,shadow.cljs.devtools.client.websocket.start,shadow.cljs.devtools.client.websocket.send,shadow.cljs.devtools.client.websocket.stop);
} else {
}

//# sourceMappingURL=shadow.cljs.devtools.client.browser.js.map
