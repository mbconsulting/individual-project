goog.provide('app.core');
app.core.base_api_url = "https://s07d2i4zvg.execute-api.us-east-1.amazonaws.com/prod";
app.core.get_pref_url = (function app$core$get_pref_url(patient_id){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([patient_id], 0));

return [app.core.base_api_url,"/db/",cljs.core.str.cljs$core$IFn$_invoke$arity$1(patient_id)].join('');
});
app.core.save_pref_url = app.core.get_pref_url;
app.core.patient_identifier = (function app$core$patient_identifier(patient){
return new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"patient","patient",1178852672).cljs$core$IFn$_invoke$arity$1(patient));
});
app.core.js_parse = (function app$core$js_parse(j){
return cljs.core.js__GT_clj.cljs$core$IFn$_invoke$arity$variadic(j,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"keywordize-keys","keywordize-keys",1310784252),true], 0));
});
app.core.clj_parse = (function app$core$clj_parse(c){
return cljs.core.clj__GT_js(c);
});
app.core.prep_body = (function app$core$prep_body(time_prefs,meds){
return JSON.stringify(app.core.clj_parse(new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"times","times",1671571467),time_prefs,new cljs.core.Keyword(null,"meds","meds",-2036408830),meds], null)));
});
/**
 * Dissociates an entry from a nested associative structure returning a new
 *   nested structure. keys is a sequence of keys. Any empty maps that result
 *   will not be present in the new structure.
 */
app.core.dissoc_in = (function app$core$dissoc_in(m,p__70580){
var vec__70581 = p__70580;
var seq__70582 = cljs.core.seq(vec__70581);
var first__70583 = cljs.core.first(seq__70582);
var seq__70582__$1 = cljs.core.next(seq__70582);
var k = first__70583;
var ks = seq__70582__$1;
var keys = vec__70581;
if(ks){
var temp__5733__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(m,k);
if(cljs.core.truth_(temp__5733__auto__)){
var nextmap = temp__5733__auto__;
var newmap = (app.core.dissoc_in.cljs$core$IFn$_invoke$arity$2 ? app.core.dissoc_in.cljs$core$IFn$_invoke$arity$2(nextmap,ks) : app.core.dissoc_in.call(null,nextmap,ks));
if(cljs.core.seq(newmap)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(m,k,newmap);
} else {
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(m,k);
}
} else {
return m;
}
} else {
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(m,k);
}
});
app.core.display_date = (function app$core$display_date(date_str){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span","span",1394872991),(cljs.core.truth_(date_str)?tick.alpha.api.format.cljs$core$IFn$_invoke$arity$2(tick.format.formatter.cljs$core$IFn$_invoke$arity$1("MM-dd-yyyy"),tick.alpha.api.date.cljs$core$IFn$_invoke$arity$1(date_str)):"unknown")], null);
});
app.core.parse_med = (function app$core$parse_med(med){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["parsing",med], 0));

var map__70584 = new cljs.core.Keyword(null,"resource","resource",251898836).cljs$core$IFn$_invoke$arity$1(med);
var map__70584__$1 = (((((!((map__70584 == null))))?(((((map__70584.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__70584.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__70584):map__70584);
var medicationCodeableConcept = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__70584__$1,new cljs.core.Keyword(null,"medicationCodeableConcept","medicationCodeableConcept",1202240780));
var status = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__70584__$1,new cljs.core.Keyword(null,"status","status",-1997798413));
var dosageInstruction = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__70584__$1,new cljs.core.Keyword(null,"dosageInstruction","dosageInstruction",-1236060204));
return cljs.core.PersistentArrayMap.createAsIfByAssoc([cljs.core.keyword.cljs$core$IFn$_invoke$arity$1(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(medicationCodeableConcept,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"coding","coding",1900940726),(0),new cljs.core.Keyword(null,"code","code",1586293142)], null))),new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"text","text",-1790561697).cljs$core$IFn$_invoke$arity$1(medicationCodeableConcept),new cljs.core.Keyword(null,"code","code",1586293142),cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(medicationCodeableConcept,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"coding","coding",1900940726),(0)], null)),new cljs.core.Keyword(null,"dose","dose",-1241257813),cljs.core.select_keys((function (){var or__4126__auto__ = cljs.core.first(dosageInstruction);
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return cljs.core.PersistentArrayMap.EMPTY;
}
})(),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"sequence","sequence",926807414),new cljs.core.Keyword(null,"timing","timing",-1849225195),new cljs.core.Keyword(null,"asNeededBoolean","asNeededBoolean",723354128)], null)),new cljs.core.Keyword(null,"interactions","interactions",550841811),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"status","status",-1997798413),status], null)]);
});
if((typeof app !== 'undefined') && (typeof app.core !== 'undefined') && (typeof app.core.all_times !== 'undefined')){
} else {
app.core.all_times = new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"key","key",-1516042587),new cljs.core.Keyword(null,"wake","wake",1262361182),new cljs.core.Keyword(null,"label","label",1718410804),"Wake Time",new cljs.core.Keyword(null,"default","default",-1987822328),"07:00",new cljs.core.Keyword(null,"position","position",-2011731912),(1)], null),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"key","key",-1516042587),new cljs.core.Keyword(null,"breakfast","breakfast",-232816887),new cljs.core.Keyword(null,"label","label",1718410804),"Breakfast Time",new cljs.core.Keyword(null,"default","default",-1987822328),"08:00",new cljs.core.Keyword(null,"position","position",-2011731912),(2)], null),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"key","key",-1516042587),new cljs.core.Keyword(null,"lunch","lunch",-875508703),new cljs.core.Keyword(null,"label","label",1718410804),"Lunch Time",new cljs.core.Keyword(null,"default","default",-1987822328),"12:00",new cljs.core.Keyword(null,"position","position",-2011731912),(3)], null),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"key","key",-1516042587),new cljs.core.Keyword(null,"dinner","dinner",-446811662),new cljs.core.Keyword(null,"label","label",1718410804),"Dinner Time",new cljs.core.Keyword(null,"default","default",-1987822328),"18:00",new cljs.core.Keyword(null,"position","position",-2011731912),(4)], null),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"key","key",-1516042587),new cljs.core.Keyword(null,"bed","bed",-1757117708),new cljs.core.Keyword(null,"label","label",1718410804),"Bed Time",new cljs.core.Keyword(null,"default","default",-1987822328),"22:00",new cljs.core.Keyword(null,"position","position",-2011731912),(5)], null)], null);
}
if((typeof app !== 'undefined') && (typeof app.core !== 'undefined') && (typeof app.core.location !== 'undefined')){
} else {
app.core.location = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"handler","handler",-195596612),new cljs.core.Keyword(null,"patient","patient",1178852672)], null));
}
if((typeof app !== 'undefined') && (typeof app.core !== 'undefined') && (typeof app.core.patient_state !== 'undefined')){
} else {
app.core.patient_state = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
}
if((typeof app !== 'undefined') && (typeof app.core !== 'undefined') && (typeof app.core.time_preferences !== 'undefined')){
} else {
app.core.time_preferences = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
}
if((typeof app !== 'undefined') && (typeof app.core !== 'undefined') && (typeof app.core.med_preferences !== 'undefined')){
} else {
app.core.med_preferences = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
}
if((typeof app !== 'undefined') && (typeof app.core !== 'undefined') && (typeof app.core.schedule_state !== 'undefined')){
} else {
app.core.schedule_state = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
}
cljs.core.add_watch(app.core.patient_state,new cljs.core.Keyword(null,"watcher","watcher",2145165251),(function (k,a,o,n){
return cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["patient-state changed",n], 0));
}));
cljs.core.add_watch(app.core.time_preferences,new cljs.core.Keyword(null,"watcher","watcher",2145165251),(function (k,a,o,n){
return cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["time-prefs changed",n], 0));
}));
cljs.core.add_watch(app.core.med_preferences,new cljs.core.Keyword(null,"watcher","watcher",2145165251),(function (k,a,o,n){
return cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["med-preferences changed",n], 0));
}));
app.core.update_interaction = (function app$core$update_interaction(med_id,previous_interaction_key,p__70586){
var map__70587 = p__70586;
var map__70587__$1 = (((((!((map__70587 == null))))?(((((map__70587.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__70587.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__70587):map__70587);
var interaction = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__70587__$1,new cljs.core.Keyword(null,"interaction","interaction",-2143888916));
var hours = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__70587__$1,new cljs.core.Keyword(null,"hours","hours",58380855));
cljs.core.reset_BANG_(app.core.med_preferences,cljs.core.assoc_in(cljs.core.deref(app.core.med_preferences),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [med_id,new cljs.core.Keyword(null,"interactions","interactions",550841811),interaction], null),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"period","period",-352129191),hours,new cljs.core.Keyword(null,"periodUnit","periodUnit",1075156777),new cljs.core.Keyword(null,"hours","hours",58380855)], null)));

if(cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2(previous_interaction_key,interaction)){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["removing",previous_interaction_key], 0));

return cljs.core.reset_BANG_(app.core.med_preferences,app.core.dissoc_in(cljs.core.deref(app.core.med_preferences),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [med_id,new cljs.core.Keyword(null,"interactions","interactions",550841811),previous_interaction_key], null)));
} else {
return null;
}
});
app.core.remove_interaction = (function app$core$remove_interaction(med_id,interaction_key){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["removing ",interaction_key," from ",med_id], 0));

return cljs.core.reset_BANG_(app.core.med_preferences,app.core.dissoc_in(cljs.core.deref(app.core.med_preferences),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [med_id,new cljs.core.Keyword(null,"interactions","interactions",550841811),cljs.core.keyword.cljs$core$IFn$_invoke$arity$1(interaction_key)], null)));
});
app.core.navigation = new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"patient","patient",1178852672),new cljs.core.Keyword(null,"display","display",242065432),"Patient",new cljs.core.Keyword(null,"link","link",-1769163468),"patient"], null),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"schedule","schedule",349275266),new cljs.core.Keyword(null,"display","display",242065432),"Schedule",new cljs.core.Keyword(null,"link","link",-1769163468),"schedule"], null),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"about","about",1423892543),new cljs.core.Keyword(null,"display","display",242065432),"About",new cljs.core.Keyword(null,"link","link",-1769163468),"about"], null)], null);
app.core.router = bidi.router.start_router_BANG_(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, ["",new cljs.core.PersistentArrayMap(null, 3, ["home",new cljs.core.Keyword(null,"home","home",-74557309),"about",new cljs.core.Keyword(null,"about","about",1423892543),"schedule",new cljs.core.Keyword(null,"schedule","schedule",349275266)], null)], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"on-navigate","on-navigate",-297227908),(function (p1__70589_SHARP_){
return cljs.core.reset_BANG_(app.core.location,(function (){var or__4126__auto__ = p1__70589_SHARP_;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"handler","handler",-195596612),new cljs.core.Keyword(null,"patient","patient",1178852672)], null);
}
})());
})], null));
app.core.nav_links = (function app$core$nav_links(l){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,".navigation",".navigation",-597252744),(function (){var iter__4529__auto__ = (function app$core$nav_links_$_iter__70590(s__70591){
return (new cljs.core.LazySeq(null,(function (){
var s__70591__$1 = s__70591;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__70591__$1);
if(temp__5735__auto__){
var s__70591__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__70591__$2)){
var c__4527__auto__ = cljs.core.chunk_first(s__70591__$2);
var size__4528__auto__ = cljs.core.count(c__4527__auto__);
var b__70593 = cljs.core.chunk_buffer(size__4528__auto__);
if((function (){var i__70592 = (0);
while(true){
if((i__70592 < size__4528__auto__)){
var map__70594 = cljs.core._nth(c__4527__auto__,i__70592);
var map__70594__$1 = (((((!((map__70594 == null))))?(((((map__70594.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__70594.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__70594):map__70594);
var n = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__70594__$1,new cljs.core.Keyword(null,"name","name",1843675177));
var d = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__70594__$1,new cljs.core.Keyword(null,"display","display",242065432));
var link = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__70594__$1,new cljs.core.Keyword(null,"link","link",-1769163468));
cljs.core.chunk_append(b__70593,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"href","href",-793805698),["/#",cljs.core.str.cljs$core$IFn$_invoke$arity$1(link)].join(''),new cljs.core.Keyword(null,"class-name","class-name",945142584),((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(n,l))?"active":"")], null),d], null));

var G__70691 = (i__70592 + (1));
i__70592 = G__70691;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__70593),app$core$nav_links_$_iter__70590(cljs.core.chunk_rest(s__70591__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__70593),null);
}
} else {
var map__70596 = cljs.core.first(s__70591__$2);
var map__70596__$1 = (((((!((map__70596 == null))))?(((((map__70596.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__70596.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__70596):map__70596);
var n = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__70596__$1,new cljs.core.Keyword(null,"name","name",1843675177));
var d = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__70596__$1,new cljs.core.Keyword(null,"display","display",242065432));
var link = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__70596__$1,new cljs.core.Keyword(null,"link","link",-1769163468));
return cljs.core.cons(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"href","href",-793805698),["/#",cljs.core.str.cljs$core$IFn$_invoke$arity$1(link)].join(''),new cljs.core.Keyword(null,"class-name","class-name",945142584),((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(n,l))?"active":"")], null),d], null),app$core$nav_links_$_iter__70590(cljs.core.rest(s__70591__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4529__auto__(app.core.navigation);
})()], null);
});
app.core.pref_time_input = rum.core.lazy_build(rum.core.build_defc,(function (label,k){
var ref = rum.core.create_ref();
var w = (function (){var G__70599 = rum.core.react(app.core.time_preferences);
return (k.cljs$core$IFn$_invoke$arity$1 ? k.cljs$core$IFn$_invoke$arity$1(G__70599) : k.call(null,G__70599));
})();
var attrs70598 = label;
return daiquiri.core.create_element("label",((cljs.core.map_QMARK_(attrs70598))?daiquiri.interpreter.attributes(attrs70598):null),((cljs.core.map_QMARK_(attrs70598))?[daiquiri.core.create_element("input",{'ref':ref,'value':(function (){var or__4126__auto__ = w;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return "";
}
})(),'onChange':(function (){
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(app.core.time_preferences,cljs.core.assoc,k,rum.core.deref(ref).value);
})},[])]:[daiquiri.interpreter.interpret(attrs70598),daiquiri.core.create_element("input",{'ref':ref,'value':(function (){var or__4126__auto__ = w;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return "";
}
})(),'onChange':(function (){
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(app.core.time_preferences,cljs.core.assoc,k,rum.core.deref(ref).value);
})},[])]));
}),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [rum.core.reactive], null),"app.core/pref-time-input");
app.core.patient_view = (function app$core$patient_view(patient_state,med_state){
return new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h1","h1",-1896887462),"Patient Preferences"], null),new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.patient-info","div.patient-info",450063994),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h3.title.first","h3.title.first",1660251878),"User Information"], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.patient-name","div.patient-name",-75447899),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.bold","span.bold",636457745),"Name: "], null),new cljs.core.Keyword(null,"fullName","fullName",-202600044).cljs$core$IFn$_invoke$arity$1(patient_state)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.dob","div.dob",1635903363),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.bold","span.bold",636457745),"DOB: "], null),app.core.display_date(new cljs.core.Keyword(null,"birthDate","birthDate",-495062262).cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"patient","patient",1178852672).cljs$core$IFn$_invoke$arity$1(patient_state)))], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"button.save-button","button.save-button",-1282459249),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"on-click","on-click",1632826543),(function (){
return ajax.core.POST.cljs$core$IFn$_invoke$arity$variadic((function (){var G__70600 = app.core.patient_identifier(patient_state);
return (app.core.save_pref_url.cljs$core$IFn$_invoke$arity$1 ? app.core.save_pref_url.cljs$core$IFn$_invoke$arity$1(G__70600) : app.core.save_pref_url.call(null,G__70600));
})(),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"body","body",-2049205669),app.core.prep_body(cljs.core.deref(app.core.time_preferences),cljs.core.deref(app.core.med_preferences)),new cljs.core.Keyword(null,"response-format","response-format",1664465322),new cljs.core.Keyword(null,"json","json",1279968570)], null)], 0));
})], null),"Save All Preferences"], null)], null),new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.patient-preferences","div.patient-preferences",-807807182),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h3.title","h3.title",1837656649),"Time Preferences"], null),(function (){var iter__4529__auto__ = (function app$core$patient_view_$_iter__70601(s__70602){
return (new cljs.core.LazySeq(null,(function (){
var s__70602__$1 = s__70602;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__70602__$1);
if(temp__5735__auto__){
var s__70602__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__70602__$2)){
var c__4527__auto__ = cljs.core.chunk_first(s__70602__$2);
var size__4528__auto__ = cljs.core.count(c__4527__auto__);
var b__70604 = cljs.core.chunk_buffer(size__4528__auto__);
if((function (){var i__70603 = (0);
while(true){
if((i__70603 < size__4528__auto__)){
var t = cljs.core._nth(c__4527__auto__,i__70603);
cljs.core.chunk_append(b__70604,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),[cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(t)),"-div"].join('')], null),app.core.pref_time_input(new cljs.core.Keyword(null,"label","label",1718410804).cljs$core$IFn$_invoke$arity$1(t),new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(t))], null));

var G__70692 = (i__70603 + (1));
i__70603 = G__70692;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__70604),app$core$patient_view_$_iter__70601(cljs.core.chunk_rest(s__70602__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__70604),null);
}
} else {
var t = cljs.core.first(s__70602__$2);
return cljs.core.cons(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),[cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(t)),"-div"].join('')], null),app.core.pref_time_input(new cljs.core.Keyword(null,"label","label",1718410804).cljs$core$IFn$_invoke$arity$1(t),new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(t))], null),app$core$patient_view_$_iter__70601(cljs.core.rest(s__70602__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4529__auto__(app.core.all_times);
})(),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h3.title","h3.title",1837656649),"Medications"], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"table","table",-564943036),new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"thead","thead",-291875296),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td","td",1479933353),"Status"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td","td",1479933353),"Medication"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td","td",1479933353),"interactions"], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"tbody","tbody",-80678300),(function (){var iter__4529__auto__ = (function app$core$patient_view_$_iter__70605(s__70606){
return (new cljs.core.LazySeq(null,(function (){
var s__70606__$1 = s__70606;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__70606__$1);
if(temp__5735__auto__){
var s__70606__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__70606__$2)){
var c__4527__auto__ = cljs.core.chunk_first(s__70606__$2);
var size__4528__auto__ = cljs.core.count(c__4527__auto__);
var b__70608 = cljs.core.chunk_buffer(size__4528__auto__);
if((function (){var i__70607 = (0);
while(true){
if((i__70607 < size__4528__auto__)){
var vec__70609 = cljs.core._nth(c__4527__auto__,i__70607);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__70609,(0),null);
var m = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__70609,(1),null);
cljs.core.chunk_append(b__70608,new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"tr","tr",-1424774646),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),["med-",cljs.core.str.cljs$core$IFn$_invoke$arity$1(k)].join('')], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td.status","td.status",-1528661831),new cljs.core.Keyword(null,"status","status",-1997798413).cljs$core$IFn$_invoke$arity$1(m)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td.med-name","td.med-name",1715582248),new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td.interactions","td.interactions",729745048),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.interaction-button.plus","span.interaction-button.plus",-1701939220),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"on-click","on-click",1632826543),((function (i__70607,vec__70609,k,m,c__4527__auto__,size__4528__auto__,b__70608,s__70606__$2,temp__5735__auto__){
return (function (){
return app.core.update_interaction(k,null,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"interaction","interaction",-2143888916),"",new cljs.core.Keyword(null,"hours","hours",58380855),(0)], null));
});})(i__70607,vec__70609,k,m,c__4527__auto__,size__4528__auto__,b__70608,s__70606__$2,temp__5735__auto__))
], null),"+"], null),(function (){var iter__4529__auto__ = ((function (i__70607,vec__70609,k,m,c__4527__auto__,size__4528__auto__,b__70608,s__70606__$2,temp__5735__auto__){
return (function app$core$patient_view_$_iter__70605_$_iter__70612(s__70613){
return (new cljs.core.LazySeq(null,((function (i__70607,vec__70609,k,m,c__4527__auto__,size__4528__auto__,b__70608,s__70606__$2,temp__5735__auto__){
return (function (){
var s__70613__$1 = s__70613;
while(true){
var temp__5735__auto____$1 = cljs.core.seq(s__70613__$1);
if(temp__5735__auto____$1){
var s__70613__$2 = temp__5735__auto____$1;
if(cljs.core.chunked_seq_QMARK_(s__70613__$2)){
var c__4527__auto____$1 = cljs.core.chunk_first(s__70613__$2);
var size__4528__auto____$1 = cljs.core.count(c__4527__auto____$1);
var b__70615 = cljs.core.chunk_buffer(size__4528__auto____$1);
if((function (){var i__70614 = (0);
while(true){
if((i__70614 < size__4528__auto____$1)){
var vec__70616 = cljs.core._nth(c__4527__auto____$1,i__70614);
var ik = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__70616,(0),null);
var ii = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__70616,(1),null);
cljs.core.chunk_append(b__70615,(function (){var ref_div = rum.core.create_ref();
var ref_select = rum.core.create_ref();
var ref_input = rum.core.create_ref();
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"ref","ref",1289896967),ref_div], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.interaction-button.minus","span.interaction-button.minus",-285815000),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"on-click","on-click",1632826543),((function (i__70614,i__70607,ref_div,ref_select,ref_input,vec__70616,ik,ii,c__4527__auto____$1,size__4528__auto____$1,b__70615,s__70613__$2,temp__5735__auto____$1,vec__70609,k,m,c__4527__auto__,size__4528__auto__,b__70608,s__70606__$2,temp__5735__auto__){
return (function (){
return app.core.remove_interaction(k,rum.core.deref(ref_select).value);
});})(i__70614,i__70607,ref_div,ref_select,ref_input,vec__70616,ik,ii,c__4527__auto____$1,size__4528__auto____$1,b__70615,s__70613__$2,temp__5735__auto____$1,vec__70609,k,m,c__4527__auto__,size__4528__auto__,b__70608,s__70606__$2,temp__5735__auto__))
], null),"-"], null),new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"select","select",1147833503),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"ref","ref",1289896967),ref_select,new cljs.core.Keyword(null,"value","value",305978217),(function (){var or__4126__auto__ = ik;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return "";
}
})(),new cljs.core.Keyword(null,"on-change","on-change",-732046149),((function (i__70614,i__70607,ref_div,ref_select,ref_input,vec__70616,ik,ii,c__4527__auto____$1,size__4528__auto____$1,b__70615,s__70613__$2,temp__5735__auto____$1,vec__70609,k,m,c__4527__auto__,size__4528__auto__,b__70608,s__70606__$2,temp__5735__auto__){
return (function (){
return app.core.update_interaction(k,ik,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"interaction","interaction",-2143888916),rum.core.deref(ref_select).value,new cljs.core.Keyword(null,"hours","hours",58380855),rum.core.deref(ref_input).value], null));
});})(i__70614,i__70607,ref_div,ref_select,ref_input,vec__70616,ik,ii,c__4527__auto____$1,size__4528__auto____$1,b__70615,s__70613__$2,temp__5735__auto____$1,vec__70609,k,m,c__4527__auto__,size__4528__auto__,b__70608,s__70606__$2,temp__5735__auto__))
], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"option","option",65132272),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"value","value",305978217),""], null),"select"], null),cljs.core.map_indexed.cljs$core$IFn$_invoke$arity$2(((function (i__70614,i__70607,ref_div,ref_select,ref_input,vec__70616,ik,ii,c__4527__auto____$1,size__4528__auto____$1,b__70615,s__70613__$2,temp__5735__auto____$1,vec__70609,k,m,c__4527__auto__,size__4528__auto__,b__70608,s__70606__$2,temp__5735__auto__){
return (function (idx,p__70619){
var vec__70620 = p__70619;
var km = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__70620,(0),null);
var mm = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__70620,(1),null);
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"option","option",65132272),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"key","key",-1516042587),[cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m)),"-",cljs.core.str.cljs$core$IFn$_invoke$arity$1(idx),"-",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(mm))].join(''),new cljs.core.Keyword(null,"value","value",305978217),km], null),new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(mm)], null);
});})(i__70614,i__70607,ref_div,ref_select,ref_input,vec__70616,ik,ii,c__4527__auto____$1,size__4528__auto____$1,b__70615,s__70613__$2,temp__5735__auto____$1,vec__70609,k,m,c__4527__auto__,size__4528__auto__,b__70608,s__70606__$2,temp__5735__auto__))
,med_state)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"ref","ref",1289896967),ref_input,new cljs.core.Keyword(null,"value","value",305978217),cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(med_state,new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [k,new cljs.core.Keyword(null,"interactions","interactions",550841811),ik,new cljs.core.Keyword(null,"period","period",-352129191)], null)),new cljs.core.Keyword(null,"on-change","on-change",-732046149),((function (i__70614,i__70607,ref_div,ref_select,ref_input,vec__70616,ik,ii,c__4527__auto____$1,size__4528__auto____$1,b__70615,s__70613__$2,temp__5735__auto____$1,vec__70609,k,m,c__4527__auto__,size__4528__auto__,b__70608,s__70606__$2,temp__5735__auto__){
return (function (){
return app.core.update_interaction(k,ik,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"interaction","interaction",-2143888916),rum.core.deref(ref_select).value,new cljs.core.Keyword(null,"hours","hours",58380855),rum.core.deref(ref_input).value], null));
});})(i__70614,i__70607,ref_div,ref_select,ref_input,vec__70616,ik,ii,c__4527__auto____$1,size__4528__auto____$1,b__70615,s__70613__$2,temp__5735__auto____$1,vec__70609,k,m,c__4527__auto__,size__4528__auto__,b__70608,s__70606__$2,temp__5735__auto__))
], null)], null),"hours"], null);
})());

var G__70693 = (i__70614 + (1));
i__70614 = G__70693;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__70615),app$core$patient_view_$_iter__70605_$_iter__70612(cljs.core.chunk_rest(s__70613__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__70615),null);
}
} else {
var vec__70623 = cljs.core.first(s__70613__$2);
var ik = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__70623,(0),null);
var ii = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__70623,(1),null);
return cljs.core.cons((function (){var ref_div = rum.core.create_ref();
var ref_select = rum.core.create_ref();
var ref_input = rum.core.create_ref();
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"ref","ref",1289896967),ref_div], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.interaction-button.minus","span.interaction-button.minus",-285815000),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"on-click","on-click",1632826543),((function (i__70607,ref_div,ref_select,ref_input,vec__70623,ik,ii,s__70613__$2,temp__5735__auto____$1,vec__70609,k,m,c__4527__auto__,size__4528__auto__,b__70608,s__70606__$2,temp__5735__auto__){
return (function (){
return app.core.remove_interaction(k,rum.core.deref(ref_select).value);
});})(i__70607,ref_div,ref_select,ref_input,vec__70623,ik,ii,s__70613__$2,temp__5735__auto____$1,vec__70609,k,m,c__4527__auto__,size__4528__auto__,b__70608,s__70606__$2,temp__5735__auto__))
], null),"-"], null),new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"select","select",1147833503),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"ref","ref",1289896967),ref_select,new cljs.core.Keyword(null,"value","value",305978217),(function (){var or__4126__auto__ = ik;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return "";
}
})(),new cljs.core.Keyword(null,"on-change","on-change",-732046149),((function (i__70607,ref_div,ref_select,ref_input,vec__70623,ik,ii,s__70613__$2,temp__5735__auto____$1,vec__70609,k,m,c__4527__auto__,size__4528__auto__,b__70608,s__70606__$2,temp__5735__auto__){
return (function (){
return app.core.update_interaction(k,ik,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"interaction","interaction",-2143888916),rum.core.deref(ref_select).value,new cljs.core.Keyword(null,"hours","hours",58380855),rum.core.deref(ref_input).value], null));
});})(i__70607,ref_div,ref_select,ref_input,vec__70623,ik,ii,s__70613__$2,temp__5735__auto____$1,vec__70609,k,m,c__4527__auto__,size__4528__auto__,b__70608,s__70606__$2,temp__5735__auto__))
], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"option","option",65132272),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"value","value",305978217),""], null),"select"], null),cljs.core.map_indexed.cljs$core$IFn$_invoke$arity$2(((function (i__70607,ref_div,ref_select,ref_input,vec__70623,ik,ii,s__70613__$2,temp__5735__auto____$1,vec__70609,k,m,c__4527__auto__,size__4528__auto__,b__70608,s__70606__$2,temp__5735__auto__){
return (function (idx,p__70626){
var vec__70627 = p__70626;
var km = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__70627,(0),null);
var mm = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__70627,(1),null);
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"option","option",65132272),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"key","key",-1516042587),[cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m)),"-",cljs.core.str.cljs$core$IFn$_invoke$arity$1(idx),"-",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(mm))].join(''),new cljs.core.Keyword(null,"value","value",305978217),km], null),new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(mm)], null);
});})(i__70607,ref_div,ref_select,ref_input,vec__70623,ik,ii,s__70613__$2,temp__5735__auto____$1,vec__70609,k,m,c__4527__auto__,size__4528__auto__,b__70608,s__70606__$2,temp__5735__auto__))
,med_state)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"ref","ref",1289896967),ref_input,new cljs.core.Keyword(null,"value","value",305978217),cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(med_state,new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [k,new cljs.core.Keyword(null,"interactions","interactions",550841811),ik,new cljs.core.Keyword(null,"period","period",-352129191)], null)),new cljs.core.Keyword(null,"on-change","on-change",-732046149),((function (i__70607,ref_div,ref_select,ref_input,vec__70623,ik,ii,s__70613__$2,temp__5735__auto____$1,vec__70609,k,m,c__4527__auto__,size__4528__auto__,b__70608,s__70606__$2,temp__5735__auto__){
return (function (){
return app.core.update_interaction(k,ik,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"interaction","interaction",-2143888916),rum.core.deref(ref_select).value,new cljs.core.Keyword(null,"hours","hours",58380855),rum.core.deref(ref_input).value], null));
});})(i__70607,ref_div,ref_select,ref_input,vec__70623,ik,ii,s__70613__$2,temp__5735__auto____$1,vec__70609,k,m,c__4527__auto__,size__4528__auto__,b__70608,s__70606__$2,temp__5735__auto__))
], null)], null),"hours"], null);
})(),app$core$patient_view_$_iter__70605_$_iter__70612(cljs.core.rest(s__70613__$2)));
}
} else {
return null;
}
break;
}
});})(i__70607,vec__70609,k,m,c__4527__auto__,size__4528__auto__,b__70608,s__70606__$2,temp__5735__auto__))
,null,null));
});})(i__70607,vec__70609,k,m,c__4527__auto__,size__4528__auto__,b__70608,s__70606__$2,temp__5735__auto__))
;
return iter__4529__auto__(new cljs.core.Keyword(null,"interactions","interactions",550841811).cljs$core$IFn$_invoke$arity$1(m));
})()], null)], null));

var G__70694 = (i__70607 + (1));
i__70607 = G__70694;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__70608),app$core$patient_view_$_iter__70605(cljs.core.chunk_rest(s__70606__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__70608),null);
}
} else {
var vec__70630 = cljs.core.first(s__70606__$2);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__70630,(0),null);
var m = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__70630,(1),null);
return cljs.core.cons(new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"tr","tr",-1424774646),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),["med-",cljs.core.str.cljs$core$IFn$_invoke$arity$1(k)].join('')], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td.status","td.status",-1528661831),new cljs.core.Keyword(null,"status","status",-1997798413).cljs$core$IFn$_invoke$arity$1(m)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td.med-name","td.med-name",1715582248),new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td.interactions","td.interactions",729745048),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.interaction-button.plus","span.interaction-button.plus",-1701939220),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"on-click","on-click",1632826543),((function (vec__70630,k,m,s__70606__$2,temp__5735__auto__){
return (function (){
return app.core.update_interaction(k,null,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"interaction","interaction",-2143888916),"",new cljs.core.Keyword(null,"hours","hours",58380855),(0)], null));
});})(vec__70630,k,m,s__70606__$2,temp__5735__auto__))
], null),"+"], null),(function (){var iter__4529__auto__ = ((function (vec__70630,k,m,s__70606__$2,temp__5735__auto__){
return (function app$core$patient_view_$_iter__70605_$_iter__70633(s__70634){
return (new cljs.core.LazySeq(null,(function (){
var s__70634__$1 = s__70634;
while(true){
var temp__5735__auto____$1 = cljs.core.seq(s__70634__$1);
if(temp__5735__auto____$1){
var s__70634__$2 = temp__5735__auto____$1;
if(cljs.core.chunked_seq_QMARK_(s__70634__$2)){
var c__4527__auto__ = cljs.core.chunk_first(s__70634__$2);
var size__4528__auto__ = cljs.core.count(c__4527__auto__);
var b__70636 = cljs.core.chunk_buffer(size__4528__auto__);
if((function (){var i__70635 = (0);
while(true){
if((i__70635 < size__4528__auto__)){
var vec__70637 = cljs.core._nth(c__4527__auto__,i__70635);
var ik = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__70637,(0),null);
var ii = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__70637,(1),null);
cljs.core.chunk_append(b__70636,(function (){var ref_div = rum.core.create_ref();
var ref_select = rum.core.create_ref();
var ref_input = rum.core.create_ref();
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"ref","ref",1289896967),ref_div], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.interaction-button.minus","span.interaction-button.minus",-285815000),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"on-click","on-click",1632826543),((function (i__70635,ref_div,ref_select,ref_input,vec__70637,ik,ii,c__4527__auto__,size__4528__auto__,b__70636,s__70634__$2,temp__5735__auto____$1,vec__70630,k,m,s__70606__$2,temp__5735__auto__){
return (function (){
return app.core.remove_interaction(k,rum.core.deref(ref_select).value);
});})(i__70635,ref_div,ref_select,ref_input,vec__70637,ik,ii,c__4527__auto__,size__4528__auto__,b__70636,s__70634__$2,temp__5735__auto____$1,vec__70630,k,m,s__70606__$2,temp__5735__auto__))
], null),"-"], null),new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"select","select",1147833503),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"ref","ref",1289896967),ref_select,new cljs.core.Keyword(null,"value","value",305978217),(function (){var or__4126__auto__ = ik;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return "";
}
})(),new cljs.core.Keyword(null,"on-change","on-change",-732046149),((function (i__70635,ref_div,ref_select,ref_input,vec__70637,ik,ii,c__4527__auto__,size__4528__auto__,b__70636,s__70634__$2,temp__5735__auto____$1,vec__70630,k,m,s__70606__$2,temp__5735__auto__){
return (function (){
return app.core.update_interaction(k,ik,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"interaction","interaction",-2143888916),rum.core.deref(ref_select).value,new cljs.core.Keyword(null,"hours","hours",58380855),rum.core.deref(ref_input).value], null));
});})(i__70635,ref_div,ref_select,ref_input,vec__70637,ik,ii,c__4527__auto__,size__4528__auto__,b__70636,s__70634__$2,temp__5735__auto____$1,vec__70630,k,m,s__70606__$2,temp__5735__auto__))
], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"option","option",65132272),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"value","value",305978217),""], null),"select"], null),cljs.core.map_indexed.cljs$core$IFn$_invoke$arity$2(((function (i__70635,ref_div,ref_select,ref_input,vec__70637,ik,ii,c__4527__auto__,size__4528__auto__,b__70636,s__70634__$2,temp__5735__auto____$1,vec__70630,k,m,s__70606__$2,temp__5735__auto__){
return (function (idx,p__70640){
var vec__70641 = p__70640;
var km = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__70641,(0),null);
var mm = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__70641,(1),null);
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"option","option",65132272),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"key","key",-1516042587),[cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m)),"-",cljs.core.str.cljs$core$IFn$_invoke$arity$1(idx),"-",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(mm))].join(''),new cljs.core.Keyword(null,"value","value",305978217),km], null),new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(mm)], null);
});})(i__70635,ref_div,ref_select,ref_input,vec__70637,ik,ii,c__4527__auto__,size__4528__auto__,b__70636,s__70634__$2,temp__5735__auto____$1,vec__70630,k,m,s__70606__$2,temp__5735__auto__))
,med_state)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"ref","ref",1289896967),ref_input,new cljs.core.Keyword(null,"value","value",305978217),cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(med_state,new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [k,new cljs.core.Keyword(null,"interactions","interactions",550841811),ik,new cljs.core.Keyword(null,"period","period",-352129191)], null)),new cljs.core.Keyword(null,"on-change","on-change",-732046149),((function (i__70635,ref_div,ref_select,ref_input,vec__70637,ik,ii,c__4527__auto__,size__4528__auto__,b__70636,s__70634__$2,temp__5735__auto____$1,vec__70630,k,m,s__70606__$2,temp__5735__auto__){
return (function (){
return app.core.update_interaction(k,ik,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"interaction","interaction",-2143888916),rum.core.deref(ref_select).value,new cljs.core.Keyword(null,"hours","hours",58380855),rum.core.deref(ref_input).value], null));
});})(i__70635,ref_div,ref_select,ref_input,vec__70637,ik,ii,c__4527__auto__,size__4528__auto__,b__70636,s__70634__$2,temp__5735__auto____$1,vec__70630,k,m,s__70606__$2,temp__5735__auto__))
], null)], null),"hours"], null);
})());

var G__70695 = (i__70635 + (1));
i__70635 = G__70695;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__70636),app$core$patient_view_$_iter__70605_$_iter__70633(cljs.core.chunk_rest(s__70634__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__70636),null);
}
} else {
var vec__70644 = cljs.core.first(s__70634__$2);
var ik = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__70644,(0),null);
var ii = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__70644,(1),null);
return cljs.core.cons((function (){var ref_div = rum.core.create_ref();
var ref_select = rum.core.create_ref();
var ref_input = rum.core.create_ref();
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"ref","ref",1289896967),ref_div], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.interaction-button.minus","span.interaction-button.minus",-285815000),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"on-click","on-click",1632826543),((function (ref_div,ref_select,ref_input,vec__70644,ik,ii,s__70634__$2,temp__5735__auto____$1,vec__70630,k,m,s__70606__$2,temp__5735__auto__){
return (function (){
return app.core.remove_interaction(k,rum.core.deref(ref_select).value);
});})(ref_div,ref_select,ref_input,vec__70644,ik,ii,s__70634__$2,temp__5735__auto____$1,vec__70630,k,m,s__70606__$2,temp__5735__auto__))
], null),"-"], null),new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"select","select",1147833503),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"ref","ref",1289896967),ref_select,new cljs.core.Keyword(null,"value","value",305978217),(function (){var or__4126__auto__ = ik;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return "";
}
})(),new cljs.core.Keyword(null,"on-change","on-change",-732046149),((function (ref_div,ref_select,ref_input,vec__70644,ik,ii,s__70634__$2,temp__5735__auto____$1,vec__70630,k,m,s__70606__$2,temp__5735__auto__){
return (function (){
return app.core.update_interaction(k,ik,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"interaction","interaction",-2143888916),rum.core.deref(ref_select).value,new cljs.core.Keyword(null,"hours","hours",58380855),rum.core.deref(ref_input).value], null));
});})(ref_div,ref_select,ref_input,vec__70644,ik,ii,s__70634__$2,temp__5735__auto____$1,vec__70630,k,m,s__70606__$2,temp__5735__auto__))
], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"option","option",65132272),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"value","value",305978217),""], null),"select"], null),cljs.core.map_indexed.cljs$core$IFn$_invoke$arity$2(((function (ref_div,ref_select,ref_input,vec__70644,ik,ii,s__70634__$2,temp__5735__auto____$1,vec__70630,k,m,s__70606__$2,temp__5735__auto__){
return (function (idx,p__70647){
var vec__70648 = p__70647;
var km = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__70648,(0),null);
var mm = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__70648,(1),null);
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"option","option",65132272),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"key","key",-1516042587),[cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m)),"-",cljs.core.str.cljs$core$IFn$_invoke$arity$1(idx),"-",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(mm))].join(''),new cljs.core.Keyword(null,"value","value",305978217),km], null),new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(mm)], null);
});})(ref_div,ref_select,ref_input,vec__70644,ik,ii,s__70634__$2,temp__5735__auto____$1,vec__70630,k,m,s__70606__$2,temp__5735__auto__))
,med_state)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"ref","ref",1289896967),ref_input,new cljs.core.Keyword(null,"value","value",305978217),cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(med_state,new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [k,new cljs.core.Keyword(null,"interactions","interactions",550841811),ik,new cljs.core.Keyword(null,"period","period",-352129191)], null)),new cljs.core.Keyword(null,"on-change","on-change",-732046149),((function (ref_div,ref_select,ref_input,vec__70644,ik,ii,s__70634__$2,temp__5735__auto____$1,vec__70630,k,m,s__70606__$2,temp__5735__auto__){
return (function (){
return app.core.update_interaction(k,ik,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"interaction","interaction",-2143888916),rum.core.deref(ref_select).value,new cljs.core.Keyword(null,"hours","hours",58380855),rum.core.deref(ref_input).value], null));
});})(ref_div,ref_select,ref_input,vec__70644,ik,ii,s__70634__$2,temp__5735__auto____$1,vec__70630,k,m,s__70606__$2,temp__5735__auto__))
], null)], null),"hours"], null);
})(),app$core$patient_view_$_iter__70605_$_iter__70633(cljs.core.rest(s__70634__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});})(vec__70630,k,m,s__70606__$2,temp__5735__auto__))
;
return iter__4529__auto__(new cljs.core.Keyword(null,"interactions","interactions",550841811).cljs$core$IFn$_invoke$arity$1(m));
})()], null)], null),app$core$patient_view_$_iter__70605(cljs.core.rest(s__70606__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4529__auto__(med_state);
})()], null)], null)], null)], null)], null);
});
app.core.about_view = (function app$core$about_view(patient_state){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),"This is the individual project for my Health Informatics class and is not inteded for production use."], null);
});
app.core.schedule_view = (function app$core$schedule_view(time_prefs,med_state){
var event_times = cljs.core.map.cljs$core$IFn$_invoke$arity$2(app.calculate_schedule.time__GT_keyword,cljs.core.map.cljs$core$IFn$_invoke$arity$2(tick.alpha.api.time,cljs.core.map.cljs$core$IFn$_invoke$arity$2(cljs.core.val,time_prefs)));
var time_preferences_full = cljs.core.map_indexed.cljs$core$IFn$_invoke$arity$2((function (i,v){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(v,new cljs.core.Keyword(null,"position","position",-2011731912),i,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"default","default",-1987822328),cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(i,(1))], 0));
}),cljs.core.sort_by.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"time","time",1385887882),cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p__70652){
var vec__70653 = p__70652;
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__70653,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__70653,(1),null);
return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"key","key",-1516042587),k,new cljs.core.Keyword(null,"time","time",1385887882),tick.alpha.api.time.cljs$core$IFn$_invoke$arity$1(v)], null);
}),time_prefs)));
var map__70651 = app.calculate_schedule.calculate(new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"meds","meds",-2036408830),med_state], null),time_preferences_full);
var map__70651__$1 = (((((!((map__70651 == null))))?(((((map__70651.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__70651.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__70651):map__70651);
var calculated_sched = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__70651__$1,new cljs.core.Keyword(null,"schedule","schedule",349275266));
var calculated_meta = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__70651__$1,new cljs.core.Keyword(null,"meta","meta",1499536964));
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"***-meta","***-meta",-866936791),calculated_meta,new cljs.core.Keyword(null,"errors","errors",-908790718).cljs$core$IFn$_invoke$arity$1(calculated_meta)], 0));

return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h1","h1",-1896887462),"Daily Schedule"], null),(cljs.core.truth_(new cljs.core.Keyword(null,"errors","errors",-908790718).cljs$core$IFn$_invoke$arity$1(calculated_meta))?new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.errors","div.errors",-955299553),(function (){var iter__4529__auto__ = (function app$core$schedule_view_$_iter__70657(s__70658){
return (new cljs.core.LazySeq(null,(function (){
var s__70658__$1 = s__70658;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__70658__$1);
if(temp__5735__auto__){
var s__70658__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__70658__$2)){
var c__4527__auto__ = cljs.core.chunk_first(s__70658__$2);
var size__4528__auto__ = cljs.core.count(c__4527__auto__);
var b__70660 = cljs.core.chunk_buffer(size__4528__auto__);
if((function (){var i__70659 = (0);
while(true){
if((i__70659 < size__4528__auto__)){
var vec__70661 = cljs.core._nth(c__4527__auto__,i__70659);
var ek = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__70661,(0),null);
var ee = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__70661,(1),null);
cljs.core.chunk_append(b__70660,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.error","div.error",314336058),new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1((ek.cljs$core$IFn$_invoke$arity$1 ? ek.cljs$core$IFn$_invoke$arity$1(med_state) : ek.call(null,med_state)))," could not be added due to an interaction."], null));

var G__70696 = (i__70659 + (1));
i__70659 = G__70696;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__70660),app$core$schedule_view_$_iter__70657(cljs.core.chunk_rest(s__70658__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__70660),null);
}
} else {
var vec__70664 = cljs.core.first(s__70658__$2);
var ek = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__70664,(0),null);
var ee = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__70664,(1),null);
return cljs.core.cons(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.error","div.error",314336058),new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1((ek.cljs$core$IFn$_invoke$arity$1 ? ek.cljs$core$IFn$_invoke$arity$1(med_state) : ek.call(null,med_state)))," could not be added due to an interaction."], null),app$core$schedule_view_$_iter__70657(cljs.core.rest(s__70658__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4529__auto__(new cljs.core.Keyword(null,"errors","errors",-908790718).cljs$core$IFn$_invoke$arity$1(calculated_meta));
})()], null):null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.times","div.times",-2139523553),(function (){var iter__4529__auto__ = (function app$core$schedule_view_$_iter__70667(s__70668){
return (new cljs.core.LazySeq(null,(function (){
var s__70668__$1 = s__70668;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__70668__$1);
if(temp__5735__auto__){
var s__70668__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__70668__$2)){
var c__4527__auto__ = cljs.core.chunk_first(s__70668__$2);
var size__4528__auto__ = cljs.core.count(c__4527__auto__);
var b__70670 = cljs.core.chunk_buffer(size__4528__auto__);
if((function (){var i__70669 = (0);
while(true){
if((i__70669 < size__4528__auto__)){
var kt = cljs.core._nth(c__4527__auto__,i__70669);
cljs.core.chunk_append(b__70670,(function (){var class_n = "time";
var class_n__$1 = ((cljs.core.contains_QMARK_(cljs.core.set(event_times),kt))?[class_n," regular-event"].join(''):class_n);
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"key","key",-1516042587),["time-",cljs.core.str.cljs$core$IFn$_invoke$arity$1(kt)].join(''),new cljs.core.Keyword(null,"class-name","class-name",945142584),class_n__$1], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.time-display","span.time-display",549882097),cljs.core.str.cljs$core$IFn$_invoke$arity$1(app.calculate_schedule.keyword__GT_time(kt))], null),(function (){var iter__4529__auto__ = ((function (i__70669,class_n,class_n__$1,kt,c__4527__auto__,size__4528__auto__,b__70670,s__70668__$2,temp__5735__auto__,event_times,time_preferences_full,map__70651,map__70651__$1,calculated_sched,calculated_meta){
return (function app$core$schedule_view_$_iter__70667_$_iter__70671(s__70672){
return (new cljs.core.LazySeq(null,((function (i__70669,class_n,class_n__$1,kt,c__4527__auto__,size__4528__auto__,b__70670,s__70668__$2,temp__5735__auto__,event_times,time_preferences_full,map__70651,map__70651__$1,calculated_sched,calculated_meta){
return (function (){
var s__70672__$1 = s__70672;
while(true){
var temp__5735__auto____$1 = cljs.core.seq(s__70672__$1);
if(temp__5735__auto____$1){
var s__70672__$2 = temp__5735__auto____$1;
if(cljs.core.chunked_seq_QMARK_(s__70672__$2)){
var c__4527__auto____$1 = cljs.core.chunk_first(s__70672__$2);
var size__4528__auto____$1 = cljs.core.count(c__4527__auto____$1);
var b__70674 = cljs.core.chunk_buffer(size__4528__auto____$1);
if((function (){var i__70673 = (0);
while(true){
if((i__70673 < size__4528__auto____$1)){
var med_id = cljs.core._nth(c__4527__auto____$1,i__70673);
cljs.core.chunk_append(b__70674,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.medication-name","span.medication-name",1951106660),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),["med-display-",cljs.core.str.cljs$core$IFn$_invoke$arity$1(med_id)].join('')], null),new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1((med_id.cljs$core$IFn$_invoke$arity$1 ? med_id.cljs$core$IFn$_invoke$arity$1(med_state) : med_id.call(null,med_state)))], null));

var G__70697 = (i__70673 + (1));
i__70673 = G__70697;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__70674),app$core$schedule_view_$_iter__70667_$_iter__70671(cljs.core.chunk_rest(s__70672__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__70674),null);
}
} else {
var med_id = cljs.core.first(s__70672__$2);
return cljs.core.cons(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.medication-name","span.medication-name",1951106660),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),["med-display-",cljs.core.str.cljs$core$IFn$_invoke$arity$1(med_id)].join('')], null),new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1((med_id.cljs$core$IFn$_invoke$arity$1 ? med_id.cljs$core$IFn$_invoke$arity$1(med_state) : med_id.call(null,med_state)))], null),app$core$schedule_view_$_iter__70667_$_iter__70671(cljs.core.rest(s__70672__$2)));
}
} else {
return null;
}
break;
}
});})(i__70669,class_n,class_n__$1,kt,c__4527__auto__,size__4528__auto__,b__70670,s__70668__$2,temp__5735__auto__,event_times,time_preferences_full,map__70651,map__70651__$1,calculated_sched,calculated_meta))
,null,null));
});})(i__70669,class_n,class_n__$1,kt,c__4527__auto__,size__4528__auto__,b__70670,s__70668__$2,temp__5735__auto__,event_times,time_preferences_full,map__70651,map__70651__$1,calculated_sched,calculated_meta))
;
return iter__4529__auto__((kt.cljs$core$IFn$_invoke$arity$1 ? kt.cljs$core$IFn$_invoke$arity$1(calculated_sched) : kt.call(null,calculated_sched)));
})()], null);
})());

var G__70698 = (i__70669 + (1));
i__70669 = G__70698;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__70670),app$core$schedule_view_$_iter__70667(cljs.core.chunk_rest(s__70668__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__70670),null);
}
} else {
var kt = cljs.core.first(s__70668__$2);
return cljs.core.cons((function (){var class_n = "time";
var class_n__$1 = ((cljs.core.contains_QMARK_(cljs.core.set(event_times),kt))?[class_n," regular-event"].join(''):class_n);
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"key","key",-1516042587),["time-",cljs.core.str.cljs$core$IFn$_invoke$arity$1(kt)].join(''),new cljs.core.Keyword(null,"class-name","class-name",945142584),class_n__$1], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.time-display","span.time-display",549882097),cljs.core.str.cljs$core$IFn$_invoke$arity$1(app.calculate_schedule.keyword__GT_time(kt))], null),(function (){var iter__4529__auto__ = ((function (class_n,class_n__$1,kt,s__70668__$2,temp__5735__auto__,event_times,time_preferences_full,map__70651,map__70651__$1,calculated_sched,calculated_meta){
return (function app$core$schedule_view_$_iter__70667_$_iter__70675(s__70676){
return (new cljs.core.LazySeq(null,(function (){
var s__70676__$1 = s__70676;
while(true){
var temp__5735__auto____$1 = cljs.core.seq(s__70676__$1);
if(temp__5735__auto____$1){
var s__70676__$2 = temp__5735__auto____$1;
if(cljs.core.chunked_seq_QMARK_(s__70676__$2)){
var c__4527__auto__ = cljs.core.chunk_first(s__70676__$2);
var size__4528__auto__ = cljs.core.count(c__4527__auto__);
var b__70678 = cljs.core.chunk_buffer(size__4528__auto__);
if((function (){var i__70677 = (0);
while(true){
if((i__70677 < size__4528__auto__)){
var med_id = cljs.core._nth(c__4527__auto__,i__70677);
cljs.core.chunk_append(b__70678,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.medication-name","span.medication-name",1951106660),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),["med-display-",cljs.core.str.cljs$core$IFn$_invoke$arity$1(med_id)].join('')], null),new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1((med_id.cljs$core$IFn$_invoke$arity$1 ? med_id.cljs$core$IFn$_invoke$arity$1(med_state) : med_id.call(null,med_state)))], null));

var G__70699 = (i__70677 + (1));
i__70677 = G__70699;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__70678),app$core$schedule_view_$_iter__70667_$_iter__70675(cljs.core.chunk_rest(s__70676__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__70678),null);
}
} else {
var med_id = cljs.core.first(s__70676__$2);
return cljs.core.cons(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.medication-name","span.medication-name",1951106660),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),["med-display-",cljs.core.str.cljs$core$IFn$_invoke$arity$1(med_id)].join('')], null),new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1((med_id.cljs$core$IFn$_invoke$arity$1 ? med_id.cljs$core$IFn$_invoke$arity$1(med_state) : med_id.call(null,med_state)))], null),app$core$schedule_view_$_iter__70667_$_iter__70675(cljs.core.rest(s__70676__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});})(class_n,class_n__$1,kt,s__70668__$2,temp__5735__auto__,event_times,time_preferences_full,map__70651,map__70651__$1,calculated_sched,calculated_meta))
;
return iter__4529__auto__((kt.cljs$core$IFn$_invoke$arity$1 ? kt.cljs$core$IFn$_invoke$arity$1(calculated_sched) : kt.call(null,calculated_sched)));
})()], null);
})(),app$core$schedule_view_$_iter__70667(cljs.core.rest(s__70668__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4529__auto__(app.calculate_schedule.all_hour_keys);
})()], null)], null);
});
app.core.root = rum.core.lazy_build(rum.core.build_defc,(function (){
var l = new cljs.core.Keyword(null,"handler","handler",-195596612).cljs$core$IFn$_invoke$arity$1(rum.core.react(app.core.location));
var s = rum.core.react(app.core.patient_state);
var m = rum.core.react(app.core.med_preferences);
var tp = rum.core.react(app.core.time_preferences);
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["location is ",l], 0));

var attrs70679 = app.core.nav_links(l);
return daiquiri.core.create_element("div",((cljs.core.map_QMARK_(attrs70679))?daiquiri.interpreter.attributes(attrs70679):null),((cljs.core.map_QMARK_(attrs70679))?[(function (){var attrs70682 = (function (){var G__70686 = l;
var G__70686__$1 = (((G__70686 instanceof cljs.core.Keyword))?G__70686.fqn:null);
switch (G__70686__$1) {
case "patient":
return app.core.patient_view(s,m);

break;
case "about":
return app.core.about_view(s);

break;
case "schedule":
return app.core.schedule_view(tp,m);

break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__70686__$1)].join('')));

}
})();
return daiquiri.core.create_element("div",((cljs.core.map_QMARK_(attrs70682))?daiquiri.interpreter.attributes(daiquiri.normalize.merge_with_class.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"class","class",-2030961996),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, ["main"], null)], null),attrs70682], 0))):{'className':"main"}),((cljs.core.map_QMARK_(attrs70682))?null:[daiquiri.interpreter.interpret(attrs70682)]));
})()]:[daiquiri.interpreter.interpret(attrs70679),(function (){var attrs70685 = (function (){var G__70687 = l;
var G__70687__$1 = (((G__70687 instanceof cljs.core.Keyword))?G__70687.fqn:null);
switch (G__70687__$1) {
case "patient":
return app.core.patient_view(s,m);

break;
case "about":
return app.core.about_view(s);

break;
case "schedule":
return app.core.schedule_view(tp,m);

break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__70687__$1)].join('')));

}
})();
return daiquiri.core.create_element("div",((cljs.core.map_QMARK_(attrs70685))?daiquiri.interpreter.attributes(daiquiri.normalize.merge_with_class.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"class","class",-2030961996),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, ["main"], null)], null),attrs70685], 0))):{'className':"main"}),((cljs.core.map_QMARK_(attrs70685))?null:[daiquiri.interpreter.interpret(attrs70685)]));
})()]));
}),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [rum.core.reactive], null),"app.core/root");
app.core.prep_meds = (function app$core$prep_meds(patient_local,patient_saved){
var loc_meds = cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,cljs.core.map.cljs$core$IFn$_invoke$arity$2(app.core.parse_med,new cljs.core.Keyword(null,"medications","medications",-889403285).cljs$core$IFn$_invoke$arity$1(patient_local)));
var saved_meds = new cljs.core.Keyword(null,"meds","meds",-2036408830).cljs$core$IFn$_invoke$arity$1(patient_saved);
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"saved-meds?","saved-meds?",-1098099590),cljs.core.empty_QMARK_(saved_meds),saved_meds], 0));

if(cljs.core.empty_QMARK_(saved_meds)){
ajax.core.POST.cljs$core$IFn$_invoke$arity$variadic((function (){var G__70688 = app.core.patient_identifier(patient_local);
return (app.core.save_pref_url.cljs$core$IFn$_invoke$arity$1 ? app.core.save_pref_url.cljs$core$IFn$_invoke$arity$1(G__70688) : app.core.save_pref_url.call(null,G__70688));
})(),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"body","body",-2049205669),app.core.prep_body(cljs.core.deref(app.core.time_preferences),loc_meds),new cljs.core.Keyword(null,"response-format","response-format",1664465322),new cljs.core.Keyword(null,"json","json",1279968570)], null)], 0));

return cljs.core.reset_BANG_(app.core.med_preferences,saved_meds);
} else {
return cljs.core.reset_BANG_(app.core.med_preferences,saved_meds);
}
});
app.core.prep_patient = (function app$core$prep_patient(p){
cljs.core.reset_BANG_(app.core.patient_state,p);

return ajax.core.GET.cljs$core$IFn$_invoke$arity$variadic(app.core.get_pref_url(app.core.patient_identifier(p)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"response-format","response-format",1664465322),new cljs.core.Keyword(null,"raw","raw",1604651272),new cljs.core.Keyword(null,"handler","handler",-195596612),(function (p1__70689_SHARP_){
var prefs = app.core.js_parse(JSON.parse(p1__70689_SHARP_));
app.core.prep_meds(p,prefs);

return cljs.core.reset_BANG_(app.core.time_preferences,(function (){var or__4126__auto__ = new cljs.core.Keyword(null,"times","times",1671571467).cljs$core$IFn$_invoke$arity$1(prefs);
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (v){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(v),new cljs.core.Keyword(null,"default","default",-1987822328).cljs$core$IFn$_invoke$arity$1(v)], null);
}),app.core.all_times));
}
})());
})], null)], 0));
});
app.core.start = (function app$core$start(){
rum.core.mount(app.core.root(),document.getElementById("app"));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["************************************************************************"], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["core.cljs v3"], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["************************************************************************"], 0));

return getPatientDetails().then((function (p1__70690_SHARP_){
return app.core.prep_patient(app.core.js_parse(p1__70690_SHARP_));
}));
});
goog.exportSymbol('app.core.start', app.core.start);

//# sourceMappingURL=app.core.js.map
