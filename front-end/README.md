# IPR
Individual Project for Health Informatics.


## Develop
From the `front-end` directory you can run the following commands.

### cljs watch
- `yarn start` = `shadow-cljs watch app`

### Styles
Compile css: `sass --watch resources/private/sass:public/css`

## Release
- `yarn build` = `shadow-cljs release app`