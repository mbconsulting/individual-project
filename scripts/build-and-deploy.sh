#!/usr/bin/env bash
# Abort if a command fails
set -eo pipefail

# Builds all lambda functions and outputs bundled assets to ./out/

if [ -z "$1" ]; then
  clj_lambdas=( get-data put-data )
else
  clj_lambdas=( $1 )
fi

if [ -z "$2" ]; then
  prefix='test-hi'
elif [ $2 == 'prod' ]; then
  prefix='health'
fi

# Required for building projects that depend on com.cognitect.aws/api
export AWS_REGION="us-east-1"

rm -rf out
mkdir out

cd back-end

for l in "${clj_lambdas[@]}"
do
  echo "Building Lambdas: $l"
  echo "------------------------------------------------------------------------------------------"
  cd $l
  rm -rf target
  clojure -Muberjar
  mv target/$l.jar target/$l-lambda.jar
  echo "Deploying Lambda $l to $prefix-$l"
  echo "------------------------------------------------------------------------------------------"
  aws lambda update-function-code --region $AWS_REGION --function-name $prefix-$l --zip-file fileb://target/$l-lambda.jar
  cd ..
  printf "\n\n"
done

cd ..
