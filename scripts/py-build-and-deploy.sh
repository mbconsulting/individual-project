#!/usr/bin/env bash
# Abort if a command fails
set -eo pipefail

# Builds all lambda functions and outputs bundled assets to ./out/

if [ -z "$1" ]; then
  py_lambdas=( sentiment )
else
  py_lambdas=( $1 )
fi

if [ -z "$2" ]; then
  prefix='test-tb'
elif [ $2 == 'prod' ]; then
  prefix='tradebot'
fi


export AWS_REGION="us-east-1"

rm -rf out
mkdir out

cd app

for l in "${py_lambdas[@]}"
do
  echo "Building Python Lambda: $l to $prefix-$l"
  echo "------------------------------------------------------------------------------------------"
  cd $l/package
  zip -r9 ../function.zip .
  cd ..
  zip -g function.zip $l.py
  aws lambda update-function-code --function-name $prefix-$l --zip-file fileb://function.zip

  cd ..
  printf "\n\n"
done

cd ..