#!/usr/bin/env bash
set -eo pipefail

print_usage() {
cat <<EOF
NAME
  $0 -- deploy code for one or all compiled Lambda function(s) for Bulk Import to AWS

SYNOPSIS
  $0 <aws-region> [clj-lambda-name]

DESCRIPTION
  Deploys compiled asset bundles (zips for CLJS projects, jars for CLJ projects) to AWS Lambda.
  Assumes that asset bundles are in ./out/ and named {name}-lambda.[zip|jar] and that target
  Lambdas are named {environment}-bulk-import-{name}.

  If no clj-lambda-name argument is supplied, deploys all Lambdas in the project.

ENVIRONMENT
  This script depends on credentials for the target AWS account being configured locally, either
  via aws configure or AWS_* environment variables. The user account must have permissions to
  update function code for all Bulk Import Lambda functions.

EXAMPLES
  $0 us-east-1
  $0 us-east-1 twitter-search

EOF
}

main() {
if [ $# -lt 1 ] ; then
    print_usage
    exit 1
  # elif [ ! -d "./out" ] ; then
  #   echo "Cannot deploy AWS Lambda code: expected ./out directory not found."
  #   exit 1
  else
    if [ -z "$2" ]; then
      clj_lambdas=( twitter-search twitter-writer send-tweet )
    else
      clj_lambdas=( $2 )
    fi


    for l in "${clj_lambdas[@]}"
    do
      echo "Deploying CLJ Lambda $l to tradebot-$l"
      echo "------------------------------------------------------------------------------------------"
      aws lambda update-function-code --region $1 --function-name tradebot-$l --zip-file fileb://out/$l-lambda.jar
      printf "\n\n"
    done
  fi
}

main "$@"
