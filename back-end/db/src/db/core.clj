(ns db.core
  (:require [cognitect.aws.client.api :as aws]))

(def client (aws/client {:api :dynamodb}))

(aws/validate-requests client true)

(defn v-bool [b]
  {:BOOL b})

(defn string [s]
  (when s {:S s}))

(defn v-vector [v]
  (when v {:SS (map str v)}))

(defn number [n]
  (when n {:N (str n)}))

(defn v-nil []
  {:NULL true})

(defn db->long [n]
  (Double. n))

(defn dict [m]
  (when m {:M (reduce-kv (fn [p k v]
                           (assoc p (name k) (cond (nil? v) (v-nil)
                                                   (boolean? v) (v-bool v)
                                                   (string? v) (string v)
                                                   (number? v) (number v)
                                                   (map? v) (dict v)
                                                   (sequential? v) (v-vector v))))
                         {}
                         m)}))

(defn db->map [m]
  (reduce-kv (fn [p k {:keys [BOOL S SS N M NULL]}]
               (assoc p k (cond NULL NULL
                                BOOL BOOL
                                S S
                                SS SS
                                N (db->long N)
                                M (db->map M))))

             {}
             m))


(defn item->map [item]
  (reduce-kv (fn [m k {:keys [BOOL S SS N M NULL]}]
               (cond NULL (assoc m k NULL)
                     BOOL (assoc m k BOOL)
                     S (assoc m k S)
                     SS (assoc m k SS)
                     N (assoc m k (db->long N))
                     M (assoc m k (db->map M))))
             {}
             item))

(defn map->item [m]
  (reduce-kv (fn [p k v]
               (assoc p (name k) (cond (nil? v) (v-nil)
                                       (boolean? v) (v-bool v)
                                       (string? v) (string v)
                                       (number? v) (number v)
                                       (map? v) (dict v)
                                       (sequential? v) (v-vector v))))

             {}
             m))

(defn save-items
  "Save several `items` to `table`."
  [production client table items]
  (let [table (if production table (str table "-test"))]
    (println :saving-items-to table :count (count items) :production production :first-item (first items))
    (->> items
         (remove nil?)
         (map #(hash-map :PutRequest {:Item (map->item %)}))
         (partition 25 25 nil)                              ; BatchWriteItem allows only 25 items to be written at a time
         (mapv #(aws/invoke client {:op      :BatchWriteItem
                                    :request {:RequestItems {table %}}})))))

(defn query [client {:keys [table index key-condition expressions]} & [last-evaluated-key]]
  (aws/invoke client {:op      :Query
                      :request (merge {:TableName table}
                                      (when index {:IndexName index})
                                      (when key-condition {:KeyConditionExpression key-condition})
                                      (when expressions {:ExpressionAttributeValues expressions})
                                      (when last-evaluated-key {:ExclusiveStartKey last-evaluated-key}))}))

(defn query-results->map [client query {:keys [Items Count ScannedCount LastEvaluatedKey]}]
  (println :items :count Count :scanned-count ScannedCount :lek LastEvaluatedKey)
  (let [items (if LastEvaluatedKey
                (concat Items (:Items (query-results->map client query (query client query)))) ; todo this may be a bad idea, I should test it
                Items)]
    (map item->map items)))


; if LastEvaluatedKey is present, run the query again with ExclusiveStartKey
