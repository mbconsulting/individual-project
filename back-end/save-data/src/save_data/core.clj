(ns save-data.core
    (:require
      [cheshire.core :refer [parse-string]]
      [db.core :as db]
      [fn-tools.handler :as ft]))

(defn get-patient [patient-id]
  (let [client db/client
        q {:table         "hi-patient"
           :key-condition "pid = :pid"
           :expressions   {":pid" (db/string patient-id)}
           :Select        "ALL_ATTRIBUTES"}]
    (println :query q)
    (->> (db/query client q)
         (db/query-results->map client q)
         first)))

; save-patient
(defn save-patient [production patient]
  (db/save-items production db/client "hi-patient" [patient]))


(defn handler [event]
  (let [data (parse-string (-> event :body) true)
        db-patient (get-patient (-> event :pathParameters :patient-id))]
    {:status 200
     :result {:saved (save-patient true {:pid   (-> event :pathParameters :patient-id)
                                         :times (or (merge (:times data) (:times db-patient)) {})
                                         :meds  (or (merge (:meds data) (:meds db-patient)) {})})
              :pid   (-> event :pathParameters :patient-id)
              :body  (parse-string (get-in event [:body "prefs"]))}}))


(ft/gen-handler handler)