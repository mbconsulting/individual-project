(ns fn-tools.handler
    (:require [clojure.pprint :as pprint]
      [clojure.walk :as walk]
      [clojure.data.json :as json])
    (:import (clojure.lang ExceptionInfo)
      (java.util Map List)))

(defn ex-data->message [e]
      "Lambda captures the type, message, and stack trace of exceptions only. Thus to preserve ex-data from ExceptionInfo
      instances we must serialize them. Given an ExceptionInfo, e, evaluate to an equivalent one where the message contains
      both the original message and ex-data, serialized as EDN."
      (doto (ex-info (with-out-str
                       (pprint/pprint {:message (ex-message e)
                                       :ex-data (ex-data e)}))
                     (ex-data e))
            (.setStackTrace (.getStackTrace e))))

(defn clojureize-event
  "Recursively convert Java collections representing Lambda events to Clojure ones"
  [event]
  (walk/prewalk (fn [node]
                  (condp instance? node
                    Map (into {}
                              (map (fn [[k v]] [(keyword k) v]))
                              node)
                    List (vec node)
                    node))
                event))

(defn make-handler [handler-fn]
      (fn [raw-event]
          (let [event (clojureize-event raw-event)
                {:keys [status result]} (try (handler-fn event)
                                             (catch ExceptionInfo e
                                               (throw (ex-data->message e))))]
               (walk/stringify-keys {:isBase64Encoded false
                                     :statusCode      status
                                     :headers         {"Access-Control-Allow-Headers" "application/x-www-form-urlencoded"
                                                       "Access-Control-Allow-Origin"  "*",
                                                       "Access-Control-Allow-Methods" "OPTIONS,POST,GET"}
                                     :body            (json/write-str result)}))))

(defmacro gen-handler [handler-fn]
  "Given a function from map -> map, define a function -handler for use by Lambda. Also specify a class, lambda-kit.handler, to generate on
   compilation by lambda-kit.build."
   (let [private-handler-sym '-handler
         public-handler-sym 'handler]
        `(do (gen-class :name fn-tools.handler
                        :methods [^:static [~public-handler-sym [java.util.Map] java.util.Map]])
             (def ~private-handler-sym
               (make-handler ~handler-fn)))))

