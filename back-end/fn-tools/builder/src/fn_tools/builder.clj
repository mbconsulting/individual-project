(ns fn-tools.builder
    (:require [uberdeps.api :as uberdeps]
      [clojure.edn :as edn]
      [me.raynes.fs :as fs]))

(defn -main [handler-namespace jar-output-path]
  (let [class-dir "target/classes.fn-tools"]
    (fs/delete-dir class-dir)
    (fs/mkdirs class-dir)
    (fs/mkdirs (fs/parent jar-output-path))
    (binding [*compile-path* class-dir]
             (println "compiling" handler-namespace)
      (compile (symbol handler-namespace)))
    (uberdeps/package (update (edn/read-string (slurp "deps.edn"))
                            :paths
                            #(conj % class-dir))
                    jar-output-path)))