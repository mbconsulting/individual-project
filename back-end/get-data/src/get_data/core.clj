(ns get-data.core
  (:require [db.core :as db]
          [fn-tools.handler :as ft]))

(defn get-patient [patient-id]
  (let [client db/client
        q {:table         "hi-patient"
           :key-condition "pid = :pid"
           :expressions   {":pid" (db/string patient-id)}
           :Select        "ALL_ATTRIBUTES"}]
    (println :query q)
    (->> (db/query client q)
         (db/query-results->map client q)
         first)))


(defn handler [event]
  ;(println :body (-> event :body))
  {:status 200
   :result (get-patient (-> event :pathParameters :patient-id))})


(ft/gen-handler handler)